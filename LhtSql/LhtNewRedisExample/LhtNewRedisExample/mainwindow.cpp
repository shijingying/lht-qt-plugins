#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "LhtHiredis/redis_manager.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked() {
    if (RedisManager::instance().InitPool("192.168.28.129", 6379, "", 5, 10, 20)) {
        qDebug() << "连接池初始化成功";
    } else {
        qDebug() << "连接池初始化失败";
    }
}

void MainWindow::on_pushButton_2_clicked() {
    RedisManager::instance().ReleasePool();
    qDebug() << "连接池释放成功";
}

void MainWindow::on_pushButton_3_clicked() {
    RedisManager::instance().CheckStatus();
    qDebug() << "连接池状态检查完成";
}

void MainWindow::on_pushButton_4_clicked() {
    std::string key = "testKey";
    int expireTime = 60; // 60秒
    if (RedisManager::instance().SetKeyExpire(key, expireTime)) {
        qDebug() << "成功设置键过期时间";
    } else {
        qDebug() << "设置过期时间失败";
    }
}

void MainWindow::on_pushButton_5_clicked() {
    std::string key = "testKey";
    if (RedisManager::instance().DelKey(key)) {
        qDebug() << "成功删除键";
    } else {
        qDebug() << "删除键失败";
    }
}

void MainWindow::on_pushButton_6_clicked() {
    std::string key = "testKey";
    if (RedisManager::instance().KeyExists(key)) {
        qDebug() << "键存在";
    } else {
        qDebug() << "键不存在";
    }
}

void MainWindow::on_pushButton_7_clicked() {
    std::string key = "testKey";
    std::string value = "testValue";
    if (RedisManager::instance().SetString(key, value)) {
        qDebug() << "成功设置字符串值";
    } else {
        qDebug() << "设置字符串值失败";
    }
}

void MainWindow::on_pushButton_8_clicked() {
    std::string key = "testKey";
    std::string value = RedisManager::instance().GetString(key);
    qDebug() << "获取的值：" << QString::fromStdString(value);
}

void MainWindow::on_pushButton_9_clicked() {
    std::string key = "hashKey";
    std::string field = "field1";
    std::string value = "value1";
    if (RedisManager::instance().HSet(key, field, value)) {
        qDebug() << "成功设置哈希字段";
    } else {
        qDebug() << "设置哈希字段失败";
    }
}

void MainWindow::on_pushButton_10_clicked() {
    std::string key = "hashKey";
    std::string field = "field1";
    std::string response;
    if (RedisManager::instance().HGet(key, field, response)) {
        qDebug() << "获取的哈希字段值：" << QString::fromStdString(response);
    } else {
        qDebug() << "获取哈希字段值失败";
    }
}

void MainWindow::on_pushButton_11_clicked() {
    std::string key = "hashKey";
    std::map<std::string, std::string> response;
    if (RedisManager::instance().HGetAll(key, response)) {
        qDebug() << "哈希字段和对应值：";
        for (const auto& pair : response) {
            qDebug() << QString::fromStdString(pair.first) << ":" << QString::fromStdString(pair.second);
        }
    } else {
        qDebug() << "获取所有哈希字段值失败";
    }
}

void MainWindow::on_pushButton_12_clicked() {
    std::string key = "hashKey";
    std::string field = "field1";
    if (RedisManager::instance().HDelField(key, field)) {
        qDebug() << "成功删除哈希字段";
    } else {
        qDebug() << "删除哈希字段失败";
    }
}

void MainWindow::on_pushButton_13_clicked() {
    std::string key = "hashKey";
    std::string count;
    if (RedisManager::instance().HFieldCount(key, count)) {
        qDebug() << "哈希字段数量：" << QString::fromStdString(count);
    } else {
        qDebug() << "获取哈希字段数量失败";
    }
}

void MainWindow::on_pushButton_14_clicked() {
    if (RedisManager::instance().flushal()) {
        qDebug() << "成功刷新所有数据";
    } else {
        qDebug() << "刷新数据失败";
    }
}

void MainWindow::on_pushButton_15_clicked() {
    std::string pattern = "*"; // 匹配所有键
    std::vector<std::string> response;
    if (RedisManager::instance().getAllKeys(pattern, response)) {
        qDebug() << "匹配的键：";
        for (const auto& key : response) {
            qDebug() << QString::fromStdString(key);
        }
    } else {
        qDebug() << "获取匹配键失败";
    }
}

