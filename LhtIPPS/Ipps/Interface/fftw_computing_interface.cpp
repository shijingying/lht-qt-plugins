#include "fftw_computing_interface.h"
#include <mutex>
FftwComputingInterface::FftwComputingInterface() {}

bool FftwComputingInterface::LhtFftInitFloat(int fftSize)
{
    //这里 fftw中除了fftwf_execute  其他都是非线程安全的加个锁保护一下
    static std::mutex mutex;
    mutex.lock();
    if(isInit){
        fftwf_free(output);
        fftwf_free(input);
        fftwf_destroy_plan(plan);
    }
    isInit = 1;
    output = (fftwf_complex*)fftwf_malloc(fftSize * sizeof(fftwf_complex));
    input = (fftwf_complex*)fftwf_malloc(fftSize * sizeof(fftwf_complex));
    plan = fftwf_plan_dft_1d(fftSize, input, output,FFTW_FORWARD, FFTW_ESTIMATE);
    mutex.unlock();
    return true;
}

void FftwComputingInterface::LhtFftFreeFloat()
{
    if(isInit){
        fftwf_free(output);
        fftwf_free(input);
        fftwf_destroy_plan(plan);
    }
}
void convert_to_fftwf_complex(float *in1, float *in2, fftwf_complex *out, int n) {
    for (int i = 0; i < n; ++i) {
        out[i][0] = in1[i]; // 实部
        out[i][1] = in2[i]; // 虚部
    }
}
bool FftwComputingInterface::LhtFftFloat(float *in1, float *in2, int fftSize, float *fftOutData, bool printResult)
{
    convert_to_fftwf_complex(in1,in2,input,fftSize);

    fftwf_execute(plan);

    for (int n = 0; n < fftSize; n++)
    {
        fftOutData[n] = (output[n][0] * output[n][0] + output[n][1] * output[n][1]);   //高精FFT用
    }

    return true;
}
