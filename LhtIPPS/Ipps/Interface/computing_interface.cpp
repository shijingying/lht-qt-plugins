﻿#include "computing_interface.h"
#include <iostream>
#include <QDebug>
#include <QTime>
ComputingInterface::ComputingInterface(QObject *parent)
    : QObject{parent}
{
    init();
}

void ComputingInterface::init()
{
    IppStatus IPP_Init_status;
    IPP_Init_status=ippInit();
    qDebug()<<ippGetStatusString(IPP_Init_status);
    const IppLibraryVersion *lib;
    lib = ippsGetLibVersion();
    qDebug()<< lib->Name<< lib->Version;
    //ippSetNumThreads(1);
}

bool ComputingInterface::LhtSortFloat(float *in1, int len, float *result, bool Ascend, bool printResult)
{
    IppStatus ret;
    QTime t;
    t.start();
    memcpy(result,in1,len * sizeof(float));
    if(Ascend)
        ret = ippsSortAscend_32f_I(result,len);
    else
        ret = ippsSortDescend_32f_I(result,len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtSortFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__<<"use time = "<<t.elapsed()<<"ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtNormalizeFloat(float *in1, int len, float *result, float vSub, float vDiv, bool printResult)
{
    QTime t;
    t.start();
    // 对数组进行归一化处理
    auto status = ippsNormalize_32f(in1, result, len, vSub, vDiv);
    if(status != ippStsNoErr){
        qDebug()<<"Error LhtNormalizeFloat: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtDotProductFloat(float *in1, float *in2, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsDotProd_32f(in1,in2,len,result);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtDotProductFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtDotProductDouble(double *in1, double *in2, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsDotProd_64f(in1,in2,len,result);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtDotProductDouble: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtMulCFloat(float *in1, float in2, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsMulC_32f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtMulCFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtMulCDouble(double *in1, double in2, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsMulC_64f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtMulCDouble: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtMulFloat(float *in1, float *in2, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsMul_32f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtMulFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtMutDouble(double *in1, double *in2, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsMul_64f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtMutDouble: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtAddFloat(float *in1, float *in2, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算两个向量的对应元素之和
    auto ret = ippsAdd_32f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtAddFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtAddDouble(double *in1, double *in2, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算两个向量的对应元素之和
    auto ret = ippsAdd_64f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtAddDouble: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtSubFloat(float *in1, float *in2, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算两个向量的对应元素之差
    auto ret = ippsSub_32f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtSubFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtSubCFloat(float* in1, float in2, int len, float* result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量与给定元素之差
    auto ret = ippsSubC_32f(in1, in2, result, len);
    if (ret != ippStsNoErr) {
        qDebug() << "Error ippsSubC_32f: " << ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if (printResult) {
        printVector(result, len);
    }
    return true;
}

bool ComputingInterface::LhtAbsFloat(float* in1, int len, float* result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量与给定元素之差
    auto ret = ippsAbs_32f(in1, result, len);
    if (ret != ippStsNoErr) {
        qDebug() << "Error ippsAbs_32f: " << ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if (printResult) {
        printVector(result, len);
    }
    return true;

}

bool ComputingInterface::LhtSubDouble(double *in1, double *in2, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算两个向量的对应元素之和
    auto ret = ippsSub_64f(in1, in2, result, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtSubDouble: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtSumFloat(float *in1, int len, float *result, IppHintAlgorithm hint, bool printResult)
{
    QTime t;
    t.start();
    // 计算量的元素之和
    auto ret = ippsSum_32f(in1, len, result,hint);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtSumFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtSumDouble(double *in1, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算量的元素之和
    auto ret = ippsSum_64f(in1, len, result);

    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtSumDouble: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtNormInfFloat(float *in1, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的无穷范数
    IppStatus status = ippsNorm_Inf_32f(in1, len, result);

    if(status != ippStsNoErr){
        qDebug()<<"Error LhtNormInfFloat: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtNormInfDouble(double *in1, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的无穷范数
    IppStatus status = ippsNorm_Inf_64f(in1, len, result);

    if(status != ippStsNoErr){
        qDebug()<<"Error LhtNormInfDouble: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtMaxFloat(float *in1, int len, float *result, bool printResult)
{

    QTime t;
    t.start();
    // 计算向量的最大值
    IppStatus status = ippsMax_32f(in1, len, result);

    if(status != ippStsNoErr){
        qDebug()<<"Error LhtMaxFloat: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtMaxDouble(double *in1, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的最大值
    IppStatus status = ippsMax_64f(in1, len, result);

    if(status != ippStsNoErr){
        qDebug()<<"Error LhtMaxDouble: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtMinFloat(float *in1, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的最小值
    IppStatus status = ippsMin_32f(in1, len, result);

    if(status != ippStsNoErr){
        qDebug()<<"Error LhtMinFloat: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtMinDouble(double *in1, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的最小值
    IppStatus status = ippsMin_64f(in1, len, result);

    if(status != ippStsNoErr){
        qDebug()<<"Error LhtMinDouble: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}

bool ComputingInterface::LhtMeanFloat(float *in1, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    IppStatus status = ippsMean_32f(in1, len, result,ippAlgHintFast);
    if(status != ippStsNoErr){
        qDebug()<<"Error LhtMeanFloat: "<< status;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,1);
    }
    return true;
}


bool ComputingInterface::LhtfindMaxindexFloat(float* in1, int len, float* maxvalue, int* maxindex,bool printResult )
{
    QTime t;
    t.start();
    IppStatus status = ippsMaxIndx_32f(in1, len, maxvalue, maxindex);
    if (status != ippStsNoErr) {
        qDebug() << "Error LhtfindMaxindexFloat: " << status;
        return false;
    }


    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if (printResult) {
        printVector(maxvalue, 1);
    }
    return true;
}

bool ComputingInterface::LhtfindMininexFloat(float* in1, int len, float* minvalue, int* minindex,bool printResult)
{
    QTime t;
    t.start();
    IppStatus status = ippsMinIndx_32f(in1, len, minvalue, minindex);
    if (status != ippStsNoErr) {
        qDebug() << "Error LhtfindMininexFloat: " << status;
        return false;
    }

    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if (printResult) {
        printVector(minvalue, 1);
    }
    return true;
}




bool ComputingInterface::LhtFftInitFloat(int len)
{
    QTime t;
    t.start();
    int FFT_size = len;
    int FFTOrder = (log(FFT_size) / log(2));

    Ipp8u* pMemInit = 0;
    int sizeSpec = 0;
    int sizeInit = 0;

    int flag = IPP_FFT_NODIV_BY_ANY;
    sizeFft = (int)FFT_size;

    pDstR = (Ipp32f*)ippMalloc(sizeof(Ipp32f) * sizeFft);
    pDstI = (Ipp32f*)ippMalloc(sizeof(Ipp32f) * sizeFft);
    pDstTemp = (Ipp32f*)ippMalloc(sizeof(Ipp32f) * sizeFft);


    pDstFc = (Ipp32fc*)ippMalloc(sizeof(Ipp32fc) * sizeFft);
    pDstFcTemp = (Ipp32f*)ippMalloc(sizeof(Ipp32f) * sizeFft);

    std::memset(pDstR, 0, sizeof(Ipp32f) * sizeFft);
    std::memset(pDstI, 0, sizeof(Ipp32f) * sizeFft);
    std::memset(pDstTemp, 0, sizeof(Ipp32f) * sizeFft);

    std::memset(pDstFc, 0, sizeof(Ipp32fc) * sizeFft);
    std::memset(pDstFcTemp, 0, sizeof(Ipp32f) * sizeFft);

    ippsFFTGetSize_C_32f(FFTOrder, flag, ippAlgHintNone, &sizeSpec, &sizeInit, &sizeBuffer);
    pMemSpec = (Ipp8u*)ippMalloc(sizeSpec);
    if (sizeInit > 0)
    {
        pMemInit = (Ipp8u*)ippMalloc(sizeInit);
    }
    if (sizeBuffer > 0)
    {
        pMemBuffer = (Ipp8u*)ippMalloc(sizeBuffer);
    }
    ippsFFTInit_C_32f(&pSpec, FFTOrder, flag, ippAlgHintNone, pMemSpec, pMemInit);
    if (sizeInit > 0)
    {
        ippFree(pMemInit);
    }

    ippsFFTGetSize_C_32fc(FFTOrder, flag, ippAlgHintNone, &sizeSpec, &sizeInit, &sizeFcBuffer);
    pMemFcSpec = (Ipp8u*)ippMalloc(sizeSpec);
    if (sizeInit > 0)
    {
        pMemInit = (Ipp8u*)ippMalloc(sizeInit);
    }
    if (sizeFcBuffer > 0)
    {
        pMemFcBuffer = (Ipp8u*)ippMalloc(sizeFcBuffer);
    }
    ippsFFTInit_C_32fc(&pSpecFc, FFTOrder, flag, ippAlgHintNone, pMemFcSpec, pMemInit);
    if (sizeInit > 0)
    {
        ippFree(pMemInit);
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return 1;
}

void ComputingInterface::LhtFftFreeFloat()
{
    if(pMemSpec){
        ippFree(pMemSpec);
        ippFree(pDstR);
        ippFree(pDstI);
        ippFree(pMemBuffer);
        ippFree(pDstTemp);
    }
}

bool ComputingInterface::LhtFftFloat(float *in1, float *in2, int len, float *result, bool printResult)
{

    ippsFFTFwd_CToC_32f(in1,in2,pDstR,pDstI,pSpec, pMemBuffer);
    //计算向量的平方根
    ippsMagnitude_32f(pDstR,pDstI,pDstTemp,len);
    // LhtCalculateRootOfSumOfSquaresFloat(pDstR,pDstI,len,result);
    int halfLen = len / 2;
    ippsCopy_32f(pDstTemp+halfLen,result,halfLen);
    ippsCopy_32f(pDstTemp,result+halfLen,halfLen);

    if(printResult){
        printVector(result,len);
    }
    return 1;
}

bool ComputingInterface::LhtFftFloat(Ipp32fc *in1, int len, float *result, bool shift)
{
    ippsFFTFwd_CToC_32fc(in1,pDstFc,pSpecFc, pMemFcBuffer);

    // LhtCalculateRootOfSumOfSquaresFloat(pDstR,pDstI,len,result);
    if(shift){
        //计算向量的平方根
        ippsMagnitude_32fc(pDstFc,pDstFcTemp,len);
        int halfLen = len / 2;
        ippsCopy_32f(pDstFcTemp+halfLen,result,halfLen);
        ippsCopy_32f(pDstFcTemp,result+halfLen,halfLen);
    }else{
        //计算向量的平方根
        ippsMagnitude_32fc(pDstFc,result,len);
    }

    return 1;
}

bool ComputingInterface::LhtFftInitDouble(int len)
{
    QTime t;
    t.start();
    int FFT_size = len;
    int FFTOrder = (log(FFT_size) / log(2));

    Ipp8u* pMemInit = 0;
    int sizeSpec = 0;
    int sizeInit = 0;

    int flag = IPP_FFT_NODIV_BY_ANY;
    sizeFftD = (int)FFT_size;
    pDstRD = (Ipp64f*)ippMalloc(sizeof(Ipp64f) * sizeFftD);
    pDstID = (Ipp64f*)ippMalloc(sizeof(Ipp64f) * sizeFftD);
    std::memset(pDstRD, 0, sizeof(Ipp64f) * sizeFftD);
    std::memset(pDstID, 0, sizeof(Ipp64f) * sizeFftD);
    ippsFFTGetSize_C_64f(FFTOrder, flag, ippAlgHintNone, &sizeSpec, &sizeInit, &sizeBufferD);
    pMemSpecD = (Ipp8u*)ippMalloc(sizeSpec);
    if (sizeInit > 0)
    {
        pMemInit = (Ipp8u*)ippMalloc(sizeInit);
    }
    if (sizeBufferD > 0)
    {
        pMemBufferD = (Ipp8u*)ippMalloc(sizeBufferD);
    }
    ippsFFTInit_C_64f(&pSpecD, FFTOrder, flag, ippAlgHintNone, pMemSpecD, pMemInit);
    if (sizeInit > 0)
    {
        ippFree(pMemInit);
    }

    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return 1;
}

void ComputingInterface::LhtFftFreeDouble()
{
    if(pMemSpecD){
        ippFree(pMemSpecD);
        ippFree(pDstRD);
        ippFree(pDstID);
        ippFree(pMemBufferD);
    }
}

bool ComputingInterface::LhtFftDouble(double *in1, double *in2, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    ippsFFTFwd_CToC_64f(in1,in2,pDstRD,pDstID,pSpecD, pMemBufferD);
    //计算向量的平方根
    LhtCalculateRootOfSumOfSquaresDouble(pDstRD,pDstID,len,result);
    int halfLen = len / 2;
    // 使用 std::rotate 进行数组旋转
    std::rotate(result, result + halfLen, result + len);
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return 1;
}

bool ComputingInterface::LhtDftInitFloat(int len)
{
    QTime t;
    t.start();
    IppStatus status;
    int bufferSize;
    int specSize;
    int initSize;
    status = ippsDFTGetSize_C_32f(len, IPP_FFT_NODIV_BY_ANY, ippAlgHintFast, &specSize, &initSize, &bufferSize);
    if (status != ippStsNoErr) {
        qDebug() << "Error getting DFT size: " << status ;
        return false;
    }

    pDFTSpec = (IppsDFTSpec_C_32f*)ippsMalloc_8u(specSize);
    pDFTBuffer = ippsMalloc_8u(bufferSize);
    if (!pDFTSpec || !pDFTBuffer) {
        qDebug() << "Error allocating memory for DFT spec or buffer.";
        return false;
    }
    //IPP_FFT_FORWARD | IPP_ALG_DFT_RADIX2 | IPP_HINT_ALG_FASTEST
    status = ippsDFTInit_C_32f(len, IPP_FFT_NODIV_BY_ANY, ippAlgHintFast, pDFTSpec, pDFTBuffer);
    if (status != ippStsNoErr) {
        qDebug() << "Error initializing DFT: " << status ;
        return false;
    }

    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

void ComputingInterface::LhtDftFreeFloat()
{
    if (pDFTSpec) {
        ippsFree(pDFTSpec);
    }
    if (pDFTBuffer) {
        ippsFree(pDFTBuffer);
    }
}

bool ComputingInterface::LhtDftFloat(float *in1, float *in2, int len, float *result1, float *result2, bool printResult)
{
    QTime t;
    t.start();
    if (!pDFTSpec || !pDFTBuffer) {
        qDebug() << "DFT not initialized.";
        return false;
    }
    auto ret = ippsDFTFwd_CToC_32f(in1,in2,result1,result2,pDFTSpec, pDFTBuffer);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtDftFloat: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result1,len);
        printVector(result2,len);
    }
    return true;
}

bool ComputingInterface::LhtCalculateRootOfSumOfSquaresFloat(const Ipp32f *v1, const Ipp32f *v2, int len, Ipp32f *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算 v1 的每个元素的平方
    auto ret =ippsSqr_32f_A24(v1, squaredV1, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresFloat1: "<< ret;
        return false;
    }
    // 计算 v2 的每个元素的平方
    ret =ippsSqr_32f_A24(v2, squaredV2, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresFloat2: "<< ret;
        return false;
    }
    // 计算每个元素的平方和
    ret =ippsAdd_32f(squaredV1, squaredV2, sumOfSquares, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresFloat3: "<< ret;
        return false;
    }
    // 计算平方和的平方根，并将结果存储在 result 中
    ret =ippsSqrt_32f_A24(sumOfSquares, result, len);

    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresFloat4: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtCalculateRootOfSumOfSquaresFloat2(const Ipp32fc *v1, int len, Ipp32f *result, bool printResult)
{
    QTime t;
    t.start();
    auto ret = ippsMagnitude_32fc(v1,result,len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresFloat2: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtCalculateRootOfSumOfSquaresDouble(const Ipp64f *v1, const Ipp64f *v2, int len, Ipp64f *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算 v1 的每个元素的平方
    auto ret =ippsSqr_64f_A53(v1, squaredV1D, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresDouble1: "<< ret;
        return false;
    }
    // 计算 v2 的每个元素的平方
    ret =ippsSqr_64f_A53(v2, squaredV2D, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresDouble2: "<< ret;
        return false;
    }
    // 计算每个元素的平方和
    ret =ippsAdd_64f(squaredV1D, squaredV2D, sumOfSquaresD, len);
    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresDouble3: "<< ret;
        return false;
    }
    // 计算平方和的平方根，并将结果存储在 result 中
    ret =ippsSqrt_64f_A53(sumOfSquaresD, result, len);

    if(ret != ippStsNoErr){
        qDebug()<<"Error LhtCalculateRootOfSumOfSquaresDouble4: "<< ret;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(result,len);
    }
    return true;
}

bool ComputingInterface::LhtNormL2Float(float *in1, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的L2范数
    IppStatus status = ippsNorm_L2_32f(in1, len, result);

    // 检查返回状态
    if (status == ippStsNoErr) {
        qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
        if(printResult)
            qDebug()<<"L2 norm: "<< *result;
        return true;
    } else {
        qDebug()<<"Error LhtNormL2Float: "<< status;
        return false;
    }

}

bool ComputingInterface::LhtCalculateSquaresFloat2(float* real, float* imag, int len, float* realout, float* imagout, bool printResult)
{
    QTime t;
    t.start();

    // 计算实部平方结果: real^2 - imag^2
    ippsSqr_32f(real, realout, len); // 实部平方 real^2
    ippsSqr_32f(imag, imagout, len); // 虚部平方 imag^2
    ippsSub_32f_I(imagout, realout, len); // 实部结果 real^2 - imag^2

    // 计算虚部结果: 2 * real * imag
    ippsMul_32f(real, imag, imagout, len); // real * imag
    ippsMulC_32f_I(2.0f, imagout, len); // 2 * real * imag

    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if (printResult) {
        printVector(realout, len);
    }
    return true;
}


bool ComputingInterface::LhtCalculateEvenpowerFloat2(float* real, float* imag, int len, float* realout, float* imagout,int n, bool printResult)
{
    QTime t;
    t.start();

    // 临时变量用于存储中间结果
    float* tempReal = new float[len];
    float* tempImag = new float[len];
    memcpy(tempReal, real, sizeof(float) * len);
    memcpy(tempImag, imag, sizeof(float) * len);

    for (int i = 0; i < n / 2; i++)
    {
        // 计算实部平方结果: real^2 - imag^2
        ippsSqr_32f(tempReal, realout, len); // 实部平方 real^2
        ippsSqr_32f(tempImag, imagout, len); // 虚部平方 imag^2
        ippsSub_32f_I(imagout, realout, len); // 实部结果 real^2 - imag^2

        // 计算虚部结果: 2 * real * imag
        ippsMul_32f(tempReal, tempImag, imagout, len); // real * imag
        ippsMulC_32f_I(2.0f, imagout, len); // 2 * real * imag

        memcpy(tempReal, realout, sizeof(float) * len);
        memcpy(tempImag, imagout, sizeof(float) * len);
    }

    delete[] tempReal;
    delete[] tempImag;


    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if (printResult) {
        printVector(realout, len);
    }
    return true;
}



bool ComputingInterface::LhtPowerSpectrFloat(float *in1, float *in2, int len, float *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算功率谱
    IppStatus status = ippsPowerSpectr_32f(in1,in2, result,len);
    // 检查返回状态
    if (status == ippStsNoErr) {
        qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
        if(printResult){
            printVector(result,len);
        }
        return true;
    } else {
        qDebug()<<"Error LhtPowerSpectrFloat: "<< status;
        return false;
    }
}

bool ComputingInterface::LhtCreateTone(Ipp32fc *result, int len,float magn,int freq , bool printResult)
{
    Ipp32f phase = 0; // 初始相位
    IppStatus status = ippsTone_32fc(result, len, magn, freq, &phase, ippAlgHintFast);
    if (status != ippStsNoErr) {
        qDebug() << "Error LhtCreateTone: " << status ;
        return false;
    }
}

bool ComputingInterface::LhtHilbertFloat(float *in, int len, Ipp32fc *out, bool printResult)
{
    QTime t;
    t.start();
    if(pHilbertLen != len){
        pHilbertLen = len;
        int bufferSize;
        int specSize;
        auto status = ippsHilbertGetSize_32f32fc(len,ippAlgHintFast,&specSize,&bufferSize);
        if (status != ippStsNoErr) {
            qDebug() << "Error LhtHilbertFloat GetSize: " << status ;
            return false;
        }
        if(pHilbertSpec!= nullptr){
            ippsFree(pHilbertSpec);
            ippsFree(pHilbertBuffer);
        }

        pHilbertSpec = (IppsHilbertSpec*)ippsMalloc_8u(specSize);
        pHilbertBuffer = ippsMalloc_8u(bufferSize);
        status = ippsHilbertInit_32f32fc(len,ippAlgHintFast,pHilbertSpec,pHilbertBuffer);
        if (status != ippStsNoErr) {
            qDebug() << "Error HilbertInit.";
            return false;
        }
    }
    auto status = ippsHilbert_32f32fc(in,out,pHilbertSpec,pHilbertBuffer);
    if (status != ippStsNoErr) {
        qDebug() << "Error HilbertFloat.";
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

bool ComputingInterface::LhtAutocorrelationComplex(Ipp32fc *in, int len, Ipp32fc *out, int outLen, bool printResult)
{
    QTime t;
    t.start();
    IppEnum funCfg = (IppEnum)(ippAlgAuto|ippsNormNone);
    if(pAutocorrelationSrcLen != len || pAutocorrelationDstLen != outLen){
        pAutocorrelationSrcLen = len;
        pAutocorrelationDstLen = outLen;
        int bufferSize;
        auto status = ippsAutoCorrNormGetBufferSize(len,outLen,ipp32fc,funCfg,&bufferSize);
        if (status != ippStsNoErr) {
            qDebug() << "Error ippsAutoCorrNormGetBufferSize: " << status ;
            return false;
        }
        if(pAutocorrelationBuffer!= nullptr){
            ippsFree(pAutocorrelationBuffer);
        }
        pAutocorrelationBuffer = ippsMalloc_8u(bufferSize);
    }
    auto status = ippsAutoCorrNorm_32fc(in, len, out, outLen, funCfg, pAutocorrelationBuffer);
    if (status != ippStsNoErr) {
        qDebug() << "Error LhtAutocorrelationComplex.";
        return false;
    }
    //1.  总长度N   申明一个2N -1 的数组 原数组拷贝到 N-1的位置
    //2.  翻转原数组  拷贝过去N
    //3.  2N-1 的数据就是结果
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

bool ComputingInterface::LhtdownOrthConversion(Ipp32fc *v1, int len, int freq1, int sampleRate,  Ipp32fc *out1, bool printResult)
{
    // 生成复指数信号
    Ipp32f freq = (Ipp32f)freq1 / sampleRate; // 计算频率参数
    freq = freq < 0 ? freq + 1 : freq;
    Ipp32f phase = 0; // 初始相位
    if(mixedDataLen != len){
        if(complexExp != nullptr){
            delete complexExp;
            complexExp = nullptr;
        }
        complexExp = (Ipp32fc*)ippsMalloc_32fc(len);
        mixedDataLen = len;
    }
    ippsTone_32fc(complexExp, len, 1.0, freq, &phase, ippAlgHintFast);
    // 混频
    ippsMul_32fc(v1, complexExp, out1, len);

    return true;
}

bool ComputingInterface::LhtdownOrthConversion( Ipp32f *v1,  Ipp32f *v2, int len, int freq1, int sampleRate,  Ipp32f *out1,  Ipp32f *out2, bool printResult)
{
    // 生成复指数信号
    Ipp32f freq = (Ipp32f)freq1 / sampleRate; // 计算频率参数
    freq = freq < 0 ? freq + 1 : freq;
    Ipp32f phase = 0; // 初始相位
    if(mixedData == nullptr || mixedDataLen != len){
        if(mixedData != nullptr){
            ippsFree(mixedData);
            ippsFree(mixeiqdData);
            ippsFree(complexExp);
        }
        complexExp = (Ipp32fc*)ippsMalloc_32fc(len);
        mixedData = (Ipp32fc*)ippsMalloc_32fc(len);
        mixeiqdData = (Ipp32fc*)ippsMalloc_32fc(len);
        mixedDataLen = len;
    }

    ippsTone_32fc(complexExp, len, 1.0, freq, &phase, ippAlgHintAccurate);

    LhtCopyRealAndimag2Complex(mixeiqdData,v1,v2,len);
    // 混频
    ippsMul_32fc(mixeiqdData, complexExp, mixedData, len);

    LhtCopyRealAndimag(mixedData,out1,out2,len);

    return true;
}

bool ComputingInterface::LhtNormL2Double(double *in1, int len, double *result, bool printResult)
{
    QTime t;
    t.start();
    // 计算向量的L2范数
    IppStatus status = ippsNorm_L2_64f(in1, len, result);

    // 检查返回状态
    if (status == ippStsNoErr) {
        qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
        if(printResult)
            qDebug()<<"L2 norm: "<< *result;
        return true;
    } else {
        qDebug()<<"Error: "<< status;
        return false;
    }
}

bool ComputingInterface::LhtFirInitFloat(const float *coeffs, int numCoeffs, IppAlgType type)
{
    QTime t;
    t.start();
    if (pFirSpec || pFirBuffer || pFirDlyLine) {
        qDebug() << "Filter already initialized.";
        return false;
    }
    int specSize = 0, bufSize = 0;
    IppStatus status = ippsFIRSRGetSize(numCoeffs, ipp32f, &specSize, &bufSize);
    if (status != ippStsNoErr) {
        qDebug() << "Error getting FIR size: " << status ;
        return false;
    }

    pFirSpec = (IppsFIRSpec_32f*)ippsMalloc_8u(specSize);
    pFirBuffer = ippsMalloc_8u(bufSize);
    //延迟线数据
    pFirDlyLine = ippsMalloc_32f(numCoeffs - 1);
    if (!pFirSpec || !pFirBuffer|| !pFirDlyLine) {
        qDebug()<< "Error allocating memory for FIR filter.";
        return false;
    }

    status = ippsFIRSRInit_32f(coeffs, numCoeffs,type , pFirSpec);
    if (status != ippStsNoErr) {
        qDebug() << "Error initializing FIR filter: " << status ;
        ippsFree(pFirSpec);
        ippsFree(pFirBuffer);
        ippsFree(pFirDlyLine);
        pFirSpec = nullptr;
        pFirBuffer = nullptr;
        pFirDlyLine = nullptr;
        return false;
    }
    ippsZero_32f(pFirDlyLine, numCoeffs - 1); // 初始化延迟线为零
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

void ComputingInterface::LhtFirFreeFloat()
{
    if (pFirSpec) {
        ippsFree(pFirSpec);
    }
    if (pFirBuffer) {
        ippsFree(pFirBuffer);
    }
    if (pFirDlyLine) {
        ippsFree(pFirDlyLine);
    }
}

bool ComputingInterface::LhtFirFloat(const float *src, int len, float *dst, bool printResult)
{
    QTime t;
    t.start();
    if (!pFirSpec || !pFirBuffer|| !pFirDlyLine) {
        qDebug() << "Filter not initialized." ;
        return false;
    }
    //滤波
    IppStatus status = ippsFIRSR_32f(src, dst, len, pFirSpec, pFirDlyLine, pFirDlyLine, pFirBuffer);
    if (status != ippStsNoErr) {
        qDebug() << "Error applying FIR filter: " << status ;
        return false;
    }

    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}


bool ComputingInterface::LhtFirInitDouble(const double *coeffs, int numCoeffs, IppAlgType type)
{
    QTime t;
    t.start();
    if (pFirSpecD || pFirBufferD || pFirDlyLineD) {
        qDebug() << "Filter already initialized.";
        return false;
    }
    int specSize = 0, bufSize = 0;
    IppStatus status = ippsFIRSRGetSize(numCoeffs, ipp32f, &specSize, &bufSize);
    if (status != ippStsNoErr) {
        qDebug() << "Error getting FIR size: " << status ;
        return false;
    }

    pFirSpecD = (IppsFIRSpec_64f*)ippsMalloc_8u(specSize);
    pFirBufferD = ippsMalloc_8u(bufSize);
    //延迟线数据
    pFirDlyLineD = ippsMalloc_64f(numCoeffs - 1);
    if (!pFirSpecD || !pFirBufferD|| !pFirDlyLineD) {
        qDebug()<< "Error allocating memory for FIR filter.";
        return false;
    }

    status = ippsFIRSRInit_64f(coeffs, numCoeffs,type , pFirSpecD);
    if (status != ippStsNoErr) {
        qDebug() << "Error initializing FIR filter: " << status ;
        ippsFree(pFirSpecD);
        ippsFree(pFirBufferD);
        ippsFree(pFirDlyLineD);
        pFirSpecD = nullptr;
        pFirBufferD = nullptr;
        pFirDlyLineD = nullptr;
        return false;
    }
    ippsZero_64f(pFirDlyLineD, numCoeffs - 1); // 初始化延迟线为零
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

void ComputingInterface::LhtFirFreeDouble()
{
    if (pFirSpecD) {
        ippsFree(pFirSpecD);
    }
    if (pFirBufferD) {
        ippsFree(pFirBufferD);
    }
    if (pFirDlyLineD) {
        ippsFree(pFirDlyLineD);
    }
}

bool ComputingInterface::LhtFirDouble(const double *src, int len, double *dst, bool printResult)
{
    QTime t;
    t.start();
    if (!pFirSpec || !pFirBuffer|| !pFirDlyLine) {
        qDebug() << "Filter not initialized." ;
        return false;
    }
    //滤波
    IppStatus status = ippsFIRSR_64f(src, dst, len, pFirSpecD, pFirDlyLineD, pFirDlyLineD, pFirBufferD);
    if (status != ippStsNoErr) {
        qDebug() << "Error applying FIR filter: " << status ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(dst,len);
    }
    return true;
}
#ifndef M_PI
#define M_PI 3.1415926
#endif
bool ComputingInterface::LhtFirInitFloatLp(float *coeffs, int numCoeffs)
{
    QTime t;
    t.start();
    if (pSpecLp || pBufLp ) {
        qDebug() << "Filter already initialized.";
        return false;
    }
    int specSize = 0, bufSize = 0;
    IppStatus status = ippsFIRSRGetSize(numCoeffs, ipp32f, &specSize, &bufSize);
    if (status != ippStsNoErr) {
        qDebug()  << "Error getting FIR size: " << status ;
        return false;
    }

    pSpecLp = (IppsFIRSpec_32f*)ippsMalloc_8u(specSize);
    pBufLp = ippsMalloc_8u(bufSize);
    if (!pSpecLp || !pBufLp ) {
        qDebug()  << "Error allocating memory for FIR filter." ;
        return false;
    }

    status = ippsFIRSRInit_32f(coeffs, numCoeffs, ippAlgAuto, pSpecLp);
    if (status != ippStsNoErr) {
        qDebug()  << "Error initializing FIR filter: " << status ;
        ippsFree(pSpecLp);
        ippsFree(pBufLp);
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

void ComputingInterface::LhtFirFreeFloatLp()
{
    if (pSpecLp) {
        ippsFree(pSpecLp);
    }
    if (pBufLp) {
        ippsFree(pBufLp);
    }
}

bool ComputingInterface::LhtFirFloatLp(const float *src, float *dst, int len, bool printResult)
{
    QTime t;
    t.start();
    if (!pSpecLp || !pBufLp ) {
        qDebug()  << "Filter not initialized." ;
        return false;
    }

    IppStatus status = ippsFIRSR_32f(src, dst, len, pSpecLp, nullptr, nullptr, pBufLp);
    if (status != ippStsNoErr) {
        qDebug()  << "Error applying FIR filter: " << status ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(dst,len);
    }
    return true;
}

bool ComputingInterface::LhtFilterMedianFloat(const float *src, float *dst, int len, int makesize, bool printResult)
{
    QTime t;
    t.start();
    if(maskSize != makesize){
        maskSize = makesize;
        int bufferSize = 0;
        // 获取所需缓冲区大小
        IppStatus status = ippsFilterMedianGetBufferSize(maskSize, ipp32f, &bufferSize);
        if (status != ippStsNoErr) {
            printf("Error in getting buffer size: %d\n", status);
            return -1;
        }

        // 分配缓冲区
        pMedianBuffer = (Ipp8u *)ippsMalloc_8u(bufferSize);
        if (pMedianBuffer == NULL) {
            printf("Error in allocating buffer.\n");
            return -1;
        }

        // 分配延迟缓冲区
        pMedianDlySrc = (Ipp32f *)ippsMalloc_32f(maskSize-1);
        pMedianDlyDst = (Ipp32f *)ippsMalloc_32f(maskSize-1);
        if (pMedianDlySrc == NULL || pMedianDlyDst == NULL) {
            printf("Error in allocating delay buffers.\n");
            ippsFree(pMedianBuffer);
            return -1;
        }

        // 初始化延迟缓冲区
        ippsZero_32f(pMedianDlySrc, maskSize-1);
        ippsZero_32f(pMedianDlyDst, maskSize-1);
    }
    // 应用中值滤波
    IppStatus  status = ippsFilterMedian_32f(src, dst, len, maskSize, pMedianDlySrc, pMedianDlyDst, pMedianBuffer);
    if (status != ippStsNoErr) {
        qDebug()  << "Error LhtFilterMedianFloat: " << status ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(dst,len);
    }
    return true;
}

void ComputingInterface::LhtCopyRealAndimag(Ipp32fc *source, Ipp32f *real_dest, Ipp32f *imag_dest, int len)
{
    QTime t;
    t.start();
    ippsCplxToReal_32fc(source,real_dest,imag_dest,len);
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    // // 提取实部
    // ippsReal_32fc(source, real_dest,, len);

    // // 提取虚部
    // ippsImag_32fc(source, imag_dest, len);

}

void ComputingInterface::LhtCopyRealAndimag2Complex(Ipp32fc *dst, Ipp32f *real_dest, Ipp32f *imag_dest, int len)
{
    QTime t;
    t.start();
    ippsRealToCplx_32f(real_dest,imag_dest,dst,len);
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
}

bool ComputingInterface::LhtThreshold(const float *src, float *dst, int len, float level, float value, bool printResult)
{
    QTime t;
    t.start();
    IppStatus status = ippsThreshold_LTAbsVal_32f(src,dst,len,level,value);
    if (status != ippStsNoErr) {
        qDebug()  << "Error LhtThreshold: " << status ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(dst,len);
    }
    return true;
}

bool ComputingInterface::LhtCartToPolar(const float *in1, const float *in2, float *pDstMagn, float *pDstPhase, int len, bool printResult)
{
    QTime t;
    t.start();
    IppStatus status = ippsCartToPolar_32f(in1,in2,pDstMagn,pDstPhase,len);
    if (status != ippStsNoErr) {
        qDebug()  << "Error LhtCartToPolar: " << status ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(pDstMagn,len);
        printVector(pDstPhase,len);
    }
    return true;
}

bool ComputingInterface::LhtFlip(const float *src, float *dst, int len, bool printResult)
{
    QTime t;
    t.start();
    IppStatus status = ippsFlip_32f(src,dst,len);
    if (status != ippStsNoErr) {
        qDebug()  << "Error LhtFlip: " << status ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    if(printResult){
        printVector(dst,len);
    }
    return true;
}

void ComputingInterface::LhtResampleInit(int inRate, int outRate)
{
    QTime t;
    t.start();
    if(pResampleState != nullptr){
        ippsFree(pResampleState);
    }
    int history=128;
    int size,len,height;
    ippsResamplePolyphaseFixedGetSize_32f(inRate,outRate,2*(history-1),&size,&len,&height,ippAlgHintFast);
    pResampleState=(IppsResamplingPolyphaseFixed_32f*)ippsMalloc_8u(size);
    auto ret = ippsResamplePolyphaseFixedInit_32f(inRate,outRate,2*(history-1),0.95f,9.0f,pResampleState,ippAlgHintFast);
    if (ret != ippStsNoErr) {
        qDebug()  << "Error LhtResampleInit: " << ret ;
        return;
    }
    pInRate = inRate;
    pOutRate = outRate;
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";

}

bool ComputingInterface::LhtResample(const float* src, float* dst, int srcLen, int& retLen,float norm , bool printResult)
{
    if (!pResampleState) {
        qDebug() << "Resample not initialized.";
        return false;
    }

    if (!src || !dst) {
        qDebug() << "Invalid source or destination pointer.";
        return false;
    }
    int history=128;
    double time=history;
    QTime t;
    t.start();
    auto ret = ippsResamplePolyphaseFixed_32f(src,srcLen,dst,norm,&time,&retLen,pResampleState);
    if (ret != ippStsNoErr) {
        qDebug()  << "Error LhtSampleUp: " << ret ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";

    if (printResult) {
        printVector(dst, retLen);
    }

    return true;
}

bool ComputingInterface::LhtSampleUp(const float *pSrc, int srcLen, float *pDst, int *pDstLen, int factor)
{
    QTime t;
    t.start();
    int pPhase = 0;
    auto ret = ippsSampleUp_32f(pSrc,srcLen,pDst,pDstLen,factor,&pPhase);
    if (ret != ippStsNoErr) {
        qDebug()  << "Error LhtSampleUp: " << ret ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}

bool ComputingInterface::LhtSampleDown(const float *pSrc, int srcLen, float *pDst, int *pDstLen, int factor)
{
    QTime t;
    t.start();
    int pPhase = 0;
    auto ret = ippsSampleDown_32f(pSrc,srcLen,pDst,pDstLen,factor,&pPhase);
    if (ret != ippStsNoErr) {
        qDebug()  << "Error LhtSampleDown: " << ret ;
        return false;
    }
    qDebug() << __FUNCDNAME__ << "use time = " << t.elapsed() << "ms";
    return true;
}


void ComputingInterface::LhtResampleFree()
{
    ippFree(pResampleState);
}
static void doFFTForward_C_64fc(
    const IppsFFTSpec_C_64fc* pSpec,
    std::vector<LhtComplexDouble>& data,
    Ipp8u* pBuffer
    )
{
    IppStatus status = ippsFFTFwd_CToC_64fc(
        reinterpret_cast<Ipp64fc*>(data.data()),   // src
        reinterpret_cast<Ipp64fc*>(data.data()),   // dst
        pSpec,
        pBuffer
        );
    if (status != ippStsNoErr) {
        throw std::runtime_error("ippsFFTFwd_CToC_64fc failed.");
    }
}

/*====================================================================
   doFFTInverse_C_64fc
     - 对输入 data 做 in-place 逆变换(Inverse FFT)
     - 由于 init 时用了 IPP_FFT_DIV_INV_BY_N,
       IPP 会在逆变换时自动 /Nfft
======================================================================*/
static void doFFTInverse_C_64fc(
    const IppsFFTSpec_C_64fc* pSpec,
    std::vector<LhtComplexDouble>& data,
    Ipp8u* pBuffer
    )
{
    IppStatus status = ippsFFTInv_CToC_64fc(
        reinterpret_cast<Ipp64fc*>(data.data()),   // src
        reinterpret_cast<Ipp64fc*>(data.data()),   // dst
        pSpec,
        pBuffer
        );
    if (status != ippStsNoErr) {
        throw std::runtime_error("ippsFFTInv_CToC_64fc failed.");
    }
}


std::vector<LhtComplexDouble> ComputingInterface::LhtCztCopyFromMatlab(
    const std::vector<LhtComplexDouble>& x,  // 输入序列, length=n
    int k,                          // 输出长度
    LhtComplexDouble w,                      // czt 参数, e.g. exp(-j*2*pi*(f2-f1)/(k*fs))
    LhtComplexDouble a                       // czt 参数, e.g. exp( j*2*pi*f1/fs )
    )
{
    using std::vector;
    using std::pow;

    const int n = (int)x.size(); // 输入长度
    const int L = 2 * n - 1;     // n + k - 1

    // (1) 计算 FFT 大小 nfft = 2^nextpow2(L)
    int nfft = 1;
    while (nfft < L) {
        nfft <<= 1;
    }

    // (2)  构造 ww[]，长度 = L
    //      MATLAB: kk = -n+1 : ...
    //              ww = w^( (kk^2)/2 )
    vector<LhtComplexDouble> ww(L);
    for (int i = 0; i < L; i++) {
        int kk = (-n + 1) + i;
        double kk2 = double(kk) * double(kk) / 2.0;
        ww[i] = pow(w, kk2);
    }

    // (3)  构造输入 y[], 长度 n
    //      y[i] = x[i]* a^(-i)* ww[n-1 + i]
    vector<LhtComplexDouble> y(n);
    for (int i = 0; i < n; i++) {
        LhtComplexDouble a_neg_i = pow(a, -double(i));
        int ww_idx = (n - 1) + i;
        y[i] = x[i] * a_neg_i * ww[ww_idx];
    }

    // (4)  准备两个长度 nfft 的向量: fy, fv
    //      fy 用于存放 y 的零填充后 FFT
    //      fv = 1./ww(...)  也做 FFT, 用于卷积核
    vector<LhtComplexDouble> fy(nfft, LhtComplexDouble(0, 0));
    std::memcpy(fy.data(), y.data(), n * sizeof(LhtComplexDouble));

    vector<LhtComplexDouble> fv(nfft, LhtComplexDouble(0, 0));

    std::vector<LhtComplexDouble> ones(L, LhtComplexDouble(1.0, 0.0));
    ippsDiv_64fc(
        reinterpret_cast<const Ipp64fc*>(ww.data()),
        reinterpret_cast<const Ipp64fc*>(ones.data()),
        reinterpret_cast<Ipp64fc*>(fv.data()),
        L
        );

    // 1) 计算 order
    int order = 0;
    {
        int tmp = nfft;
        while (tmp > 1) { tmp >>= 1; order++; }
    }

    // 2) 计算所需 spec/init/buf 大小
    int sizeSpec = 0, sizeInit = 0, sizeBuf = 0;
    IppStatus st = ippsFFTGetSize_C_64fc(
        order,
        IPP_FFT_DIV_INV_BY_N,    // => 逆变换时除以 nfft
        ippAlgHintFast,
        &sizeSpec,
        &sizeInit,
        &sizeBuf
        );
    if (st != ippStsNoErr) {
        throw std::runtime_error("ippsFFTGetSize_C_64fc failed.");
    }

    // 3) 分配内存 (spec, init, buffer)
    Ipp8u* pSpecMem = ippsMalloc_8u(sizeSpec);
    Ipp8u* pInitMem = ippsMalloc_8u(sizeInit);
    Ipp8u* pBufMem = ippsMalloc_8u(sizeBuf);

    if (!pSpecMem || !pInitMem || !pBufMem) {
        // 防御性判断
        if (pSpecMem) ippsFree(pSpecMem);
        if (pInitMem) ippsFree(pInitMem);
        if (pBufMem)  ippsFree(pBufMem);
        throw std::runtime_error("ippsMalloc_8u failed to allocate memory.");
    }

    // 4) 初始化 FFT spec
    IppsFFTSpec_C_64fc* pSpec = nullptr;
    st = ippsFFTInit_C_64fc(
        &pSpec,
        order,
        IPP_FFT_DIV_INV_BY_N,
        ippAlgHintFast,
        pSpecMem,
        pInitMem
        );
    if (st != ippStsNoErr || !pSpec) {
        ippsFree(pSpecMem);
        ippsFree(pInitMem);
        ippsFree(pBufMem);
        throw std::runtime_error("ippsFFTInit_C_64fc failed.");
    }

    // ======================== FFT计算 ========================
    // (5)  Forward FFT on fy & fv
    doFFTForward_C_64fc(pSpec, fy, pBufMem);
    doFFTForward_C_64fc(pSpec, fv, pBufMem);

    // (6) 频域相乘
    IppStatus status = ippsMul_64fc_I(
        reinterpret_cast<const Ipp64fc*>(fv.data()),  // pSrc2
        reinterpret_cast<Ipp64fc*>(fy.data()),  // pSrcDst (in-place)
        nfft
        );
    // (7) IFFT => fy
    doFFTInverse_C_64fc(pSpec, fy, pBufMem);
    //  因为 IPP_FFT_DIV_INV_BY_N, 已自动 /nfft

    // (8) 取出 & 乘回 ww
    //     g[i] = fy[n-1 + i] * ww[n-1 + i], i=0..k-1
    std::vector<LhtComplexDouble> g(k);
    \
        ippsMul_64fc(
            reinterpret_cast<const Ipp64fc*>(fy.data() + (n - 1)),
            reinterpret_cast<const Ipp64fc*>(ww.data() + (n - 1)),
            reinterpret_cast<Ipp64fc*>      (g.data()),
            k
            );

    ippsFree(pSpecMem);
    ippsFree(pInitMem);
    ippsFree(pBufMem);

    return g;
}

//czt示例
#if 0
int main()
{
    // ========== 1) 参数设置 (与 MATLAB 一致) ==========
    int n = 24000;          // 输入长度
    int k = 1024*2;        // 输出CZT长度
    double f1 = 100.0;
    double f2 = 150.0;
    double fs = 1000.0;

    LhtComplexDouble W = std::exp(LhtComplexDouble(0.0, -2.0 * M_PI * (f2 - f1) / (k * fs)));
    LhtComplexDouble A = std::exp(LhtComplexDouble(0.0, 2.0 * M_PI * f1 / fs));

    std::vector<LhtComplexDouble> x(n, LhtComplexDouble(1.0, 0.0));

    try {
        auto result = LhtCztCopyFromMatlab(x, k, W, A);

        std::cout << "First 20 results:\n";
        for (int i = 0; i < 20; i++) {
            std::cout << i << ": ("
                      << result[i].real() << ", "
                      << result[i].imag() << ")\n";
        }
    }
    catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << "\n";
    }

    return 0;
}

#endif

bool ComputingInterface::LhtFCwtInit(Wavelet *pwav, SCALETYPE st, int fs, float f0, float f1, int fn, int pthreads,
                                     bool puse_optimalization_schemes, bool puse_normalization)
{
    if(pFcwt != nullptr){
        delete pFcwt;
        pFcwt = nullptr;
        delete pScales;
        pScales = nullptr;
    }
    pFcwt = new FCWT(pwav, pthreads, puse_optimalization_schemes, puse_normalization);

    pScales = new Scales(pwav, st, fs, f0, f1, fn);

    return 1;
}
#include <QFile>
void ComputingInterface::LhtFCwt(complex<float> *pinput, int psize, complex<float> *poutput)
{
    float fn[132];
    pScales->getFrequencies(fn,132);
    QFile w("D:/data/cwtRetF.dat");
    w.open(QIODevice::WriteOnly);
    w.write((char*)fn,132 * 4);
    w.close();
    pFcwt->cwt(pinput, psize, poutput, pScales);
}

void ComputingInterface::LhtFindMinInterval(float *input, int len, float th,int &index1 , int &index2 , int &length, bool useSimd)
{
    length = len;
    index1 = -1 , index2= -1;
    std::vector<int> indexList;
    bool flagFind = true;
    indexList.clear();
    if(useSimd){
        //这里使用指令集加速牛丁丁
        __m256 threshold = _mm256_set1_ps(th);

        // 遍历数据，使用 AVX2 加速
        for (int i = 0; i < len - 1; i += 8) {
            // 加载 8 个连续的浮点数
            __m256 current_values = _mm256_loadu_ps(&input[i]);
            __m256 next_values = _mm256_loadu_ps(&input[i + 1]);

            __m256 cmp_gt = _mm256_cmp_ps(current_values, next_values, _CMP_GT_OS);
            __m256 cmp_gt_th = _mm256_cmp_ps(current_values, threshold, _CMP_GT_OS);
            if(flagFind){
                __m256 final_cmp = _mm256_and_ps(cmp_gt, cmp_gt_th);
                int mask = _mm256_movemask_ps(final_cmp);
                for (int j = 0; j < 8; ++j) {
                    if ((mask >> j) & 1) {
                        indexList.push_back(i + j);
                        flagFind = false;
                        break;
                    }
                }
            }else{
                __m256 prev_next_cmp = _mm256_cmp_ps(current_values, next_values, _CMP_LT_OS);  // prev_values < current_values
                int prev_mask = _mm256_movemask_ps(prev_next_cmp);
                if (prev_mask != 0) {
                    flagFind = true;
                }
            }
        }
    }
    else {
        //这里使用普通代码
        for(int i = 0 ; i < (len - 1); i++){

            if(!flagFind && input[i] < input[i+1])
                flagFind = true;

            if(flagFind && input[i] > th && input[i] > input[i+1]){
                flagFind = false;
                indexList.push_back(i);
            }
        }
    }
    for(int i = 0 ; i < indexList.size() - 1; i++){

        if((indexList[i+1] - indexList[i]) < length){
            length = indexList[i+1] - indexList[i];
            index1 = indexList[i];
            index2 = indexList[i+1];
        }
    }

}
typedef ComputingInterface::LhtAStarNode Node;
// 启发式函数：使用曼哈顿距离估算
float heuristic(const Node& a, const Node& b) {
    return abs(a.x - b.x) + abs(a.y - b.y);
}
#include <queue>
#include <cmath>
#include <unordered_map>
#include <functional>
#include <QFile>
// A* 算法
vector<Node> aStar(Node start, Node goal, const vector<vector<int>>& grid) {
    priority_queue<Node, vector<Node>, greater<Node>> openList; // 最小堆
    unordered_map<int, unordered_map<int, Node>> cameFrom; // 用于记录路径

    openList.push(start);

    // 四个方向的移动（上下左右）
    vector<pair<int, int>> directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    while (!openList.empty()) {
        Node current = openList.top();
        openList.pop();

        // 如果找到目标节点
        if (current.x == goal.x && current.y == goal.y) {
            vector<Node> path;
            while (!(current.x == start.x && current.y == start.y)) {
                path.push_back(current);
                current = cameFrom[current.x][current.y];
            }
            path.push_back(start);
            reverse(path.begin(), path.end());
            return path;
        }

        // 检查所有相邻节点
        for (const auto& dir : directions) {
            int newX = current.x + dir.first;
            int newY = current.y + dir.second;

            if (newX >= 0 && newX < grid.size() && newY >= 0 && newY < grid[0].size() && grid[newX][newY] == 0) {
                Node neighbor(newX, newY);
                neighbor.g = current.g + 1; // 假设每步代价为1
                neighbor.h = heuristic(neighbor, goal);
                neighbor.f = neighbor.g + neighbor.h;

                if (cameFrom.find(newX) == cameFrom.end() || cameFrom[newX].find(newY) == cameFrom[newX].end()) {
                    cameFrom[newX][newY] = current;
                    openList.push(neighbor);
                }
            }
        }
    }

    return {}; // 如果没有路径，返回空
}

vector<Node> ComputingInterface::LhtAStar(Node start, Node goal, const vector<vector<int> > &grid)
{
    vector<Node> path = aStar(start, goal, grid);
    return path;
}


//fcwt示例
#if 0
int main(int argc, char * argv[]) {

    int n = 1000; //signal length
    const int fs = 1000; //sampling frequency
    float twopi = 2.0*3.1415;

    //3000 frequencies spread logartihmically between 1 and 32 Hz
    const float f0 = 0.1;
    const float f1 = 20;
    const int fn = 200;

    //Define number of threads for multithreaded use
    const int nthreads = 8;

    //input: n real numbers
    std::vector<float> sig(n);

    //input: n complex numbers
    std::vector<complex<float>> sigc(n);

    //output: n x scales x 2 (complex numbers consist of two parts)
    std::vector<complex<float>> tfm(n*fn);

    //initialize with 1 Hz cosine wave
    for(auto& el : sig) {
        el = cos(twopi*((float)(&el - &sig[0])/(float)fs));
    }

    //initialize with 1 Hz cosine wave
    for(auto& el : sigc) {
        el = complex<float>(cos(twopi*((float)(&el - &sigc[0])/(float)fs)), 0.0f);
    }

    //Start timing
    auto start = chrono::high_resolution_clock::now();

    //Create a wavelet object
    Wavelet *wavelet;

    //Initialize a Morlet wavelet having sigma=1.0;
    Morlet morl(1.0f);
    wavelet = &morl;

    //Other wavelets are also possible
    //DOG dog(int order);
    //Paul paul(int order);

    //Create the continuous wavelet transform object
    //constructor(wavelet, nthreads, optplan)
    //
    //Arguments
    //wavelet   - pointer to wavelet object
    //nthreads  - number of threads to use
    //optplan   - use FFTW optimization plans if true
    FCWT fcwt(wavelet, nthreads, true, false);

    //Generate frequencies
    //constructor(wavelet, dist, fs, f0, f1, fn)
    //
    //Arguments
    //wavelet   - pointer to wavelet object
    //dist      - FCWT_LOGSCALES | FCWT_LINSCALES for logarithmic or linear distribution of scales across frequency range
    //fs        - sample frequency
    //f0        - beginning of frequency range
    //f1        - end of frequency range
    //fn        - number of wavelets to generate across frequency range
    Scales scs(wavelet, FCWT_LINFREQS, fs, f0, f1, fn);

    //Perform a CWT
    //cwt(input, length, output, scales)
    //
    //Arguments:
    //input     - floating pointer to input array
    //length    - integer signal length
    //output    - floating pointer to output array
    //scales    - pointer to scales object
    fcwt.cwt(&sigc[0], n, &tfm[0], &scs);

    //End timing
    auto finish = chrono::high_resolution_clock::now();

    //Calculate total duration
    chrono::duration<double> elapsed = finish - start;

    cout << "=== fCWT example ===" << endl;
    cout << "Calculate CWT of a 100k sample sinusodial signal using a [" << f0 << "-" << f1 << "] Hz linear frequency range and " << fn << " wavelets." << endl;
    cout << "====================" << endl;
    cout << "fCWT finished in " << elapsed.count() << "s" << endl;

    return 0;
}

#endif

template<typename T>
void ComputingInterface::printVector(const T &value, int len)
{
    for (int var = 0; var < len; ++var) {
        qDebug() <<"index:"<<var<<"="<< value[var] ;
    }

}

QString ComputingInterface::arrayToString(const float *  array, int size) {
    QString result;
    QTextStream stream(&result);

    for (int i = 0; i < size; ++i) {
        if (i != 0) {
            stream << ", "; // 在每个float之间添加逗号和空格
        }
        stream << array[i];
    }
    return result;
}

bool ComputingInterface::lhtHilbertFloatInit(int len)
{
    return 1;
}
