#ifndef FFTW_COMPUTING_INTERFACE_H
#define FFTW_COMPUTING_INTERFACE_H

#include "fftw3.h"

class FftwComputingInterface
{
public:
    FftwComputingInterface();


    //fft  初始化  比较耗时
    bool LhtFftInitFloat(int len);
    //fft 析构
    void LhtFftFreeFloat();
#pragma message("使用时需要前置函数 LhtFftInitFloat")
    //fft计算  可支持任意长度的dft
    bool LhtFftFloat(float * in1 , float * in2 ,int len,float * result,bool printResult = false);

private:
    // fft 参数
    fftwf_complex* output = nullptr;
    fftwf_complex* input = nullptr;
    fftwf_plan plan;
    bool isInit = false;
};

#endif // FFTW_COMPUTING_INTERFACE_H
