#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QMainWindow>
#include "includes.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void initTableView();

    void initProgress();

    void initFilter();

    void initPos();

    void initQSplitter();

    void initDockWidget();

    void initFloatWidget();

    void initMap();

    void initPlot();
protected:
    void contextMenuEvent(QContextMenuEvent *event);
private slots:
    void on_select_img_btn_clicked();

    void onMouseRelease(QRect);

    void slotDrawPolygon(QVector<QPointF>);

    void on_rect_btn_clicked();

    void on_polygon_btn_clicked();
    void on_float_btn_clicked();

    void on_checkBox_stateChanged(int arg1);

    void on_checkBox_2_stateChanged(int arg1);

    void on_checkBox_3_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;

    LhtExamples m_lhtExamples;

    int                         m_progress = 0;

    QTimer                      m_timer;

    SpinningRound   *           m_roundLabel;

    ProgressOfTheBarrel *       m_proressOfTheBarrel;

    QDialog *                   m_dialog ;

    CustomPlotExamples  *       m_customPlotExamples;

    LocalAddressInterfacePtr    m_pos;
};
#endif // MAINWINDOW_H
