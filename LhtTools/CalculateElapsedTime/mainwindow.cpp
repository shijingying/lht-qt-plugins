#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRandomGenerator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    LHT_TIME_CONSUMING;
    for (int i = 0; i < 1e6; i++) {
        int randomNum = QRandomGenerator::global()->bounded(1, 100);
        int result = i * randomNum;
    }
}

