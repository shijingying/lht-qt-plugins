#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#define LHT_TIME_CONSUMING_CAT2(x, y) x##y
#define LHT_TIME_CONSUMING_CAT(x, y) LHT_TIME_CONSUMING_CAT2(x, y)
#define LHT_TIME_CONSUMING const auto LHT_TIME_CONSUMING_CAT(lhtTimeConsuming_, __COUNTER__) = LhtTimeConsuming(__func__, __LINE__)
//耗时计算
class LhtTimeConsuming {
public:
    LhtTimeConsuming(const char* func, int line)
        : func_name(func), line_number(line) {
        start_time = std::chrono::high_resolution_clock::now();
    }

    ~LhtTimeConsuming() {
        // 记录结束时间
        auto end_time = std::chrono::high_resolution_clock::now();

        // 计算耗时，并转换为毫秒
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        // 打印耗时（单位：毫秒）
        std::cout << "Time consuming in function: " << func_name << " at line " << line_number
                  << " is " << duration.count() << " ms."<<std::endl;
    }

private:
    const char* func_name;
    int line_number;
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
};

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
