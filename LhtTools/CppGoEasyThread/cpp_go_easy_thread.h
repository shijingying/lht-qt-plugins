﻿// cpp_go_easy_thread.h
#ifndef CPP_GO_EASY_THREAD_H
#define CPP_GO_EASY_THREAD_H

#include <iostream>
#include <thread>
#include <utility>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <future>
#include <atomic>
#include <vector>

// Channel类，用于实现消息传递
template<typename T>
class Channel {
public:
    Channel() = default;

    Channel(int count) : maxCount_(count) {}

    [[deprecated("此函数即将删除，请使用构造函数初始化大小")]]
    void setMaxCount(int count){
        maxCount_.store(count);
    }

    bool send(const T& value) {
        {
            std::unique_lock<std::mutex> lock(mutex_);
            queue_.push(value);
            if(queue_.size() > maxCount_.load())
                return false;
        }
        cv_.notify_one();
        return true;
    }

    bool operator <<(const T& value) {
        return send(value);
    }

    T receive() {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_.wait(lock, [this] { return !queue_.empty(); });
        T value = queue_.front();
        queue_.pop();
        return value;
    }

    void operator>>(T& value) {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_.wait(lock, [this] { return !queue_.empty(); });
        value = queue_.front();
        queue_.pop();
    }
private:
    std::atomic_int maxCount_ = 100;
    std::queue<T> queue_;
    std::mutex mutex_;
    std::condition_variable cv_;
};

// 线程池类
class ThreadPool {
public:
    // 构造函数，初始化线程池并启动工作线程
    ThreadPool(size_t numThreads = std::thread::hardware_concurrency()) {
        start(numThreads);
    }

    // 析构函数，停止线程池并加入所有线程
    ~ThreadPool() {
        stop();
    }

    // 禁用拷贝和赋值
    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    // 提交任务到线程池
    void enqueue(std::function<void()> task) {
        {
            std::unique_lock<std::mutex> lock(mutex_);
            if(stop_) {
                throw std::runtime_error("enqueue on stopped ThreadPool");
            }
            tasks_.emplace(std::move(task));
        }
        cv_.notify_one();
    }

private:
    std::vector<std::thread> workers_;              // 工作线程
    std::queue<std::function<void()>> tasks_;       // 任务队列

    std::mutex mutex_;                              // 互斥锁
    std::condition_variable cv_;                    // 条件变量
    bool stop_ = false;                             // 停止标志

    // 启动线程池
    void start(size_t numThreads) {
        for(size_t i = 0; i < numThreads; ++i){
            workers_.emplace_back([this]() {
                while(true){
                    std::function<void()> task;
                    {
                        std::unique_lock<std::mutex> lock(this->mutex_);
                        this->cv_.wait(lock, [this]() { return this->stop_ || !this->tasks_.empty(); });
                        if(this->stop_ && this->tasks_.empty())
                            return;
                        task = std::move(this->tasks_.front());
                        this->tasks_.pop();
                    }
                    task();
                }
            });
        }
    }

    // 停止线程池
    void stop(){
        {
            std::unique_lock<std::mutex> lock(mutex_);
            stop_ = true;
        }
        cv_.notify_all();
        for(auto &worker : workers_){
            if(worker.joinable())
                worker.join();
        }
    }
};

// 全局线程池实例
inline ThreadPool threadPool;

// 线程调度函数，使用线程池
template<typename Function>
void go_impl(Function&& f) {
    threadPool.enqueue([f = std::forward<Function>(f)](){
        try {
            f();
        } catch (const std::exception& e){
            std::cerr << "线程异常: " << e.what() << std::endl;
        } catch (...){
            std::cerr << "线程发生未知异常。" << std::endl;
        }
    });
}

// 保持原来的宏定义不变
#define LhtGo(...) go_impl([&]() { __VA_ARGS__; })


#include <future>
template<typename Function, typename ResultType>
void go_with_promise(std::promise<ResultType>&& promise, Function&& f) {
    auto sp = std::make_shared<std::promise<ResultType>>(std::move(promise));
    threadPool.enqueue([sp, f = std::forward<Function>(f)]() mutable {
        try {
            sp->set_value(f());
        } catch (const std::exception& e){
            sp->set_exception(std::current_exception());
        } catch (...){
            sp->set_exception(std::current_exception());
        }
    });
}

#define LhtGoPromise(result, func) \
{ \
        using ResultType = decltype(func()); \
        std::promise<ResultType> promise; \
        std::future<ResultType> future = promise.get_future(); \
        go_with_promise(std::move(promise), func); \
        result = std::move(future); \
}
#endif // CPP_GO_EASY_THREAD_H
