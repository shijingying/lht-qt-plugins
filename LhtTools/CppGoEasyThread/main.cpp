﻿#include "cpp_go_easy_thread.h" // 上面定义的 go 宏

#include <chrono>

class GrpcTools {
public:
    void StartGRPCServer(const std::string& protocol, const std::string& address) {
        std::cout << "启动GRPC服务器，协议：" << protocol << "，地址：" << address << std::endl;
        std::thread::id this_id = std::this_thread::get_id();
        // 打印线程 ID
        std::cout << "当前线程的 ID 是: " << this_id << std::endl;
        // 模拟服务器运行
        std::this_thread::sleep_for(std::chrono::seconds(2));
        std::cout << "GRPC服务器已停止。" << std::endl;
    }
};

void testThread(int i )
{
    std::cout << "index ：" << i<< std::endl;
    std::thread::id this_id = std::this_thread::get_id();
    // 打印线程 ID
    std::cout << "当前线程的 ID 是: " << this_id << std::endl;
    // 模拟服务器运行
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "testThread已停止。" << std::endl;
}
int task() {
    std::this_thread::sleep_for(std::chrono::seconds(2));
    return 42;
}
int main() {

    //这里用的是完美转发，所以他原本的左值和右值都是保留的
    //唯一的问题就是他的数据目前没有办法保证是安全的，存在线程安全问题
    GrpcTools GrpcToolsInstance;
    //调用类的方法示例 1
    LhtGo(GrpcToolsInstance.StartGRPCServer("tcp", ":50051"));
    //调用方法示例 2
    LhtGo(testThread(1));

    //传输数据的示例以及lamda表达式传入示例 3
    Channel<int> ch;
    LhtGo({
        for (int i = 1; i < 100; ++i) {
            ch << i;
        }
        std::cout << "Channel 发送退出！ "<< std::endl;
    });

    // 线程B：接收数据
    LhtGo({
        int val;
        for (;;) {
            ch >> val;
            std::cout << "[ThreadB1] 接收: " << val << std::endl;
        }
    });
    LhtGo({
        int val;
        for (;;) {
            ch >> val;
            std::cout << "[ThreadB2] 接收: " << val << std::endl;
        }
    });
    LhtGo({
        int val;
        for (;;) {
            ch >> val;
            std::cout << "[ThreadB3] 接收: " << val << std::endl;
        }
    });
    std::future<int> resultFuture;

    auto task = []() -> int {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        return 42;
    };
    //示例 4  这里的话可以用在比如画图的时候，需要预处理一下数据就可以这么做
    //感觉意义不大,暂时还没有什么使用场景
    LhtGoPromise(resultFuture, task);

    std::cout << "正在等待任务结果..." << std::endl;
    int result = resultFuture.get();
    std::cout << "任务完成，结果是: " << result << std::endl;

    system("pause");
    return 0;
}
