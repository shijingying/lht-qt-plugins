#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLineEdit>
#include <QVBoxLayout>
#include "trie_model.h"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // 创建主窗口
    QWidget *window = new QWidget(this);
    QVBoxLayout *layout = new QVBoxLayout(window);

    // 创建LineEdit
    QLineEdit *lineEdit = new QLineEdit();
    lineEdit->setPlaceholderText("请输入电话号码前缀...");
    layout->addWidget(lineEdit);

    // 生成100,000个随机电话号码
    QStringList phoneNumbers = generateRandomPhoneNumbers(100000);

    // 构建Trie
    Trie *trie = new Trie;
    for(const QString &number : phoneNumbers) {
        trie->insert(number);
    }

    // 创建自定义模型
    TrieModel *model = new TrieModel(trie, 100, window);

    // 设置QCompleter
    QCompleter *completer = new QCompleter(model, window);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setFilterMode(Qt::MatchStartsWith);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    completer->setCompletionRole(Qt::DisplayRole);
    lineEdit->setCompleter(completer);

    // 连接输入信号以更新过滤
    QObject::connect(lineEdit, &QLineEdit::textChanged, model, &TrieModel::setFilter);

    // 设置布局和窗口属性
    window->setLayout(layout);
    window->setWindowTitle("电话号码提示搜索");
    this->resize(400, 100);
    this->setCentralWidget(window);
}

MainWindow::~MainWindow()
{
    delete ui;
}
