#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "scope_exit.h"

#include <QFile>

void example() {
    int* arr = new int[10];
    NSCOPE_EXIT_ARRAY(arr);
    int* obj = new int;
    NSCOPE_EXIT_ARRAY(obj);

    QFile *file = new QFile;
    NSCOPE_EXIT_CUSTOM(file,[&]{
        file->close();
        std::cout << "file deleted!" << file->fileName().toStdString() << std::endl;
        delete file;
    }
                       );
    file->setFileName("D:/test.txt");
    file->open(QIODevice::WriteOnly);
    QByteArray array;
    array.append(1);
    file->write(array);

    std::cout << "Working with array and object..." << std::endl;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    example();  // 当作用域结束时，数组会被删除


}

MainWindow::~MainWindow()
{
    delete ui;
}
