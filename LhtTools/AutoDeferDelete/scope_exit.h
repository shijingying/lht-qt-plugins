#pragma once
#include <iostream>
#include <utility>
#include <functional>

#define SCOPE_EXIT_CAT2(x, y) x##y
#define SCOPE_EXIT_CAT(x, y) SCOPE_EXIT_CAT2(x, y)
// 宏重载，允许调用者不提供清理函数
#define NSCOPE_EXIT(obj) const auto SCOPE_EXIT_CAT(scopeExit_, __COUNTER__) = ScopeExit::MakeScopeExit::Create(obj)
#define NSCOPE_EXIT_CUSTOM(obj, cleanup) const auto SCOPE_EXIT_CAT(scopeExit_, __COUNTER__) = ScopeExit::MakeScopeExit::Create(obj, cleanup)

#define NSCOPE_EXIT_ARRAY(obj) const auto SCOPE_EXIT_CAT(scopeExit_, __COUNTER__) = ScopeExit::MakeScopeExit::CreateArray(obj)
#define NSCOPE_EXIT_ARRAY_CUSTOM(obj, cleanup) const auto SCOPE_EXIT_CAT(scopeExit_, __COUNTER__) = ScopeExit::MakeScopeExit::CreateArray(obj, cleanup)

namespace ScopeExit {
template<typename F>
class ScopeExit {
public:
    explicit ScopeExit(F&& fn) : m_fn(std::move(fn)) {}

    ~ScopeExit() {
        m_fn();
    }

    ScopeExit(const ScopeExit&) = delete;
    ScopeExit& operator=(const ScopeExit&) = delete;

    ScopeExit(ScopeExit&&) noexcept = default;
    ScopeExit& operator=(ScopeExit&&) noexcept = default;

private:
    F m_fn;
};

struct MakeScopeExit {
    template<typename T>
    static ScopeExit<std::function<void()>> Create(T* obj, std::function<void()> fn = nullptr) {
        if (!fn) {
            fn = [obj]() { delete obj; std::cout << "Object delete!" << std::endl; };
        }
        return ScopeExit<std::function<void()>>(std::move(fn));
    }

    template<typename T>
    static ScopeExit<std::function<void()>> CreateArray(T* obj, std::function<void()> fn = nullptr) {
        if (!fn) {
            fn = [obj]() { delete[] obj; std::cout << "Array delete!" << std::endl; };
        }
        return ScopeExit<std::function<void()>>(std::move(fn));
    }
};
}

