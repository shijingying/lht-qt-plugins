﻿#include "star_map_plot.h"
#include "ui_star_map_plot.h"
#include "QsLog.h"
StarMapPlot::StarMapPlot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StarMapPlot)
{
    ui->setupUi(this);
    customPlot = new QCustomPlot(ui->widget);
    ui->verticalLayout->addWidget(customPlot);
}

StarMapPlot::~StarMapPlot()
{
    delete ui;
}

void StarMapPlot::addData(float *iData, float *qData, int len)
{
    if(!m_isInit)
    {
        QLOG_ERROR()<<"Star Map is uninit";
        return;
    }
    // 创建QCustomPlot对
    QCPGraph *qpskGraph = customPlot->graph();
    qpskGraph->setLineStyle(QCPGraph::lsNone); // 不连接相位点
    qpskGraph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::black, Qt::green, 5)); // 设置相位点的样式
    QVector<double> XData, YData;
    XData.reserve(len);
    YData.reserve(len);
    float iDataSum = 0,qDataSum = 0,
            iDataMean = 0,qDataMean = 0;
    //归一化到二分之根号二 附近
    for(int i = 0 ; i < len ;i++){
        iDataSum += abs(iData[i]);
        qDataSum += abs(qData[i]);
    }
    iDataMean = iDataSum / len;
    qDataMean = qDataSum / len;
    auto temp = sqrt(2) / 2;
    for(int i = 0 ; i < len ;i++)
    {
        XData.append((iData[i]/iDataMean) * temp);
        YData.append((qData[i]/qDataMean) * temp);
    }

    qpskGraph->setData(XData, YData);
    customPlot->replot();
}

void StarMapPlot::init(int low)
{
    m_low = low;

    // 设置星座图的坐标范围
    customPlot->xAxis->setRange(-m_low, m_low);
    customPlot->yAxis->setRange(-m_low, m_low);

    // 创建星座图数据
    QVector<QPointF> constellationPoints;
    constellationPoints << QPointF(-m_low, -m_low)
                        << QPointF(m_low, m_low);
    //显示图例,图例背景轻微透明

    customPlot->setBackground(QColor(0,100,255,100));
    // 绘制星座图的四个点并连接它们
    QCPGraph *constellationGraph = customPlot->addGraph();
    constellationGraph->setLineStyle(QCPGraph::lsLine);
    constellationGraph->setPen(QPen(Qt::black));
    QVector<double> xData, yData;
    for (const QPointF& point : constellationPoints) {
        xData.append(point.x());
        yData.append(point.y());
    }
    constellationGraph->setData(xData, yData);

    QVector<QPointF> constellationPoints2;
    constellationPoints2 << QPointF(-m_low, m_low)
                         << QPointF(m_low, -m_low);
    QCPGraph *constellationGraph2 = customPlot->addGraph();
    constellationGraph2->setLineStyle(QCPGraph::lsLine);
    constellationGraph2->setPen(QPen(Qt::black));
    QVector<double> xData2, yData2;
    for (const QPointF& point : constellationPoints2) {
        xData2.append(point.x());
        yData2.append(point.y());
    }
    constellationGraph2->setData(xData2, yData2);

    customPlot->addGraph();
    // 显示星座图的内切圆
    for(int i = 1 ;i <= m_low ; i++){
        double radius = i;
        QCPItemEllipse *outerCircle = new QCPItemEllipse(customPlot);
        outerCircle->topLeft->setCoords(-radius, radius);
        outerCircle->bottomRight->setCoords(radius, -radius);
        outerCircle->setPen(QPen(Qt::black));
        outerCircle->setBrush(QBrush(Qt::NoBrush)); // 透明填充
    }


    //    customPlot->xAxis->setVisible(0);
    //    customPlot->yAxis->setVisible(0);
    customPlot->xAxis->setTicks(0);
    customPlot->yAxis->setTicks(0);
    customPlot->xAxis->setTickLabels(0);
    customPlot->yAxis->setTickLabels(0);
    customPlot->xAxis->setBasePen(QPen(Qt::black, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::black, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->setTickPen(QPen(Qt::black, 1)); // Y轴刻度线实线
    // 设置坐标轴刻度线的样式为实线
    customPlot->xAxis->grid()->setPen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->grid()->setPen(QPen(Qt::black, 1)); // Y轴刻度线实线
    customPlot->xAxis->grid()->setZeroLinePen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->grid()->setZeroLinePen(QPen(Qt::black, 1)); // Y轴刻度线实线
    // 设置星座图的标题和轴标签
    // customPlot->plotLayout()->insertRow(0);
    // customPlot->plotLayout()->addElement(0, 0, new QCPTextElement(customPlot, QStringLiteral("星座图"), QFont("sans", 12, QFont::Bold)));
    customPlot->xAxis->setLabel("I");
    customPlot->yAxis->setLabel("Q");

    // 显示星座图
    customPlot->replot();

    m_isInit = 1;
}

void StarMapPlot::test()
{
    // 设置星座图的坐标范围
    customPlot->xAxis->setRange(-1.0, 1.0);
    customPlot->yAxis->setRange(-1.0, 1.0);

    // 创建星座图数据
    QVector<QPointF> constellationPoints;
    constellationPoints << QPointF(-1.0, -1.0)
                        << QPointF(1.0, 1.0);/*
                        << QPointF(- 1.0, -1.0)
                        << QPointF(- 1.0, 1.0);*/

    // 绘制星座图的四个点并连接它们
    QCPGraph *constellationGraph = customPlot->graph();
    constellationGraph->setLineStyle(QCPGraph::lsLine);
    constellationGraph->setPen(QPen(Qt::black));
    QVector<double> xData, yData;
    for (const QPointF& point : constellationPoints) {
        xData.append(point.x());
        yData.append(point.y());
    }
    constellationGraph->setData(xData, yData);

    QVector<QPointF> constellationPoints2;
    constellationPoints2 << QPointF(-1.0, 1.0)
                         << QPointF(1.0, -1.0);
    QCPGraph *constellationGraph2 = customPlot->graph();
    constellationGraph2->setLineStyle(QCPGraph::lsLine);
    constellationGraph2->setPen(QPen(Qt::black));
    QVector<double> xData2, yData2;
    for (const QPointF& point : constellationPoints2) {
        xData2.append(point.x());
        yData2.append(point.y());
    }
    constellationGraph2->setData(xData2, yData2);

    // 添加更多均匀分布的相位点（BPSK相位点）
    QVector<QPointF> qpskPoints;
    int numQPSKPoints = 100; // 想要添加的QPSK相位点数量
    for (int i = 0; i < numQPSKPoints; ++i) {
        double randomI = (qrand() % 2000 - 1000) / 1000.0; // 在 -1.0 到 1.0 之间生成随机数
        double randomQ = (qrand() % 2000 - 1000) / 1000.0;
        qpskPoints << QPointF(randomI, randomQ);
    }

    QCPGraph *qpskGraph = customPlot->addGraph();
    qpskGraph->setLineStyle(QCPGraph::lsNone); // 不连接相位点
    qpskGraph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, Qt::red, Qt::white, 5)); // 设置相位点的样式
    QVector<double> qpskXData, qpskYData;
    for (const QPointF& point : qpskPoints) {
        qpskXData.append(point.x());
        qpskYData.append(point.y());
    }
    qpskGraph->setData(qpskXData, qpskYData);

    // 显示星座图的内切圆
    double radius = 1;
    QCPItemEllipse *outerCircle = new QCPItemEllipse(customPlot);
    outerCircle->topLeft->setCoords(-radius, radius);
    outerCircle->bottomRight->setCoords(radius, -radius);
    outerCircle->setPen(QPen(Qt::black));
    outerCircle->setBrush(QBrush(Qt::NoBrush)); // 透明填充

    //    customPlot->xAxis->setVisible(0);
    //    customPlot->yAxis->setVisible(0);
    customPlot->xAxis->setTicks(0);
    customPlot->yAxis->setTicks(0);
    customPlot->xAxis->setTickLabels(0);
    customPlot->yAxis->setTickLabels(0);
    customPlot->xAxis->setBasePen(QPen(Qt::black, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::black, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->setTickPen(QPen(Qt::black, 1)); // Y轴刻度线实线
    // 设置坐标轴刻度线的样式为实线
    customPlot->xAxis->grid()->setPen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->grid()->setPen(QPen(Qt::black, 1)); // Y轴刻度线实线
    customPlot->xAxis->grid()->setZeroLinePen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->grid()->setZeroLinePen(QPen(Qt::black, 1)); // Y轴刻度线实线
    // 设置星座图的标题和轴标签
    // customPlot->plotLayout()->insertRow(0);
    // customPlot->plotLayout()->addElement(0, 0, new QCPTextElement(customPlot, "星座图", QFont("sans", 12, QFont::Bold)));
    customPlot->xAxis->setLabel("I");
    customPlot->yAxis->setLabel("Q");

    // 显示星座图
    customPlot->replot();

}
