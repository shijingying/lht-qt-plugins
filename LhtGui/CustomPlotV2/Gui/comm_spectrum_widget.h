#ifndef COMM_SPECTRUM_WIDGET_H
#define COMM_SPECTRUM_WIDGET_H

#include <QWidget>
#include "../QCustomPlot/qcustomplot.h"
namespace Ui {
class CommSpectrumWidget;
}

class CommSpectrumWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CommSpectrumWidget(QWidget *parent = nullptr);
    ~CommSpectrumWidget();

    void initAxis();
public slots:
    void updateView(std::shared_ptr<float> data,int len);
private:
    Ui::CommSpectrumWidget *ui;

    QCustomPlot * customPlot;
};

#endif // COMM_SPECTRUM_WIDGET_H
