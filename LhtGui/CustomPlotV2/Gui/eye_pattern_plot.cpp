﻿#include "eye_pattern_plot.h"
#include "ui_eye_pattern_plot.h"
#include "qDebug"
EyePatternPlot::EyePatternPlot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EyePatternPlot)
{
    ui->setupUi(this);

    customPlot = new QCustomPlot(ui->widget);
    ui->verticalLayout->addWidget(customPlot);

    connect(customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*, int, QMouseEvent*)), this, SLOT(OnPlotClick(QCPAbstractPlottable*, int, QMouseEvent*)));//关联选点信号
    TextTip = new QCPItemText(customPlot);
    TextTip->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
    TextTip->position->setType(QCPItemPosition::ptPlotCoords);
    QFont font;
    font.setPixelSize(15);
    TextTip->setFont(font); // make font a bit larger
    TextTip->setPen(QPen(Qt::black)); // show black border around text
    TextTip->setBrush(Qt::white);
    TextTip->setVisible(false);
}

EyePatternPlot::~EyePatternPlot()
{
    delete ui;
}

void EyePatternPlot::addEyePatternData(float *iData, float *qData, int len,int step)
{
    if(!m_isInit)
    {
        qDebug()<<"Star Map is uninit";
        return;
    }

    int count = len / step;

    for(int i = 0 ; i < count ; i++){
        QCPGraph * lines = customPlot->addGraph();
        lines->setSmooth(true); //启用曲线平滑
        lines->setPen(QPen(Qt::yellow)); // 曲线的颜色
        QVector<double> XData, YData;
        XData.reserve(step);
        YData.reserve(step);
        for(int j = 0 ; j < step ; j++){
            XData.append( (i * step + j));
            YData.append( (*(qData + i * step + j)));
        }
        lines->setData(XData, YData);
        QCPGraph * lines1 = customPlot->addGraph();
        lines1->setSmooth(true); //启用曲线平滑
        lines1->setPen(QPen(Qt::yellow)); // 曲线的颜色
        QVector<double> XData1, YData1;
        XData1.reserve(3);
        YData1.reserve(3);
        for(int j = 0 ; j < step ; j++){
            XData1.append( (j));
            YData1.append( (*(iData + i * step + j)));
        }
        lines1->setData(XData1, YData1);
    }
    customPlot->replot();
}

//选点处理函数
void EyePatternPlot::OnPlotClick(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *event)
{
    QCPGraph *graph = qobject_cast<QCPGraph*>(plottable);
    if (graph)
    {
        QVector<double> x ;
        QVector<double> y ;
        // 获取所有数据点
        for (auto it = graph->data()->constBegin(); it != graph->data()->constEnd(); ++it)
        {
            x.append(it->key);
            y.append(it->value);
        }
        // 显示被点击的点的文本提示
        if (dataIndex >= 0 && dataIndex < x.size())
        {
            QString text = "(" + QString::number(x[dataIndex]) + "," + QString::number(y[dataIndex]) + ")";
            qDebug() <<text;
            TextTip->setText(text);

            // 设置文本位置为数据点位置，减小偏移量
            TextTip->position->setCoords(x[dataIndex], y[dataIndex]);

            // 设置文本的对齐方式（居中于点的上方）
            TextTip->setPositionAlignment(Qt::AlignTop | Qt::AlignHCenter);
            // 确保 TextTip 显示在最顶层
            TextTip->setLayer("overlay");

            TextTip->setVisible(true);
        }
    }
}
void EyePatternPlot::init(int low)
{
    m_low = low;

    customPlot->setOpenGl(true);
    customPlot->addGraph();
    qDebug() << "opengl status:" << customPlot->openGl();

    customPlot->xAxis->setRange(0, low);
    customPlot->yAxis->setRange(0 - 1, low + 1);
    // 刻度显示
    customPlot->xAxis->setTicks(true);
    customPlot->yAxis->setTicks(true);
    // 刻度值显示
    customPlot->xAxis->setTickLabels(true);
    customPlot->yAxis->setTickLabels(true);
    // 网格显示
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->yAxis->grid()->setVisible(true);
    // 子网格显示
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    // 右和上坐标轴、刻度值显示
    customPlot->xAxis->setVisible(true);
    customPlot->yAxis->setVisible(true);
    customPlot->xAxis->setTicks(true);
    customPlot->yAxis->setTicks(true);
    customPlot->xAxis->setBasePen(QPen(Qt::white));
    customPlot->xAxis->setTickPen(QPen(Qt::black));
    customPlot->xAxis->setTickLabelColor(Qt::black);
    customPlot->yAxis->setBasePen(QPen(Qt::white));
    customPlot->yAxis->setTickPen(QPen(Qt::black));
    customPlot->yAxis->setTickLabelColor(Qt::black);
    customPlot->graph(0)->setSmooth(true); //启用曲线平滑
    auto m_coreData = customPlot->graph(0)->data()->coreData();
    // 可能需要预分配容器内存，预分配内存仅需一次
    m_coreData->reserve(4);
    m_coreData->resize(4);
    customPlot->axisRect()->setRangeZoom(Qt::Horizontal | Qt::Vertical); // 允许在X轴和Y轴上缩放
    customPlot->axisRect()->setRangeZoomAxes(customPlot->xAxis, customPlot->yAxis); // 设置可缩放的轴
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom| QCP::iSelectAxes |QCP::iSelectLegend | QCP::iSelectPlottables);
    m_isInit = 1;
}

void EyePatternPlot::test()
{
    // 设置星座图的坐标范围
    customPlot->xAxis->setRange(-1.0, 1.0);
    customPlot->yAxis->setRange(-1.0, 1.0);

    // 创建星座图数据
    QVector<QPointF> constellationPoints;
    constellationPoints << QPointF(-1.0, -1.0)
                        << QPointF(1.0, 1.0);/*
                        << QPointF(- 1.0, -1.0)
                        << QPointF(- 1.0, 1.0);*/

    // 绘制星座图的四个点并连接它们
    QCPGraph *constellationGraph = customPlot->graph();
    constellationGraph->setLineStyle(QCPGraph::lsLine);
    constellationGraph->setPen(QPen(Qt::black));
    QVector<double> xData, yData;
    for (const QPointF& point : constellationPoints) {
        xData.append(point.x());
        yData.append(point.y());
    }
    constellationGraph->setData(xData, yData);

    QVector<QPointF> constellationPoints2;
    constellationPoints2 << QPointF(-1.0, 1.0)
                         << QPointF(1.0, -1.0);
    QCPGraph *constellationGraph2 = customPlot->graph();
    constellationGraph2->setLineStyle(QCPGraph::lsLine);
    constellationGraph2->setPen(QPen(Qt::black));
    QVector<double> xData2, yData2;
    for (const QPointF& point : constellationPoints2) {
        xData2.append(point.x());
        yData2.append(point.y());
    }
    constellationGraph2->setData(xData2, yData2);

    // 添加更多均匀分布的相位点（QPSK相位点）
    QVector<QPointF> qpskPoints;
    int numQPSKPoints = 100; // 想要添加的QPSK相位点数量
    for (int i = 0; i < numQPSKPoints; ++i) {
        double randomI = (qrand() % 2000 - 1000) / 1000.0; // 在 -1.0 到 1.0 之间生成随机数
        double randomQ = (qrand() % 2000 - 1000) / 1000.0;
        qpskPoints << QPointF(randomI, randomQ);
    }

    QCPGraph *qpskGraph = customPlot->addGraph();
    qpskGraph->setLineStyle(QCPGraph::lsNone); // 不连接相位点
    qpskGraph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, Qt::red, Qt::white, 5)); // 设置相位点的样式
    QVector<double> qpskXData, qpskYData;
    for (const QPointF& point : qpskPoints) {
        qpskXData.append(point.x());
        qpskYData.append(point.y());
    }
    qpskGraph->setData(qpskXData, qpskYData);

    // 显示星座图的内切圆
    double radius = 1;
    QCPItemEllipse *outerCircle = new QCPItemEllipse(customPlot);
    outerCircle->topLeft->setCoords(-radius, radius);
    outerCircle->bottomRight->setCoords(radius, -radius);
    outerCircle->setPen(QPen(Qt::black));
    outerCircle->setBrush(QBrush(Qt::NoBrush)); // 透明填充

    //    customPlot->xAxis->setVisible(0);
    //    customPlot->yAxis->setVisible(0);
    customPlot->xAxis->setTicks(0);
    customPlot->yAxis->setTicks(0);
    customPlot->xAxis->setTickLabels(0);
    customPlot->yAxis->setTickLabels(0);
    customPlot->xAxis->setBasePen(QPen(Qt::black, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::black, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->setTickPen(QPen(Qt::black, 1)); // Y轴刻度线实线
    // 设置坐标轴刻度线的样式为实线
    customPlot->xAxis->grid()->setPen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->grid()->setPen(QPen(Qt::black, 1)); // Y轴刻度线实线
    customPlot->xAxis->grid()->setZeroLinePen(QPen(Qt::black, 1)); // X轴刻度线实线
    customPlot->yAxis->grid()->setZeroLinePen(QPen(Qt::black, 1)); // Y轴刻度线实线
    // 设置星座图的标题和轴标签
    customPlot->plotLayout()->insertRow(0);
    customPlot->plotLayout()->addElement(0, 0, new QCPTextElement(customPlot, "星座图", QFont("sans", 12, QFont::Bold)));
    customPlot->xAxis->setLabel("I");
    customPlot->yAxis->setLabel("Q");

    // 显示星座图
    customPlot->replot();

}
