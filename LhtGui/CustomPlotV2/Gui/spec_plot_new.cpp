#include "spec_plot_new.h"
#include "ui_spec_plot_new.h"

SpecPlotNew::SpecPlotNew(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::SpecPlotNew)
{
    ui->setupUi(this);
    m_specPlot = new CSpecPlotter(ui->widget);
    ui->verticalLayout->addWidget(m_specPlot);
    m_specPlot->setFftFill(true);
    m_specPlot->setPeakHold(true);
    quint32 fs = 1.92e6;
    m_specPlot->setSampleRate(fs);
    m_specPlot->setSpanFreq(fs);
    m_specPlot->setFftPlotColor(QColor(255, 255, 0));
    m_specPlot->setRunningState(true);
    m_specPlot->setFreqUnits(1000000);//MHz
    m_specPlot->setFilterBoxEnabled(true);
    m_specPlot->setHiLowCutFrequencies(0, 0);
    m_specPlot->setCenterFreq(265000000);
    m_specPlot->setDemodCenterFreq(265000000);
    m_specPlot->clearWaterfall();
    m_specPlot->clearMask();
    m_specPlot->setFftFill(0);

}

SpecPlotNew::~SpecPlotNew()
{
    delete ui;
}

void SpecPlotNew::addNewData(std::shared_ptr<float> data, int len)
{
    m_specPlot->NewFftData(data.get(),len,-130);
}
