#ifndef WATERFALL_PLOT_NEW_H
#define WATERFALL_PLOT_NEW_H

#include <QWidget>
#include <QMainWindow>
#include "../QCustomPlot/qcustomplot.h"
#include "waterfall_plot.h"
#include <QApplication>
#include <QMainWindow>
#include <QRandomGenerator>
#include <iostream>
#include "LhtIPPS/Ipps/Interface/computing_interface.h"

namespace Ui {
class WaterfallPlotNew;
}

class WaterfallPlotNew : public QWidget
{
    Q_OBJECT

public:
    explicit WaterfallPlotNew(QWidget *parent = nullptr);
    ~WaterfallPlotNew();

    void setDisplayType(DisplayTypes type){
        m_waterPlot->setDisplayType(type);
    }

    void addNewData(std::shared_ptr<float> data, int len);
private:
    Ui::WaterfallPlotNew *ui;
    CPlotter * m_waterPlot;
    int m_currentLen = 0;
    QVector<double> m_x;
    QVector<double> m_y;
};

#endif // WATERFALL_PLOT_NEW_H
