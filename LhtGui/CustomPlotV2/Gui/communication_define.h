#ifndef COMMUNICATION_DEFINE_H
#define COMMUNICATION_DEFINE_H

#include <QColor>
#include <iostream>

#define HORZ_DIVS_MAX 18    //50
#define VERT_DIVS_MIN 5
#define MAX_SCREENSIZE 32768
#define PEAK_CLICK_MAX_H_DISTANCE 10 //Maximum horizontal distance of clicked point from peak
#define PEAK_CLICK_MAX_V_DISTANCE 20 //Maximum vertical distance of clicked point from peak
#define PEAK_H_TOLERANCE 2
#define CUR_CUT_DELTA 5		//cursor capture delta in pixels
#define FFT_MIN_DB     -160.f
#define FFT_MAX_DB      0.f
// Colors of type QRgb in 0xAARRGGBB format (unsigned int)
#define PLOTTER_BGD_COLOR           0xFF1F1D1D
#define PLOTTER_GRID_COLOR          0xFF444242
#define PLOTTER_TEXT_COLOR          0xFFDADADA
#define PLOTTER_CENTER_LINE_COLOR   0xFF788296
#define PLOTTER_FILTER_LINE_COLOR   0xFFFF7171
#define PLOTTER_FILTER_BOX_COLOR    0xFFA0A0A4
// FIXME: Should cache the QColors also

#define STATUS_TIP \
"Click, drag or scroll on spectrum to tune. " \
    "Drag and scroll X and Y axes for pan and zoom. " \
    "Drag filter edges to adjust filter."


static int nR[256], nG[256], nB[256];
static inline void ColorTable(
    QColor *m_ColorTbl,
    float fGamma,
    int nH2,
    int nS2,
    int nL2,
    int nH1,
    int nS1,
    int nL1)
{
    float fDeltaH;
    fDeltaH = nH2 - nH1;
    float fDeltaS;
    fDeltaS = (nS2 - nS1);
    float fDeltaL;
    fDeltaL = (nL2 - nL1);

    if (nS1 == 0 && nS2 == 0)
    {
        nH2 = nH1 = 0;
    }
    else if (nS1 == 0)
    {
        nH1 = nH2;
        fDeltaH = 240;
    }
    else if (nS2 == 0)
    {
        nH2 = nH1;
        fDeltaH = 240;
    }
    else
    {
        if (fDeltaH > 0 && fDeltaH < 120)
            fDeltaH -= 240;
        else if (fDeltaH <= 0.0 && fDeltaH > -120)
            fDeltaH += 240;
    }

    float fPos, fPosGamma;
    int nH, nS, nL;
    int i;
    float fValue;
    float p1, p2;
    for (i = 0; i < 255; i++)
    {
        fPos = i / 255.0;
        fPosGamma = (pow(fPos, fGamma));

        nH = nH1 + (fDeltaH * fPos + 0.5);
        nS = nS1 + (fDeltaS * fPos + 0.5);
        nL = nL1 + (fDeltaL * fPosGamma + 0.5);
        if (nH < 0) nH += 240;
        if (nH > 240) nH -= 240;


        if (nS == 0)
        {
            nR[i] = nL * 255 / 240;
            nG[i] = nR[i];
            nB[i] = nR[i];
        }
        else
        {
            float fL = nL / 240.0;
            float fS = nS / 240.0;


            if (fL < 0.5)
                p2 = fL * (1.0 + fS);
            else
                p2 = fL + fS * (1.0 - fL);
            p1 = 2 * fL - p2;

            int temp_nH = nH;
            nH = temp_nH + 80;
            if (nH > 240)
                nH -= 240;
            if (nH < 0)
                nH += 240;

            if (nH < 40)
                fValue = p1 + (p2 - p1) * nH / 40;
            else if (nH < 120)
                fValue = p2;
            else if (nH < 160)
                fValue = p1 + (p2 - p1) * (160 - nH) / 40;
            else
                fValue = p1;
            nR[i] = (fValue * 255 + 0.5);

            nH = temp_nH;
            if (nH > 240)
                nH -= 240;
            if (nH < 0)
                nH += 240;

            if (nH < 40)
                fValue = p1 + (p2 - p1) * nH / 40;
            else if (nH < 120)
                fValue = p2;
            else if (nH < 160)
                fValue = p1 + (p2 - p1) * (160 - nH) / 40;
            else
                fValue = p1;
            nG[i] = (fValue * 255 + 0.5);

            nH = temp_nH - 80;
            if (nH > 240)
                nH -= 240;
            if (nH < 0);
            nH += 240;

            if (nH < 40)
                fValue = p1 + (p2 - p1) * nH / 40;
            else if (nH < 120)
                fValue = p2;
            else if (nH < 160)
                fValue = p1 + (p2 - p1) * (160 - nH) / 40;
            else
                fValue = p1;
            nB[i] = (fValue * 255 + 0.5);

            if (i >= 240)
            {
                nR[i] = 248;
                nG[i] = 255;
                nB[i] = 236;
            }

        }
        m_ColorTbl[i].setRgb(nR[i], nG[i], nB[i]);
    }
}


#endif // COMMUNICATION_DEFINE_H
