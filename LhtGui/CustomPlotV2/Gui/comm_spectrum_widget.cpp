#include "comm_spectrum_widget.h"
#include "ui_comm_spectrum_widget.h"

CommSpectrumWidget::CommSpectrumWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::CommSpectrumWidget)
{
    ui->setupUi(this);

    customPlot = new QCustomPlot(ui->widget);
    ui->verticalLayout->addWidget(customPlot);
    initAxis();
}

CommSpectrumWidget::~CommSpectrumWidget()
{
    delete ui;
}

void CommSpectrumWidget::initAxis()
{
    customPlot->setOpenGl(true);
    // add two new graphs and set their look:
    customPlot->addGraph();
    customPlot->xAxis->setRange(0, 16384);
    customPlot->yAxis->setRange(-150, 0);
    // 刻度显示
    customPlot->xAxis->setTicks(true);
    customPlot->yAxis->setTicks(true);
    // 刻度值显示
    customPlot->xAxis->setTickLabels(true);
    customPlot->yAxis->setTickLabels(true);
    // 网格显示
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->yAxis->grid()->setVisible(true);
    // 子网格显示
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    // 右和上坐标轴、刻度值显示
    customPlot->xAxis->setVisible(true);
    customPlot->yAxis->setVisible(true);
    customPlot->xAxis->setTicks(true);
    customPlot->yAxis->setTicks(true);

    customPlot->xAxis->setBasePen(QPen(Qt::white));
    customPlot->xAxis->setTickPen(QPen(Qt::black));
    customPlot->xAxis->setTickLabelColor(Qt::black);

    customPlot->yAxis->setBasePen(QPen(Qt::white));
    customPlot->yAxis->setTickPen(QPen(Qt::black));
    customPlot->yAxis->setTickLabelColor(Qt::black);
    customPlot->graph(0)->setSmooth(true); //启用曲线平滑

    customPlot->axisRect()->setRangeZoom(Qt::Horizontal | Qt::Vertical); // 允许在X轴和Y轴上缩放
    customPlot->axisRect()->setRangeZoomAxes(customPlot->xAxis, customPlot->yAxis); // 设置可缩放的轴
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom| QCP::iSelectAxes |QCP::iSelectLegend | QCP::iSelectPlottables);
}

void CommSpectrumWidget::updateView(std::shared_ptr<float> data, int len)
{
    QVector<double> x(len), y0(len);
    for (int i=0; i<len; ++i)
    {
        x[i] = i;
        y0[i] = data.get()[i];
    }
    customPlot->graph(0)->setData(x, y0);
    customPlot->replot();
}
