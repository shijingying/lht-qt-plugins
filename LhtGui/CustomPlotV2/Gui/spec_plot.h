﻿/* -*- c++ -*- */
#ifndef SPEC_PLOTTER_H
#define SPEC_PLOTTER_H

#include <QtGui>
#include <QFont>
#include <QFrame>
#include <QImage>
#include <vector>
#include <QMap>
#include <QPoint>
#include <QFuture>
#include "communication_define.h"
#include "bookmarks.h"

class CSpecPlotter : public QFrame
{
    Q_OBJECT

public:
    explicit CSpecPlotter(QWidget* parent = 0);
    ~CSpecPlotter();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    //void SetSdrInterface(CSdrInterface* ptr){m_pSdrInterface = ptr;}
    void draw();		//call to draw new fft data onto screen plot
    void setRunningState(bool running) { m_Running = running; }
    void setClickResolution(int clickres) { m_ClickResolution = clickres; }
    void setFilterClickResolution(int clickres) { m_FilterClickResolution = clickres; }
    void setFilterBoxEnabled(bool enabled) { m_FilterBoxEnabled = enabled; }
    void setCenterLineEnabled(bool enabled) { m_CenterLineEnabled = enabled; }
    void setTooltipsEnabled(bool enabled) { m_TooltipsEnabled = enabled; }
    void setBookmarksEnabled(bool enabled) { m_BookmarksEnabled = enabled; }

    void setNewFftData(float* fftData, int size);
    void setNewFftData(float* fftData, float* wfData, int size);

    void setCenterFreq(quint64 f);
    void setFreqUnits(qint32 unit) { m_FreqUnits = unit; }

    void setReferenceLevel(double level) {
        m_referenceLevel = level;
        qDebug() << "m_referenceLevel:" << m_referenceLevel << endl;
        drawOverlay();
    }

    void setPeak(int sw)
    {
        m_peak = sw;
    }
    void setDemodCenterFreq(quint64 f) {
        m_DemodCenterFreq = f;
        f_min = m_CenterFreq - m_SampleFreq / 2.f;
    }

    /*! \brief Move the filter to freq_hz from center. */
    void setFilterOffset(qint64 freq_hz)
    {
        m_DemodCenterFreq = m_CenterFreq + freq_hz;
        drawOverlay();
    }
    qint64 getFilterOffset(void)
    {
        return m_DemodCenterFreq - m_CenterFreq;
    }

    int getFilterBw()
    {
        return m_DemodHiCutFreq - m_DemodLowCutFreq;
    }

    void setHiLowCutFrequencies(int LowCut, int HiCut)
    {
        m_DemodLowCutFreq = LowCut;
        m_DemodHiCutFreq = HiCut;
        drawOverlay();
    }

    void getHiLowCutFrequencies(int* LowCut, int* HiCut)
    {
        *LowCut = m_DemodLowCutFreq;
        *HiCut = m_DemodHiCutFreq;
    }

    void setDemodRanges(int FLowCmin, int FLowCmax, int FHiCmin, int FHiCmax, bool symetric);

    /* Shown bandwidth around SetCenterFreq() */
    void setSpanFreq(quint32 s)
    {
        if (s > 0 && s < INT_MAX) {
            m_Span = (qint32)s;
            setFftCenterFreq(m_FftCenter);
        }
        drawOverlay();
    }

    void setMaxBandiwdth(int m) { m_FLowCmin = -m / 2; m_FHiCmax = m / 2; }


    void setHdivDelta(int delta) { m_HdivDelta = delta; }
    void setVdivDelta(int delta) { m_VdivDelta = delta; }

    void setFreqDigits(int digits) { m_FreqDigits = digits >= 0 ? digits : 0; }

    /* Determines full bandwidth. */
    void setSampleRate(float rate)
    {
        if (rate > 0.0)
        {
            m_SampleFreq = rate;
            drawOverlay();
        }
    }

    float getSampleRate(void)
    {
        return m_SampleFreq;
    }

    void setFftCenterFreq(qint64 f) {
        qint64 limit = ((qint64)m_SampleFreq + m_Span) / 2 - 1;
        m_FftCenter = qBound(-limit, f, limit);
        //m_drawMutex.lock();
        drawOverlay();
        //m_drawMutex.unlock();
    }

    int     getNearestPeak(QPoint pt);
    void    setWaterfallSpan(quint64 span_ms);
    quint64 getWfTimeRes(void);
    void    setFftRate(int rate_hz);
    void    clearWaterfall(void);
    bool    saveWaterfall(const QString& filename) const;


Q_SIGNALS:
    void newCenterFreq(qint64 f);
    void newDemodFreq(qint64 freq, qint64 delta); /* delta is the offset from the center */
    void newLowCutFreq(int f);
    void newHighCutFreq(int f);
    void newFilterFreq(int low, int high);  /* substitute for NewLow / NewHigh */
    void pandapterRangeChanged(float min, float max);
    void newZoomLevel(float level);
    void signBoxSelectDemo(int route, QString freq, QString band);

public Q_SLOTS:
    // zoom functions
    void resetHorizontalZoom(void);
    void resetVericalZoom();
    void moveToCenterFreq(void);
    void moveToDemodFreq(void);
    void zoomOnXAxis(float level);

    // other FFT Q_SLOTS
    void setFftPlotColor(const QColor color);
    void setFftFill(bool enabled);
    void setPeakHold(bool enabled);
    void setFftRange(float min, float max);
    void setPandapterRange(float min, float max);
    void setWaterfallRange(float min, float max);
    void setPeakDetection(bool enabled, float c);
    void updateOverlay();
    void NewFftData(float* fftData, int size, float);
    void NewFftData1();
    void setPercent2DScreen(int percent)
    {
        m_Percent2DScreen = percent;
        m_Size = QSize(0, 0);
        resizeEvent(NULL);
    }
    void recelevel(float l);
protected:
    //re-implemented widget event handlers
    void paintEvent(QPaintEvent* event);
    void resizeEvent(QResizeEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);
    void keyPressEvent(QKeyEvent* event);
    void contextMenuEvent(QContextMenuEvent* event);


private:
    enum eCapturetype {
        NOCAP,
        LEFT,
        CENTER,
        RIGHT,
        YAXIS,
        XAXIS,
        BOOKMARK,
        NB
    };

    void        drawOverlay();
    void        makeFrequencyStrs();
    int         xFromFreq(qint64 freq);
    qint64      freqFromX(int x);
    qint64      fftDataFromX(int x);
    void        zoomStepX(float factor, int x);
    qint64      roundFreq(qint64 freq, int resolution);
    quint64     msecFromY(int y);
    void        clampDemodParameters();
    bool        isPointCloseTo(int x, int xr, int delta)
    {
        return ((x > (xr - delta)) && (x < (xr + delta)));
    }
    void getScreenIntegerFFTData(qint32 plotHeight, qint32 plotWidth,
        float maxdB, float mindB,
        qint64 startFreq, qint64 stopFreq,
        float* inBuf, qint32* outBuf,
        qint32* maxbin, qint32* minbin);
    void calcDivSize(qint64 low, qint64 high, int divswanted, qint64& adjlow, qint64& step, int& divs);

    void DrawSpectrum();

    bool        m_PeakHoldActive;
    bool        m_PeakHoldValid;
    qint32      m_fftbuf[MAX_SCREENSIZE];

    quint8      m_wfbuf[MAX_SCREENSIZE]; // used for accumulating waterfall data at high time spans
    qint32      m_fftPeakHoldBuf[MAX_SCREENSIZE];
    float* m_fftData = nullptr;     /*! pointer to incoming FFT data */
    float* m_wfData;
    float      m_tempwfData[16384];
    int        m_fft_size;
    int         m_fftDataSize;
    int m_refenceLevelHeight = 0;

    eCapturetype    m_CursorCaptured;
    QPixmap     m_2DPixmap;
    QPixmap     m_OverlayPixmap;     //频谱
    QPixmap     m_OverlayPixmap2;
    QPixmap     m_WaterfallPixmap;
    QPixmap     m_xAxisPixmap;
    QColor      m_ColorTbl[256];
    QSize       m_Size;
    QString     m_Str;
    QStringList m_HDivText;
    bool        m_Running;
    bool        m_DrawOverlay;
    qint64      m_CenterFreq;       // The HW frequency
    qint64      m_FftCenter;        // Center freq in the -span ... +span range
    qint64      m_DemodCenterFreq;
    qint64      m_StartFreqAdj;
    qint64      m_FreqPerDiv;
    bool        m_CenterLineEnabled;  /*!< Distinguish center line. */
    bool        m_FilterBoxEnabled = 0;   /*!< Draw filter box. */
    bool        m_TooltipsEnabled;     /*!< Tooltips enabled */
    bool        m_BookmarksEnabled;   /*!< Show/hide bookmarks on spectrum */
    int         m_DemodHiCutFreq;
    int         m_DemodLowCutFreq;
    int         m_DemodFreqX;		//screen coordinate x position
    int         m_DemodHiCutFreqX;	//screen coordinate x position
    int         m_DemodLowCutFreqX;	//screen coordinate x position
    int         m_CursorCaptureDelta;
    int         m_GrabPosition;
    int         m_Percent2DScreen;

    int         m_FLowCmin;
    int         m_FLowCmax;
    int         m_FHiCmin;
    int         m_FHiCmax;
    bool        m_symetric;

    int         m_HorDivs;   /*!< Current number of horizontal divisions. Calculated from width. */
    int         m_VerDivs;   /*!< Current number of vertical divisions. Calculated from height. */

    float       m_PandMindB;
    float       m_PandMaxdB;
    float       m_WfMindB;
    float       m_WfMaxdB;

    qint64      m_Span;
    float       m_SampleFreq;    /*!< Sample rate. */
    qint32      m_FreqUnits;
    int         m_ClickResolution;
    int         m_FilterClickResolution;
    bool        m_bRButtonDown;
    int         m_nRButtonDownStart;
    int         m_nRButtonDownStop;

    int         m_Xzero;
    int         m_Yzero;  /*!< Used to measure mouse drag direction. */
    int         m_FreqDigits;  /*!< Number of decimal digits in frequency strings. */

    QFont       m_Font;         /*!< Font used for plotter (system font) */
    int         m_HdivDelta; /*!< Minimum distance in pixels between two horizontal grid lines (vertical division). */
    int         m_VdivDelta; /*!< Minimum distance in pixels between two vertical grid lines (horizontal division). */

    quint32     m_LastSampleRate;

    QColor      m_FftColor, m_FftFillCol, m_PeakHoldColor;
    bool        m_FftFill;

    float       m_PeakDetection;
    QMap<int, int>   m_Peaks;

    QList< QPair<QRect, qint64> >     m_BookmarkTags;

    // Waterfall averaging
    quint64     tlast_wf_ms;        // last time waterfall has been updated
    quint64     msec_per_wfline;    // milliseconds between waterfall updates
    quint64     wf_span;            // waterfall span in milliseconds (0 = auto)
    int         fft_rate;           // expected FFT rate (needed when WF span is auto)
    QMutex      m_mutex;
    float		m_meanFft;
    float level = -120;
    int tempindex = 8192;
    QTimer  m_timer;
    float zoomstart = 0;
    float zoomend = 0;
    bool m_selectFreq;
    QMutex m_drawMutex;

    QWidget* transparent = nullptr;
    float transparentStart = 0;

    QWidget* transparentY = nullptr;
    float transparentStartY = 0;

    QTimer* updateTimer;
    float m_factor = 1;

    float m_currentPtx = 0;
    float f_min;


    int         m_XAxisYCenter;
    int         m_YAxisWidth;

    int m_xAxisHeight = 30;
    int m_spectrumHeight = 100;
    int m_waterfallHeight = 100;
    QPoint m_lastPoint;

    int m_startX = 0;
    int m_endX = 0;
    int m_startY = 0;
    int m_endY = 0;
    bool m_isBoxSelect = false;
    bool m_isRelease = true;

    double m_referenceLevel;
    int      m_peak = 0;
    int flag = true;
    double m_fftDataSpectrum = 0;
    Bookmarks* m_pThis;

public:
    qint64      SelectFreq;

private:
    QPoint selectStart;
};

#endif // PLOTTER_H
