#pragma once

#include <QWidget>
#include <QMainWindow>
#include "../QCustomPlot/qcustomplot.h"
#include <QApplication>
#include <QMainWindow>
#include <QRandomGenerator>
#include "LhtIPPS/Ipps/Interface/computing_interface.h"

namespace Ui {
class StarMapPlot;
}

class StarMapPlot : public QWidget
{
    Q_OBJECT

public:
    explicit StarMapPlot(QWidget *parent = nullptr);
    ~StarMapPlot();

    void addData(float* iData,float *qData,int len);
    /**
    * @author lee
    * @param low 设置坐标值 结果为  -low 到  low
    */
    void init(int low );
    void test();
private:
    Ui::StarMapPlot *ui;
    int m_low;
    bool m_isInit = false;
    QCustomPlot * customPlot;
};

