#ifndef SPEC_PLOT_NEW_H
#define SPEC_PLOT_NEW_H

#include <QWidget>
#include <QMainWindow>
#include "../QCustomPlot/qcustomplot.h"
#include "spec_plot.h"
#include <QApplication>
#include <QMainWindow>
#include <QRandomGenerator>
#include <iostream>
#include "LhtIPPS/Ipps/Interface/computing_interface.h"

namespace Ui {
class SpecPlotNew;
}

class SpecPlotNew : public QWidget
{
    Q_OBJECT

public:
    explicit SpecPlotNew(QWidget *parent = nullptr);
    ~SpecPlotNew();

    void addNewData(std::shared_ptr<float> data, int len);
private:
    Ui::SpecPlotNew *ui;
    CSpecPlotter * m_specPlot;
    int m_currentLen = 0;
    QVector<double> m_x;
    QVector<double> m_y;
};

#endif // WATERFALL_PLOT_NEW_H
