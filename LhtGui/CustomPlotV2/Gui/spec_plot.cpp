﻿#include <cmath>

#ifndef _MSC_VER
#include <sys/time.h>
#else
#include <Windows.h>
#include <cstdint>
#include <QDateTime>
#include <winsock.h>
#include <QMenu>
#include <future>
#include "spec_plot.h"
#include <QtConcurrent>
#endif

#include <QColor>
#include <QDateTime>
#include <QDebug>
#include <QFont>
#include <QPainter>
#include <QtGlobal>
#include <QToolTip>


static inline bool val_is_out_of_range(float val, float min, float max)
{
    return (val < min || val > max);
}

static inline bool out_of_range(float min, float max)
{
    return (val_is_out_of_range(min, FFT_MIN_DB, FFT_MAX_DB) ||
        val_is_out_of_range(max, FFT_MIN_DB, FFT_MAX_DB) ||
        max < min + 10.f);
}

/** Current time in milliseconds since Epoch */
static inline quint64 time_ms(void)
{
    return QDateTime::currentMSecsSinceEpoch();
}

CSpecPlotter::CSpecPlotter(QWidget* parent) : QFrame(parent)
{
    m_pThis = new Bookmarks;
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setFocusPolicy(Qt::StrongFocus);
    setAttribute(Qt::WA_PaintOnScreen, false);
    setAutoFillBackground(false);
    setAttribute(Qt::WA_OpaquePaintEvent, false);
    setAttribute(Qt::WA_NoSystemBackground, true);
    setMouseTracking(true);

    setTooltipsEnabled(true);

    ColorTable(m_ColorTbl,1.8, 56, 240, 231, 138, 221, 12);
    m_PeakHoldActive = false;
    m_PeakHoldValid = false;

    m_FftCenter = 0;
    //m_CenterFreq = DEVCFG->specFreq();
    //m_DemodCenterFreq = DEVCFG->specFreq();
    m_DemodHiCutFreq = 5000;
    m_DemodLowCutFreq = -5000;

    m_FLowCmin = -2500000;
    m_FLowCmax = -1000;
    m_FHiCmin = 1000;
    m_FHiCmax = 2500000;
    m_symetric = true;

    m_ClickResolution = 100;
    m_FilterClickResolution = 100;
    m_CursorCaptureDelta = CUR_CUT_DELTA;

    m_FilterBoxEnabled = true;
    m_CenterLineEnabled = true;
    m_BookmarksEnabled = true;

    m_Span = 96000;
    m_SampleFreq = 96000;

    m_HorDivs = 18;
    m_VerDivs = 6;
    m_PandMaxdB = m_WfMaxdB = 0.f;
    m_PandMindB = m_WfMindB = -175.f;

    m_FreqUnits = 1000000;
    m_CursorCaptured = NOCAP;
    m_Running = false;
    m_DrawOverlay = true;

    m_2DPixmap = QPixmap(0, 0);
    m_OverlayPixmap = QPixmap(0, 0);
    m_OverlayPixmap2 = QPixmap(0, 0);
    m_WaterfallPixmap = QPixmap(0, 0);
    m_xAxisPixmap = QPixmap(0, 0);

    m_Size = QSize(0, 0);
    m_GrabPosition = 0;
    m_Percent2DScreen = 50;	//percent of screen used for 2D display
    m_VdivDelta = 30;
    m_HdivDelta = 70;

    m_FreqDigits = 3;
    m_bRButtonDown = false;
    m_nRButtonDownStart = -1;
    m_nRButtonDownStop = -1;

    m_Peaks = QMap<int, int>();
    //setPeakDetection(false, 2);
    setPeakDetection(true, 2);
    m_PeakHoldValid = false;

    setFftPlotColor(QColor(0xFF, 0xFF, 0xFF, 0xFF));
    setFftFill(false);

    // always update waterfall
    tlast_wf_ms = 0;
    msec_per_wfline = 0;
    wf_span = 0;
    fft_rate = 15;
    m_wfData = new float[MAX_SCREENSIZE];
    m_fftData = new float[MAX_SCREENSIZE];
    memset(m_wfbuf, 255, MAX_SCREENSIZE);
    memset(m_tempwfData, 0, 16384);

    updateTimer = new QTimer(this);
    connect(updateTimer, &QTimer::timeout, [=]
        {
            m_drawMutex.lock();
            repaint();
            m_drawMutex.unlock();
        });

    updateTimer->start(50);

    transparentY = new QWidget(this);
    transparentY->setStyleSheet("background-color:rgba(255,255,255,60%);");
    transparentY->hide();

}

CSpecPlotter::~CSpecPlotter()
{

    
}

QSize CSpecPlotter::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize CSpecPlotter::sizeHint() const
{
    return QSize(180, 180);
}


void CSpecPlotter::mouseMoveEvent(QMouseEvent* event)
{
    QPoint pt = event->pos();


    if (!m_isRelease && (pt.x() < m_OverlayPixmap.width() - m_YAxisWidth))
    {
        m_endX = pt.x();
        m_endY = pt.y();
    }


    if (pt.y() > (m_Size.height() - m_xAxisHeight)) {
        if (XAXIS != m_CursorCaptured)
            setCursor(QCursor(Qt::OpenHandCursor));
        m_CursorCaptured = XAXIS;
        if (m_TooltipsEnabled)
            QToolTip::hideText();
    }

    /* mouse ent er / mouse leave events */
    else if (m_OverlayPixmap.rect().contains(pt))
    {
        //is in Overlay bitmap region
        if (event->buttons() == Qt::NoButton)
        {
            bool onTag = false;
            if (pt.y() < 15 * 10) // FIXME
            {
                for (int i = 0; i < m_BookmarkTags.size() && !onTag; i++)
                {
                    if (m_BookmarkTags[i].first.contains(event->pos()))
                        onTag = true;
                }
            }
            // if no mouse button monitor grab regions and change cursor icon
            if (onTag)
            {
                setCursor(QCursor(Qt::PointingHandCursor));
                m_CursorCaptured = BOOKMARK;
            }
            else if (isPointCloseTo(pt.x(), m_DemodFreqX, m_CursorCaptureDelta))
            {
                // in move demod box center frequency region
                if (CENTER != m_CursorCaptured)
                    setCursor(QCursor(Qt::SizeHorCursor));
                m_CursorCaptured = CENTER;
                if (m_TooltipsEnabled)
                    QToolTip::showText(event->globalPos(),
                        QString("Demod: %1 kHz")
                        .arg(m_DemodCenterFreq / 1.e3f, 0, 'f', 3),
                        this);
            }
            else if (isPointCloseTo(pt.x(), m_DemodHiCutFreqX, m_CursorCaptureDelta))
            {
                // in move demod hicut region
                if (RIGHT != m_CursorCaptured)
                    setCursor(QCursor(Qt::SizeFDiagCursor));
                m_CursorCaptured = RIGHT;
                if (m_TooltipsEnabled)
                    QToolTip::showText(event->globalPos(),
                        QString("High cut: %1 Hz")
                        .arg(m_DemodHiCutFreq),
                        this);
            }
            else if (isPointCloseTo(pt.x(), m_DemodLowCutFreqX, m_CursorCaptureDelta))
            {
                // in move demod lowcut region
                if (LEFT != m_CursorCaptured)
                    setCursor(QCursor(Qt::SizeBDiagCursor));
                m_CursorCaptured = LEFT;
                if (m_TooltipsEnabled)
                    QToolTip::showText(event->globalPos(),
                        QString("Low cut: %1 Hz")
                        .arg(m_DemodLowCutFreq),
                        this);
            }
            //

            else if (isPointCloseTo(pt.x(), this->width() - m_YAxisWidth / 2, m_YAxisWidth / 2))
            {
                if (YAXIS != m_CursorCaptured)
                    setCursor(QCursor(Qt::OpenHandCursor));
                m_CursorCaptured = YAXIS;
                if (m_TooltipsEnabled)
                    QToolTip::hideText();
            }
            else if (isPointCloseTo(pt.y(), m_XAxisYCenter, m_CursorCaptureDelta + 5))
            {
                if (XAXIS != m_CursorCaptured)
                    setCursor(QCursor(Qt::OpenHandCursor));
                m_CursorCaptured = XAXIS;
                if (m_TooltipsEnabled)
                    QToolTip::hideText();
            }
            else
            {	//if not near any grab boundaries
                if (NOCAP != m_CursorCaptured)
                {
                    setCursor(QCursor(Qt::ArrowCursor));
                    m_CursorCaptured = NB;
                }
                if (m_TooltipsEnabled)
                {
                    QToolTip::showText(event->globalPos(),
                        QString("F: %1 kHz")
                        .arg(freqFromX(pt.x()) / 1.e3f, 0, 'f', 3),
                        this);
                    /*SelectFreq = freqFromX(pt.x());*/
                }

            }
            m_GrabPosition = 0;
        }
    }
    else
    {
        // not in Overlay region
        if (event->buttons() == Qt::NoButton)
        {
            bool onTag = false;
            if (pt.y() < 15 * 10) // FIXME
            {
                for (int i = 0; i < m_BookmarkTags.size() && !onTag; i++)
                {
                    if (m_BookmarkTags[i].first.contains(event->pos()))
                        onTag = true;
                }
            }
            // if no mouse button monitor grab regions and change cursor icon
            if (onTag)
            {
                setCursor(QCursor(Qt::PointingHandCursor));
                m_CursorCaptured = BOOKMARK;
            }
            else if (isPointCloseTo(pt.x(), m_DemodFreqX, m_CursorCaptureDelta))
            {
                // in move demod box center frequency region
                if (CENTER != m_CursorCaptured)
                    setCursor(QCursor(Qt::SizeHorCursor));
                m_CursorCaptured = CENTER;
                if (m_TooltipsEnabled)
                    QToolTip::showText(event->globalPos(),
                        QString("Demod: %1 kHz")
                        .arg(m_DemodCenterFreq / 1.e3f, 0, 'f', 3),
                        this);
            }
            else if (isPointCloseTo(pt.x(), m_DemodHiCutFreqX, m_CursorCaptureDelta))
            {
                // in move demod hicut region
                if (RIGHT != m_CursorCaptured)
                    setCursor(QCursor(Qt::SizeFDiagCursor));
                m_CursorCaptured = RIGHT;
                if (m_TooltipsEnabled)
                    QToolTip::showText(event->globalPos(),
                        QString("High cut: %1 Hz")
                        .arg(m_DemodHiCutFreq),
                        this);
            }
            else if (isPointCloseTo(pt.x(), m_DemodLowCutFreqX, m_CursorCaptureDelta))
            {
                // in move demod lowcut region
                if (LEFT != m_CursorCaptured)
                    setCursor(QCursor(Qt::SizeBDiagCursor));
                m_CursorCaptured = LEFT;
                if (m_TooltipsEnabled)
                    QToolTip::showText(event->globalPos(),
                        QString("Low cut: %1 Hz")
                        .arg(m_DemodLowCutFreq),
                        this);
            }
            else if (isPointCloseTo(pt.x(), this->width() - m_YAxisWidth / 2, m_YAxisWidth / 2))
            {
                if (YAXIS != m_CursorCaptured)
                    setCursor(QCursor(Qt::OpenHandCursor));
                m_CursorCaptured = YAXIS;
                if (m_TooltipsEnabled)
                    QToolTip::hideText();
            }
            else if (isPointCloseTo(pt.y(), m_XAxisYCenter, m_CursorCaptureDelta + 10))
            {
                if (XAXIS != m_CursorCaptured)
                    setCursor(QCursor(Qt::OpenHandCursor));
                m_CursorCaptured = XAXIS;
                if (m_TooltipsEnabled)
                    QToolTip::hideText();
            }
            else
            {	//if not near any grab boundaries
                if (NOCAP != m_CursorCaptured)
                {
                    setCursor(QCursor(Qt::ArrowCursor));
                    m_CursorCaptured = NB;
                }
                if (m_TooltipsEnabled)
                {
                    QDateTime tt;
                    tt.setMSecsSinceEpoch(msecFromY(pt.y()));

                    QToolTip::showText(event->globalPos(),
                        QString("%1\n%2 kHz\n%3 dB")
                        .arg(tt.toString("yyyy.MM.dd hh:mm:ss.zzz"))
                        .arg(freqFromX(pt.x()) / 1.e3f, 0, 'f', 3)
                        .arg(m_fftDataSpectrum / 1.0, 0, 'f', 3),
                        this);
                }
            }
            m_GrabPosition = 0;
        }

    }

    int height = pt.y() - m_WaterfallPixmap.height();

    // process mouse moves while in cursor capture modes
    if (isPointCloseTo(pt.x(), this->width() - m_YAxisWidth / 2, m_YAxisWidth / 2) && (height < m_OverlayPixmap.height()))
    {
        if (event->buttons() & Qt::LeftButton)
        {
            setCursor(QCursor(Qt::ClosedHandCursor));
            // move Y scale up/down
            float delta_px = m_Yzero - pt.y();
            float delta_db = delta_px * fabs(m_PandMindB - m_PandMaxdB) /
                (float)m_OverlayPixmap.height();
            m_PandMindB -= delta_db;
            m_PandMaxdB -= delta_db;
            if (out_of_range(m_PandMindB, m_PandMaxdB))
            {
                m_PandMindB += delta_db;
                m_PandMaxdB += delta_db;
            }
            else {
                setFftCenterFreq(m_FftCenter + delta_db);
                updateOverlay();
                m_PeakHoldValid = false;
                m_Yzero = pt.y();
            }
        }
        if (event->buttons() & Qt::RightButton)
        {
            transparentY->setGeometry((float)m_OverlayPixmap.width() - 35, transparentStartY, (float)m_OverlayPixmap.width(), pt.y() - transparentStartY);
            transparentY->show();
        }
    }
    else if (XAXIS == m_CursorCaptured)
    {
        if (event->buttons() & (Qt::LeftButton | Qt::MidButton))
        {
            setCursor(QCursor(Qt::ClosedHandCursor));
            // pan viewable range or move center frequency
            int delta_px = m_Xzero - pt.x();
            qint64 delta_hz = delta_px * m_Span / m_OverlayPixmap.width();
            if (event->buttons() & Qt::MidButton)
            {
                m_CenterFreq += delta_hz;
                m_DemodCenterFreq += delta_hz;
                emit newCenterFreq(m_CenterFreq);
            }
            else
            {
                setFftCenterFreq(m_FftCenter + delta_hz);
            }
            updateOverlay();

            m_PeakHoldValid = false;

            m_Xzero = pt.x();
        }
        else if (event->buttons() & Qt::RightButton)
        {

            setCursor(QCursor(Qt::SplitHCursor));
            if (m_bRButtonDown)
            {
                m_nRButtonDownStop = pt.x();
            }
            transparent->setGeometry(transparentStart, 0, pt.x() - transparentStart, (float)this->height());
            transparent->show();
        }
    }
    else if (LEFT == m_CursorCaptured)
    {
        // moving in demod lowcut region
        if (event->buttons() & (Qt::LeftButton | Qt::RightButton))
        {
            // moving in demod lowcut region with left button held
            if (m_GrabPosition != 0)
            {
                m_DemodLowCutFreq = freqFromX(pt.x() - m_GrabPosition) - m_DemodCenterFreq;
                m_DemodLowCutFreq = roundFreq(m_DemodLowCutFreq, m_FilterClickResolution);

                if (m_symetric && (event->buttons() & Qt::LeftButton))  // symetric adjustment
                {
                    m_DemodHiCutFreq = -m_DemodLowCutFreq;
                }
                clampDemodParameters();

                emit newFilterFreq(m_DemodLowCutFreq, m_DemodHiCutFreq);
                if (m_Running)
                    m_DrawOverlay = true;
                else
                    drawOverlay();
            }
            else
            {
                // save initial grab postion from m_DemodFreqX
                m_GrabPosition = pt.x() - m_DemodLowCutFreqX;
            }
        }
        else if (event->buttons() & ~Qt::NoButton)
        {
            setCursor(QCursor(Qt::ArrowCursor));
            m_CursorCaptured = NOCAP;
        }
    }
    else if (RIGHT == m_CursorCaptured)
    {
        // moving in demod highcut region
        if (event->buttons() & (Qt::LeftButton | Qt::RightButton))
        {
            // moving in demod highcut region with right button held
            if (m_GrabPosition != 0)
            {
                m_DemodHiCutFreq = freqFromX(pt.x() - m_GrabPosition) - m_DemodCenterFreq;
                m_DemodHiCutFreq = roundFreq(m_DemodHiCutFreq, m_FilterClickResolution);

                if (m_symetric && (event->buttons() & Qt::LeftButton)) // symetric adjustment
                {
                    m_DemodLowCutFreq = -m_DemodHiCutFreq;
                }
                clampDemodParameters();

                emit newFilterFreq(m_DemodLowCutFreq, m_DemodHiCutFreq);
                updateOverlay();
            }
            else
            {
                // save initial grab postion from m_DemodFreqX
                m_GrabPosition = pt.x() - m_DemodHiCutFreqX;
            }
        }
        else if (event->buttons() & ~Qt::NoButton)
        {
            setCursor(QCursor(Qt::ArrowCursor));
            m_CursorCaptured = NOCAP;
        }
    }
    else if (CENTER == m_CursorCaptured)
    {
        // moving inbetween demod lowcut and highcut region
        if (event->buttons() & Qt::LeftButton)
        {   // moving inbetween demod lowcut and highcut region with left button held
            if (m_GrabPosition != 0)
            {
                m_DemodCenterFreq = roundFreq(freqFromX(pt.x() - m_GrabPosition),
                    m_ClickResolution);
                emit newDemodFreq(m_DemodCenterFreq,
                    m_DemodCenterFreq - m_CenterFreq);
                updateOverlay();
                m_PeakHoldValid = false;
            }
            else
            {
                // save initial grab postion from m_DemodFreqX
                m_GrabPosition = pt.x() - m_DemodFreqX;
            }
        }
        else if (event->buttons() & ~Qt::NoButton)
        {
            setCursor(QCursor(Qt::ArrowCursor));
            m_CursorCaptured = NOCAP;
        }
    }
    else
    {
        // cursor not captured
        m_GrabPosition = 0;
    }
    if (!this->rect().contains(pt))
    {
        if (NOCAP != m_CursorCaptured)
            setCursor(QCursor(Qt::ArrowCursor));
        m_CursorCaptured = NOCAP;
    }
}


int CSpecPlotter::getNearestPeak(QPoint pt)
{
    QMap<int, int>::const_iterator i = m_Peaks.lowerBound(pt.x() - PEAK_CLICK_MAX_H_DISTANCE);
    QMap<int, int>::const_iterator upperBound = m_Peaks.upperBound(pt.x() + PEAK_CLICK_MAX_H_DISTANCE);
    float   dist = 1.0e10;
    int     best = -1;

    for (; i != upperBound; i++)
    {
        int x = i.key();
        int y = i.value();

        if (abs(y - pt.y()) > PEAK_CLICK_MAX_V_DISTANCE)
            continue;

        float d = powf(y - pt.y(), 2) + powf(x - pt.x(), 2);
        if (d < dist)
        {
            dist = d;
            best = x;
        }
    }

    return best;
}

/** Set waterfall span in milliseconds */
void CSpecPlotter::setWaterfallSpan(quint64 span_ms)
{
    wf_span = span_ms;
    msec_per_wfline = wf_span / m_WaterfallPixmap.height();
    clearWaterfall();
}

void CSpecPlotter::clearWaterfall()
{
    m_WaterfallPixmap.fill(Qt::black);
    memset(m_wfbuf, 255, MAX_SCREENSIZE);
}

/**
 * @brief Save waterfall to a graphics file
 * @param filename
 * @return TRUE if the save successful, FALSE if an erorr occurred.
 *
 * We assume that frequency strings are up to date
 */
bool CSpecPlotter::saveWaterfall(const QString& filename) const
{
    QBrush          axis_brush(QColor(0x00, 0x00, 0x00, 0x70), Qt::SolidPattern);
    QPixmap         pixmap(m_WaterfallPixmap);
    QPainter        painter(&pixmap);
    QRect           rect;
    QDateTime       tt;
    QFont           font("sans-serif");
    QFontMetrics    font_metrics(font);
    float           pixperdiv;
    int             x, y, w, h;
    int             hxa, wya = 85;
    int             i;

    w = pixmap.width();
    h = pixmap.height();
    hxa = font_metrics.height() + 5;    // height of X axis
    y = h - hxa;
    pixperdiv = (float)w / (float)m_HorDivs;

    painter.setBrush(axis_brush);
    painter.setPen(QColor(0x0, 0x0, 0x0, 0x70));
    painter.drawRect(0, y, w, hxa);
    painter.drawRect(0, 0, wya, h - hxa - 1);
    painter.setFont(font);
    painter.setPen(QColor(0xFF, 0xFF, 0xFF, 0xFF));

    // skip last frequency entry
    for (i = 2; i < m_HorDivs - 1; i++)
    {
        // frequency tick marks
        x = (int)((float)i * pixperdiv);
        painter.drawLine(x, y, x, y + 5);

        // frequency strings
        x = (int)((float)i * pixperdiv - pixperdiv / 2.0);
        rect.setRect(x, y, (int)pixperdiv, hxa);
        painter.drawText(rect, Qt::AlignHCenter | Qt::AlignBottom, m_HDivText[i]);
    }
    rect.setRect(w - pixperdiv - 10, y, pixperdiv, hxa);
    painter.drawText(rect, Qt::AlignRight | Qt::AlignBottom, tr("MHz"));

    quint64 msec;
    int tdivs = h / 70 + 1;
    pixperdiv = (float)h / (float)tdivs;
    tt.setTimeSpec(Qt::OffsetFromUTC);
    for (i = 1; i < tdivs; i++)
    {
        y = (int)((float)i * pixperdiv);
        if (msec_per_wfline > 0)
            msec = tlast_wf_ms - y * msec_per_wfline;
        else
            msec = tlast_wf_ms - y * 1000 / fft_rate;

        tt.setMSecsSinceEpoch(msec);
        rect.setRect(0, y - font_metrics.height(), wya - 5, font_metrics.height());
        painter.drawText(rect, Qt::AlignRight | Qt::AlignVCenter, tt.toString("yyyy.MM.dd"));
        painter.drawLine(wya - 5, y, wya, y);
        rect.setRect(0, y, wya - 5, font_metrics.height());
        painter.drawText(rect, Qt::AlignRight | Qt::AlignVCenter, tt.toString("hh:mm:ss"));
    }

    return pixmap.save(filename, 0, -1);
}

/** Get waterfall time resolution in milleconds / line. */
quint64 CSpecPlotter::getWfTimeRes(void)
{
    if (msec_per_wfline)
        return msec_per_wfline;
    else
        return 1000 * fft_rate / m_WaterfallPixmap.height(); // Auto mode
}

void CSpecPlotter::setFftRate(int rate_hz)
{
    fft_rate = rate_hz;
    clearWaterfall();
}

// Called when a mouse button is pressed
void CSpecPlotter::mousePressEvent(QMouseEvent* event)
{
    QPoint pt = event->pos();
    SelectFreq = freqFromX(pt.x());
    m_lastPoint = event->globalPos();

    if (event->button() == Qt::LeftButton) {
        if (event->x() < m_OverlayPixmap.width() - m_YAxisWidth) {
            selectStart = event->pos();
            m_startX = event->x();
            m_endX = m_startX;
            m_startY = event->y();
            m_endY = m_startY;
            m_isRelease = false;
        }
    }

    if (NOCAP == m_CursorCaptured || NB == m_CursorCaptured)
    {
        if (isPointCloseTo(pt.x(), m_DemodFreqX, m_CursorCaptureDelta))
        {
            // move demod box center frequency region
            m_CursorCaptured = CENTER;
            m_GrabPosition = pt.x() - m_DemodFreqX;
        }
        else if (isPointCloseTo(pt.x(), m_DemodLowCutFreqX, m_CursorCaptureDelta))
        {
            // filter low cut
            m_CursorCaptured = LEFT;
            m_GrabPosition = pt.x() - m_DemodLowCutFreqX;
        }
        else if (isPointCloseTo(pt.x(), m_DemodHiCutFreqX, m_CursorCaptureDelta))
        {
            // filter high cut
            m_CursorCaptured = RIGHT;
            m_GrabPosition = pt.x() - m_DemodHiCutFreqX;
        }
        else
        {
            if (event->button() == Qt::LeftButton)
            {
                int     best = -1;

                if (m_PeakDetection > 0)
                    best = getNearestPeak(pt);
                if (best != -1)
                    m_DemodCenterFreq = freqFromX(best);
                else
                    m_DemodCenterFreq = freqFromX(event->pos().x());
                m_DemodCenterFreq = roundFreq(freqFromX(event->pos().x()), m_ClickResolution);
                m_currentPtx = pt.x();
                float zoomRatio = pt.x() / (m_OverlayPixmap.width() * 1.0);//dang
                float currentXSize = m_fftDataSize / m_factor;
                auto startPoint = (m_CenterFreq - m_SampleFreq / 2);
                if (f_min == startPoint)
                    tempindex = zoomRatio * 16384;
                else
                    tempindex = (f_min - startPoint) / m_SampleFreq * 16384 + currentXSize * zoomRatio;
                //tempindex = (f_min - startPoint) / m_SampleFreq * 16384 * zoomRatio;

            //if cursor not captured set demod frequency and start demod box capture
            //emit newDemodFreq(m_DemodCenterFreq, m_DemodCenterFreq - m_CenterFreq);

            // save initial grab postion from m_DemodFreqX
            // setCursor(QCursor(Qt::CrossCursor));
            //m_CursorCaptured = CENTER;
            //m_GrabPosition = 0;

                drawOverlay();
            }
            //搬移坐标轴，不移动频谱
            else if (event->button() == Qt::MidButton)
            {
                //                // set center freq
                //                m_CenterFreq = roundFreq(freqFromX(pt.x()), m_ClickResolution);
                //                m_DemodCenterFreq = m_CenterFreq;
                //                emit newCenterFreq(m_CenterFreq);
                //                emit newDemodFreq(m_DemodCenterFreq, m_DemodCenterFreq - m_CenterFreq);
                //                drawOverlay();
            }
            //右键重置坐标轴位置
            else if (event->button() == Qt::RightButton)
            {
                //                // reset frequency zoom
                //                resetHorizontalZoom();
                //                resetVericalZoom();
                //                draw();
            }
        }
    }
    else
    {

        int height =  pt.y() - m_WaterfallPixmap.height();
        //modify by zxy 2022-11-18：修复了在屏幕中间也能点出右键菜单的问题
        if (isPointCloseTo(pt.x(), this->width() - m_YAxisWidth / 2, m_YAxisWidth / 2) && (height < m_OverlayPixmap.height()))
            //if (m_CursorCaptured == YAXIS)
        {
            m_Yzero = pt.y();
            if (event->button() == Qt::RightButton)
            {
                float ratio1 = (float)height / m_OverlayPixmap.height();
                zoomstart = m_PandMaxdB - ratio1 * (m_PandMaxdB - m_PandMindB);
                //  transparentStartY = m_currentDisplay.load() == kAll ? pt.y() +m_waterfallHeight : pt.y();
                transparentStartY = pt.y();
                transparentY = new QWidget(this);
                transparentY->setStyleSheet("background-color:rgba(255,255,255,60%);");
                transparentY->hide();
            }
        }

        else if (m_CursorCaptured == XAXIS)
        {
            m_Xzero = pt.x();
            if (event->button() == Qt::RightButton)
            {
                // reset frequency zoom
                //      resetHorizontalZoom();
                draw();
                m_bRButtonDown = true;
                m_nRButtonDownStart = m_Xzero;

                transparentStart = event->pos().x();
                transparent = new QWidget(this);
                transparent->setStyleSheet("background-color:rgba(255,255,255,60%);");
                transparent->hide();
            }
        }
        else if (m_CursorCaptured == BOOKMARK)
        {
            for (int i = 0; i < m_BookmarkTags.size(); i++)
            {
                if (m_BookmarkTags[i].first.contains(event->pos()))
                {
                    m_DemodCenterFreq = m_BookmarkTags[i].second;
                    emit newDemodFreq(m_DemodCenterFreq, m_DemodCenterFreq - m_CenterFreq);
                    break;
                }
            }
        }
    }
}

void CSpecPlotter::mouseReleaseEvent(QMouseEvent* event)
{
    //QApplication::processEvents(QEventLoop::AllEvents, 100);


    if (transparent != nullptr)
        transparent->hide();
    if (transparentY != nullptr)
        transparentY->hide();
    QPoint pt = event->pos();

    if (event->button() == Qt::LeftButton)
        m_isRelease = true;

    int ptHeight = pt.y() - m_WaterfallPixmap.height();
    if (ptHeight < 0)
    {
        // not in Overlay region
        if (NOCAP != m_CursorCaptured)
            setCursor(QCursor(Qt::ArrowCursor));

        m_CursorCaptured = NOCAP;
        m_GrabPosition = 0;
        m_bRButtonDown = false;
        m_nRButtonDownStop = -1;
        m_nRButtonDownStart = -1;
    }
    else
    {
        //处于Y轴

        if (isPointCloseTo(pt.x(), this->width() - m_YAxisWidth / 2, m_YAxisWidth / 2) && (ptHeight < m_OverlayPixmap.height()))
        {
            setCursor(QCursor(Qt::OpenHandCursor));
            m_Yzero = -1;

            if (event->button() == Qt::RightButton)
            {
                float ratio1 = (float)ptHeight / (float)m_OverlayPixmap.height();
                zoomend = m_PandMaxdB - ratio1 * (m_PandMaxdB - m_PandMindB);
                //                if (zoomend == zoomstart)
                //                {
                //                    QMenu * menu = new QMenu(this);
                //                    menu->setAttribute(Qt::WA_DeleteOnClose);
                //                    menu->addAction(QStringLiteral("zoom in"), this, [=] {
                //                        float zoom_fac = 0.6;//滚轮向上或者向下缩放倍率
                //                        float ratio = (float)ptHeight / (float)m_OverlayPixmap.height();
                //                        float db_range = m_PandMaxdB - m_PandMindB;
                //                        float y_range = (float)m_OverlayPixmap.height();
                //                        float db_per_pix = db_range / y_range;
                //                        float fixed_db = m_PandMaxdB - ptHeight * db_per_pix;

                //                        db_range = qBound(10.f, db_range * zoom_fac, FFT_MAX_DB - FFT_MIN_DB);
                //                        m_PandMaxdB = fixed_db + ratio * db_range;
                //                        if (m_PandMaxdB > FFT_MAX_DB)
                //                            m_PandMaxdB = FFT_MAX_DB;

                //                        m_PandMindB = m_PandMaxdB - db_range;
                //                        m_PeakHoldValid = false;
                //                        //NewFftData1();
                //                        emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
                //                    });
                //                    menu->addAction(QStringLiteral("zoom out"), this, [=] {
                //                        float zoom_fac = 1.4;//滚轮向上或者向下缩放倍率
                //                        float ratio = (float)pt.y() / (float)m_OverlayPixmap.height();
                //                        float db_range = m_PandMaxdB - m_PandMindB;
                //                        float y_range = (float)m_OverlayPixmap.height();
                //                        float db_per_pix = db_range / y_range;
                //                        float fixed_db = m_PandMaxdB - pt.y() * db_per_pix;

                //                        db_range = qBound(10.f, db_range * zoom_fac, FFT_MAX_DB - FFT_MIN_DB);
                //                        m_PandMaxdB = fixed_db + ratio * db_range;
                //                        if (m_PandMaxdB > FFT_MAX_DB)
                //                            m_PandMaxdB = FFT_MAX_DB;

                //                        m_PandMindB = m_PandMaxdB - db_range;
                //                        m_PeakHoldValid = false;
                //                        //NewFftData1();
                //                        emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
                //                    });
                //                    menu->addAction(QStringLiteral("zoom full"), this, [=] {
                //                        m_PandMindB = -160;
                //                        m_PandMaxdB = 0;
                //                        m_PeakHoldValid = false;
                //                        NewFftData1();
                //                        emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);

                //                    });
                //                    menu->exec(QCursor::pos());
                //                }
                //  else
                {
                    //缩放幅度太小会导致y轴的坐标轴被放的很大，导致看不到y轴
                    //限制坐标轴的范围 最小为10个db
                    float ratio = (zoomstart - zoomend);
                    if (ratio > 10)
                    {
                        m_PandMindB = zoomend;
                        m_PandMaxdB = zoomstart;
                        m_PeakHoldValid = false;
                        NewFftData1();
                        emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
                        //qDebug() << "区间缩放" << "zoomend = " << zoomend << " zoomstart = " << zoomstart;
                    }
                    else
                    {
                        //m_PandMindB = (zoomstart + zoomend)/2 + 5;
                        //m_PandMaxdB = (zoomstart + zoomend)/2 - 5;
                        //m_PeakHoldValid = false;
                        //NewFftData1();
                        //emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
                        //qDebug() << "区间缩放" << "zoomend = " << zoomend << " zoomstart = " << zoomstart;
                    }
                }
            }
            drawOverlay();
        }
        else if (XAXIS == m_CursorCaptured)
        {
            //点击x轴，会进行缩放
            if (event->button() == Qt::RightButton)
            {

                int endx = pt.x();
                if (m_Xzero != endx)
                {
                    qint64 startFreq = freqFromX(m_Xzero);
                    qint64 stopFreq = freqFromX(endx);

                    qint64 fc = (startFreq + stopFreq) / 2;
                    qint64 new_range = qAbs(stopFreq - startFreq);
                    setFftCenterFreq(fc - m_CenterFreq);
                    setSpanFreq((quint32)new_range);
                    float factor = (float)m_SampleFreq / (float)m_Span;
                    emit  newZoomLevel(factor);

                    //   draw();
                    m_PeakHoldValid = false;
                }
                m_bRButtonDown = false;
                m_nRButtonDownStop = -1;
                m_nRButtonDownStart = -1;
            }

            setCursor(QCursor(Qt::OpenHandCursor));
            m_Xzero = -1;
        }
        else if (event->button() == Qt::RightButton) {
              
                if ((event->globalPos() == m_lastPoint)) {
                    resetHorizontalZoom();
                    resetVericalZoom();
                    draw();
              
                }
            }
    }



    //modify by zxy 2022-11-14
    //单击后频谱图会间歇性出现横线
    //NewFftData1();
}

void CSpecPlotter::contextMenuEvent(QContextMenuEvent* event) {


}

// Make a single zoom step on the X axis.
void CSpecPlotter::zoomStepX(float step, int x)
{
    // calculate new range shown on FFT
    float new_range = qBound(10.0f, m_Span * step, m_SampleFreq * 10.0f);

    // Frequency where event occured is kept fixed under mouse
    float ratio = (float)x / (float)m_OverlayPixmap.width();
    float fixed_hz = freqFromX(x);
    float f_max = fixed_hz + (1.0 - ratio) * new_range;
    f_min = f_max - new_range;

    // ensure we don't go beyond the rangelimits
    if (f_min < m_CenterFreq - m_SampleFreq / 2.f)
        f_min = m_CenterFreq - m_SampleFreq / 2.f;

    if (f_max > m_CenterFreq + m_SampleFreq / 2.f)
        f_max = m_CenterFreq + m_SampleFreq / 2.f;
    new_range = f_max - f_min;
    //modify by zxy 2022-11-21 限制缩放尺寸
    //if (new_range < 9500)
    //	return;
    qint64 fc = (qint64)(f_min + (f_max - f_min) / 2.0);

    setFftCenterFreq(fc - m_CenterFreq);
    setSpanFreq((quint32)new_range);

    float factor = (float)m_SampleFreq / (float)m_Span;
    m_factor = factor;
    emit newZoomLevel(factor);
    //qDebug() << QString("Spectrum zoom: %1x").arg(factor, 0, 'f', 1);
    //NewFftData1();
    //m_PeakHoldValid = false;
    //draw();
}

// Zoom on X axis (absolute level)
void CSpecPlotter::zoomOnXAxis(float level)
{
    float current_level = (float)m_SampleFreq / (float)m_Span;

    zoomStepX(current_level / level, xFromFreq(m_DemodCenterFreq));
}

// Called when a mouse wheel is turned
void CSpecPlotter::wheelEvent(QWheelEvent* event)
{
    QPoint pt = event->pos();
    int numDegrees = event->delta() / 8;
    int numSteps = numDegrees / 15;  /** FIXME: Only used for direction **/

    /** FIXME: zooming could use some optimisation **/
    //modify by zxy 2022-11-18：修复了在雨图y轴能操作频谱图缩放的bug
    int height = pt.y() - m_WaterfallPixmap.height();
    if (isPointCloseTo(pt.x(), this->width() - m_YAxisWidth / 2, m_YAxisWidth / 2) && (height < m_OverlayPixmap.height()))
    {
        // Vertical zoom. Wheel down: zoom out, wheel up: zoom in
        // During zoom we try to keep the point (dB or kHz) under the cursor fixed
        float zoom_fac = event->delta() < 0 ? 1.1 : 0.9;
        float ratio = (float)height / (float)m_OverlayPixmap.height();
        float db_range = m_PandMaxdB - m_PandMindB;
        float y_range = (float)m_OverlayPixmap.height();
        float db_per_pix = db_range / y_range;
        float fixed_db = m_PandMaxdB - height * db_per_pix;

        db_range = qBound(10.f, db_range * zoom_fac, FFT_MAX_DB - FFT_MIN_DB);
        m_PandMaxdB = fixed_db + ratio * db_range;
        if (m_PandMaxdB > FFT_MAX_DB)
            m_PandMaxdB = FFT_MAX_DB;

        m_PandMindB = m_PandMaxdB - db_range;
        m_PeakHoldValid = false;
        //NewFftData1();
        emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
    }
    else if (m_CursorCaptured == XAXIS || m_CursorCaptured == NB)
    {
        zoomStepX(event->delta() < 0 ? 1.1 : 0.9, pt.x());
    }
    else if (event->modifiers() & Qt::ControlModifier)
    {
        // filter width
        m_DemodLowCutFreq -= numSteps * m_ClickResolution;
        m_DemodHiCutFreq += numSteps * m_ClickResolution;
        clampDemodParameters();
        emit newFilterFreq(m_DemodLowCutFreq, m_DemodHiCutFreq);
    }

    else if (event->modifiers() & Qt::ShiftModifier)
    {
        // filter shift
        m_DemodLowCutFreq += numSteps * m_ClickResolution;
        m_DemodHiCutFreq += numSteps * m_ClickResolution;
        clampDemodParameters();
        emit newFilterFreq(m_DemodLowCutFreq, m_DemodHiCutFreq);
    }
    else
    {
        // inc/dec demod frequency
        m_DemodCenterFreq += (numSteps * m_ClickResolution);
        m_DemodCenterFreq = roundFreq(m_DemodCenterFreq, m_ClickResolution);
        emit newDemodFreq(m_DemodCenterFreq, m_DemodCenterFreq - m_CenterFreq);
    }

    updateOverlay();
}

void CSpecPlotter::keyPressEvent(QKeyEvent* event)
{
    //红线移动
    /*if (event->key() == Qt::Key_Left) {
        m_DemodCenterFreq = roundFreq(freqFromX(m_currentPtx -= 10), m_ClickResolution);
    }
    else if (event->key() == Qt::Key_Right) {
        m_DemodCenterFreq = roundFreq(freqFromX(m_currentPtx += 10), m_ClickResolution);
    }
    float zoomRatio = m_currentPtx / (m_OverlayPixmap.width()*1.0);
    //qDebug() << "m_fftDataSize = " << m_fftDataSize / m_factor;
    tempindex = zoomRatio * m_fftDataSize;//
    drawOverlay();*/
    //频谱移动
    int delta_px = 10;
    qint64 delta_hz = delta_px * m_Span / m_OverlayPixmap.width();
    float delta_db = delta_px * fabs(m_PandMindB - m_PandMaxdB) /
        (float)m_OverlayPixmap.height();
    if (event->key() == Qt::Key_Left) {
        setFftCenterFreq(m_FftCenter + delta_hz);
        updateOverlay();
    }
    else if (event->key() == Qt::Key_Right) {
        setFftCenterFreq(m_FftCenter - delta_hz);
        updateOverlay();
    }
    else if (event->key() == Qt::Key_Up) {
        m_PandMindB += delta_db;
        m_PandMaxdB += delta_db;
        if (out_of_range(m_PandMindB, m_PandMaxdB))
        {
            m_PandMindB -= delta_db;
            m_PandMaxdB -= delta_db;
        }
        else
        {
            emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
            if (m_Running)
                m_DrawOverlay = true;
            else
                drawOverlay();
            m_Yzero += delta_db;
        }
    }
    else if (event->key() == Qt::Key_Down) {
        m_PandMindB -= delta_db;
        m_PandMaxdB -= delta_db;
        if (out_of_range(m_PandMindB, m_PandMaxdB))
        {
            m_PandMindB += delta_db;
            m_PandMaxdB += delta_db;
        }
        else
        {
            emit pandapterRangeChanged(m_PandMindB, m_PandMaxdB);
            if (m_Running)
                m_DrawOverlay = true;
            else
                drawOverlay();
            m_Yzero -= delta_db;
        }
    }
    m_PeakHoldValid = false;

}

// Called when screen size changes so must recalculate bitmaps
void CSpecPlotter::resizeEvent(QResizeEvent*)
{
    if (!size().isValid())
        return;

    {
        m_Size = size();
        int pix_height = size().height();

        m_mutex.lock();
        m_OverlayPixmap = QPixmap(m_Size.width(), pix_height);
        m_OverlayPixmap.fill(Qt::black);

        // m_2DPixmap = QPixmap(m_Size.width(), pix_height - m_xAxisHeight);
        m_2DPixmap = QPixmap(m_Size.width(), pix_height - m_xAxisHeight);
        m_2DPixmap.fill(Qt::black);

        m_WaterfallPixmap = QPixmap(m_Size.width(), 0);
        m_WaterfallPixmap.fill(Qt::black);

        m_mutex.unlock();

        m_PeakHoldValid = false;

        if (wf_span > 0)
            msec_per_wfline = wf_span / (size().height() - m_xAxisHeight);
        memset(m_wfbuf, 255, MAX_SCREENSIZE);
    }

    drawOverlay();
}

void CSpecPlotter::recelevel(float L)
{
    level = L;
    drawOverlay();
}

// Called by QT when screen needs to be redrawn
void CSpecPlotter::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    int height = 0;
    QMutexLocker lock(&m_mutex);

    painter.drawPixmap(0, height, m_2DPixmap);
    height += m_2DPixmap.height();
    painter.drawPixmap(0, height, m_xAxisPixmap);
}

// Called to update spectrum data for displaying on the screen
void CSpecPlotter::draw()
{
    if (m_DrawOverlay)
    {
        drawOverlay();
        m_DrawOverlay = false;
    }
    DrawSpectrum();
}

void CSpecPlotter::DrawSpectrum()
{

    int     xmin, xmax;
    QPoint LineBuf[MAX_SCREENSIZE];
    int pixmap_width = m_2DPixmap.width();
    int pixmap_height = m_2DPixmap.height();

    if (pixmap_width == 0 || pixmap_height == 0)
        return;

    m_mutex.lock();
    // first copy into 2Dbitmap the overlay bitmap.
    QPixmap Pixmap2d = m_OverlayPixmap.copy(0, 0, pixmap_width, m_OverlayPixmap.height() - m_xAxisHeight);

    QPixmap Pixmap2_yaxis = m_OverlayPixmap.copy(pixmap_width - m_YAxisWidth - 5, 0, m_YAxisWidth + 5, pixmap_height);

    QPixmap Pixmap2d_line = m_OverlayPixmap.copy(0, 0, pixmap_width, m_OverlayPixmap.height() - m_xAxisHeight);

    m_mutex.unlock();
    QPainter painter2d(&Pixmap2d);
    QPainter painter2d_line(&Pixmap2d_line);

#if QT_VERSION >= 0x050000
    painter2d.translate(0.5, 0.5);
#endif

    // get new scaled fft data
    getScreenIntegerFFTData(pixmap_height, qMin(pixmap_width, MAX_SCREENSIZE),
        m_PandMaxdB, m_PandMindB,
        m_FftCenter - (qint64)m_Span / 2,
        m_FftCenter + (qint64)m_Span / 2,
        m_fftData, m_fftbuf,
        &xmin, &xmax);

    // draw the pandapter
    painter2d.setPen(m_FftColor);
    qint32 size = xmax - xmin;
    if (xmin < 0 || xmax < 0)
        return;
    for (int i = 0; i < size; i++) {
        LineBuf[i].setX(i + xmin);
        LineBuf[i].setY(m_fftbuf[i + xmin]);
        /*qDebug()<<"LineBug x:" << LineBuf[i].x();*/
    }
    //如果是自动找最大峰
    auto maxValue = 255;
    auto maxIndex = 0;

    if (m_FftFill) {
        painter2d.setBrush(QBrush(m_FftFillCol, Qt::SolidPattern));
        if (size < MAX_SCREENSIZE - 2) {
            LineBuf[size].setX(xmax - 1);
            LineBuf[size].setY(pixmap_height);
            LineBuf[size + 1].setX(xmin);
            LineBuf[size + 1].setY(pixmap_height);
            painter2d.drawPolygon(LineBuf, size + 2);
        }
        else {
            LineBuf[MAX_SCREENSIZE - 2].setX(xmax - 1);
            LineBuf[MAX_SCREENSIZE - 2].setY(pixmap_height);
            LineBuf[MAX_SCREENSIZE - 1].setX(xmin);
            LineBuf[MAX_SCREENSIZE - 1].setY(pixmap_height);
            painter2d.drawPolygon(LineBuf, size);

        }
    }
    else {
        painter2d.drawPolyline(LineBuf, size);
        painter2d.drawPixmap(pixmap_width - m_YAxisWidth - 5, 0, m_YAxisWidth + 5, pixmap_height, Pixmap2_yaxis);
        //  painter2d.drawPixmap(-1, pixmap_height - 30, pixmap_width, 30, m_xAxisPixmap);
    }

    // Peak detection
    //if (m_PeakDetection > 0) {
    if (0) {
        m_Peaks.clear();

        float   mean = 0;
        float   sum_of_sq = 0;
        for (int i = 0; i < size; i++) {
            mean += m_fftbuf[i + xmin];
            sum_of_sq += m_fftbuf[i + xmin] * m_fftbuf[i + xmin];
        }
        mean /= size;
        float stdev = sqrt(sum_of_sq / size - mean * mean);

        int lastPeak = -1;
        for (int i = 0; i < size; i++) {
            //m_PeakDetection times the std over the mean or better than current peak
            float d = (lastPeak == -1) ? (mean - m_PeakDetection * stdev) :
                m_fftbuf[lastPeak + xmin];

            if (m_fftbuf[i + xmin] < d)
                lastPeak = i;

            if (lastPeak != -1 &&
                (i - lastPeak > PEAK_H_TOLERANCE || i == size - 1)) {
                m_Peaks.insert(lastPeak + xmin, m_fftbuf[lastPeak + xmin]);
                painter2d.drawEllipse(lastPeak + xmin - 5,
                    m_fftbuf[lastPeak + xmin] - 5, 10, 10);
                lastPeak = -1;
            }
        }
    }

    {//左文字
        QRect rect;
        painter2d.setPen(QColor(255, 255, 255));
        rect.setRect(0, 0, 150, 20);

        //qDebug() << tempindex << m_fft_size;
        if (m_fftData != nullptr && tempindex >= 0 && tempindex < m_fft_size) {
            if (m_peak) {
                for (int i = 0; i < size; i++) {
                    if (maxValue > m_fftbuf[i + xmin]) {
                        maxIndex = i + xmin;
                        maxValue = m_fftbuf[i + xmin];
                        //tempindex = maxIndex;
                    }
                }
                m_DemodCenterFreq = freqFromX(maxIndex);
                m_DemodCenterFreq = roundFreq(freqFromX(maxIndex), m_ClickResolution);
                m_currentPtx = maxIndex;
                double zoomRatio = (double)maxIndex / (m_OverlayPixmap.width() * 1.0);//
                float currentXSize = m_fftDataSize / m_factor;
                auto startPoint = (m_CenterFreq - m_SampleFreq / 2);
                tempindex = (f_min - startPoint) / m_SampleFreq * 16384 + currentXSize * zoomRatio;
                painter2d.drawEllipse(maxIndex - 2, maxValue - 5, 10, 10);


            }
            //修正一下点位 找附近最大值
            static int findLeftLen = 20;   //附近20个点位
            auto startIndex = (tempindex - findLeftLen) < 0 ? 0 : (tempindex - findLeftLen);
            auto endIndex = (tempindex + findLeftLen) > 16384 ? 16384 : (tempindex + findLeftLen);
            /*auto startIndex = (tempindex - findLeftLen) < 0 ? 0 : (tempindex - findLeftLen);
            auto endIndex = (tempindex + findLeftLen) > 16384 ? 16384 : (tempindex + findLeftLen);*/
            float templevel = -200;
            for (int i = startIndex; i < endIndex; i++) {
                if (templevel < m_fftData[i])
                {
                    templevel = m_fftData[i];
                }
            }
            //SelectFreq = freqFromX(templevel);
            QString str = QStringLiteral("选择频率:") + QString::number(m_DemodCenterFreq / 1.e6f, 'f', 3) + QString(" MHz");
            painter2d.drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, str);
            rect.setRect(0, 20, 150, 20);
            str = QStringLiteral("选择幅度:") + QString::number(templevel) + QString(" dB");
            m_fftDataSpectrum = templevel;
            painter2d.drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, str);
        }
        else
        {
            QString str = QStringLiteral("选择频率:") + QString::number(SelectFreq / 1.e6f, 'f', 3) + QString(" MHz");
            painter2d.drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, str);
            rect.setRect(0, 20, 150, 20);
            float templevel = m_fftData[tempindex];
            str = QStringLiteral("选择幅度:") + QString::number(templevel) + QString(" dB");
            m_fftDataSpectrum = templevel;
            painter2d.drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, str);

        }

        rect.setRect(0, 40, 150, 20);
        QString str = QStringLiteral("中心频率:") + QString::number(m_CenterFreq) + QString("Hz");
        painter2d.drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, str);

        rect.setRect(0, 60, 150, 20);
        str = QStringLiteral("显示带宽:") + QString::number(m_SampleFreq / 1000000) + QString("MHz");
        painter2d.drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, str);
    }





    //画Peak hong'xian
    if (m_peak) {
        painter2d.setOpacity(1.0);
        painter2d.setBrush(QBrush(Qt::green));
        painter2d.setPen(QColor(PLOTTER_FILTER_LINE_COLOR));
        painter2d.drawLine(maxIndex, 0, maxIndex, pixmap_height);
    }
    else {
        painter2d.setOpacity(1.0);
        painter2d.setPen(QColor(PLOTTER_FILTER_LINE_COLOR));
        painter2d.drawLine(0, m_refenceLevelHeight, pixmap_width - m_YAxisWidth, m_refenceLevelHeight);
    }

    painter2d.end();
    QMutexLocker lock(&m_mutex);
    m_2DPixmap = Pixmap2d.copy();
}

/**
 * Set new FFT data.
 * @param fftData Pointer to the new FFT data (same data for pandapter and waterfall).
 * @param size The FFT size.
 *
 * When FFT data is set using this method, the same data will be used for both the
 * pandapter and the waterfall.
 */
void CSpecPlotter::setNewFftData(float* fftData, int size)
{
    /** FIXME **/
    if (!m_Running)
        m_Running = true;

    m_wfData = fftData;
    m_fftData = fftData;
    m_fftDataSize = size;

    draw();
}

/**
 * Set new FFT data.
 * @param fftData Pointer to the new FFT data used on the pandapter.
 * @param wfData Pointer to the FFT data used in the waterfall.
 * @param size The FFT size.
 *
 * This method can be used to set different FFT data set for the pandapter and the
 * waterfall.
 */

void CSpecPlotter::setNewFftData(float* fftData, float* wfData, int size)
{
    /** FIXME **/
    if (!m_Running)
        m_Running = true;

    m_wfData = wfData;
    m_fftData = fftData;
    m_fftDataSize = size;

    draw();
}

void CSpecPlotter::NewFftData1()
{
    if (!m_Running)
        m_Running = true;

    /*m_wfData = m_fftData1;
    m_fftData = m_fftData1;
    m_fftDataSize = m_fft_size;*/

    draw();
}
void CSpecPlotter::NewFftData(float* fftData, int size, float maxdb)
{
    /** FIXME **/
    if (!m_Running)
        m_Running = true;

    level = maxdb;
    m_fft_size = size;

    for (int i = 0; i < size; ++i) {
        m_fftData[i] = fftData[i];
        m_wfData[i] = fftData[i];
    }

    /*  memcpy(m_fftData,fftData,sizeof(float)*size);
    memcpy(m_fftData1, fftData, sizeof(float)*size);
    memcpy(m_wfData, fftData, sizeof(float)*size);*/
    auto h = m_2DPixmap.height();
    int axish = 30;
    auto cut_h = (m_PandMaxdB - m_PandMindB) * (axish / (float)h);

    auto temp = m_PandMindB + cut_h;
    for (int i = 0; i < size; i++) {
        m_wfData[i] = m_wfData[i] > m_referenceLevel ? m_wfData[i] : -254;
    }
    m_fftDataSize = size;

    if (flag)
    {
        resetHorizontalZoom();
        flag = false;
    }
    m_drawMutex.lock();
    draw();
    m_drawMutex.unlock();
}



void CSpecPlotter::getScreenIntegerFFTData(qint32 plotHeight, qint32 plotWidth,
    float maxdB, float mindB,
    qint64 startFreq, qint64 stopFreq,
    float* inBuf, qint32* outBuf,
    int* xmin, int* xmax)
{
    qint32 i;
    qint32 y;
    qint32 x;
    qint32 ymax = 10000;
    qint32 xprev = -1;
    qint32 minbin, maxbin;
    qint32 m_BinMin, m_BinMax;
    qint32 m_FFTSize = m_fftDataSize;
    float* m_pFFTAveBuf = inBuf;
    float  dBGainFactor = ((float)plotHeight) / fabs(maxdB - mindB);
    qint32* m_pTranslateTbl = new qint32[qMax(m_FFTSize, plotWidth)];

    /** FIXME: qint64 -> qint32 **/
    m_BinMin = (qint32)((float)startFreq * (float)m_FFTSize / m_SampleFreq);
    m_BinMin += (m_FFTSize / 2);
    m_BinMax = (qint32)((float)stopFreq * (float)m_FFTSize / m_SampleFreq);
    m_BinMax += (m_FFTSize / 2);

    minbin = m_BinMin < 0 ? 0 : m_BinMin;
    if (m_BinMin > m_FFTSize)
        m_BinMin = m_FFTSize - 1;
    if (m_BinMax <= m_BinMin)
        m_BinMax = m_BinMin + 1;
    maxbin = m_BinMax < m_FFTSize ? m_BinMax : m_FFTSize;
    bool largeFft = (m_BinMax - m_BinMin) > plotWidth; // true if more fft point than plot points

    if (largeFft)
    {
        // more FFT points than plot points
        for (i = minbin; i < maxbin; i++)
            m_pTranslateTbl[i] = ((qint64)(i - m_BinMin) * plotWidth) / (m_BinMax - m_BinMin);
        *xmin = m_pTranslateTbl[minbin];
        *xmax = m_pTranslateTbl[maxbin - 1];
    }
    else
    {
        // more plot points than FFT points
        for (i = 0; i < plotWidth; i++)
            m_pTranslateTbl[i] = m_BinMin + (i * (m_BinMax - m_BinMin)) / plotWidth;
        *xmin = 0;
        *xmax = plotWidth;
    }

    if (largeFft)
    {
        // more FFT points than plot points
        for (i = minbin; i < maxbin; i++)
        {
            y = (qint32)(dBGainFactor * (maxdB - m_pFFTAveBuf[i]));

            if (y > plotHeight)
                y = plotHeight;
            else if (y < 0)
                y = 0;

            x = m_pTranslateTbl[i];	//get fft bin to plot x coordinate transform

            if (x == xprev)   // still mappped to same fft bin coordinate
            {
                if (y < ymax) // store only the max value
                {
                    outBuf[x] = y;
                    ymax = y;
                }

            }
            else
            {
                outBuf[x] = y;
                xprev = x;
                ymax = y;
            }
        }
    }
    else
    {
        // more plot points than FFT points
        for (x = 0; x < plotWidth; x++)
        {
            i = m_pTranslateTbl[x]; // get plot to fft bin coordinate transform
            if (i < 0 || i >= m_FFTSize)
                y = plotHeight;
            else
                y = (qint32)(dBGainFactor * (maxdB - m_pFFTAveBuf[i]));

            if (y > plotHeight)
                y = plotHeight;
            else if (y < 0)
                y = 0;

            outBuf[x] = y;
        }
    }

    delete[] m_pTranslateTbl;
}

void CSpecPlotter::setFftRange(float min, float max)
{
    setWaterfallRange(min, max);
    setPandapterRange(min, max);
}

void CSpecPlotter::setPandapterRange(float min, float max)
{
    if (out_of_range(min, max))
        return;

    m_PandMindB = min;
    m_PandMaxdB = max;
    updateOverlay();
    m_PeakHoldValid = false;
}

void CSpecPlotter::setWaterfallRange(float min, float max)
{
    if (out_of_range(min, max))
        return;

    m_WfMindB = min;
    m_WfMaxdB = max;
    // no overlay change is necessary
}

// Called to draw an overlay bitmap containing grid and text that
// does not need to be recreated every fft data update.
void CSpecPlotter::drawOverlay()
{
    if (m_OverlayPixmap.isNull())
        return;

    int     pixmap_width = m_OverlayPixmap.width();
    int     pixmap_height = m_OverlayPixmap.height();
    int     x, y;
    float   pixperdiv;
    float   adjoffset;
    float   dbstepsize;
    float   mindbadj;
    QRect   rect;
    qint64  StartFreq;
    QFontMetrics    metrics(m_Font);
    m_mutex.lock();
    auto OverlayPixmap = m_OverlayPixmap.copy();
    auto WaterfallPixmap = m_WaterfallPixmap.copy();

    m_mutex.unlock();
    QPainter        painter_overlay(&OverlayPixmap);
    QPainter        painter_waterfall(&WaterfallPixmap);

    painter_overlay.initFrom(this);
    painter_overlay.setFont(m_Font);
    painter_overlay.setBrush(Qt::SolidPattern);
    painter_overlay.fillRect(0, 0, pixmap_width, pixmap_height, QColor(PLOTTER_BGD_COLOR));


    painter_waterfall.initFrom(this);
    painter_waterfall.setFont(m_Font);

#define HOR_MARGIN 5
#define VER_MARGIN 5

    // X and Y axis areas
    m_YAxisWidth = metrics.width("XXXX") + 2 * HOR_MARGIN;
    m_XAxisYCenter = pixmap_height + WaterfallPixmap.height() - metrics.height() / 2;
    int xAxisHeight = metrics.height() + 2 * VER_MARGIN;
    int xAxisTop = pixmap_height - xAxisHeight;
    int fLabelTop = xAxisTop + VER_MARGIN;

    if (m_BookmarksEnabled) {
        m_BookmarkTags.clear();
        static const QFontMetrics fm(painter_overlay.font());
        static const int fontHeight = fm.ascent() + 1;
        static const int slant = 5;
        static const int levelHeight = fontHeight + 5;
        static const int nLevels = 10;
        QList<BookmarkInfo> bookmarks = m_pThis->getBookmarksInRange(m_CenterFreq + m_FftCenter - m_Span / 2,
            m_CenterFreq + m_FftCenter + m_Span / 2);

        int tagEnd[nLevels] = { 0 };
        for (int i = 0; i < bookmarks.size(); i++) {
            x = xFromFreq(bookmarks[i].frequency);

#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
            int nameWidth = fm.width(bookmarks[i].name);
#else
            int nameWidth = fm.boundingRect(bookmarks[i].name).width();
#endif

            int level = 0;
            while (level < nLevels && tagEnd[level] > x)
                level++;

            if (level == nLevels)
                level = 0;

            tagEnd[level] = x + nameWidth + slant - 1;
            m_BookmarkTags.append(qMakePair<QRect, qint64>(QRect(x, level * levelHeight, nameWidth + slant, fontHeight), bookmarks[i].frequency));

            QColor color = QColor(255, 255, 255);// QColor(bookmarks[i].GetColor());
            //color.setAlpha(0x60);
            // Vertical line
            painter_overlay.setPen(QPen(color, 1, Qt::DashLine));
            painter_overlay.drawLine(x, level * levelHeight + fontHeight + slant, x, xAxisTop);


            // Horizontal line
            painter_overlay.setPen(QPen(color, 1, Qt::SolidLine));
            painter_overlay.drawLine(x + slant, level * levelHeight + fontHeight,
                x + nameWidth + slant - 1,
                level * levelHeight + fontHeight);

            // Diagonal line
            painter_overlay.drawLine(x + 1, level * levelHeight + fontHeight + slant - 1,
                x + slant - 1, level * levelHeight + fontHeight + 1);

            //color.setAlpha(0xFF);

            painter_overlay.setPen(QPen(color, 2, Qt::SolidLine));
            painter_overlay.drawText(x + slant, level * levelHeight, nameWidth,
                fontHeight, Qt::AlignVCenter | Qt::AlignHCenter,
                bookmarks[i].name);

        }
    }

    if (0) {
        x = xFromFreq(m_CenterFreq);
        if (x > 0 && x < pixmap_width) {
            painter_overlay.setPen(QColor(PLOTTER_CENTER_LINE_COLOR));
            painter_overlay.drawLine(x, 0, x, xAxisTop);

        }
    }

    // Level grid
    qint64 mindBAdj64 = 0;
    qint64 dbDivSize = 0;

    calcDivSize((qint64)m_PandMindB, (qint64)m_PandMaxdB,
        qMax((pixmap_height - xAxisHeight) / m_VdivDelta, VERT_DIVS_MIN), mindBAdj64, dbDivSize,
        m_VerDivs);

    dbstepsize = (float)dbDivSize;
    mindbadj = mindBAdj64;

    pixperdiv = (float)(pixmap_height - xAxisHeight) * (float)dbstepsize / (m_PandMaxdB - m_PandMindB);
    adjoffset = (float)(pixmap_height - xAxisHeight) * (mindbadj - m_PandMindB) / (m_PandMaxdB - m_PandMindB);

    /*StartFreq = m_CenterFreq + m_FftCenter - m_Span / 2;
    pixperdiv = (float)pixmap_height * (float)m_FreqPerDiv / (float)m_Span;
    adjoffset = pixperdiv * float(m_StartFreqAdj - StartFreq) / (float)m_FreqPerDiv;*/

#ifdef PLOTTER_DEBUG
    qDebug() << "minDb =" << m_PandMindB << "maxDb =" << m_PandMaxdB
        << "mindbadj =" << mindbadj << "dbstepsize =" << dbstepsize
        << "pixperdiv =" << pixperdiv << "adjoffset =" << adjoffset;
#endif
    //设置y轴显示
    m_YAxisWidth = metrics.width("-120 ");

    painter_overlay.setPen(QColor(65, 71, 81));
    painter_overlay.fillRect(this->width() - m_YAxisWidth - 5, 0, m_YAxisWidth + 5, this->height() - m_xAxisHeight, QColor(65, 71, 81));

    static  int yAxis = 0;

    painter_overlay.setPen(QColor(PLOTTER_TEXT_COLOR));
    for (int i = 0; i <= m_VerDivs; i++) {
        y = pixmap_height - m_xAxisHeight - (int)((float)(i + 1) * pixperdiv + adjoffset);
        if (y < pixmap_height - xAxisHeight - m_xAxisHeight)
            painter_overlay.setPen(QPen(QColor(PLOTTER_GRID_COLOR), 1, Qt::SolidLine));
        painter_overlay.drawLine(0, y, pixmap_width, y);

        auto size = (y - yAxis) / 5.0;
        painter_overlay.setPen(QPen(QColor(255, 255, 255), 1, Qt::SolidLine));
        for (int i = 1; i < 5; i++) {
            painter_overlay.drawLine(this->width() - m_YAxisWidth - 5, yAxis - size * i, this->width() - m_YAxisWidth - 3, yAxis - size * i);
        }

        painter_overlay.drawLine(this->width() - m_YAxisWidth - 5, y, this->width() - m_YAxisWidth, y);
        yAxis = y;
    }

    // draw amplitude values (y axis)

    int th = metrics.height();
    int dB;
    //painter_overlay.setPen(QColor(PLOTTER_TEXT_COLOR));
    for (int i = 0; i < m_VerDivs; i++) {
        y = pixmap_height - m_xAxisHeight - (int)((float)i * pixperdiv + adjoffset);

        if (y < pixmap_height - m_xAxisHeight - xAxisHeight) {
            dB = mindbadj + dbstepsize * i;
            painter_overlay.setPen(QColor(PLOTTER_TEXT_COLOR));
            rect.setRect(this->width() - m_YAxisWidth, y - th / 2, m_YAxisWidth, th);
            painter_overlay.drawText(rect, Qt::AlignRight | Qt::AlignVCenter, QString::number(dB));
        }
    }


    // 画参考电平线
    float num = (m_referenceLevel - mindbadj) / dbstepsize;
    m_refenceLevelHeight = pixmap_height - m_xAxisHeight - (int)((float)num * pixperdiv + adjoffset);
    painter_overlay.setOpacity(1.0);
    painter_overlay.setPen(QColor(PLOTTER_FILTER_LINE_COLOR));
    painter_overlay.drawLine(0, m_refenceLevelHeight, pixmap_width - m_YAxisWidth, m_refenceLevelHeight);


    //设置x轴显示
    painter_overlay.setPen(QColor(65, 71, 81));
    painter_overlay.fillRect(0, pixmap_height - m_xAxisHeight, pixmap_width, m_xAxisHeight, QColor(65, 71, 81));

    StartFreq = m_CenterFreq + m_FftCenter - m_Span / 2;
    QString label;
    label.setNum(float((StartFreq + m_Span) / m_FreqUnits), 'f', m_FreqDigits);
    calcDivSize(StartFreq, StartFreq + m_Span,
        qMin(pixmap_width / (metrics.width(label) + metrics.width("O")), HORZ_DIVS_MAX),
        m_StartFreqAdj, m_FreqPerDiv, m_HorDivs);
    pixperdiv = (float)pixmap_width * (float)m_FreqPerDiv / (float)m_Span;
    adjoffset = pixperdiv * float(m_StartFreqAdj - StartFreq) / (float)m_FreqPerDiv;

    painter_overlay.setPen(QPen(QColor(PLOTTER_GRID_COLOR), 1, Qt::SolidLine));
    for (int i = 0; i <= m_HorDivs; i++) {
        x = (int)((float)i * pixperdiv + adjoffset);
        if (x > m_YAxisWidth) {
            painter_overlay.drawLine(x, 0, x, xAxisTop);
        }

    }

    // draw frequency values (x axis)

    makeFrequencyStrs();
    painter_overlay.setPen(QColor(PLOTTER_TEXT_COLOR));
    static  int xAxis = 0;
    xAxis = 0;
    //[HORZ_DIVS_MAX + 1]
    m_mutex.lock();
    auto text = m_HDivText;
    for (int i = 0; i <= m_HorDivs; i++) {
        int tw = metrics.width(m_HDivText[i]);
        x = (int)((float)i * pixperdiv + adjoffset);
        if (x > m_YAxisWidth) {
            //频谱图的横坐标
            painter_overlay.setPen(QColor(255, 255, 255));
            painter_waterfall.setPen(QColor(255, 255, 255));
            //painter.drawLine((x + xAxis) / 2, h - 30, (x + xAxis) / 2, h - 25);
            painter_overlay.drawLine(x, pixmap_height - 30, x, pixmap_height - 20);
            //     painter_waterfall.drawLine(x, pixmap_height - 30, x, pixmap_height - 20);
            auto size = (x - xAxis) / 5.0;
            for (int i = 1; i < 5; i++) {
                painter_overlay.drawLine(xAxis + size * i, pixmap_height - 30, xAxis + size * i, pixmap_height - 25);
                //       painter_waterfall.drawLine(xAxis + size * i, pixmap_height - 30, xAxis + size * i, pixmap_height - 25);
            }
            rect.setRect(x - tw / 2, fLabelTop, tw, metrics.height());
            painter_overlay.drawText(rect, Qt::AlignHCenter | Qt::AlignVCenter, text[i]);
            //    painter_waterfall.drawText(rect, Qt::AlignHCenter | Qt::AlignBottom, text[i]);
            xAxis = x;
        }
    }
    m_mutex.unlock();
    rect.setRect(this->width() - m_YAxisWidth - 5, -10, m_YAxisWidth, m_YAxisWidth);
    painter_overlay.drawText(rect, Qt::AlignHCenter | Qt::AlignVCenter, QString("dB"));

    rect.setRect(0, pixmap_height - m_YAxisWidth, m_YAxisWidth, m_YAxisWidth);
    painter_overlay.drawText(rect, Qt::AlignHCenter | Qt::AlignVCenter, QString("MHz"));

    rect.setRect(this->width() - m_YAxisWidth * 2 - 10, pixmap_height - m_YAxisWidth, m_YAxisWidth, m_YAxisWidth);
    painter_overlay.drawText(rect, Qt::AlignHCenter | Qt::AlignVCenter, QString("MHz"));
    //横纵坐标的交汇矩形
    painter_overlay.setPen(QColor(98, 108, 123));
    painter_overlay.fillRect(this->width() - m_YAxisWidth - 5, pixmap_height - 30, m_YAxisWidth + 5, m_YAxisWidth + 5, QColor(98, 108, 123));
    // Draw demod filter box

    if (m_FilterBoxEnabled) {
        m_DemodFreqX = xFromFreq(m_DemodCenterFreq);
        m_DemodLowCutFreqX = xFromFreq(m_DemodCenterFreq + m_DemodLowCutFreq);
        m_DemodHiCutFreqX = xFromFreq(m_DemodCenterFreq + m_DemodHiCutFreq);

        int dw = m_DemodHiCutFreqX - m_DemodLowCutFreqX;

        painter_overlay.setOpacity(0.3);
        painter_overlay.fillRect(m_DemodLowCutFreqX, 0, dw, pixmap_height,
            QColor(PLOTTER_FILTER_BOX_COLOR));

        //画纵轴复位线
        if (!m_peak)
        {
            painter_overlay.setOpacity(1.0);
            painter_overlay.setPen(QColor(PLOTTER_FILTER_LINE_COLOR));
            painter_overlay.drawLine(m_DemodFreqX, 0, m_DemodFreqX, pixmap_height - m_YAxisWidth);
        }

    }

    // 参考电平
    //auto h = m_2DPixmap.height();
    //auto cut_h = (m_PandMaxdB - m_PandMindB) * (m_xAxisHeight / (float)h);
    ////    m_referenceLevel = m_PandMindB + cut_h;

    //int levelHeight = ( -m_referenceLevel /(m_PandMaxdB - m_PandMindB)) * (h+m_xAxisHeight) ;

    //qDebug() << "m_referenceLevel:" << m_referenceLevel << "; levelHeight:" << levelHeight << endl;
  /*  painter_overlay.setOpacity(1.0);
    painter_overlay.setPen(QColor(PLOTTER_FILTER_LINE_COLOR));
    painter_overlay.drawLine(0, levelHeight, pixmap_width - m_YAxisWidth, levelHeight);*/

    m_mutex.lock();
    m_OverlayPixmap = OverlayPixmap;
    m_WaterfallPixmap = WaterfallPixmap;
    m_mutex.unlock();
    if (!m_Running) {
        // if not running so is no data updates to draw to screen
        // copy into 2Dbitmap the overlay bitmap.
        m_mutex.lock();
        m_2DPixmap = m_OverlayPixmap.copy(0, 0, pixmap_width, pixmap_height - m_xAxisHeight);

        m_mutex.unlock();
        update();
    }
    m_xAxisPixmap = m_OverlayPixmap.copy(0, pixmap_height - m_xAxisHeight, pixmap_width, m_xAxisHeight);
    painter_overlay.end();
    painter_waterfall.end();
}

// Create frequency division strings based on start frequency, span frequency,
// and frequency units.
// Places in QString array m_HDivText
// Keeps all strings the same fractional length
void CSpecPlotter::makeFrequencyStrs()
{
    QMutexLocker lock(&m_mutex);
    qint64  StartFreq = m_StartFreqAdj;
    float   freq;
    int     i, j;

    if ((1 == m_FreqUnits) || (m_FreqDigits == 0))
    {
        m_HDivText.clear();
        // if units is Hz then just output integer freq
        for (int i = 0; i <= m_HorDivs; i++)
        {
            freq = (float)StartFreq / (float)m_FreqUnits;
            m_HDivText.append(QString::number((int)freq));
            StartFreq += m_FreqPerDiv;
        }
        return;
    }
    // here if is fractional frequency values
    // so create max sized text based on frequency units
    m_HDivText.clear();
    for (int i = 0; i <= m_HorDivs; i++)
    {
        freq = (float)StartFreq / (float)m_FreqUnits;
        QString str;
        m_HDivText.append(str.setNum(freq, 'f', m_FreqDigits));
        StartFreq += m_FreqPerDiv;
    }
    // now find the division text with the longest non-zero digit
    // to the right of the decimal point.
    int max = 0;
    for (i = 0; i <= m_HorDivs; i++)
    {
        int dp = m_HDivText[i].indexOf('.');
        int l = m_HDivText[i].length() - 1;
        for (j = l; j > dp; j--)
        {
            if (m_HDivText[i][j] != '0')
                break;
        }
        if ((j - dp) > max)
            max = j - dp;
    }
    // truncate all strings to maximum fractional length
    StartFreq = m_StartFreqAdj;
    m_HDivText.clear();
    for (i = 0; i <= m_HorDivs; i++)
    {
        freq = (float)StartFreq / (float)m_FreqUnits;
        QString str;
        m_HDivText.append(str.setNum(freq, 'f', max));
        //m_HDivText[i].setNum(freq,'f', max);
        StartFreq += m_FreqPerDiv;
    }
}

// Convert from screen coordinate to frequency
int CSpecPlotter::xFromFreq(qint64 freq)
{
    int w = m_OverlayPixmap.width();
    qint64 StartFreq = m_CenterFreq + m_FftCenter - m_Span / 2;
    int x = (int)w * ((float)freq - StartFreq) / (float)m_Span;
    if (x < 0)
        return 0;
    if (x > (int)w)
        return m_OverlayPixmap.width();
    return x;
}

// Convert from frequency to screen coordinate
qint64 CSpecPlotter::freqFromX(int x)
{
    int w = m_OverlayPixmap.width();
    qint64 StartFreq = m_CenterFreq + m_FftCenter - m_Span / 2;
    qint64 f = (qint64)(StartFreq + (float)m_Span * (float)x / (float)w);
    return f;
}
// Convert from frequency to screen coordinate
qint64 CSpecPlotter::fftDataFromX(int x)
{
    int w = m_OverlayPixmap.width();
    qint64 StartFreq = m_CenterFreq + m_FftCenter - m_Span / 2;
    qint64 f = (qint64)(StartFreq + (float)m_Span * (float)x / (float)w);

    return f;
}

/** Calculate time offset of a given line on the waterfall */
quint64 CSpecPlotter::msecFromY(int y)
{
    // ensure we are in the waterfall region
    if (y < m_OverlayPixmap.height())
        return 0;

    int dy = y - m_OverlayPixmap.height();

    if (msec_per_wfline > 0)
        return tlast_wf_ms - dy * msec_per_wfline;
    else
        return tlast_wf_ms - dy * 1000 / fft_rate;
}
// Round frequency to click resolution value
qint64 CSpecPlotter::roundFreq(qint64 freq, int resolution)
{
    qint64 delta = resolution;
    qint64 delta_2 = delta / 2;
    if (freq >= 0)
        return (freq - (freq + delta_2) % delta + delta_2);
    else
        return (freq - (freq + delta_2) % delta - delta_2);
}

// Clamp demod freqeuency limits of m_DemodCenterFreq
void CSpecPlotter::clampDemodParameters()
{
    if (m_DemodLowCutFreq < m_FLowCmin)
        m_DemodLowCutFreq = m_FLowCmin;
    if (m_DemodLowCutFreq > m_FLowCmax)
        m_DemodLowCutFreq = m_FLowCmax;

    if (m_DemodHiCutFreq < m_FHiCmin)
        m_DemodHiCutFreq = m_FHiCmin;
    if (m_DemodHiCutFreq > m_FHiCmax)
        m_DemodHiCutFreq = m_FHiCmax;
}

void CSpecPlotter::setDemodRanges(int FLowCmin, int FLowCmax,
    int FHiCmin, int FHiCmax,
    bool symetric)
{
    m_FLowCmin = FLowCmin;
    m_FLowCmax = FLowCmax;
    m_FHiCmin = FHiCmin;
    m_FHiCmax = FHiCmax;
    m_symetric = symetric;
    clampDemodParameters();
    updateOverlay();
}

void CSpecPlotter::setCenterFreq(quint64 f)
{
    if ((quint64)m_CenterFreq == f)
        return;

    qint64 offset = m_CenterFreq - m_DemodCenterFreq;

    m_CenterFreq = f;
    m_DemodCenterFreq = m_CenterFreq - offset;
    zoomOnXAxis(1);
    update();
    updateOverlay();

    m_PeakHoldValid = false;
}

// Ensure overlay is updated by either scheduling or forcing a redraw
void CSpecPlotter::updateOverlay()
{
    if (m_Running)
        m_DrawOverlay = true;
    else
        drawOverlay();
}

/** Reset horizontal zoom to 100% and centered around 0. */
void CSpecPlotter::resetHorizontalZoom(void)
{
    setFftCenterFreq(0);
    setSpanFreq((qint32)m_SampleFreq);
}

void CSpecPlotter::resetVericalZoom()
{
    m_PandMaxdB = m_WfMaxdB = 0.f;
    m_PandMindB = m_WfMindB = -175.f;
    drawOverlay();
}

/** Center FFT plot around 0 (corresponds to center freq). */
void CSpecPlotter::moveToCenterFreq(void)
{
    setFftCenterFreq(0);
    updateOverlay();
    m_PeakHoldValid = false;
}

/** Center FFT plot around the demodulator frequency. */
void CSpecPlotter::moveToDemodFreq(void)
{
    setFftCenterFreq(m_DemodCenterFreq - m_CenterFreq);
    updateOverlay();

    m_PeakHoldValid = false;
}

/** Set FFT plot color. */
void CSpecPlotter::setFftPlotColor(const QColor color)
{
    m_FftColor = color;
    m_FftFillCol = color;
    m_FftFillCol.setAlpha(0x1A);
    m_PeakHoldColor = color;
    m_PeakHoldColor.setAlpha(60);
}

/** Enable/disable filling the area below the FFT plot. */
void CSpecPlotter::setFftFill(bool enabled)
{
    m_FftFill = enabled;
}

/** Set peak hold on or off. */
void CSpecPlotter::setPeakHold(bool enabled)
{
    m_PeakHoldActive = enabled;
    m_PeakHoldValid = false;
}

/**
 * Set peak detection on or off.
 * @param enabled The new state of peak detection.
 * @param c Minimum distance of peaks from mean, in multiples of standard deviation.
 */
void CSpecPlotter::setPeakDetection(bool enabled, float c)
{
    if (!enabled || c <= 0)
        m_PeakDetection = -1;
    else
        m_PeakDetection = c;
}

void CSpecPlotter::calcDivSize(qint64 low, qint64 high, int divswanted, qint64& adjlow, qint64& step, int& divs)
{
#ifdef PLOTTER_DEBUG
    qDebug() << "low: " << low;
    qDebug() << "high: " << high;
    qDebug() << "divswanted: " << divswanted;
#endif

    if (divswanted == 0)
        return;

    static const qint64 stepTable[] = { 1, 2, 5 };
    static const int stepTableSize = sizeof(stepTable) / sizeof(stepTable[0]);
    qint64 multiplier = 1;
    step = 1;
    divs = high - low;
    int index = 0;
    adjlow = (low / step) * step;

    while (divs > divswanted)
    {
        step = stepTable[index] * multiplier;
        divs = int((high - low) / step);
        adjlow = (low / step) * step;
        index = index + 1;
        if (index == stepTableSize)
        {
            index = 0;
            multiplier = multiplier * 10;
        }
    }
    if (adjlow < low)
        adjlow += step;

#ifdef PLOTTER_DEBUG
    qDebug() << "adjlow: " << adjlow;
    qDebug() << "step: " << step;
    qDebug() << "divs: " << divs;
#endif
}


