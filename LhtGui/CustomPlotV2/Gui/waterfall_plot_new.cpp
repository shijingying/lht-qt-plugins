#include "waterfall_plot_new.h"
#include "ui_waterfall_plot_new.h"


WaterfallPlotNew::WaterfallPlotNew(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::WaterfallPlotNew)
{
    ui->setupUi(this);
    m_waterPlot = new CPlotter(ui->widget);
    ui->verticalLayout->addWidget(m_waterPlot);
    m_waterPlot->setFftFill(true);
    m_waterPlot->setPeakHold(true);
    quint32 fs = 1.92e6;
    m_waterPlot->setSampleRate(fs);
    m_waterPlot->setSpanFreq(fs);
    m_waterPlot->setFftPlotColor(QColor(255, 255, 0));
    m_waterPlot->setRunningState(true);
    m_waterPlot->setFreqUnits(1000000);//MHz
    m_waterPlot->setFilterBoxEnabled(true);
    m_waterPlot->setHiLowCutFrequencies(0, 0);
    m_waterPlot->setCenterFreq(265000000);
    m_waterPlot->setDemodCenterFreq(265000000);
    m_waterPlot->clearWaterfall();
    m_waterPlot->clearMask();
    m_waterPlot->setFftFill(0);

}

WaterfallPlotNew::~WaterfallPlotNew()
{
    delete ui;
}

void WaterfallPlotNew::addNewData(std::shared_ptr<float> data, int len)
{
    m_waterPlot->NewFftData(data.get(),len,-130);
}
