#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QProcess>
#include "Control/control_logic.h"
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initUi();

    void initExe();

private slots:
    void slotNewLog( QString message);
private:
    Ui::MainWindow *ui;

    QWidget *   m_mianWidget;
    QWidget *   m_logWidget;
    QWidget *   m_leftWidget;
    QTextEdit *m_logEdit;
};
#endif // MAINWINDOW_H
