#ifndef CONTROL_LOGIC_H
#define CONTROL_LOGIC_H

#include <QObject>
#include <QProcess>
#include "QsLog/QsLog.h"
#include "Config/global_config.h"
using namespace QsLogging;

class ControlLogic : public QObject
{
    Q_OBJECT
public:
    static ControlLogic * getInstance(){
        return m_instance;
    }
    void init();

    void initExe(QWidget*,QWidget*);

    void freeAll();
signals:
    void signalNewLog( QString message);
public slots:
    void qsLogFunc(const QString &message, int level);
private:

    void initQsLog(QsLogging::Level level, const QString &logPath, long long maxSizeBytes, int maxOldLogCount);

private:
    static ControlLogic * m_instance;

    explicit ControlLogic(QObject *parent = nullptr);

    QMap<int,QWidget*> m_exeId;

    QList<QProcess *> m_processList;
};

#endif // CONTROL_LOGIC_H
