#include "control_logic.h"
#include <QCoreApplication>
#include <QFile>
#include <QPushButton>
#include <QLayout>
#include <QProcess>
#include <Windows.h>
#include <QWindow>
// 定义一个结构体来保存窗口信息
struct WindowInfo {
    HWND hwnd;
    std::wstring title;
};
// 回调函数，用于枚举窗口
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam) {
    int length = GetWindowTextLength(hwnd);
    if (length == 0) {
        return TRUE; // 跳过没有标题的窗口
    }

    std::vector<WindowInfo>& windows = *reinterpret_cast<std::vector<WindowInfo>*>(lParam);

    std::wstring title(length + 1, L'\0');
    GetWindowText(hwnd, &title[0], length + 1);
    title.resize(length); // 删除多余的空字符

    windows.push_back({ hwnd, title });
    return TRUE;
}
// 获取所有顶级窗口及其标题
std::vector<WindowInfo> GetAllWindows() {
    std::vector<WindowInfo> windows;
    EnumWindows(EnumWindowsProc, reinterpret_cast<LPARAM>(&windows));
    return windows;
}

ControlLogic * ControlLogic::m_instance = new ControlLogic;

ControlLogic::ControlLogic(QObject *parent)
    : QObject{parent}
{

}

void ControlLogic::init()
{
    //日志初始化
    initQsLog(InfoLevel,QCoreApplication::applicationDirPath() + "/log.txt",1024 * 100 ,10);
    //配置文件初始化
    GlobalConfig::getInstance()->init();
}

void ControlLogic::initExe(QWidget * m_leftWidget, QWidget *m_mianWidget)
{
    auto conf = GlobalConfig::getInstance()->getExeConf();

    for(auto it : conf.ExeConf){
        //判断程序有效性
        if(it.Effectiveness && QFile::exists(it.ProgramPath)){
            QPushButton * btn = new QPushButton();
            btn->setText(it.ProgramChineseName);
            btn->setToolTip(QString::number(it.index));
            connect(btn,&QPushButton::clicked,[=]{
                for(auto it : m_exeId){
                    it->hide();
                }
                int id = btn->toolTip().toInt();
                m_exeId[id]->show();
            });
            m_leftWidget->layout()->addWidget(btn);
            QProcess *pProcess = new QProcess;
            pProcess->setProgram(it.ProgramPath);
            pProcess->setArguments(it.ProgramAtgs);
            pProcess->start();
            pProcess->waitForStarted();
            m_processList.append(pProcess);
            QLOG_INFO()<<it.ProgramChineseName<<" loading";
            bool stop = 0;
            while(!stop){
                // if(it.index == 3){
                // std::vector<WindowInfo> windows = GetAllWindows();
                // for (const auto& window : windows) {
                //     qDebug() << L"Window Title: " << window.title << L" (HWND: " << window.hwnd << L")" ;
                // }
                // }
                HWND hwnd = FindWindow(NULL, LPCWSTR(it.ProgramChineseName.toStdWString().c_str()));
                if (hwnd != NULL) {
                    WId winId = reinterpret_cast<WId>(hwnd);
                    QWindow *window = QWindow::fromWinId(winId);
                    QWidget* widget = QWidget::createWindowContainer(window, m_mianWidget, Qt::Widget);
                    m_exeId[it.index] = widget;
                    m_mianWidget->layout()->addWidget(widget);
                    widget->hide();
                    QLOG_INFO() << it.ProgramChineseName << " load success";
                    stop = 1;
                }

            }

        }
    }
    m_exeId[1]->show();
}

void ControlLogic::freeAll()
{
    for(auto it : m_processList){
        if(it)
            it->close();
    }
}


void logFunction(const QString& message, QsLogging::Level level)
{

}

void ControlLogic::qsLogFunc(const QString &message, int level)
{
   //显示到界面上，或者其他功能
    if(level >= InfoLevel){
       signalNewLog(message);
   }
}

void ControlLogic::initQsLog(QsLogging::Level level, const QString &logPath, long long maxSizeBytes, int maxOldLogCount)
{
    using namespace QsLogging;
    // 1. init the logging mechanism
    qRegisterMetaType<QsLogging::Level>("QsLogging::Level");
    Logger& logger = Logger::instance();
    logger.setLoggingLevel(level);
    const QString sLogPath(logPath);

    // 2. add two destinations
    DestinationPtr fileDestination(DestinationFactory::MakeFileDestination(
        sLogPath, EnableLogRotation, MaxSizeBytes(maxSizeBytes), MaxOldLogCount(maxOldLogCount)));
    DestinationPtr debugDestination(DestinationFactory::MakeDebugOutputDestination());
    // DestinationPtr functorDestination(DestinationFactory::MakeFunctorDestination(&logFunction));//日志执行后函数             两种添加方法 第一种外部函数
    DestinationPtr functorDestination(DestinationFactory::MakeFunctorDestination(ControlLogic::getInstance(),SLOT(qsLogFunc(const QString& , int ))));//日志执行后函数       两种添加方法 第二种类函数,不能是线程类
    logger.addDestination(debugDestination);
    logger.addDestination(fileDestination);
    logger.addDestination(functorDestination);

}
