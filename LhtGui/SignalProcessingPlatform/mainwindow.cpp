#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QProcess>
#include <QPushButton>
#include <QSplitter>
#include <QTextEdit>
#include <QTimer>
#include <QWindow>
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ControlLogic::getInstance()->init();

    initUi();

    initExe();
}

MainWindow::~MainWindow()
{
    ControlLogic::getInstance()->freeAll();

    delete ui;
}

void MainWindow::initUi()
{
    m_mianWidget = new QWidget;
    m_mianWidget->setStyleSheet("border:1px solid black");
    m_logWidget = new QWidget;
    m_logEdit = new QTextEdit(m_logWidget);
    // logEdit->setText("Log Widget");
    // m_logEdit->setReadOnly(1);
    QVBoxLayout *logWidgetLayout = new QVBoxLayout(m_logWidget);
    logWidgetLayout->addWidget(m_logEdit);
    m_logWidget->setStyleSheet("border:1px solid black");
    m_leftWidget = new QWidget;
    m_leftWidget->setStyleSheet("border:1px solid black  ");
    QVBoxLayout  *leftWidgetLayout = new QVBoxLayout(m_leftWidget);
    m_leftWidget->setLayout(leftWidgetLayout);
    QSplitter *splitter1 = new QSplitter(Qt::Horizontal);
    QSplitter *splitter = new QSplitter(Qt::Vertical);
    QVBoxLayout  *mainWidgetLayout = new QVBoxLayout(m_mianWidget);
    m_mianWidget->setLayout(mainWidgetLayout);
    splitter->addWidget(m_mianWidget);
    splitter->addWidget(m_logWidget);
    // 设置小部件的占比
    splitter->setStretchFactor(0, 5); // mainWidget 占比 3
    splitter->setStretchFactor(1, 1); // logWidget 占比 1
    splitter1->addWidget(m_leftWidget);
    splitter1->addWidget(splitter);
    m_leftWidget->setMaximumWidth(200);
    m_logWidget->setMaximumHeight(200);
    ui->verticalLayout->addWidget(splitter1);

    connect(ControlLogic::getInstance(),&ControlLogic::signalNewLog,this,&MainWindow::slotNewLog);
}

void MainWindow::initExe()
{
    ControlLogic::getInstance()->initExe(m_leftWidget,m_mianWidget);
}

void MainWindow::slotNewLog(QString message)
{
    m_logEdit->append(message);
}
