#ifndef GLOBAL_DEFINE_H
#define GLOBAL_DEFINE_H

#include <QObject>
#include <QList>
#include <QPair>
#include <QMap>

struct TzExeInfo
{
    int index;
    //程序中文名
    QString ProgramChineseName;
    //程序路径
    QString ProgramPath;
    //程序启动参数
    QStringList ProgramAtgs;
    //程序有效性
    bool Effectiveness;
};
struct TzExeConfig{
    //程序配置
    QMap<int,TzExeInfo> ExeConf;

    int size;
};

#endif // GLOBAL_DEFINE_H
