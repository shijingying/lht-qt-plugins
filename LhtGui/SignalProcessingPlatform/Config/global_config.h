#ifndef GLOBALCONFIG_H
#define GLOBALCONFIG_H

#include <QObject>
#include <QSettings>
#include "global_define.h"
/*
 * @brief 全局配置文件
 */
class GlobalConfig : public QObject
{
    Q_OBJECT
public:
    static GlobalConfig * getInstance(){
        return _instance;
    }

    void init(QString path = "");

    void setKeyValue(QString group , QString key, QVariant value);

    TzExeConfig getExeConf(){
        return m_exeConf;
    }

signals:


private:

    static GlobalConfig* _instance;
    explicit GlobalConfig(QObject *parent = nullptr);

    void initLog();

    void initExeConf();
private:
    QSettings   *       m_settings = nullptr;
    QString             m_configPath;
    TzExeConfig         m_exeConf;
};

#endif // GLOBALCONFIG_H
