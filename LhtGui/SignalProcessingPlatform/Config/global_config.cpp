#include "global_config.h"
#include <QCoreApplication>
GlobalConfig* GlobalConfig::_instance = new  GlobalConfig;

void GlobalConfig::init(QString path)
{
    if(path.isEmpty()){
        m_configPath = QCoreApplication::applicationDirPath()+"/config/Config.ini";
    }
    if(m_settings){
        delete m_settings;
    }
    m_settings = new QSettings(m_configPath,QSettings::IniFormat);

    initExeConf();
}

void GlobalConfig::setKeyValue(QString group, QString key, QVariant value)
{
    m_settings->setValue(group+"/"+key,value);
}

GlobalConfig::GlobalConfig(QObject *parent) : QObject(parent)
{

}

void GlobalConfig::initLog()
{

}

void GlobalConfig::initExeConf()
{
    auto keys = m_settings->childGroups();
    for(auto it : keys){
        m_settings->beginGroup(it);
        TzExeInfo conf;
        conf.index = m_settings->value("index").toInt();
        conf.ProgramChineseName = QString::fromUtf8(m_settings->value("ProgramChineseName").toString().toUtf8());
        conf.ProgramPath = m_settings->value("ProgramPath").toString();
        auto list = m_settings->value("ProgramAtgs").toString().split(",");
        for(auto it : list){
            it = it.replace(" ","");
            if(!it.isEmpty()){
                conf.ProgramAtgs.append(it);
            }
        }

        conf.Effectiveness = m_settings->value("Effectiveness").toBool();
        m_exeConf.ExeConf[conf.index] = conf;
        m_settings->endGroup();
    }

}
