#include "local_address_interface.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QNetworkInterface>
#include <QHostAddress>
#include <QList>
#include <QEventLoop>

LocalAddressInterface::LocalAddressInterface()
{}

QString LocalAddressInterface::getLocalIPAddress() {
    // 获取所有网络接口
    QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();

    // 遍历所有接口，找出有效的 IPv4 地址
    for (const QNetworkInterface &interface : allInterfaces) {
        QList<QNetworkAddressEntry> entries = interface.addressEntries();
        for (const QNetworkAddressEntry &entry : entries) {
            QHostAddress ipAddress = entry.ip();
            if (ipAddress.protocol() == QAbstractSocket::IPv4Protocol &&
                ipAddress != QHostAddress(QHostAddress::LocalHost)) {
                return ipAddress.toString();
            }
        }
    }
    return QString();
}

void LocalAddressInterface::fetchLocationFromIP() {
    auto ip = getLocalIPAddress();

    QNetworkAccessManager *manager = new QNetworkAccessManager();
    QNetworkRequest request(QUrl("http://ip-api.com/json/"));
    // QEventLoop loop;
    qRegisterMetaType<LocalAddressInfo>("LocalAddressInfo");
    QObject::connect(manager, &QNetworkAccessManager::finished, [&](QNetworkReply *reply) {
        if (reply->error() == QNetworkReply::NoError) {
            QByteArray response = reply->readAll();
            QJsonDocument jsonDoc = QJsonDocument::fromJson(response);
            QJsonObject jsonObj = jsonDoc.object();

            QString city = jsonObj["city"].toString();
            QString region = jsonObj["regionName"].toString();
            QString country = jsonObj["country"].toString();
            LocalAddressInfo info;
            info.city = city;
            info.region = region;
            info.country = country;
            qDebug() << "City:" << city;
            qDebug() << "Region:" << region;
            qDebug() << "Country:" << country;
            signLocation(info);
        } else {
            qDebug() << "Error:" << reply->errorString();
        }
        reply->deleteLater();
        // loop.quit();
    });
    manager->get(request);
    // loop.exec();
}
