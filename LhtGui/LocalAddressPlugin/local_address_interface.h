#ifndef LOCAL_ADDRESS_INTERFACE_H
#define LOCAL_ADDRESS_INTERFACE_H

#include <QObject>
#include <iostream>

struct LocalAddressInfo
{
    QString city;
    QString region;
    QString country;

};

class LocalAddressInterface;
typedef std::shared_ptr<LocalAddressInterface> LocalAddressInterfacePtr;

class LocalAddressInterface : public QObject,
                              public std::enable_shared_from_this<LocalAddressInterface>
{
    Q_OBJECT
public:
    static std::shared_ptr<LocalAddressInterface> CreateInterface(){
        return std::shared_ptr<LocalAddressInterface>(new LocalAddressInterface());
    }
    //获取本地城市等信息
    void fetchLocationFromIP();

signals:
    void signLocation(LocalAddressInfo);
private:
    explicit LocalAddressInterface();
    QString getLocalIPAddress();
};


#endif // LOCAL_ADDRESS_INTERFACE_H
