#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <complex>
#include <QFile>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initPlot();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_draw_btn_clicked()
{
    // ui->widget->addData(inputfI + 1000,inputfQ+ 1000,len - 1000);
    ui->widget->addEyePatternData(inputfI + 1000,inputfQ+ 1000,2048);
}

void MainWindow::initPlot()
{
    QFile file("E:/git/lht-qt-plugins/LhtGui/EyePattern/build/Desktop_Qt_5_15_2_MSVC2019_64bit-Debug/debug/QPSK_Symbol_syschronize4M_1K_20dB.dat");

    file.open(QIODevice::ReadOnly);

    char * input = new char[512e3];
    int readLen = 40960;
    file.read(input,readLen);

    float * inputf = (float*)input;
    inputfI = new float[512e3];
    inputfQ = new float[512e3];
    auto len = readLen / 8;
    for(int i = 0 ; i < len ; i++){
        inputfI[i] = inputf[ 2 * i] + 1;
        inputfQ[i] = inputf[ 2 * i + 1] + 1;
    }

    ui->widget->init(3);
}

