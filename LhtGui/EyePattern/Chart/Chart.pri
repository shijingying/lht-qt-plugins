QT       += charts printsupport
QT       += printsupport opengl
DEFINES += QCUSTOMPLOT_USE_OPENGL

HEADERS += \
    $$PWD/QCustomPlot/include/GL/freeglut.h \
    $$PWD/QCustomPlot/include/GL/freeglut_ext.h \
    $$PWD/QCustomPlot/include/GL/freeglut_std.h \
    $$PWD/QCustomPlot/include/GL/freeglut_ucall.h \
    $$PWD/QCustomPlot/include/GL/glut.h

CONFIG(debug, debug | release) {
    LIBS += -L$$PWD/QCustomPlot/lib/debug -lfreeglut_staticd
    LIBS += -L$$PWD/QCustomPlot/lib/debug -lfreeglutd
}

CONFIG(release, debug | release) {
    LIBS += -L$$PWD/QCustomPlot/lib/release -lfreeglut_static
    LIBS += -L$$PWD/QCustomPlot/lib/release -lfreeglut
}


HEADERS += \
    $$PWD/QCustomPlot/qcustomplot.h \
    $$PWD/StarMap/eye_pattern_plot.h

SOURCES += \
    $$PWD/QCustomPlot/qcustomplot.cpp \
    $$PWD/StarMap/eye_pattern_plot.cpp

FORMS += \
    $$PWD/StarMap/eye_pattern_plot.ui
