#ifndef CONSTELLATION_PLOT_H
#define CONSTELLATION_PLOT_H

#include <QWidget>
#include <QMainWindow>
#include "../QCustomPlot/qcustomplot.h"
#include <QApplication>
#include <QMainWindow>
#include <QRandomGenerator>

namespace Ui {
class StarMapPlot;
}

class StarMapPlot : public QWidget
{
    Q_OBJECT

public:
    explicit StarMapPlot(QWidget *parent = nullptr);
    ~StarMapPlot();

    void addEyePatternData(float* iData,float *qData,int len,int step = 3);
    /**
    * @author lee
    * @param low 设置坐标值 结果为  -low 到  low
    */
    void init(int low );
    void test();
private slots:
    void OnPlotClick(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *event);
private:
    Ui::StarMapPlot *ui;
    QCPItemText *TextTip;
    int m_low;
    bool m_isInit = false;
};

#endif // CONSTELLATION_PLOT_H
