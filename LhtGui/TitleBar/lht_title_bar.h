#ifndef LHT_TITLE_BAR_H
#define LHT_TITLE_BAR_H

#include <QMainWindow>

class LhtTitleBar : public QMainWindow
{
    Q_OBJECT
public:
    explicit LhtTitleBar(QWidget *parent = nullptr);

signals:

private:
    void setWidgetBorderless(const QWidget *widget);

    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
};

#endif // LHT_TITLE_BAR_H
