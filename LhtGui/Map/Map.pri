HEADERS += \
    $$PWD/CefView/CefViewWidget.h \
    $$PWD/CefView/DownloadManager.h \
    $$PWD/map_interface.h

SOURCES += \
    $$PWD/CefView/CefViewWidget.cpp \
    $$PWD/CefView/DownloadManager.cpp \
    $$PWD/map_interface.cpp

CONFIG += c++17

DEFINES += WIN32
DEFINES += _WINDOWS
DEFINES += UNICODE
DEFINES += _UNICODE
DEFINES += _SILENCE_CXX17_RESULT_OF_DEPRECATION_WARNING=1
DEFINES += OS_WINDOWS=1

# CONFIG(debug,debug|release){
#     OBJECTS_DIR = $$PWD/build/debug_output/obj/
#     MOC_DIR = $$PWD/build/debug_output/moc/
#     RCC_DIR = $$PWD/build/debug_output/rcc/
#     UI_DIR = $$PWD/build/debug_output/ui/
# }else{
#     OBJECTS_DIR = $$PWD/build/release_output/obj/
#     MOC_DIR = $$PWD/build/release_output/moc/
#     RCC_DIR = $$PWD/build/release_output/rcc/
#     UI_DIR = $$PWD/build/release_output/ui/
# }

LIBS += -L$$PWD/lib/debug/ -lQCefView
LIBS += -lkernel32 -luser32 -lgdi32 -lwinspool -lshell32 -lole32 -loleaut32 -luuid -lcomdlg32 -ladvapi32


DEPENDPATH  += $$PWD/include
INCLUDEPATH += $$PWD/include

