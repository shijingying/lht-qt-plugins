#include "map_interface.h"
#include <QLayout>
void MapInterface::setParent(QWidget *parent)
{
    _parent = parent;
    if(_parent && _parent->layout())
        _parent->layout()->addWidget(m_mainCefViewWidget);
}

void MapInterface::setUrl(const QString &path)
{
    _path = path;
    init(_path);
}

void MapInterface::reload()
{

}

MapInterface::MapInterface(QWidget *parent)
    : QWidget{parent}
{}

void MapInterface::init(const QString &path)
{
    if (m_mainCefViewWidget) {
        m_mainCefViewWidget->deleteLater();
        m_mainCefViewWidget = nullptr;
    }
    QCefSetting setting;

#if CEF_VERSION_MAJOR < 100
    setting.setPlugins(false);
#endif

    setting.setWindowlessFrameRate(60);
    setting.setBackgroundColor(QColor::fromRgba(qRgba(255, 255, 220, 255)));

    m_mainCefViewWidget = new CefViewWidget(path, &setting, this);

    if(_parent && _parent->layout())
        _parent->layout()->addWidget(m_mainCefViewWidget);

    // allow show context menu for both OSR and NCW mode
    m_mainCefViewWidget->setContextMenuPolicy(Qt::DefaultContextMenu);
}
