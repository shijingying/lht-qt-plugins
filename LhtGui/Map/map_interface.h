#ifndef MAP_INTERFACE_H
#define MAP_INTERFACE_H

#include <QObject>
#include <iostream>

#include "CefView/CefViewWidget.h"

class MapInterface;
typedef std::shared_ptr<MapInterface> MapInterfacePtr;

class MapInterface: public QWidget,
                     public std::enable_shared_from_this<MapInterface>
{
    Q_OBJECT
public:

    static MapInterfacePtr CreateInterface(){
        return std::shared_ptr<MapInterface>(new MapInterface());
    }
    void setParent(QWidget * parent);

    void setUrl(const QString& path);

    void reload();


private:
    explicit MapInterface(QWidget *parent = nullptr);

    void init(const QString & path);
private:
    CefViewWidget* m_mainCefViewWidget = nullptr;

    QString _path;

    QWidget * _parent = nullptr;
signals:
};

#endif // MAP_INTERFACE_H
