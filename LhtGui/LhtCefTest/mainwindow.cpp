#include "mainwindow.h"
#include "ui_mainwindow.h"

// #include <QCefContext.h>
#include <QDir>
#include <QDebug>
#define URL_ROOT "http://QCefViewDoc"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// void MainWindow::init(QString path)
// {
//     if(view != nullptr){
//         view->deleteLater();
//         view = nullptr;
//     }
//     view = new QWebEngineView(ui->widget);
//     view->resize(1920, 1080); // 设置窗口大小
//     view->page()->load(QUrl(path));  //打开百度
//     view->show(); // 显示视图
// }


void MainWindow::on_pushButton_clicked()
{
    auto str = ui->lineEdit->text();
    init(str);
}
void MainWindow::init(QString path)
{
    if (m_pRightCefViewWidget) {
        m_pRightCefViewWidget->deleteLater();
        m_pRightCefViewWidget = nullptr;
    }
    QCefSetting setting;

#if CEF_VERSION_MAJOR < 100
    setting.setPlugins(false);
#endif

    setting.setWindowlessFrameRate(60);
    setting.setBackgroundColor(QColor::fromRgba(qRgba(255, 255, 220, 255)));

    m_pRightCefViewWidget = new CefViewWidget(path, &setting, this);

    ui->widget->layout()->addWidget(m_pRightCefViewWidget);

    // allow show context menu for both OSR and NCW mode
    m_pRightCefViewWidget->setContextMenuPolicy(Qt::DefaultContextMenu);
}


// void MainWindow::on_pushButton_clicked()
// {
//     auto str = ui->lineEdit->text();
//     init(str);
// }
