#include "lht_tableview_quick_widget.h"
#include <QQmlEngine>
#include <QQmlComponent>

LhtTableviewQuickWidget::LhtTableviewQuickWidget(QWidget *parent)
    : QQuickWidget(parent) {
    // Load the QML file
    setSource(QUrl(QStringLiteral("../../LhtTableViewComponent/lht_tableview_component.qml")));
    setResizeMode(QQuickWidget::SizeRootObjectToView);
}

// Wrapper functions for QML methods
void LhtTableviewQuickWidget::setRowCount(int rowCount) {
    if (QQuickItem *tableView = rootObject()) {
        QMetaObject::invokeMethod(tableView, "setRowCount", Q_ARG(QVariant, rowCount));
    }
}

void LhtTableviewQuickWidget::setColumnCount(int columnCount) {
    if (QQuickItem *tableView = rootObject()) {
        QMetaObject::invokeMethod(tableView, "setColumnCount", Q_ARG(QVariant, columnCount));
    }
}

void LhtTableviewQuickWidget::setCellData(int row, int column, const QString &data) {
    if (QQuickItem *tableView = rootObject()) {
        QMetaObject::invokeMethod(tableView, "setCellData", Q_ARG(QVariant, row), Q_ARG(QVariant, column), Q_ARG(QVariant, data));
    }
}

QVariant LhtTableviewQuickWidget::copyRow(int row) {
    QVariant copiedRow;
    if (QQuickItem *tableView = rootObject()) {
        QMetaObject::invokeMethod(tableView, "copyRow", Q_RETURN_ARG(QVariant, copiedRow), Q_ARG(QVariant, row));
    }
    return copiedRow;
}

void LhtTableviewQuickWidget::assignRows(int startRow, const QVariantList &rowsData) {
    if (QQuickItem *tableView = rootObject()) {
        QMetaObject::invokeMethod(tableView, "assignRows", Q_ARG(QVariant, startRow), Q_ARG(QVariant, rowsData));
    }
}
