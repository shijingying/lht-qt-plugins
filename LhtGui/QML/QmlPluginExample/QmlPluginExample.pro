QT       += core gui quickwidgets qml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17


QT += gui quick qml quickcontrols2

RESOURCES += Res.qrc

include(LhtTableViewComponent/src/TaoQuick.pri)
include(3rdparty/3rdparty.pri)

# DEFINES +=TaoQuickShowPath=\\\"file:///$${path}/\\\"
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    DeviceAddTable/DeviceAddItem.cpp \
    DeviceAddTable/DeviceAddModel.cpp \
    lht_tableview_quick_widget.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    DeviceAddTable/DeviceAddItem.h \
    DeviceAddTable/DeviceAddModel.h \
    lht_tableview_quick_widget.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    LhtTableViewComponent/lht_tableview_component.qml
