#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickView>
#include "DeviceAddTable/DeviceAddModel.h"
#include "Trans/Trans.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QQuickView		view;
    view.engine()->addImportPath(TaoQuickImportPath);
    view.rootContext()->setContextProperty("taoQuickImportPath", TaoQuickImportPath);

    DeviceAddModel model;
    view.rootContext()->setContextProperty("deviceAddModel", &model);

    // view.setSource(QUrl("qrc:/main.qml"));
    view.setSource(QUrl("qrc:/LhtTableViewComponent/lht_tableview_component.qml"));
    view.show();

    QString taoQuickShowPath = "E:/git/lht-qt-plugins/LhtGui/QML/QmlPluginExample/";
    QString transDir		 = taoQuickShowPath + "Trans/";
    if (transDir.startsWith("file:///"))
    {
        transDir = transDir.remove("file:///");
    }
    if (transDir.startsWith("qrc:/"))
    {
        transDir = transDir.remove("qrc");
    }
    Trans	  trans;
    view.setMinimumSize({ 800, 600 });
    view.resize(1440, 960);
    trans.beforeUiReady(view.rootContext(), transDir);

    return app.exec();
}
