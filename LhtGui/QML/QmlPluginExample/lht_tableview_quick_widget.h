#ifndef LHT_TABLEVIEW_QUICK_WIDGET_H
#define LHT_TABLEVIEW_QUICK_WIDGET_H

#include <QQuickWidget>
#include <QQuickItem>
#include <QQmlContext>

class LhtTableviewQuickWidget : public QQuickWidget
{
public:
    LhtTableviewQuickWidget(QWidget *parent = nullptr);

    // Expose the QML methods to C++
    Q_INVOKABLE void setRowCount(int rowCount);
    Q_INVOKABLE void setColumnCount(int columnCount);
    Q_INVOKABLE void setCellData(int row, int column, const QString &data);
    Q_INVOKABLE QVariant copyRow(int row);
    Q_INVOKABLE void assignRows(int startRow, const QVariantList &rowsData);

};

#endif // LHT_TABLEVIEW_QUICK_WIDGET_H
