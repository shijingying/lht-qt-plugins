#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initTableView();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initTableView()
{
    ui->widget_tableview->show();
    ui->widget_tableview->setRowCount(5);
    ui->widget_tableview->setColumnCount(3);
    ui->widget_tableview->setCellData(0, 0, "Row 1, Col 1");
    ui->widget_tableview->setCellData(0, 1, "Row 1, Col 2");
    ui->widget_tableview->setCellData(0, 2, "Row 1, Col 3");
}
