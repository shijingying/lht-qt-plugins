HEADERS += \
    $$PWD/../LhtIPPS/Ipps/Interface/computing_interface.h \
    $$PWD/CustomPlotV2/Gui/bookmarks.h \
    $$PWD/CustomPlotV2/Gui/comm_spectrum_widget.h \
    $$PWD/CustomPlotV2/Gui/communication_define.h \
    $$PWD/CustomPlotV2/Gui/eye_pattern_plot.h \
    $$PWD/CustomPlotV2/Gui/spec_plot.h \
    $$PWD/CustomPlotV2/Gui/spec_plot_new.h \
    $$PWD/CustomPlotV2/Gui/star_map_plot.h \
    $$PWD/CustomPlotV2/Gui/waterfall_plot.h \
    $$PWD/CustomPlotV2/Gui/waterfall_plot_new.h \
    $$PWD/DrawLine/shot_label.h \
    $$PWD/DrawLine/shot_merge_label.h \
    $$PWD/DrawLine/shot_polygon_label.h \
    $$PWD/Progress/progress_of_the_barrel.h \
    $$PWD/Progress/spinning_round.h \
    $$PWD/TableView/single_cell_header.h \
    $$PWD/TableView/single_header.h \
    $$PWD/TableView/table.h \
    $$PWD/TableView/table_header.h

SOURCES += \
    $$PWD/../LhtIPPS/Ipps/Interface/computing_interface.cpp \
    $$PWD/CustomPlotV2/Gui/bookmarks.cpp \
    $$PWD/CustomPlotV2/Gui/comm_spectrum_widget.cpp \
    $$PWD/CustomPlotV2/Gui/eye_pattern_plot.cpp \
    $$PWD/CustomPlotV2/Gui/spec_plot.cpp \
    $$PWD/CustomPlotV2/Gui/spec_plot_new.cpp \
    $$PWD/CustomPlotV2/Gui/star_map_plot.cpp \
    $$PWD/CustomPlotV2/Gui/waterfall_plot.cpp \
    $$PWD/CustomPlotV2/Gui/waterfall_plot_new.cpp \
    $$PWD/DrawLine/shot_label.cpp \
    $$PWD/DrawLine/shot_merge_label.cpp \
    $$PWD/DrawLine/shot_polygon_label.cpp \
    $$PWD/Progress/progress_of_the_barrel.cpp \
    $$PWD/Progress/spinning_round.cpp \
    $$PWD/TableView/single_cell_header.cpp \
    $$PWD/TableView/table.cpp \
    $$PWD/TableView/table_header.cpp

FORMS += \
    $$PWD/CustomPlotV2/Gui/comm_spectrum_widget.ui \
    $$PWD/CustomPlotV2/Gui/eye_pattern_plot.ui \
    $$PWD/CustomPlotV2/Gui/spec_plot_new.ui \
    $$PWD/CustomPlotV2/Gui/star_map_plot.ui \
    $$PWD/CustomPlotV2/Gui/waterfall_plot_new.ui \
    $$PWD/TableView/single_cell_header.ui

include($$PWD/TitleBar/TitleBar.pri)
include($$PWD/LocalAddressPlugin/LocalAddressPlugin.pri)
include($$PWD/Map/Map.pri)
include($$PWD/CustomPlotV2/QCustomPlot/QCustomPlot.pri)


LIBS += -L$$PWD/lib
LIBS += -lipps -lippcore -lippvm

INCLUDEPATH += $$PWD//include/ipp
DEPENDPATH += $$PWD//include/ipp

RESOURCES += \
    $$PWD/qrc/resource.qrc

