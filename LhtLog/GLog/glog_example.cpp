#include "glog_example.h"
#include "glog/logging.h"

GLogExample::GLogExample() {
}

void GLogExample::GLogInit()
{
    FLAGS_log_dir = "D:\\work\\TzTest\\TzSpecDemo\\Logs";
    google::InitGoogleLogging("log");
    google::SetLogDestination(google::GLOG_INFO, "D:\\work\\TzTest\\TzSpecDemo\\Logs\\INFO_");
    google::SetLogDestination(google::GLOG_WARNING, "D:\\work\\TzTest\\TzSpecDemo\\Logs\\WARNING_");
    google::SetLogDestination(google::GLOG_ERROR, "D:\\work\\TzTest\\TzSpecDemo\\Logs\\ERROR_");
    google::SetStderrLogging(google::GLOG_INFO);
    // google::SetLogFilenameExtension("niuniu");
    FLAGS_colorlogtostderr = true;  // Set log color
    FLAGS_logbufsecs = 0;  // Set log output speed(s)
    FLAGS_max_log_size = 1024;  // Set max log file size
    FLAGS_stop_logging_if_full_disk = true;  // If disk is full
    char str[20] = "hello log!";
    LOG(INFO) << str;
    std::string cStr = "hello google!";
    LOG(INFO) << cStr;
    LOG(INFO) << "info test" << "hello log!";  //输出一个Info日志
    LOG(WARNING) << "warning test";  //输出一个Warning日志
    LOG(ERROR) << "error test";  //输出一个Error日志

}
