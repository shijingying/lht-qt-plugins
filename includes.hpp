#ifndef INCLUDES_HPP
#define INCLUDES_HPP

#include "LhtExamples/lht_examples.h"
#include "LhtGui/TitleBar/lht_title_bar.h"
#include "LhtGui/DrawLine/shot_polygon_label.h"
#include "LhtGui/LocalAddressPlugin/local_address_interface.h"
#include "LhtGui/Map/map_interface.h"

#include "LhtExamples/custom_plot_examples.h"

const static QStringList FilterList{
    "ceshi",
    "niudingding",
    "askdhkhas",
    "leehuitao",
    "wenzhuang",
    "zhouzhengtao",
    "zhuxue",
    "leeniuniu",
    "wangkelei",
    "wangyi"
};

#endif // INCLUDES_HPP
