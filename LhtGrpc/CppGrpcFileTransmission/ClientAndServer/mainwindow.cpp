#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtConcurrent>
#include <QFutureWatcher>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // 连接客户端的进度更新信号到 MainWindow 的槽函数
    connect(&m_client, &TestClient::signProgressUpdate, this, &MainWindow::onClientProgressUpdate);
    connect(&m_client, &TestClient::signSpeedUpdate, this, &MainWindow::onSpeedUpdate);
    QThread::connect(&m_service, &TestService::signProgressUpdate, this, &MainWindow::onServiceProgressUpdate);
}

MainWindow::~MainWindow()
{
    m_service.quit();
    m_service.wait(100);
    delete ui;
}

void MainWindow::on_start_service_btn_clicked()
{
    m_service.startService();
}


void MainWindow::on_start_client_btn_clicked()
{
    auto path = ui->lineEdit->text().toStdString();

    auto ip = ui->ip_line->text().toStdString();
    auto watch = new QFutureWatcher<void>(this);

    QFuture<void> future = QtConcurrent::run([=]{
        m_client.connectServer(ip);
        m_client.sendFileInfo(path);
        m_client.startTransmission();
    });


    // 监控任务完成信号
    connect(watch, &QFutureWatcher<void>::finished, this, [=]() {

        // QMessageBox::information(this, "Info", "File transmission complete.");
        watch->deleteLater();
    });

    watch->setFuture(future);

}

void MainWindow::onServiceProgressUpdate(int progress)
{
    ui->recv_progressBar->setValue(progress);
}

void MainWindow::onClientProgressUpdate(int progress)
{
    ui->send_progressBar->setValue(progress);
}

void MainWindow::onSpeedUpdate(float speed)
{
    ui->speed_label->setText(QString::number(speed) + " MB/S");
}
