#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "Pri/Client/test_client.h"
#include "Pri/Server/test_service.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected slots:
    void onServiceProgressUpdate(int progress);
    void onClientProgressUpdate(int progress);
    void onSpeedUpdate(float speed);
private slots:
    void on_start_service_btn_clicked();

    void on_start_client_btn_clicked();

private:
    Ui::MainWindow *ui;

    TestClient m_client;
    TestService m_service;

};
#endif // MAINWINDOW_H
