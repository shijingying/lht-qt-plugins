#ifndef GRPC_DEFINE_H
#define GRPC_DEFINE_H

#include "proto/TransmissionFile.grpc.pb.h"
#include <grpcpp/grpcpp.h>
#include <iostream>
#include <fstream>
#ifdef UseQtObject
#include <QObject>
#include <QThread>
#endif

#define FILE_DEFAULT_PATH "D:/data/"
#define BLOCK_SIZE 3*1024*1024

struct GrpcInterfaceCallBack : public QThread
{
    virtual void ProgressUpdate(int progress) = 0;

    virtual void SpeedUpdate(float speed){}
};


#endif // GRPC_DEFINE_H
