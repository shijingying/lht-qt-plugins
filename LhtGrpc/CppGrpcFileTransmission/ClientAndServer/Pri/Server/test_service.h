#ifndef TEST_SERVICE_H
#define TEST_SERVICE_H

#include <QObject>
#include <QThread>
#include "../grpc_service_interface.h"

class TestService :public GrpcInterfaceCallBack
{
    Q_OBJECT
public:
    explicit TestService();

    void startService();

    ~TestService();

signals:
    void signProgressUpdate(int progress);
protected:
    void run();
private:
    GrpcServiceInterfacePtr m_service;

    // GrpcInterfaceCallBack interface
public:
    void ProgressUpdate(int progress);
};

#endif // TEST_SERVICE_H
