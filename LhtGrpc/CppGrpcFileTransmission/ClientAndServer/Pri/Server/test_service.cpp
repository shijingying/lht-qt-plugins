#include "test_service.h"
#include <QDebug>
TestService::TestService()
{

}

void TestService::startService()
{
    this->start();
}

TestService::~TestService()
{
    if(m_service){
        qDebug()<<"start Destroyed";
        m_service->stopService();
        qDebug()<<"Destroyed end";
    }
    quit();
    wait(100);
}

void TestService::run()
{
    m_service = GrpcServiceInterface::CreateService1(this);
    m_service->startService();
}

void TestService::ProgressUpdate(int progress)
{
    signProgressUpdate(progress);
    qDebug()<<"service progress = "<<progress;
}
