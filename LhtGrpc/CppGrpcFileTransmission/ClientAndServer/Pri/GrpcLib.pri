DEFINES += UseQtObject

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

DISTFILES += $$PWD/proto/TransmissionFile.proto
HEADERS += $$PWD/proto/TransmissionFile.pb.h \
    $$PWD/Client/test_client.h \
    $$PWD/Server/test_service.h \
    $$PWD/grpc_client_interface.h \
    $$PWD/grpc_define.h \
    $$PWD/grpc_service_interface.h
SOURCES += $$PWD/proto/TransmissionFile.pb.cc \
    $$PWD/Client/test_client.cpp \
    $$PWD/Server/test_service.cpp \
    $$PWD/grpc_client_interface.cpp \
    $$PWD/grpc_service_interface.cpp
HEADERS += $$PWD/proto/TransmissionFile.grpc.pb.h
SOURCES += $$PWD/proto/TransmissionFile.grpc.pb.cc

CONFIG(debug, debug|release) {
    LIBS += -L$$PWD/lib/debug
    LIBS += zlibd.lib
    LIBS += libprotobufd.lib
} else {
    LIBS += -L$$PWD/lib/release
    LIBS += zlib.lib
    LIBS += libprotobuf.lib
}

#关于grpc的安装  windows下https://blog.csdn.net/weixin_44229257/article/details/124104808

LIBS += absl_flags_parse.lib
LIBS += grpc++_reflection.lib
LIBS += grpc++.lib
LIBS += grpc.lib
LIBS += upb.lib
LIBS += utf8_range.lib
LIBS += re2.lib
LIBS += cares.lib
LIBS += gpr.lib
LIBS += libssl.lib
LIBS += libcrypto.lib
LIBS += address_sorting.lib
LIBS += absl_flags_usage.lib
LIBS += absl_flags_usage_internal.lib
LIBS += absl_flags_internal.lib
LIBS += absl_flags_marshalling.lib
LIBS += absl_flags_reflection.lib
LIBS += absl_flags_config.lib
LIBS += absl_flags_private_handle_accessor.lib
LIBS += absl_flags_commandlineflag.lib
LIBS += absl_flags_commandlineflag_internal.lib
LIBS += absl_flags_program_name.lib
LIBS += abseil_dll.lib

LIBS += -ladvapi32 -liphlpapi -lwsock32 -lws2_32 -lcrypt32 -lgdi32
LIBS += -lkernel32 -luser32 -lwinspool -lshell32 -lole32 -loleaut32
LIBS += -luuid -lcomdlg32
