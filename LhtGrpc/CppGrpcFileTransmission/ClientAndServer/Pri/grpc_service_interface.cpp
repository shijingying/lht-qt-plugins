#include "grpc_service_interface.h"

std::unique_ptr<grpc::Server> GrpcServiceInterface::m_server = nullptr;
std::shared_ptr<GrpcServiceInterface> GrpcServiceInterface::m_ptr = nullptr;

void GrpcServiceInterface::startService()
{
    grpc::ServerBuilder builder;
    builder.AddListeningPort("0.0.0.0:6661", grpc::InsecureServerCredentials());
    builder.RegisterService(m_ptr.get());
    m_server = (builder.BuildAndStart());
    std::cout << "Server listening on " << "127.0.0.1:6661" <<std::endl;
    m_server->Wait();
}

void GrpcServiceInterface::stopService()
{
    m_server->Shutdown();
}

grpc::Status GrpcServiceInterface::TransferFileInfo(grpc::ServerContext *context, const FileInfo *request, UploadStatus *response)
{
    auto fileName = request->filename();
    auto userId = request->userid();
    m_fileTotalSize = request->filetotalsize();
    // 打开文件以写入模式 (binary 模式用于处理二进制数据)
    outputFile.open(FILE_DEFAULT_PATH + fileName, std::ios::out | std::ios::binary);
    if (!outputFile.is_open()) {
        response->set_message("Failed to open file.");
        response->set_success(false);
        return grpc::Status(grpc::StatusCode::INTERNAL, "Failed to open file.");
    }
    response->set_message("ok");
    response->set_success(true);
    return ::grpc::Status::OK;
}

grpc::Status GrpcServiceInterface::TransferData(grpc::ServerContext *context, ::grpc::ServerReader<FileChunk> *reader, UploadStatus *response)
{
    FileChunk fileChunk;
    double recvFileSize = 0;
    m_currentProgress = 0;
    while (reader->Read(&fileChunk)) {
        const std::string& data = fileChunk.content();
        outputFile.write(data.c_str(), data.size());

        if (!outputFile.good()) {
            response->set_message("Error writing to file.");
            response->set_success(false);
            return grpc::Status(grpc::StatusCode::INTERNAL, "Error writing to file.");
        }
        recvFileSize += data.size();
        //计算一下发送进度
        int progress = 100 * recvFileSize / m_fileTotalSize;

        if(m_currentProgress != progress){
            m_currentProgress = progress;
#ifdef UseQtObject
            signUpdateProgress(progress);
#endif
            if(m_callBack){
                m_callBack->ProgressUpdate(progress);
            }
        }

    }

    outputFile.close();
    if (!outputFile.good()) {
        response->set_message("File write failed.");
        response->set_success(false);
        return grpc::Status(grpc::StatusCode::INTERNAL, "File write failed.");
    }

    response->set_message("File transfer successful.");
    response->set_success(true);

    return grpc::Status::OK;
}

GrpcServiceInterface::GrpcServiceInterface(GrpcInterfaceCallBack *callBack):
    m_callBack(callBack)
{

}
