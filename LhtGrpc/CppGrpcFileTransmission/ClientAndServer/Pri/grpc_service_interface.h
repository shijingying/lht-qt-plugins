#ifndef GRPC_SERVICE_INTERFACE_H
#define GRPC_SERVICE_INTERFACE_H

#include "grpc_define.h"

using namespace ::filetransfer;
typedef std::shared_ptr<grpc::Channel> LhtChannel;
typedef std::unique_ptr<FileTransferService::Stub> LhtStub;
typedef grpc::ClientContext LhtClientContext;

class GrpcServiceInterface :
#ifdef UseQtObject
                             public QObject,
#endif
                             std::enable_shared_from_this<GrpcServiceInterface>,
                             public FileTransferService::Service
{
#ifdef UseQtObject
    Q_OBJECT
#endif
public:
    static std::shared_ptr<GrpcServiceInterface> CreateService1(GrpcInterfaceCallBack * callback = nullptr){
        m_ptr = std::shared_ptr<GrpcServiceInterface>(new GrpcServiceInterface(callback));
        return m_ptr;
    }

    void startService();

    void stopService();
    // Service interface
public:
    grpc::Status TransferFileInfo(grpc::ServerContext *context, const FileInfo *request, UploadStatus *response);

    grpc::Status TransferData(grpc::ServerContext *context, ::grpc::ServerReader<FileChunk> *reader, UploadStatus *response);
#ifdef UseQtObject
signals:
    //0 -100
    void signUpdateProgress(int progress);
#endif
private:
    std::ofstream outputFile;

    int m_currentProgress = 0;

    long long m_fileTotalSize = 0;

    GrpcInterfaceCallBack * m_callBack;

    static std::shared_ptr<GrpcServiceInterface> m_ptr;

    static std::unique_ptr<grpc::Server> m_server;
private:

    GrpcServiceInterface(GrpcInterfaceCallBack * callBack);

};
typedef std::shared_ptr<GrpcServiceInterface> GrpcServiceInterfacePtr;
#endif // GRPC_SERVICE_INTERFACE_H
