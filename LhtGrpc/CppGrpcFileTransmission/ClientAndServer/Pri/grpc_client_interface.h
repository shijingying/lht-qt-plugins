#ifndef GRPC_CLIENT_INTERFACE_H
#define GRPC_CLIENT_INTERFACE_H

#include "grpc_define.h"

using namespace ::filetransfer;
typedef std::shared_ptr<grpc::Channel> LhtChannel;
typedef std::unique_ptr<FileTransferService::Stub> LhtStub;
typedef grpc::ClientContext LhtClientContext;
typedef std::unique_ptr< ::grpc::ClientWriter< ::filetransfer::FileChunk>> LhtTransferDataClient;

class GrpcClientInterface :
#ifdef UseQtObject
                            public QObject ,
#endif
                            std::enable_shared_from_this<GrpcClientInterface>
{
#ifdef UseQtObject
    Q_OBJECT
#endif
public:
    static std::shared_ptr<GrpcClientInterface> CreateClient(std::string & serverUrl,GrpcInterfaceCallBack * callback = nullptr){
        return std::shared_ptr<GrpcClientInterface>(new GrpcClientInterface(serverUrl,callback));
    }

    bool sendFileInfo(int userId,std::string filePath);

    bool startSendFile();

#ifdef UseQtObject
signals:
    //0 -100
    void signUpdateProgress(int progress);
#endif
private:
    int m_userId;

    int m_currentProgress = 0;

    std::string m_serverUrl;

    std::string m_filePath;

    std::string m_fileName;

    long long  m_fileTotalSize;

    GrpcInterfaceCallBack * m_callBack;

    std::chrono::steady_clock::time_point m_lastUpdateTime;

    int64_t m_lastSendFileSize = 0;
private:

    GrpcClientInterface(std::string & serverUrl,GrpcInterfaceCallBack * callback);

    std::streamsize getFileSize();
};


typedef std::shared_ptr<GrpcClientInterface> GrpcClientInterfacePtr;

#endif // GRPC_CLIENT_INTERFACE_H
