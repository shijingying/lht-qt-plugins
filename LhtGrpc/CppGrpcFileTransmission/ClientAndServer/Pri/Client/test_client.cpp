#include "test_client.h"
#include <QDebug>

TestClient::TestClient() {}

void TestClient::connectServer(std::string url)
{
    m_client = GrpcClientInterface::CreateClient(url,this);
}

void TestClient::sendFileInfo(std::string filePath)
{
    m_client->sendFileInfo(1,filePath);
}

void TestClient::startTransmission()
{
    m_client->startSendFile();
}

void TestClient::ProgressUpdate(int progress)
{
    signProgressUpdate(progress);
    // qDebug()<<"service progress = "<<progress;
}

void TestClient::SpeedUpdate(float speed)
{
    signSpeedUpdate(speed);
}
