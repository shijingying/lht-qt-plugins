#ifndef TEST_CLIENT_H
#define TEST_CLIENT_H

#include <QObject>
#include <QThread>
#include "../grpc_client_interface.h"

class TestClient : public GrpcInterfaceCallBack
{
    Q_OBJECT
public:
    TestClient();

    void connectServer(std::string url);

    void sendFileInfo(std::string filePath);

    void startTransmission();
    // GrpcInterfaceCallBack interface
signals:
    void signProgressUpdate(int progress);

    void signSpeedUpdate(float speed);

public:
    void ProgressUpdate(int progress);

    void SpeedUpdate(float speed);

private:
    GrpcClientInterfacePtr m_client;
};

#endif // TEST_CLIENT_H
