#include "grpc_client_interface.h"
#include <QDebug>
// 获取文件大小
std::streamsize GrpcClientInterface::getFileSize() {
    std::ifstream file(m_filePath, std::ios::binary | std::ios::ate);
    if (!file.is_open()) {
        std::cerr << "Failed to open file: " << m_filePath << std::endl;
        return -1;
    }
    return file.tellg();  // 返回文件大小
}
// 获取路径中的文件名
std::string getFileName(const std::string& filePath) {
    // 查找最后一个 / 或者 \ 的位置
    size_t pos = filePath.find_last_of("/\\");

    // 如果找到了，返回文件名部分
    if (pos != std::string::npos) {
        return filePath.substr(pos + 1);
    }

    // 如果没有找到分隔符，整个字符串就是文件名
    return filePath;
}

bool GrpcClientInterface::sendFileInfo(int userId,std::string filePath)
{
    m_userId = userId;

    m_filePath = filePath;

    m_fileName = getFileName(filePath);

    m_fileTotalSize = getFileSize();

    auto channel = grpc::CreateChannel(m_serverUrl, grpc::InsecureChannelCredentials());

    std::unique_ptr<filetransfer::FileTransferService::Stub> stub = filetransfer::FileTransferService::NewStub(channel);

    grpc::ClientContext context;

    filetransfer::FileInfo file_info;
    file_info.set_filename(m_fileName);
    file_info.set_filetotalsize(m_fileTotalSize);
    file_info.set_userid(userId);

    filetransfer::UploadStatus response;

    grpc::Status status = stub->TransferFileInfo(&context, file_info, &response);

    if (status.ok()) {
        std::cout << "File info sent successfully!" << std::endl;
        std::cout << "Server response: " << response.message() << std::endl;
        return 1;
    } else {
        std::cout << "Error: " << status.error_message() << std::endl;
        return 0;
    }

}

bool GrpcClientInterface::startSendFile()
{

    grpc::ClientContext dataContext;

    filetransfer::UploadStatus uploadResponse;

    auto channel = grpc::CreateChannel(m_serverUrl, grpc::InsecureChannelCredentials());

    std::unique_ptr<filetransfer::FileTransferService::Stub> stub = filetransfer::FileTransferService::NewStub(channel);
    // 初始化流
    LhtTransferDataClient fileStream = stub->TransferData(&dataContext, &uploadResponse);
    // 打开文件
    std::ifstream file(m_filePath, std::ios::binary);
    if (!file.is_open()) {
        std::cerr << "Failed to open file: " << m_filePath << std::endl;
        return 0;
    }
    // 发送文件块
    char *buffer = new char[BLOCK_SIZE];  // 定义块大小为4KB
    double sendFileSize = 0;
    m_currentProgress = 0;
    auto start = std::chrono::steady_clock::now();
    m_lastUpdateTime = std::chrono::steady_clock::now();
    while (file.good()) {
        file.read(buffer, BLOCK_SIZE);
        std::streamsize bytesRead = file.gcount();

        if (bytesRead > 0) {
            // 填充 FileChunk 消息
            filetransfer::FileChunk chunk;
            chunk.set_content(buffer, bytesRead);

            // 通过流发送文件块
            if (!fileStream->Write(chunk)) {
                qDebug() << "Failed to send file chunk." ;
                break;
            }
            sendFileSize += bytesRead;
            //计算一下发送进度
            int progress = 100 * sendFileSize / m_fileTotalSize;

            if(m_currentProgress != progress){
                m_currentProgress = progress;
#ifdef UseQtObject
                signUpdateProgress(progress);
#endif
                // 获取当前时间
                auto now = std::chrono::steady_clock::now();
                // 计算传输速率 (MB/s)
                auto elapsedSeconds = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_lastUpdateTime);
                // std::chrono::duration<double> elapsedSeconds = now - m_lastUpdateTime;
                double elapsedTime = elapsedSeconds.count();  // 获取时间差，单位：秒
                int64_t bytesSentSinceLast = sendFileSize - m_lastSendFileSize;
                double speedMBps = (1000 * bytesSentSinceLast / (1024.0 * 1024.0)) / elapsedTime ;  // MB/s

                // 更新记录
                m_lastSendFileSize = sendFileSize;
                m_lastUpdateTime = now;
                if(m_callBack){
                    m_callBack->ProgressUpdate(progress);
                    m_callBack->SpeedUpdate(speedMBps);  // 回传速率
                }
            }
        }
    }
    {//计算文件传输速度
        auto end = std::chrono::steady_clock::now();
        qDebug() << "File uploaded successfully" ;
        qDebug() << "Time taken: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" ;
        qDebug() << "File size: " << m_fileTotalSize << " bytes";
        auto speed = (m_fileTotalSize / std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) * 1000 / 1024 / 1024;
        qDebug() << "Speed: " << speed << " MB/s" ;
    }
    // 关闭文件流
    file.close();

    // 结束数据流传输
    fileStream->WritesDone();

    // 检查传输状态
    grpc::Status finalStatus = fileStream->Finish();
    if (!finalStatus.ok()) {
        qDebug() << "File transfer failed: " << finalStatus.error_message().c_str() ;
        return 0;
    }

    qDebug() << "File transfer completed successfully!";
    return 1;

}

GrpcClientInterface::GrpcClientInterface(std::string &serverUrl, GrpcInterfaceCallBack *callback)
    :m_userId(-1),m_serverUrl(serverUrl),m_fileTotalSize(0),m_callBack(callback)
{

}
