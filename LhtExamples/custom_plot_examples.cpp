#include "custom_plot_examples.h"
#include "LhtGui/CustomPlotV2/QCustomPlot/qcustomplot.h"
#include "LhtGui/CustomPlotV2/Gui/eye_pattern_plot.h"
#include "LhtGui/CustomPlotV2/Gui/star_map_plot.h"
#include "LhtGui/CustomPlotV2/Gui/comm_spectrum_widget.h"
#include "LhtGui/CustomPlotV2/Gui/waterfall_plot_new.h"
#include "LhtGui/CustomPlotV2/Gui/spec_plot_new.h"
#include <QtConcurrent>
CustomPlotExamples::CustomPlotExamples(QObject *parent)
    : QObject{parent}
{}

void CustomPlotExamples::initLinePlot(QWidget * widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    QCustomPlot * customPlot = new QCustomPlot();
    layout->addWidget(customPlot);
    customPlot->setOpenGl(true);
    // add two new graphs and set their look:
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    customPlot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    customPlot->addGraph();
    customPlot->graph(1)->setPen(QPen(Qt::red)); // line color red for second graph
    // generate some points of data (y0 for first, y1 for second graph):
    QVector<double> x(251), y0(251), y1(251);
    for (int i=0; i<251; ++i)
    {
        x[i] = i;
        y0[i] = qExp(-i/150.0)*qCos(i/10.0); // exponentially decaying cosine
        y1[i] = qExp(-i/150.0);              // exponential envelope
    }
    // configure right and top axis to show ticks but no labels:
    // (see QCPAxisRect::setupFullAxesBox for a quicker method to do this)
    customPlot->xAxis2->setVisible(true);
    customPlot->xAxis2->setTickLabels(false);
    customPlot->yAxis2->setVisible(true);
    customPlot->yAxis2->setTickLabels(false);
    // make left and bottom axes always transfer their ranges to right and top axes:
    connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    // pass data points to graphs:
    customPlot->graph(0)->setData(x, y0);
    customPlot->graph(1)->setData(x, y1);
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    customPlot->graph(0)->rescaleAxes();
    // same thing for graph 1, but only enlarge ranges (in case graph 1 is smaller than graph 0):
    customPlot->graph(1)->rescaleAxes(true);
    // Note: we could have also just called customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}

void CustomPlotExamples::initHistogramPlot(QWidget * widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    QCustomPlot * customPlot = new QCustomPlot();
    layout->addWidget(customPlot);
    customPlot->setOpenGl(true);
    // set dark background gradient:
    QLinearGradient gradient(0, 0, 0, 400);
    gradient.setColorAt(0, QColor(90, 90, 90));
    gradient.setColorAt(0.38, QColor(105, 105, 105));
    gradient.setColorAt(1, QColor(70, 70, 70));
    customPlot->setBackground(QBrush(gradient));

    // create empty bar chart objects:
    QCPBars *regen = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    QCPBars *nuclear = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    QCPBars *fossil = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    regen->setAntialiased(false); // gives more crisp, pixel aligned bar borders
    nuclear->setAntialiased(false);
    fossil->setAntialiased(false);
    regen->setStackingGap(1);
    nuclear->setStackingGap(1);
    fossil->setStackingGap(1);
    // set names and colors:
    fossil->setName("Fossil fuels");
    fossil->setPen(QPen(QColor(111, 9, 176).lighter(170)));
    fossil->setBrush(QColor(111, 9, 176));
    nuclear->setName("Nuclear");
    nuclear->setPen(QPen(QColor(250, 170, 20).lighter(150)));
    nuclear->setBrush(QColor(250, 170, 20));
    regen->setName("Regenerative");
    regen->setPen(QPen(QColor(0, 168, 140).lighter(130)));
    regen->setBrush(QColor(0, 168, 140));
    // stack bars on top of each other:
    nuclear->moveAbove(fossil);
    regen->moveAbove(nuclear);

    // prepare x axis with country labels:
    QVector<double> ticks;
    QVector<QString> labels;
    ticks << 1 << 2 << 3 << 4 << 5 << 6 << 7;
    labels << "USA" << "Japan" << "Germany" << "France" << "UK" << "Italy" << "Canada";
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    textTicker->addTicks(ticks, labels);
    customPlot->xAxis->setTicker(textTicker);
    customPlot->xAxis->setTickLabelRotation(60);
    customPlot->xAxis->setSubTicks(false);
    customPlot->xAxis->setTickLength(0, 4);
    customPlot->xAxis->setRange(0, 8);
    customPlot->xAxis->setBasePen(QPen(Qt::white));
    customPlot->xAxis->setTickPen(QPen(Qt::white));
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->xAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->xAxis->setLabelColor(Qt::white);

    // prepare y axis:
    customPlot->yAxis->setRange(0, 12.1);
    customPlot->yAxis->setPadding(5); // a bit more space to the left border
    customPlot->yAxis->setLabel("Power Consumption in\nKilowatts per Capita (2007)");
    customPlot->yAxis->setBasePen(QPen(Qt::white));
    customPlot->yAxis->setTickPen(QPen(Qt::white));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white));
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setLabelColor(Qt::white);
    customPlot->yAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::SolidLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));

    // Add data:
    QVector<double> fossilData, nuclearData, regenData;
    fossilData  << 0.86*10.5 << 0.83*5.5 << 0.84*5.5 << 0.52*5.8 << 0.89*5.2 << 0.90*4.2 << 0.67*11.2;
    nuclearData << 0.08*10.5 << 0.12*5.5 << 0.12*5.5 << 0.40*5.8 << 0.09*5.2 << 0.00*4.2 << 0.07*11.2;
    regenData   << 0.06*10.5 << 0.05*5.5 << 0.04*5.5 << 0.06*5.8 << 0.02*5.2 << 0.07*4.2 << 0.25*11.2;
    fossil->setData(ticks, fossilData);
    nuclear->setData(ticks, nuclearData);
    regen->setData(ticks, regenData);

    // setup legend:
    customPlot->legend->setVisible(true);
    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
    customPlot->legend->setBrush(QColor(255, 255, 255, 100));
    customPlot->legend->setBorderPen(Qt::NoPen);
    QFont legendFont = QFont();
    legendFont.setPointSize(10);
    customPlot->legend->setFont(legendFont);
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}
#include <QDebug>
void CustomPlotExamples::initSpecPlot(QWidget * widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    m_specViewWidget = new CommSpectrumWidget();
    layout->addWidget(m_specViewWidget);

}

void CustomPlotExamples::setDisplayType(DisplayTypes type)
{
    m_waterfallPlotNew->setDisplayType(type);
}

void CustomPlotExamples::initSpecPlot2(QWidget *widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    m_specViewWidgetGreat = new SpecPlotNew();
    layout->addWidget(m_specViewWidgetGreat);
}

void CustomPlotExamples::initWaterfallPlot(QWidget * widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    m_waterfallPlotNew = new WaterfallPlotNew();
    layout->addWidget(m_waterfallPlotNew);
    _readFileExamples = new ReadFileExamples(this);
    qRegisterMetaType<std::shared_ptr<float>>("std::shared_ptr<float>");
    connect(_readFileExamples,&ReadFileExamples::signUpdateView,m_specViewWidget,&CommSpectrumWidget::updateView,Qt::QueuedConnection);
    connect(_readFileExamples,&ReadFileExamples::signUpdateView,m_specViewWidgetGreat,&SpecPlotNew::addNewData,Qt::QueuedConnection);
    connect(_readFileExamples,&ReadFileExamples::signUpdateView,m_waterfallPlotNew,&WaterfallPlotNew::addNewData,Qt::QueuedConnection);
    _readFileExamples->start();
}

void CustomPlotExamples::initEyePlot(QWidget * widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    auto eyePatternWidget = new EyePatternPlot();
    layout->addWidget(eyePatternWidget);
    eyePatternWidget->installEventFilter(this);
    eyePatternWidget->resize(320, 220);
    eyePatternWidget->init(2);
    QFile file(":/resource/test.dat");

    file.open(QIODevice::ReadOnly);

    char * input = new char[512e3];
    int readLen = 40960;
    file.read(input,readLen);

    float * inputf = (float*)input;
    auto inputfI = new float[512e3];
    auto inputfQ = new float[512e3];
    auto len = readLen / 8;
    for(int i = 0 ; i < len ; i++){
        inputfI[i] = inputf[ 2 * i] + 1;
        inputfQ[i] = inputf[ 2 * i + 1] + 1;
    }
    eyePatternWidget->addEyePatternData(inputfI + 1000,inputfQ+ 1000,512);
}

void CustomPlotExamples::initStarPlot(QWidget * widget)
{
    QVBoxLayout *layout=new QVBoxLayout(widget);
    StarMapPlot * starMapWidget = new StarMapPlot();
    layout->addWidget(starMapWidget);
    starMapWidget->installEventFilter(this);
    starMapWidget->resize(320, 220);
    starMapWidget->init(2);

    auto iData = new float[512];
    auto qData = new float[512];
    int len = 512;

    std::srand(std::time(nullptr)); // 用当前时间作为随机种子

    for (int i = 0; i < len / 2; ++i) {
        // 左下角点位
        iData[i] = -5 - static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / 0.5));
        qData[i] = -5 - static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / 0.5));
    }

    for (int i = len / 2; i < len; ++i) {
        // 右上角点位
        iData[i] = 5 + static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / 0.5));
        qData[i] = 5 + static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / 0.5));
    }
    starMapWidget->addData(iData,qData,512);
}
