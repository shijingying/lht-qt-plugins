#ifndef READ_FILE_EXAMPLES_H
#define READ_FILE_EXAMPLES_H

#include <QObject>
#include "LhtThread/lockless_thread_pool.h"
#include "LhtIPPS/Ipps/Interface/computing_interface.h"

class ReadFileExamples :  public QThread
{
    Q_OBJECT
public:
    explicit ReadFileExamples(QObject *parent = nullptr);

signals:
    void signUpdateView(std::shared_ptr<float> data, int len);

protected:
    void run();

private:

};

#endif // READ_FILE_EXAMPLES_H
