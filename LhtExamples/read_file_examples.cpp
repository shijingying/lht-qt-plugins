#include "read_file_examples.h"

#include <QFile>

ReadFileExamples::ReadFileExamples(QObject *parent)
    : QThread{parent}
{}

void ReadFileExamples::run()
{
    ComputingInterface *_ippsInterface = new ComputingInterface;
    QFile file(":/resource/BPSK_4800.pcm");
    int Len = 16384;
    _ippsInterface->LhtFftInitFloat(Len);
    QByteArray buffer;
    auto iq32fData = new float[Len * 2];
    Ipp32f* iq32fDataResult = new Ipp32f[Len];
    int bufferSize = 16384 * 4;
    buffer.resize(bufferSize);
    if(file.open(QIODevice::ReadOnly)){
        while(1){
            qint64 bytesRead = file.read(buffer.data(), bufferSize);
            if(file.atEnd() || (bytesRead  != bufferSize)){
                file.seek(0);
                continue;
            }
            ippsConvert_16s32f((const Ipp16s*)buffer.data(),iq32fData,Len * 2);
            Ipp32fc* temp = (Ipp32fc*)iq32fData;
            _ippsInterface->LhtFftFloat(temp,Len,iq32fDataResult,1);
            for (int i=0; i<Len; ++i)
            {
                iq32fDataResult[i] = -20*log10(iq32fDataResult[i]) + 30;
            }
            std::shared_ptr<float> data(new float[Len], std::default_delete<float[]>());
            memcpy(data.get(),iq32fDataResult,Len*4);
            emit signUpdateView(std::move(data),Len);
            QThread::msleep(50);
        }
    }
    delete []iq32fData;
}
