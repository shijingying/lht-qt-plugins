#ifndef CUSTOM_PLOT_EXAMPLES_H
#define CUSTOM_PLOT_EXAMPLES_H

#include <QLayout>
#include <QObject>
#include "read_file_examples.h"
#include "LhtGui/CustomPlotV2/Gui/waterfall_plot.h"

class CommSpectrumWidget;
class SpecPlotNew;
class WaterfallPlotNew;

class CustomPlotExamples : public QObject
{
    Q_OBJECT
public:
    explicit CustomPlotExamples(QObject *parent = nullptr);

    void initLinePlot(QWidget *);

    void initHistogramPlot(QWidget *);

    void initSpecPlot(QWidget *);

    void setDisplayType(DisplayTypes type);

    void initSpecPlot2(QWidget *);

    void initWaterfallPlot(QWidget *);

    void initEyePlot(QWidget *);

    void initStarPlot(QWidget *);
    ReadFileExamples * _readFileExamples;
signals:
    void signUpdateView(std::shared_ptr<float> data, int len);
private:
    CommSpectrumWidget * m_specViewWidget;
    SpecPlotNew * m_specViewWidgetGreat;
    WaterfallPlotNew * m_waterfallPlotNew;
};

#endif // CUSTOM_PLOT_EXAMPLES_H
