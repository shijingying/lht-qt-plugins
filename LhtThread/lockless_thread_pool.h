#ifndef LOCKLESS_THREAD_POOL_H
#define LOCKLESS_THREAD_POOL_H
#include <QThreadPool>
#include <Windows.h>

/**
 * @brief 无锁线程基类，消息处理器仓库，管理线程
 * @param Handler 具体的操作
 */

struct LhtBufferCache
{
    uint32_t len;
    std::unique_ptr<char[]> pkt_data; // 数据缓冲区
    LhtBufferCache* next;//下一个缓存
};

class LhtLocklessThreadPool : public QObject
{
    Q_OBJECT
public:

    static LhtLocklessThreadPool* instance(){
        return m_instance;
    }

    explicit LhtLocklessThreadPool(QObject *parent = nullptr);

    ~LhtLocklessThreadPool();

    void addWorker(QRunnable * work);

    bool deleteWorker(QRunnable * work);
signals:

private:
    int                     m_threadMaxCount;
    QThreadPool             m_pool;
    static LhtLocklessThreadPool *  m_instance;
};

class LocklessDataCache {
public:
    LocklessDataCache();

    // 接收数据，使用移动语义接管缓冲区
    void recv(std::unique_ptr<char[]> data, uint32_t len);

    // 缓存在函数内释放，需预先申请指针空间
    bool readData(char* buffer, uint32_t& nbyte);

    // 缓存由调用方释放，无拷贝过程，使用引用参数
    bool readDataZeroCopy(LhtBufferCache*& ret);

    void clearBuffer();

    void setPoolLen(uint32_t nLen);

    int getCurrentSize() {
        return m_currentSize;
    }

private:
    uint32_t poolSize = 1000;          // 清空数据时使用
    std::atomic_int m_currentSize = 0; // 当前缓存大小
    LhtBufferCache* m_buffer_rd = nullptr; // 读指针
    LhtBufferCache* m_buffer_wr = nullptr; // 写指针
};
/**
 * @class LocklessThread
 * @brief 无锁线程基类，高效的事件驱动线程。
 *
 * 该类使用两个指针一个用于接收数据时的写入m_buffer_wr指针位于链表最后一位
 * m_buffer_rd指针位于未处理的数据第一位用处handleData处理数据
 *
 * @note 内部存在两种数据处理的方法  开关为m_useZeroCopy是否启用0拷贝
 *
 * @version 1.0
 * @date 2024-07-15
 *
 * @author leehuitao
 */

class LocklessThread : public QThread
{
public:
    LocklessThread(QObject *parent = nullptr) : QThread(parent) {
        containerLen = 1000;
        packLen = 1296;
        stopped = true;
    }

    ~LocklessThread() {
        stopped = true;
    }

    // 接收数据，使用移动语义接管缓冲区
    void recv(std::unique_ptr<char[]> buf, int len) {
        m_lhtBufferCache.recv(std::move(buf), len);
    }

    virtual void stop() {
        stopped = true;
        quit();
        wait(100);
        // LhtLocklessThreadPool::instance()->deleteWorker(this);
    }

    __declspec(deprecated("需要预先设置pack len否则无法启动读线程"))
    virtual void init() {
        if (stopped) {
            start();
            // LhtLocklessThreadPool::instance()->addWorker(this);
        }
    }

    void setPoolLen(unsigned int nLen) {
        containerLen = nLen;
        m_lhtBufferCache.setPoolLen(nLen);
    }

    void setPackLen(unsigned int nLen) { packLen = nLen; }
    void setUseZeroCopy(bool use) { m_useZeroCopy = use; }

    int currentSize() { return m_lhtBufferCache.getCurrentSize(); }

    void clearDataBuf() {
        m_lhtBufferCache.clearBuffer();
    }

protected:
    void run() override {
        if (packLen == -1 && !m_useZeroCopy) {
            return;
        }

        char* data = nullptr;
        if (!m_useZeroCopy) {
            data = new char[packLen];
        }

        uint32_t len;
        LhtBufferCache* p_pkt_bufferNow = nullptr;
        stopped = false;

        while (true) {
            if (m_useZeroCopy) {
                if (!m_lhtBufferCache.readDataZeroCopy(p_pkt_bufferNow)) {
                    Sleep(1);
                } else {
                    handleData(p_pkt_bufferNow->pkt_data.get(), p_pkt_bufferNow->len);
                    delete p_pkt_bufferNow;
                    p_pkt_bufferNow = nullptr;
                }
            } else {
                if (!m_lhtBufferCache.readData(data, len)) {
                    Sleep(1);
                } else {
                    handleData(data, len);
                }
            }

            if (stopped) {
                break;
            }
        }

        // 清理非零拷贝模式下的缓冲区
        if (!m_useZeroCopy) {
            delete[] data;
        }
    }

    virtual bool handleData(char* buf, int len) = 0; // 纯虚函数，处理数据

private:
    LocklessDataCache m_lhtBufferCache;

    unsigned int containerLen;
    unsigned int packLen;
    volatile bool stopped;
    bool m_useZeroCopy = true; // 默认使用零拷贝
};
#endif // LOCKLESS_THREAD_POOL_H
