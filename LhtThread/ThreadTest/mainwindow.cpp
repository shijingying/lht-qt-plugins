#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtConcurrent>
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_test_btn_clicked()
{
    m_testThread = new TestThread;
    m_testThread->init();
    index = 0;
    auto watch = new QFutureWatcher<void>(this);
    auto ret = QtConcurrent::run([&](){
        QFile file("D:/test.dat");
        file.open(QIODevice::ReadOnly);
        const int readLen = 16384 *4;
        thread_local unsigned char data[readLen];
        while(readLen == file.read((char*)data,readLen)){
            m_testThread->recv(data,readLen);
            index++;
        }
    });
    connect(watch, &QFutureWatcher<void>::finished, this, [=]() {
        QMessageBox::information(this, "Info", QString("File read complete. index = %1").arg(index));
        m_testThread->deleteLater();
        watch->deleteLater();
    });
    watch->setFuture(ret);
}

