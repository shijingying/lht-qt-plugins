#ifndef TEST_THREAD_H
#define TEST_THREAD_H

#include <QObject>
#include "../circular_buffer_data_pool.h"

class TestThread : public CicularBufferDataPool
{
    Q_OBJECT
public:
    explicit TestThread(QObject *parent = nullptr);

signals:

private:
    // CicularBufferDataPool interface
protected:
    bool handleData(unsigned char *buf, int len);
};

#endif // TEST_THREAD_H
