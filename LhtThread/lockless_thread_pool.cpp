#include "lockless_thread_pool.h"
#include <LhtLog/QsLog/QsLog.h>
LocklessDataCache::LocklessDataCache() {

}

void LocklessDataCache::recv(std::unique_ptr<char[]> data, uint32_t len) {
    if (m_currentSize > poolSize) // 如果缓存满了，跳过
        return;

    // 创建新节点并移动数据
    LhtBufferCache* p_pkt_bufferNow = new LhtBufferCache;
    p_pkt_bufferNow->len = len;
    p_pkt_bufferNow->pkt_data = std::move(data);
    p_pkt_bufferNow->next = nullptr;

    if (m_buffer_wr == nullptr) { // 如果写入指针为空
        m_buffer_rd = p_pkt_bufferNow;
    } else {
        m_buffer_wr->next = p_pkt_bufferNow;
    }
    m_buffer_wr = p_pkt_bufferNow; // 更新写指针
    m_currentSize++;
}

bool LocklessDataCache::readData(char* buffer, uint32_t& nbyte) {
    if (m_currentSize > 0 && m_buffer_rd == m_buffer_wr) { // 无数据可读
        return false;
    }

    // 复制数据到调用者缓冲区
    memcpy(buffer, m_buffer_rd->pkt_data.get(), m_buffer_rd->len);
    nbyte = m_buffer_rd->len;

    // 移动读指针并释放节点
    LhtBufferCache* p_pkt_now = m_buffer_rd;
    m_buffer_rd = p_pkt_now->next;
    m_currentSize--;
    delete p_pkt_now;
    return true;
}

bool LocklessDataCache::readDataZeroCopy(LhtBufferCache*& ret) {
    if (m_currentSize > 0 && m_buffer_rd == m_buffer_wr) { // 无数据可读
        return false;
    }
    ret = m_buffer_rd;
    m_buffer_rd = m_buffer_rd->next;
    m_currentSize--;
    return true;
}

void LocklessDataCache::clearBuffer() {
    while (m_buffer_rd != m_buffer_wr) {
        LhtBufferCache* p_pkt_now = m_buffer_rd;
        m_buffer_rd = p_pkt_now->next;
        delete p_pkt_now;
    }
    m_buffer_wr = nullptr;
    m_currentSize = 0;
}

void LocklessDataCache::setPoolLen(uint32_t nLen) {
    poolSize = nLen;
}

LhtLocklessThreadPool::LhtLocklessThreadPool(QObject *parent)
{
    m_threadMaxCount = QThread::idealThreadCount();
    m_pool.setMaxThreadCount(m_threadMaxCount);
    QLOG_INFO()<< "thread pool max count : "<<m_threadMaxCount;
}

LhtLocklessThreadPool::~LhtLocklessThreadPool()
{
    m_pool.waitForDone(); // 等待所有任务完成
    m_pool.clear(); // 获取正在运行的任务
}

void LhtLocklessThreadPool::addWorker(QRunnable *work)
{
    m_pool.start(work);
}

bool LhtLocklessThreadPool::deleteWorker(QRunnable *work)
{
    auto ret = m_pool.tryTake(work);
    return  ret;
}
