#include "circular_buffer_data_pool.h"


bool CicularBufferDataPool::isBufferFull() const
{
    return (writeIndex.load(std::memory_order_acquire) - readIndex.load(std::memory_order_acquire)) >= containerLen;
}

bool CicularBufferDataPool::isBufferEmpty() const
{
    return writeIndex.load(std::memory_order_acquire) == readIndex.load(std::memory_order_acquire);
}
