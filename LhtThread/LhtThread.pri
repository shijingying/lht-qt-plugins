HEADERS += \
    $$PWD/atomic_event_driven_thread.h \
    $$PWD/circular_buffer_data_pool.h \
    $$PWD/coro_generator.h \
    $$PWD/data_pool.h \
    $$PWD/event_driven_thread.h \
    $$PWD/lht_thread_pool.h \
    $$PWD/lockless_thread_pool.h \
    $$PWD/move2ThBasic.h

SOURCES += \
    $$PWD/circular_buffer_data_pool.cpp \
    $$PWD/lht_thread_pool.cpp \
    $$PWD/lockless_thread_pool.cpp
