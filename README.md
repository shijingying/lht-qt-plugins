# 一些相关模块或者库的编译方式
https://gitee.com/shijingying/GoTestServer
# 📚简介
本项目为本人在工作中遇到过的，或者平时学习到的一些觉得好用的方法/基类/功能等，慢慢适配C++20中
- 新增了GRPC传输文件类库本地测试320MB/S  LhtGrpc库中
-  **新增Yolo+TensorRt10+目标追踪 示例代码支持图片，视频，摄像头（支持YoloV8 V10）  LhtYolo库中** 
-  **新增CUDA处理PNG图片的透明度和亮度代码包含VS版本和QT版本，LhtCuda库中** 
- 新增C++版本分布式框架，Grpc+Redis+C++ 支持简单的服务发现，服务注册，服务健康管理，只需简单配置开箱即用，支持分布式log，统一的服务接口
- 新增界面示例包含表格，Customplot图表，页面浮动缩放，图形视图
- 新增右键过滤器，折线图，柱状图，频谱，瀑布图，眼图，星座图的图表显示，一些动态库在[这里](https://gitee.com/shijingying/lht-qt-plugins/tree/master/build)

# 📦软件架构
- Qt 5.15.2 + msvc 2019 + （部分需要C++20）
- Windows

# 🛠️库介绍


| 模块                |     介绍        |        Activity     |
| -------------------|----------------- |----------------- |
|  :star: LhtThread|  好用的线程基类，异步处理      | 原子智能指针优化线程/循环缓存优化线程/无锁线程/事件驱动线程/普通线程/重载Run/C++线程 |
|  :star: LhtTools | 一些简单易用的方法 | 类Go的defer方法，类Go的协程方法，方便的C++函数耗时打印 |
| LhtGrpc |     GRPC的测试案例          | C++Grpc文件传输测试速度在本地约为320MB/S 、QT GRPC 调用测试 |
| LhtNetwork|     绝大部分网络的使用相关     | [QT C/S框架](https://gitee.com/shijingying/lht-qt-plugins/tree/master/LhtNetwork/QtServerClientNetworkFramework)，一些网络框架  C++ Boost Tcp服务框架/Qt Tcp服务客户端框架/Qt  tcp udp serial http 客户端框架|
|  :star: LhtSql|    多种数据库的并发使用        | Mysql线程池/redis线程池/Mongo线程池 |
| LhtYolo|    TensorRT加速Yolo+目标追踪     | (支持YoloV8，V10)包含pt转onnx，onnx生成trt，导入engine，推理，目标追踪等功能 |
|  :star: LhtCompute |    （Win ippp/Arm CMSIS-DSP支持（准备中）） ipps加速算法/openblas加速/fftw   | 测试用例：向量加减乘除加速，低通滤波等加速，dft,fft加速，复数的快速转换，排序，数组反转，数字下变频，快速相位提取，功率谱计算，非整数倍重采样，希尔伯特变换，自相关，2024.12.29增加CZT代码，增加小波变换  |
| LhtLocalization |     国产化   | Windows下虚拟国产话设备的方法，Qt安装，编译，QCefView的编译安装  |
| LhtCuda    |     CUDA相关代码   | cuda11.8版本，信号处理中常用的处理算法，归并排序，双调排序, 复数乘法，点积，最大值，最小值，均值，fft，dft，下变频，重采样，多流多线程，耗时，信息获取，多流FFT，nsight system的使用 |
| LhtCamera    |     一些相机的SDK的调用          | 海康/大华/OPT |
| LhtGoPlugins |     减少相同的代码，用Go做一些C++上做的麻烦的事         | C++Grpc接口库，Go（Grpc）数据库中间件，系统状态发布，网络测试客户端等|
| LhtEncode                |     编码和解码             | OPUS/AAC/MD5/SHA/OpenSsl/H264 |
| LhtExamples             |     测试程序                | 包含各个模块的测试使用|
| LhtFile |     简单的文件处理         | QFile/QDataStream/QTextStream/Qt文件映射写入 |
| LhtGui |     一些与UI相关的代码         | 表格、页面缩放、浮动、重绘、图表、视图 |
| LhtImage|     图像处理，转换等       | Halcon图像算法/图像转换 灰度图/YUV/RGB/MONO8/QT图像与cv::Mat转换 |
| LhtLog|     日志系统           | QsLog/GLog |
| LhtModel|     Epoll等         | Epoll/select/iocp |
| LhtOffice|  word/excel等的导出      | 用Concurrent不卡线程 |


#  :fa-tree: 界面展示
| 介绍  |  界面展示 |
|---|---|
| 常用的各种表格功能，如按钮，筛选等   | ![Image text](https://gitee.com/shijingying/lht-qt-plugins/raw/master/LhtGui/qrc/displays/table.png)  |
| 基于CefView的地图功能  | ![Image text](https://gitee.com/shijingying/lht-qt-plugins/raw/master/LhtGui/qrc/displays/map.png)  |
| 各种信号处理的图表，如眼图，星座图，瀑布图等 | ![Image text](https://gitee.com/shijingying/lht-qt-plugins/raw/master/LhtGui/qrc/displays/data_pic_tab.png) |
| 页面浮动，分栏  | ![Image text](https://gitee.com/shijingying/lht-qt-plugins/raw/master/LhtGui/qrc/displays/page_float.png)  |



# 🧡Star

