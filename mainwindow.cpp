#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "LhtGui/TableView/table.h"

#include <QDockWidget>
#include <QFileDialog>
#include <QSplitter>
#include <QTreeWidget>
#include <QWidgetAction>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //高级线程测试
    m_lhtExamples.init();

    m_lhtExamples.recv(int(1), double(1), QString("1"));

    //数据库测试 如果需要请打开Pro文件中的      #DEFINES += BUILD_SQL  更换自己的lib dll 以及  include
    m_lhtExamples.initSql();

    //相机测试  如果需要请打开Pro文件中的       #DEFINES += BUILD_CAMERA  更换自己的lib dll 以及  include
    m_lhtExamples.initCamera();

    //自定义的进度条
    initProgress();
    //过滤器
    initFilter();
    //可排序和筛选的表格
    initTableView();
    //地理位置信息和地图
    initPos();
    //水平分割器
    initQSplitter();
    //浮动窗口
    initDockWidget();
    //子窗口浮动示例
    initFloatWidget();
    //初始化地图
    initMap();
    //图表
    initPlot();

    connect(ui->img_lab,&ShotLabel::signMouseRelease,this,&MainWindow::onMouseRelease);
    connect(ui->img_lab_2,&ShotPolygonLabel::signDrawPolygon,this,&MainWindow::slotDrawPolygon);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initTableView()
{
    Table * table = new Table(ui->widget);
    ui->verticalLayout->addWidget(table);
    table->init();
    // table->setFixedSize(QSize(350,350));
    table->show();
}

void MainWindow::initProgress()
{
    m_roundLabel = new SpinningRound(ui->tab_2);
    m_roundLabel->setImagePath("D:/3.png");
    m_roundLabel->resize(150,150);

    m_proressOfTheBarrel = new ProgressOfTheBarrel(ui->tab_2);
    m_proressOfTheBarrel->setImagePath("D:/3.png");
    m_proressOfTheBarrel->resize(150,150);
    m_proressOfTheBarrel->setGeometry(300,0,150,150);

    connect(&m_timer,&QTimer::timeout,[=]{
        m_progress += 3;
        if(m_progress == 360)
            m_progress = 0;
        m_roundLabel->setProgress(m_progress);
        m_proressOfTheBarrel->setProgress(m_progress);
    });
    m_timer.start(30);
}

void MainWindow::contextMenuEvent(QContextMenuEvent* event)
{
    QMenu modelMenu;

    auto skipText = QStringLiteral("skip me");

    //添加一个筛选lineEdit
    auto* txtBox = new QLineEdit(&modelMenu);

    txtBox->setPlaceholderText(QStringLiteral("Filter"));
    txtBox->setClearButtonEnabled(true);

    auto* txtBoxAction = new QWidgetAction(&modelMenu);
    txtBoxAction->setDefaultWidget(txtBox);

    modelMenu.addAction(txtBoxAction);

    //Add result treeview to the context menu
    auto* treeView = new QTreeWidget(&modelMenu);
    treeView->header()->close();

    auto* treeViewAction = new QWidgetAction(&modelMenu);
    treeViewAction->setDefaultWidget(treeView);

    modelMenu.addAction(treeViewAction);

    QMap<QString, QTreeWidgetItem*> topLevelItems;
    auto topItem = new QTreeWidgetItem(treeView);
    topItem->setText(0, "测试");

    for (auto const& assoc : FilterList)
    {
        auto item = new QTreeWidgetItem(topItem);
        item->setText(0, assoc);
        item->setData(0, Qt::UserRole, assoc);
    }

    treeView->expandAll();

    connect(treeView, &QTreeWidget::itemClicked, [&](QTreeWidgetItem* item, int)
            {
                QString modelName = item->data(0, Qt::UserRole).toString();

                qDebug()<<"itemClicked = " <<modelName;

                modelMenu.close();
            });

    //Setup filtering
    connect(txtBox, &QLineEdit::textChanged, [&](const QString& text)
            {

                for (int i = 0; i < topItem->childCount(); ++i)
                {
                    auto child = topItem->child(i);
                    auto modelName = child->data(0, Qt::UserRole).toString();
                    const bool match = (modelName.contains(text, Qt::CaseInsensitive));
                    child->setHidden(!match);

                }

            });

    // make sure the text box gets focus so the user doesn't have to click on it
    txtBox->setFocus();
    treeView->setFixedHeight(380);
    modelMenu.setFixedSize(240, 400);
    modelMenu.exec(event->globalPos());

}



void MainWindow::initFilter()
{

}

void MainWindow::initPos()
{
    m_pos = LocalAddressInterface::CreateInterface();
    connect(m_pos.get(),&LocalAddressInterface::signLocation,this,[=] (auto pos) {
        ui->pos_lineEdit->setText(pos.country+":"+pos.region+":"+pos.city);
    }, Qt::QueuedConnection);
    m_pos->fetchLocationFromIP();
}

void MainWindow::initQSplitter()
{
    // 创建水平分割器
    QSplitter *splitter = new QSplitter(Qt::Horizontal);

    QTextEdit *leftWidget = new QTextEdit;
    leftWidget->setPlainText("左侧控件");
    QTextEdit *rightWidget = new QTextEdit;
    rightWidget->setPlainText("右侧控件");

    splitter->addWidget(leftWidget);
    splitter->addWidget(rightWidget);

    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 1);

    // 创建垂直分割器
    QSplitter *verticalSplitter = new QSplitter(Qt::Vertical);

    QTextEdit *bottomWidget = new QTextEdit;
    bottomWidget->setPlainText("底部控件");

    verticalSplitter->addWidget(splitter);
    verticalSplitter->addWidget(bottomWidget);

    verticalSplitter->setStretchFactor(0, 1);
    verticalSplitter->setStretchFactor(1, 1);
    splitter->setStyleSheet("QSplitter::handle { background-color: gray; width: 5px; }");
    verticalSplitter->setStyleSheet("QSplitter::handle { background-color: gray; height: 5px; }");

    // 将最终的分割器添加到主窗口布局
    ui->horizontalLayout_2->addWidget(verticalSplitter);
}

void MainWindow::initDockWidget()
{
    //创建停靠窗口
    QDockWidget *qw1= new QDockWidget("浮动窗口",this);
    //    qw1->setFeatures(QDockWidget::DockWidgetMovable);//设置此窗体可移动
    qw1->setFeatures(QDockWidget::AllDockWidgetFeatures);//设置此窗体所有特性--可关闭,可移动,可浮动
    qw1->setAllowedAreas(Qt::AllDockWidgetAreas);

    //    QTextEdit *te1 = new QTextEdit("allow moveable",qw1);
    QTextEdit *te1 = new QTextEdit();
    te1->setText("allow moveable");
    qw1->setWidget(te1);
    this->addDockWidget(Qt::LeftDockWidgetArea,qw1);//放在主窗体的左边
}

void MainWindow::initFloatWidget()
{
    m_dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout(m_dialog);
    m_dialog->setLayout(layout);
    connect(m_dialog,&QDialog::rejected,this,[=]{
        on_float_btn_clicked();
    });
}

void MainWindow::initMap()
{
    auto ptr = MapInterface::CreateInterface();
    ptr->setUrl("https://amap.com/");
    ptr->setParent(ui->map_widget);

}

void MainWindow::initPlot()
{
    m_customPlotExamples = new CustomPlotExamples;

    m_customPlotExamples->initLinePlot(ui->line_chart_widget);

    m_customPlotExamples->initHistogramPlot(ui->histogram_widget);

    m_customPlotExamples->initSpecPlot(ui->spec_widget);

    m_customPlotExamples->initSpecPlot2(ui->spec_widget_2);

    m_customPlotExamples->initWaterfallPlot(ui->waterfall_plot_widget);

    m_customPlotExamples->initEyePlot(ui->eye_plot_widget);

    m_customPlotExamples->initStarPlot(ui->star_plot_widget);
}

void MainWindow::on_select_img_btn_clicked()
{
    //打开操作
    QString fileName = QFileDialog::getOpenFileName(
        this, tr("open file"));
    if(!fileName.isEmpty()){
        ui->img_path->setText(fileName);
    }
    ui->img_lab->setImage(fileName);
    ui->img_lab_2->setImage(fileName);
    ui->img_merge_lab->setImagePath(fileName);
}

void MainWindow::onMouseRelease(QRect rect)
{
    ui->pos_text->append("X:"+QString::number(rect.x())+"Y:"+QString::number(rect.y())+"W:"+QString::number(rect.width())+"H:"+QString::number(rect.height()));
}

void MainWindow::slotDrawPolygon(QVector<QPointF> points)
{
    for(auto it : points){
        ui->pos_text->append("X:"+QString::number(it.x())+"Y:"+QString::number(it.y()));
    }
}

void MainWindow::on_rect_btn_clicked()
{
    ui->img_merge_lab->setDrawType(DrawRect);
}

void MainWindow::on_polygon_btn_clicked()
{
    ui->img_merge_lab->setDrawType(DrawPolygon);
}

void MainWindow::on_float_btn_clicked()
{
    if (ui->float_widget->parent() == ui->tab_7) {
        ui->float_widget->setParent(nullptr);
        m_dialog->layout()->addWidget(ui->float_widget);
        m_dialog->show();  // 显示浮动对话框
        // ui->float_widget->setAttribute(Qt::WA_TranslucentBackground); // 使背景透明
        // ui->float_widget->setAutoFillBackground(true);
        // ui->float_widget->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
        // ui->float_widget->show();
    }else{
        // 重新将浮动面板嵌入子窗口
        m_dialog->hide();
        ui->float_widget->setParent(ui->tab_7);
        ui->float_widget->setWindowFlags(Qt::Widget);
        ui->verticalLayout_3->addWidget(ui->float_widget);
    }

}


void MainWindow::on_checkBox_stateChanged(int arg1)
{
    m_customPlotExamples->setDisplayType(kSpectrum);
}


void MainWindow::on_checkBox_2_stateChanged(int arg1)
{
    m_customPlotExamples->setDisplayType(kWaterfall);
}


void MainWindow::on_checkBox_3_stateChanged(int arg1)
{
    m_customPlotExamples->setDisplayType(kAll);
}

