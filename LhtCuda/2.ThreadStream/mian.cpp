#include "kernel.cuh"
#include <thread>
#include <vector>
const int RunCount = 60;

void cpuAddVector(const int* a, const int* b, int* c, unsigned int size);

void doWork(int threadID);

long long  start1, stop1;
//单流测试
//int main()
//{
//    const int arraySize = 8192 * RunCount;
//    int *a = new int[arraySize];
//    int *b = new int[arraySize];
//    int *c = new int[arraySize];
//
//    for (int i = 0; i < arraySize;i++) {
//        a[i] = i;
//        b[i] = 8192 - i;
//    }
//
//    cudaInit(a, b, arraySize);
//
//    getThreadNum();
//    // Add vectors in parallel.
//    CHECK_GPU_TIME_START1
//    for (int i = 0; i < RunCount; i++) {
//         addWithCuda(c, arraySize);
//        
//    }
//    CHECK_GPU_TIME_END1
//
//    //printf("{1,2,3,4,5} + {10,20,30,40,50} = {%d,%d,%d,%d,%d}\n",
//    //    c[0], c[1], c[2], c[3], c[4]);
//    CHECK_GPU_TIME_START1
//    for (int j = 0; j < RunCount; j++) {
//        for (int i = 0; i < RunCount; i++) {
//            cpuAddVector(a, b, c, 8192);
//        }
//    }
//    
//    CHECK_GPU_TIME_END1
//    // cudaDeviceReset must be called before exiting in order for profiling and
//    // tracing tools such as Nsight and Visual Profiler to show complete traces.
//        cudaError_t cudaStatus = cudaDeviceReset();
//    if (cudaStatus != cudaSuccess) {
//        fprintf(stderr, "cudaDeviceReset failed!");
//        return 1;
//    }
//
//    return 0;
//}
//
//单线程 多流

int main() {
    cudaInit(nullptr, nullptr, 0);

    getThreadNum();
    test1();
    system("pause");
    return 1;
}
void cpuAddVector(const int* a, const int* b, int *c,unsigned int size) {
    for (int i = 0; i < size; i++) {
        c[i] = a[i] + b[i];
    }
}

// 要在线程中执行的函数
void doWork(int threadID) {
    int* c = new int[8192 * 10];
    for (int i = 0; i < RunCount; i++) {
        addWithCuda(c, 8192 * 10);
    }
    
}