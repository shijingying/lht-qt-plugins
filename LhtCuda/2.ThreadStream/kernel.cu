﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "kernel.cuh"

cudaEvent_t start, stop;

float elapsedTime = 0.0;

#define CHECK_GPU_TIME_START cudaEventCreate(&start);\
 cudaEventCreate(&stop);\
 cudaEventRecord(start, 0);

#define CHECK_GPU_TIME_END cudaEventRecord(stop, 0);\
cudaEventSynchronize(stop);\
cudaEventElapsedTime(&elapsedTime, start, stop);\
std::cout << elapsedTime << std::endl;\
cudaEventDestroy(start);\
cudaEventDestroy(stop);

int getThreadNum()
{
    cudaDeviceProp prop;
    int count;

    cudaGetDeviceCount(&count);
    printf("gpu num %d\n", count);
    cudaGetDeviceProperties(&prop, 0);
    printf("max thread num: %d\n", prop.maxThreadsPerBlock);
    printf("max grid dimensions: %d, %d, %d)\n",
        prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2]);
    printf("Support Streams: %d\n", prop.concurrentKernels);
    return prop.maxThreadsPerBlock;
}
int* dev_a = 0;
int* dev_b = 0;
int* dev_c = 0;

cudaError cudaInit( const int* a, const int* b, unsigned int size)
{
    cudaError_t cudaStatus;
    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
    }
    cudaStatus = cudaFree(0);
    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
    }

    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
    }

    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
    }

    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
    }
    return cudaStatus;
}

__global__ void addKernel(int *c, const int *a, const int *b)
{
    int i = threadIdx.x;
    c[i] = a[i] + b[i];
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t addWithCuda(int* c, unsigned int size)
{
    cudaError_t cudaStatus;

    //CHECK_GPU_TIME_START1

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<(size / 1024) < 1 ? 1  : (size / 1024), (size / 1024) < 1 ? size % 1024 : 1024>>>(dev_c, dev_a, dev_b);

    //CHECK_GPU_TIME_END1
    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
    }

    // Copy output vector from GPU buffer to host memory.
    //cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    //if (cudaStatus != cudaSuccess) {
    //    fprintf(stderr, "cudaMemcpy failed!");
    //}
    
    return cudaStatus;
}


#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <iostream>
#include <thread>
#include <stdio.h>

#include <thread>
#include <stdio.h>


const int N = 1 << 20;

__global__ void kernel1(float* x, int n)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    for (int i = tid; i < n; i += blockDim.x * gridDim.x) {
        x[i] = sqrt(pow(3.14159, i));
    }
}
__global__ void kernel22(float* x, int n)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    for (int i = tid; i < n; i += blockDim.x * gridDim.x) {
        x[i] = sqrt(pow(3.14159, i));
    }
}

void launch_kernel(cudaStream_t stream)
{
    float* data;
    cudaMalloc(&data, N * sizeof(float));
    kernel22 << <1, 64, 0, stream >> > (data, N);
    cudaStreamSynchronize(0);
    return;
}

//测试多线程多流
int test1()
{
    const int num_threads = 8;
    std::thread threads[num_threads];
    cudaStream_t streams[num_threads];
    for (int i = 0; i < num_threads; i++) {
        cudaStreamCreate(&streams[i]);
        threads[i] = std::thread(launch_kernel, streams[i]);
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }

    cudaDeviceReset();

    return 0;

}