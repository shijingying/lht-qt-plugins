#include "lht_basic_kernel.cuh"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "device_functions.h"

//点加
__global__ void lhtAdd(float* in1, float* in2, float* out, int len)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < len)
		out[index] = in1[index] + in2[index];
}
//点减
__global__ void lhtSub(float* in1, float* in2, float* out, int len)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < len)
		out[index] = in1[index] - in2[index];
}
//点乘
__global__ void lhtMulc(float* in1, float* in2, float* out, int len)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < len)
		out[index] = in1[index] * in2[index];
}

//乘系数
__global__ void lhtMulc(float* in1, float int2, float* out, int len)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < len)
		out[index] = in1[index] * int2;
}

//复数乘
__global__ void lhtMulc(cufftComplex* in1, cufftComplex* in2, cufftComplex* out, int len)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < len) {
		//float a = in1[index].x; // 实部
		//float b = in1[index].y; // 虚部
		//float c = in2[index].x; // 实部
		//float d = in2[index].y; // 虚部
		out[index].x = in1[index].x * in2[index].x - in1[index].y * in2[index].y; // 结果的实部
		out[index].y = in1[index].x * in2[index].y + in1[index].y * in2[index].x; // 结果的虚部
	}
}
// lock-based
__device__ volatile int g_mutex;

__global__ void lhtMergeSort(float* in1, float* out,float * temp,int blockSize,int len)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index * 2 + 2 > len)
		return;
	int tId = threadIdx.x;
	int bId = blockIdx.x;
	//1.两个点排序  块内同步
	auto ptr = in1 + index * 2;
	auto ptrResult = out + index * 2;
	auto ptrTemp = temp + index * 2;
	//if (ptr[0] > ptr[1]) {
	//	ptrResult[0] = ptr[1];
	//	ptrResult[1] = ptr[0];
	//}
	//else {
	//	ptrResult[0] = ptr[0];
	//	ptrResult[1] = ptr[1];
	//}
	//保证当前块内的数据已经排序完成   到这里排序后的数据在result中  后续操作用result和temp 做处理
	__syncthreads();
	//2.块内排序   需要一个共享空间做缓存  块内同步
	//__shared__ float tempVec[1024];  暂时不用了
	int sortLen = 2;
	int currentIndex = 0;
	while (sortLen <= blockDim.x)
	{
		if ((index % sortLen == 0)) {
			auto ptr0 = ptrResult; auto ptr1 = ptrResult + (sortLen / 2);
			auto ptrMid = ptr1; auto ptrLast = ptrResult + sortLen;
			for (int i = 0; i < sortLen; i++) {
				if (*ptr0 < *ptr1 && ptr0 != ptrMid) ptrTemp[i] = *ptr0++;
				else if (*ptr0 >= *ptr1 && ptr1 != ptrLast) ptrTemp[i] = *ptr1++;
				else {
					if (ptr0 == ptrMid) ptrTemp[i] = *ptr1++;
					else ptrTemp[i] = *ptr0++;
				}
			}
		}
		//保证当前块内的数据已经排序完成
		__syncthreads();
		//数据拷贝回result中
		ptrResult[0] = ptrTemp[0];
		ptrResult[1] = ptrTemp[1];
		sortLen *= 2;
	}
	__threadfence();
	//3.块间同步 块间同步
	sortLen = 2;
	while (sortLen <= blockSize)
	{
		auto sortLens = sortLen * blockDim.x;
		if ((bId % sortLen == 0)) {
			auto ptr0 = ptrResult; auto ptr1 = ptrResult + (sortLens / 2);
			auto ptrMid = ptr1; auto ptrLast = ptrResult + sortLens;

			for (int i = 0; i < sortLens; i++) {
				if (*ptr0 < *ptr1 && ptr0 != ptrMid) ptrTemp[i] = *ptr0++;
				else if (*ptr0 >= *ptr1 && ptr1 != ptrLast) ptrTemp[i] = *ptr1++;
				else {
					if (ptr0 == ptrMid) ptrTemp[i] = *ptr1++;
					else ptrTemp[i] = *ptr0++;
				}
			}
		}
		__threadfence();
		//数据拷贝回result中
		ptrResult[0] = ptrTemp[0];
		ptrResult[1] = ptrTemp[1];
		sortLen *= 2;
	}
	__threadfence();

}

/* Every thread gets exactly one value in the unsorted array. */
#define THREADS 512 // 2^9
#define BLOCKS 32768 // 2^15
#define NUM_VALS THREADS*BLOCKS

void print_elapsed(clock_t start, clock_t stop)
{
	double elapsed = ((double)(stop - start)) / CLOCKS_PER_SEC;
	printf("Elapsed time: %.3fs\n", elapsed);
}

float random_float()
{
	return (float)rand() / (float)RAND_MAX;
}

void array_print(float* arr, int length)
{
	int i;
	for (i = 0; i < length; ++i) {
		printf("%1.3f ", arr[i]);
	}
	printf("\n");
}

void array_fill(float* arr, int length)
{
	srand(time(NULL));
	int i;
	for (i = 0; i < length; ++i) {
		arr[i] = random_float();
	}
}

__global__ void bitonic_sort_step(float* dev_values, int j, int k)
{
	unsigned int i, ixj; /* Sorting partners: i and ixj */
	i = threadIdx.x + blockDim.x * blockIdx.x;
	ixj = i ^ j;

	/* The threads with the lowest ids sort the array. */
	if ((ixj) > i) {
		if ((i & k) == 0) {
			/* Sort ascending */
			if (dev_values[i] > dev_values[ixj]) {
				/* exchange(i,ixj); */
				float temp = dev_values[i];
				dev_values[i] = dev_values[ixj];
				dev_values[ixj] = temp;
			}
		}
		if ((i & k) != 0) {
			/* Sort descending */
			if (dev_values[i] < dev_values[ixj]) {
				/* exchange(i,ixj); */
				float temp = dev_values[i];
				dev_values[i] = dev_values[ixj];
				dev_values[ixj] = temp;
			}
		}
	}
}

/**
 * Inplace bitonic sort using CUDA.
 */
void lhtBitonicMergeSort(float* in1, float* out, int len)
{
	float* dev_values;
	size_t size = NUM_VALS * sizeof(float);

	cudaMalloc((void**)&dev_values, size);
	cudaMemcpy(dev_values, in1, size, cudaMemcpyHostToDevice);

	dim3 blocks(BLOCKS, 1);    /* Number of blocks   */
	dim3 threads(THREADS, 1);  /* Number of threads  */

	int j, k;
	/* Major step */
	for (k = 2; k <= NUM_VALS; k <<= 1) {
		/* Minor step */
		for (j = k >> 1; j > 0; j = j >> 1) {
			bitonic_sort_step << <blocks, threads >> > (dev_values, j, k);
		}
	}
	cudaMemcpy(in1, dev_values, size, cudaMemcpyDeviceToHost);
	cudaFree(dev_values);
}

//排序
__host__ void lhtSort(float* in1, float* out, int len)
{
	//归并
	//float* temp;
	//cudaMalloc(&temp,sizeof(float)* len);
	//cudaMemset(temp,0, sizeof(float) * len);
	//int blockSize = (len - 1) / 1024 + 1;
	//dim3 dimGrid(blockSize);
	//dim3 dimBlock(1024);
	//lhtMergeSort <<<dimGrid , dimBlock >>>(in1, out, temp, blockSize,len);
	//cudaFree(temp);
	//双调
	clock_t start, stop;

	float* values = (float*)malloc(NUM_VALS * sizeof(float));
	array_fill(values, NUM_VALS);

	start = clock();
	lhtBitonicMergeSort(in1, out, len); /* Inplace */
	stop = clock();

	print_elapsed(start, stop);
}

//向量和  包含下面的均值都是规约求和 
__global__ void lhtSum(float* in1, float* out, int len)
{
	unsigned int tid = threadIdx.x;
	if (tid >= len) return;
	//获取对应的指针
	float* idata = in1 + blockIdx.x * blockDim.x;

	for (int stride = 1; stride < blockDim.x; stride *= 2)//这里是做规约的  stride为步长 每次*2
	{
		if ((tid % (2 * stride)) == 0)
		{
			idata[tid] += idata[tid + stride];  //每次按步长合并两个值
		}
		//块内同步
		__syncthreads();
	}
	//在外面需要将线程块的值相加
	if (tid == 0)
		out[blockIdx.x] = idata[0];
}

//向量绝对值的和
__global__ void lhtAbsSum(float* in1, float* out, int len)
{
	unsigned int tid = threadIdx.x;

	if (tid >= len) return;
	//获取对应的指针
	float* idata = in1 + blockIdx.x * blockDim.x;

	for (int stride = 1; stride < blockDim.x; stride *= 2)
	{
		if ((tid % (2 * stride)) == 0)
		{
			idata[tid] += (idata[tid + stride] > 0 ? idata[tid + stride] : -idata[tid + stride]);
		}
		//块内同步
		__syncthreads();
	}
	//在外面需要将线程块的值相加
	if (tid == 0)
		out[blockIdx.x] = idata[0];
}
//向量均值
__global__ void lhtMean(float* in1, float* out, int len)
{
	int blocksize = 1024;
	float gpu_sum = 0;
	dim3 block(blocksize, 1);
	dim3 grid((len - 1) / block.x + 1, 1);
	lhtSum(in1, out, len);
	for (int i = 0; i < grid.x; i++)
		gpu_sum += out[i];
	out[0] = gpu_sum / len;
}

//向量绝对值均值
__global__ void lhtAbsMean(float* in1, float* out, int len)
{
	int blocksize = 1024;
	float gpu_sum = 0;
	dim3 block(blocksize, 1);
	dim3 grid((len - 1) / block.x + 1, 1);
	lhtSum(in1, out, len);
	for (int i = 0; i < grid.x; i++)
		gpu_sum += out[i];
	out[0] = gpu_sum / len;
}

//向量最大值
__global__ void lhtMax(float* in1, float* out, int len)
{
	int tid = threadIdx.x;
	if (tid >= len)return;

	float* data = in1 + blockIdx.x * blockDim.x;
	for (int stride = 1; stride < blockDim.x; stride *= 2)
	{
		if ((tid % (2 * stride)) == 0)
		{
			if (data[tid] < data[tid + stride])
			{
				data[tid] = data[tid + stride];
			}
		}
		__syncthreads();
	}
	if (tid == 0)
	{
		out[blockIdx.x] = data[0];
	}
}

//向量最小值
__global__ void lhtMin(float* in1, float* out, int len)
{
	int tid = threadIdx.x;
	if (tid >= len)return;

	float* data = in1 + blockIdx.x * blockDim.x;
	for (int stride = 1; stride < blockDim.x; stride *= 2)
	{
		if ((tid % (2 * stride)) == 0)
		{
			if (data[tid] < data[tid + stride])
			{
				data[tid] = data[tid + stride];
			}
		}
		__syncthreads();
	}
	if (tid == 0)
	{
		out[blockIdx.x] = data[0];
	}
}



void LhtCopy2Host(float* in1, float* out, int len)
{
	cudaMemcpy(out, in1, sizeof(float)*len, cudaMemcpyDeviceToHost);
}