QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# Add OpenCV Include Path for CUDA Compilation
CUDA_INC += -I'$$PWD/../../../../../git/plugin-library/VideoPlayer/include'

INCLUDEPATH += 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v12.6\include'
LIBS += -L'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v12.6\lib\x64' \
-lcublas  -lcuda -lcudadevrt \
-lcudart -lopengl32 -lcudart_static -lcufft \
-lcufftw -lcurand -lcusolver -lcusparse

OTHER_FILES +=$$PWD/kernel.cu
OTHER_FILES +=$$PWD/kernel.cuh
CUDA_SOURCES +=$$PWD/kernel.cu

CUDA_SDK = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v12.6'
CUDA_DIR = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v12.6'
QMAKE_LIBDIR += $$CUDA_DIR/lib/x64
SYSTEM_TYPE = 64

CUDA_ARCH = sm_52
NVCCFLAGS = --use_fast_math

CUDA_INC = $$join('C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v12.6\include','" -I"','-I"','"')
MSVCRT_LINK_FLAG_DEBUG = "/MDd"
MSVCRT_LINK_FLAG_RELEASE = "/MD"

CUDA_OBJECTS_DIR = ./

# Add OpenCV Include Path for CUDA Compilation
CUDA_INC += -I'$$PWD/../../../../../git/plugin-library/VideoPlayer/include'

# Ensure OpenCV is linked correctly during the CUDA compilation
CONFIG(debug, debug|release) {
    cuda.input = CUDA_SOURCES
    cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}process.obj
    cuda.commands = $$CUDA_DIR/bin/nvcc.exe -D_DEBUG $$NVCC_OPTIONS $$CUDA_INC $$CUDA_LIBS --machine $$SYSTEM_TYPE \
                      -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} -Xcompiler $$MSVCRT_LINK_FLAG_DEBUG
    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}
else {
    cuda.input = CUDA_SOURCES
    cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}process.obj
    cuda.commands = $$CUDA_DIR/bin/nvcc.exe $$NVCC_OPTIONS $$CUDA_INC $$CUDA_LIBS --machine $$SYSTEM_TYPE \
                    -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} -Xcompiler $$MSVCRT_LINK_FLAG_RELEASE
    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../git/plugin-library/VideoPlayer/lib/ -lopencv_world4100
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../git/plugin-library/VideoPlayer/lib/ -lopencv_world4100d

INCLUDEPATH += $$PWD/../../../../../git/plugin-library/VideoPlayer/include
DEPENDPATH += $$PWD/../../../../../git/plugin-library/VideoPlayer/include
