#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include "kernel.cuh"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_image1_clicked()
{
    // 创建文件对话框，允许选择图片或视频文件
    QStringList filters;
    filters << "Image Files (*.jpg *.jpeg *.png *.gif *.bmp *.tiff *.webp)"
            << "Video Files (*.mp4 *.avi *.mkv *.mov *.flv *.webm)"
            << "All Files (*.*)";

    // 打开文件对话框
    auto filePath = QFileDialog::getOpenFileName(
        nullptr,
        "Open File",
        "",
        filters.join(";;") // 使用过滤器
        );

    // 如果选择了文件
    if (!filePath.isEmpty()) {
        ui->lineEdit_image1->setText(filePath);
        auto pix = QPixmap(filePath);
        auto w = pix.width();
        auto h = pix.height();
        auto lw = ui->image1_lab->width();
        auto lh = ui->image1_lab->height();
        // 计算宽度与标签宽度的比值 和 高度与标签高度的比值
        float widthRatio = static_cast<float>(w) / lw;
        float heightRatio = static_cast<float>(h) / lh;

        image1 = cv::imread(filePath.toStdString(), cv::IMREAD_UNCHANGED);
        // 比较两个比值，决定是按宽度还是按高度缩放
        if (widthRatio > heightRatio) {
            // 如果宽度与标签宽度的比值大，则按宽度缩放
            int newWidth = lw; // 使用标签的宽度
            int newHeight = static_cast<int>(newWidth * h / w); // 根据宽度计算高度，保持宽高比

            ui->image1_lab->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));
            //cv::resize(image1, image1, cv::Size(newWidth, newHeight));
            image1Result = cv::Mat(h, w, CV_8UC4, image1.data);
            initImage1Memory( w, h, image1);
        }
        else {
            // 如果高度与标签高度的比值大，则按高度缩放
            int newHeight = lh; // 使用标签的高度
            int newWidth = static_cast<int>(newHeight * w / h); // 根据高度计算宽度，保持宽高比

            ui->image1_lab->setPixmap(pix.scaled(newWidth, newHeight, Qt::KeepAspectRatio));
            //cv::resize(image1, image1, cv::Size(newWidth, newHeight));
            image1Result = cv::Mat(h, w, CV_8UC4, image1.data);
            initImage1Memory(w, h, image1);
        }
    }
    else {
        QMessageBox::warning(nullptr, "No File Selected", "You didn't select any file.");
    }
}

void MainWindow::on_btn_image2_clicked()
{
    // 创建文件对话框，允许选择图片或视频文件
    QStringList filters;
    filters << "Image Files (*.jpg *.jpeg *.png *.gif *.bmp *.tiff *.webp)"
            << "Video Files (*.mp4 *.avi *.mkv *.mov *.flv *.webm)"
            << "All Files (*.*)";

    // 打开文件对话框
    auto filePath = QFileDialog::getOpenFileName(
        nullptr,
        "Open File",
        "",
        filters.join(";;") // 使用过滤器
        );

    // 如果选择了文件
    if (!filePath.isEmpty()) {
        ui->lineEdit_image2->setText(filePath);
        auto pix = QPixmap(filePath);
        auto w = pix.width();
        auto h = pix.height();
        auto lw = ui->image2_lab->width();
        auto lh = ui->image2_lab->height();
        // 计算宽度与标签宽度的比值 和 高度与标签高度的比值
        float widthRatio = static_cast<float>(w) / lw;
        float heightRatio = static_cast<float>(h) / lh;
        image2 = cv::imread(filePath.toStdString(), cv::IMREAD_UNCHANGED);
        // 比较两个比值，决定是按宽度还是按高度缩放
        if (widthRatio > heightRatio) {
            // 如果宽度与标签宽度的比值大，则按宽度缩放
            int newWidth = lw; // 使用标签的宽度
            int newHeight = static_cast<int>(newWidth * h / w); // 根据宽度计算高度，保持宽高比

            ui->image2_lab->setPixmap(pix.scaled(newWidth, newHeight, Qt::KeepAspectRatio));
            //cv::resize(image2, image2, cv::Size(newWidth, newHeight));
            image2Result = cv::Mat(h, w, CV_8UC4, image2.data);
            initImage2Memory( w, h,image2 );
        }
        else {
            // 如果高度与标签高度的比值大，则按高度缩放
            int newHeight = lh; // 使用标签的高度
            int newWidth = static_cast<int>(newHeight * w / h); // 根据高度计算宽度，保持宽高比

            ui->image2_lab->setPixmap(pix.scaled(newWidth, newHeight, Qt::KeepAspectRatio));
            //cv::resize(image2, image2, cv::Size(newWidth, newHeight));
            image2Result = cv::Mat(h, w, CV_8UC4, image2.data);
            initImage2Memory(w, h, image2);
        }
    }
    else {
        QMessageBox::warning(nullptr, "No File Selected", "You didn't select any file.");
    }
}

void MainWindow::on_btn_merge_clicked()
{
    LHT_TIME_CONSUMING;
    cv::Mat ret;
    imageAdd(image1Result, image2Result, ret);
    cv::cvtColor(ret, ret, cv::COLOR_BGRA2RGBA);
    QImage image(
        (uchar*)(ret.data),
        ret.cols,
        ret.rows,
        ret.step,
        QImage::Format_RGBA8888
        );
    ui->label_8->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_image1_slider_valueChanged(int value)
{
    if (image1.empty())
        return;

    LHT_TIME_CONSUMING;
    auto brightness = ui->image1_slider_brightness->value();
    ui->image1_transparency->setText(QString::number(value));
    adjustBrightnessAndOpacity(image1, value, brightness,image1Result);
    cv::Mat convertedImage;
    cv::cvtColor(image1Result, image1Result, cv::COLOR_BGRA2RGBA);
    QImage image(
        (uchar*)(image1Result.data),
        image1Result.cols,
        image1Result.rows,
        image1Result.step,
        QImage::Format_RGBA8888
        );
    ui->image1_lab->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_image2_slider_valueChanged(int value)
{
    LHT_TIME_CONSUMING;
    if (image2.empty())
        return;
    auto brightness = ui->image2_slider_brightness->value();
    ui->image2_transparency->setText(QString::number(value));
    adjustBrightnessAndOpacity(image2, value, brightness, image2Result,2);
    cv::Mat convertedImage;
    cv::cvtColor(image2Result, image2Result, cv::COLOR_BGRA2RGBA);
    QImage image(
        (uchar*)(image2Result.data),
        image2Result.cols,
        image2Result.rows,
        image2Result.step,
        QImage::Format_RGBA8888
        );
    ui->image2_lab->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_image1_slider_brightness_valueChanged(int value)
{
    if (image1.empty())
        return;
    auto opacity = ui->image1_slider->value();
    adjustBrightnessAndOpacity(image1, opacity, value, image1Result);
    ui->image1_brightness->setText(QString::number(value));
    int width = image1Result.cols;
    int height = image1Result.rows;
    cv::cvtColor(image1Result, image1Result, cv::COLOR_BGRA2RGBA);
    QImage image(
        (uchar*)(image1Result.data),
        image1Result.cols,
        image1Result.rows,
        image1Result.step,
        QImage::Format_RGBA8888
        );
    ui->image1_lab->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_image2_slider_brightness_valueChanged(int value)
{
    if (image2.empty())
        return;
    auto opacity = ui->image2_slider->value();
    adjustBrightnessAndOpacity(image2, opacity, value, image2Result, 2);
    ui->image2_brightness->setText(QString::number(value));
    int width = image2Result.cols;
    int height = image2Result.rows;
    cv::cvtColor(image2Result, image2Result, cv::COLOR_BGRA2RGBA);
    QImage image(
        (uchar*)(image2Result.data),
        image2Result.cols,
        image2Result.rows,
        image2Result.step,
        QImage::Format_RGBA8888
        );
    ui->image2_lab->setPixmap(QPixmap::fromImage(image));
}
