﻿#include <cuda_runtime.h>
#include "kernel.cuh"

#define BLOCK_SIZE 32  // 线程块大小
uchar* d_image1, *d_result1;
uchar* d_image2, *d_result2;

__global__ void imageAddKernel(uchar* img1, uchar* img2, uchar* imgres, int width1, int height1, int width2, int height2) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    uchar r1 = 0, g1 = 0, b1 = 0, a1 = 0;
    uchar r2 = 0, g2 = 0, b2 = 0, a2 = 0;

    // 计算当前像素位置
    int idx1 = (y * width1 + x) * 4;  // 对应 img1
    int idx2 = (y * width2 + x) * 4;  // 对应 img2

    if (x < width1 && y < height1) {
        // 获取当前图像1的RGBA值
        r1 = img1[idx1];
        g1 = img1[idx1 + 1];
        b1 = img1[idx1 + 2];
        a1 = img1[idx1 + 3];  // 获取透明度
    }

    if (x < width2 && y < height2) {
        // 获取当前图像2的RGBA值
        r2 = img2[idx2];
        g2 = img2[idx2 + 1];
        b2 = img2[idx2 + 2];
        a2 = img2[idx2 + 3];  // 获取透明度
    }

    // 处理合并后的RGBA值
    if (x < width1 && y < height1 && x < width2 && y < height2) {
        int index = idx2 > idx1 ? idx2 : idx1;
        // 透明度合并公式
        float alpha1 = a1 / 255.0f;
        float alpha2 = a2 / 255.0f;
        float out_alpha = alpha1 + alpha2 * (1 - alpha1);  // 每个图像的透明度独立合并

        r1 = (uchar)((r1 * alpha1 + r2 * alpha2 * (1 - alpha1)) / out_alpha);
        g1 = (uchar)((g1 * alpha1 + g2 * alpha2 * (1 - alpha1)) / out_alpha);
        b1 = (uchar)((b1 * alpha1 + b2 * alpha2 * (1 - alpha1)) / out_alpha);
        a1 = (uchar)(out_alpha * 255);  // 最终透明度

        imgres[index] = r1;
        imgres[index + 1] = g1;
        imgres[index + 2] = b1;
        imgres[index + 3] = a1;
    }
    else if (x < width1 && y < height1) {
        // 只保留图像1的像素
        imgres[idx1] = r1;
        imgres[idx1 + 1] = g1;
        imgres[idx1 + 2] = b1;
        imgres[idx1 + 3] = a1;
    }
    else if (x < width2 && y < height2) {
        // 只保留图像2的像素
        imgres[idx2] = r2;
        imgres[idx2 + 1] = g2;
        imgres[idx2 + 2] = b2;
        imgres[idx2 + 3] = a2;
    }
}

__global__ void adjustBrightnessAndOpacityKernel(uchar* d_image, uchar* d_result, int width, int height, int brightness, float opacity) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x < width && y < height) {
        // 计算当前像素位置
        int idx = (y * width + x) * 4;  // 假设图像是RGBA格式，每个像素4个字节

        // 获取当前像素的RGBA值
        uchar r = d_image[idx];
        uchar g = d_image[idx + 1];
        uchar b = d_image[idx + 2];
        uchar a = d_image[idx + 3];

        // 调整亮度（限制在0到255之间）
        r = min(max(r + brightness, 0), 255);
        g = min(max(g + brightness, 0), 255);
        b = min(max(b + brightness, 0), 255);
        a = static_cast<uchar>(a * opacity);  // 透明度值应该在0到255之间
        // 保存修改后的像素
        d_result[idx] = r;
        d_result[idx + 1] = g;
        d_result[idx + 2] = b;
        d_result[idx + 3] = a;  // 保持透明度不变
    }
}

void freeImageMemory(uchar*& d_image, uchar*& d_result)
{
    // 如果指针已经指向有效的内存块，释放它
    if (d_image != nullptr)
    {
        cudaFree(d_image);
        d_image = nullptr;
    }
    if (d_result != nullptr)
    {
        cudaFree(d_result);
        d_result = nullptr;
    }
}

void initImage1Memory(int w, int h, const cv::Mat& image)
{
    // 释放之前分配的内存（如果有）
    freeImageMemory(d_image1, d_result1);

    // 图像数据指针
    uchar* h_image = image.data;

    // 分配新内存
    cudaMalloc(&d_image1, w * h * 4 * sizeof(uchar));
    cudaMalloc(&d_result1, w * h * 4 * sizeof(uchar));

    // 将数据从主机内存复制到设备内存
    cudaMemcpy(d_image1, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
    cudaMemcpy(d_result1, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
}

void initImage2Memory(int w, int h, const cv::Mat& image)
{
    // 释放之前分配的内存（如果有）
    freeImageMemory(d_image2, d_result2);

    // 图像数据指针
    uchar* h_image = image.data;

    // 分配新内存
    cudaMalloc(&d_image2, w * h * 4 * sizeof(uchar));
    cudaMalloc(&d_result2, w * h * 4 * sizeof(uchar));

    // 将数据从主机内存复制到设备内存
    cudaMemcpy(d_image2, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
    cudaMemcpy(d_result2, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
}

void imageAdd(const cv::Mat& image1, const cv::Mat& image2, cv::Mat& result)
{
    LHT_TIME_CONSUMING;
    int width1 = image1.cols;
    int height1 = image1.rows;

    int width2 = image2.cols;
    int height2 = image2.rows;

    int w = std::max(width1, width2);
    int h = std::max(height1, height2);
    result = cv::Mat(h, w, CV_8UC4);
    uchar* d_result;
    cudaMalloc(&d_result, w * h * 4 * sizeof(uchar));

    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((w + BLOCK_SIZE - 1) / BLOCK_SIZE, (h + BLOCK_SIZE - 1) / BLOCK_SIZE);
    uchar* h_result = result.data;
    uchar* h_image1 = d_result1;
    uchar* h_image2 = d_result2;

    imageAddKernel << <numBlocks, threadsPerBlock >> > (d_result1, d_result2, d_result, width1, height1, width2, height2);

    cudaDeviceSynchronize();
    if (cudaGetLastError() != cudaSuccess) {
        std::cerr << "CUDA error: " << cudaGetErrorString(cudaGetLastError()) << std::endl;
        return;
    }
    cudaMemcpy(h_result, d_result, w * h * 4 * sizeof(uchar), cudaMemcpyDeviceToHost);

    cudaFree(d_result);

}

void adjustBrightnessAndOpacity(const cv::Mat& image, int transparency, int brightness, cv::Mat& result, int index)
{
    LHT_TIME_CONSUMING;
    int width = image.cols;
    int height = image.rows;

    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((width + BLOCK_SIZE - 1) / BLOCK_SIZE, (height + BLOCK_SIZE - 1) / BLOCK_SIZE);
    float opacity = (100 - transparency * 1.f) / 100;
    uchar* h_result = result.data;
    uchar* d_image = (index == 1) ? d_image1 : d_image2;
    uchar* d_result = (index == 1) ? d_result1 : d_result2;
    // 创建事件来记录内核的时间
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

     //启动透明度更新内核
    //cudaDeviceSynchronize();

    cudaEventRecord(start);
    for (int i = 0; i < 100; i++) 
        adjustBrightnessAndOpacityKernel << <numBlocks, threadsPerBlock >> > (d_image, d_result, width, height, brightness, opacity);
    

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float opacityTime = 0.0f;
    cudaEventElapsedTime(&opacityTime, start, stop);

     //打印每个内核的执行时间
    std::cout << "Opacity kernel execution time: " << opacityTime << " ms" << std::endl;

    //cudaDeviceSynchronize();
    if (cudaGetLastError() != cudaSuccess) {
        std::cerr << "CUDA error: " << cudaGetErrorString(cudaGetLastError()) << std::endl;
        return;
    }
    cudaMemcpy(h_result, d_result, width * height * 4 * sizeof(uchar), cudaMemcpyDeviceToHost);
}

#if 0

__global__ void updateOpacityKernel(uchar* d_result, uchar* d_image, float opacity, int width, int height) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x < width && y < height) {
        // 计算当前像素位置
        int idx = (y * width + x) * 4;  // 假设图像是RGBA格式，每个像素4个字节

        // 获取当前像素的RGBA值
        //uchar r = d_image[idx];
        //uchar g = d_image[idx + 1];
        //uchar b = d_image[idx + 2];
        uchar a = d_image[idx + 3];

        // 修改透明度
        a = static_cast<uchar>(a * opacity);  // 透明度值应该在0到255之间

        // 保存修改后的像素
        //d_result[idx] = r;
        //d_result[idx + 1] = g;
        //d_result[idx + 2] = b;
        d_result[idx + 3] = a;
    }
}


__global__ void adjustBrightnessKernel(uchar* d_image, uchar* d_result, int width, int height, int brightness) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x < width && y < height) {
        // 计算当前像素位置
        int idx = (y * width + x) * 4;  // 假设图像是RGBA格式，每个像素4个字节

        // 获取当前像素的RGBA值
        uchar r = d_image[idx];
        uchar g = d_image[idx + 1];
        uchar b = d_image[idx + 2];
        uchar a = d_image[idx + 3];

        // 调整亮度（限制在0到255之间）
        r = min(max(r + brightness, 0), 255);
        g = min(max(g + brightness, 0), 255);
        b = min(max(b + brightness, 0), 255);

        // 保存修改后的像素
        d_result[idx] = r;
        d_result[idx + 1] = g;
        d_result[idx + 2] = b;
        d_result[idx + 3] = a;  // 保持透明度不变
    }
}

int test() {
    // 加载图像（RGBA格式）
    cv::Mat image = cv::imread("C:/Users/70898/Pictures/Screenshots/2024-07-08-195655.png", cv::IMREAD_UNCHANGED);  // 读取带透明度的图像
    if (image.empty()) {
        std::cerr << "Failed to load image!" << std::endl;
        return -1;
    }

    int width = image.cols;
    int height = image.rows;

    // 图像数据指针
    uchar* h_image = image.data;
    uchar* h_result = new uchar[width * height * 4];

    // 分配GPU内存
    uchar* d_image, * d_result;
    cudaMalloc(&d_image, width * height * 4 * sizeof(uchar));
    cudaMalloc(&d_result, width * height * 4 * sizeof(uchar));
    cv::Mat result;
    {
    LHT_TIME_CONSUMING;
    // 将图像数据复制到GPU
    cudaMemcpy(d_image, h_image, width * height * 4 * sizeof(uchar), cudaMemcpyHostToDevice);

    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((width + BLOCK_SIZE - 1) / BLOCK_SIZE, (height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    // 透明度值（修改为0.5表示50%透明）
    float opacity = 0.5f;

        //for (int i = 0; i < 100; i++) {
            // 启动CUDA内核
            updateOpacityKernel << <numBlocks, threadsPerBlock >> > (d_result, d_image, opacity, width, height);

            // 检查CUDA内核执行是否成功
            cudaDeviceSynchronize();
        //}

    // 将结果从GPU复制回主机
    cudaMemcpy(h_result, d_result, width * height * 4 * sizeof(uchar), cudaMemcpyDeviceToHost);

    // 创建修改后的图像
     result = cv::Mat(height, width, CV_8UC4, h_result);
    }
    // 将 RGBA 转换为 RGB
    cv::Mat rgb_result;
    cv::cvtColor(result, rgb_result, cv::COLOR_RGBA2BGR);
    // 保存修改后的图像
    cv::imshow("output_image", rgb_result);

    cv::waitKey(0);

    // 清理资源
    cudaFree(d_image);
    cudaFree(d_result);
    delete[] h_result;
    return 0;
}

void updateOpacity(const cv::Mat& image, int transparency, cv::Mat& result, int index)
{
    int width = image.cols;
    int height = image.rows;

    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((width + BLOCK_SIZE - 1) / BLOCK_SIZE, (height + BLOCK_SIZE - 1) / BLOCK_SIZE);
    float opacity = (100 - transparency * 1.f) / 100;
    uchar* h_result = result.data;
    uchar* d_image = (index == 1) ? d_resultBrightness1 : d_resultBrightness2;
    uchar* d_result = (index == 1) ? d_resultOpacity1 : d_resultOpacity2;

    updateOpacityKernel << <numBlocks, threadsPerBlock >> > (d_result, d_image, opacity, width, height);

    cudaDeviceSynchronize();
    if (cudaGetLastError() != cudaSuccess) {
        std::cerr << "CUDA error: " << cudaGetErrorString(cudaGetLastError()) << std::endl;
        return;
    }
    cudaMemcpy(h_result, d_result, width * height * 4 * sizeof(uchar), cudaMemcpyDeviceToHost);

}


void adjustBrightness(const cv::Mat& image, int brightness, cv::Mat& result, int index)
{
    int width = image.cols;
    int height = image.rows;

    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((width + BLOCK_SIZE - 1) / BLOCK_SIZE, (height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    uchar* h_result = result.data;
    uchar* d_image = (index == 1) ? d_resultOpacity1 : d_resultOpacity2;
    uchar* d_result = (index == 1) ? d_resultBrightness1 : d_resultBrightness2;

    adjustBrightnessKernel << <numBlocks, threadsPerBlock >> > (d_image, d_result, width, height, brightness);

    cudaDeviceSynchronize();
    if (cudaGetLastError() != cudaSuccess) {
        std::cerr << "CUDA error: " << cudaGetErrorString(cudaGetLastError()) << std::endl;
        return;
    }
    cudaMemcpy(h_result, d_result, width * height * 4 * sizeof(uchar), cudaMemcpyDeviceToHost);

    cudaFree(d_image);
}

#endif