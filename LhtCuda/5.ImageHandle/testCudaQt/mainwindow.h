#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <opencv2/opencv.hpp>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected slots:
    void on_btn_image1_clicked();

    void on_btn_image2_clicked();

    void on_btn_merge_clicked();

    void on_image1_slider_valueChanged(int value);

    void on_image2_slider_valueChanged(int value);

    void on_image1_slider_brightness_valueChanged(int value);

    void on_image2_slider_brightness_valueChanged(int value);
private:
    Ui::MainWindow *ui;

    cv::Mat image1;
    cv::Mat image2;
    cv::Mat image1Result;
    cv::Mat image2Result;

    cv::Mat result;
};
#endif // MAINWINDOW_H
