#include "mainwindow.h"
#include <QApplication>


#include <cuda_runtime.h>

void setGPU(int gpuIndex) {
    int deviceCount = 0;

    // 查询系统中可用的 GPU 数量
    cudaGetDeviceCount(&deviceCount);

    if (deviceCount == 0) {
        std::cerr << "没有找到 GPU 设备！" << std::endl;
        return;
    }

    std::cout << "系统中发现 " << deviceCount << " 个 GPU。" << std::endl;

    // 检查指定的 GPU 索引是否有效
    if (gpuIndex >= deviceCount) {
        std::cerr << "无效的 GPU 索引。" << std::endl;
        return;
    }

    // 设置使用 GPU 1（注意：GPU 索引是从 0 开始的，所以 GPU 1 对应索引 1）
    cudaSetDevice(gpuIndex);

    // 输出当前选择的 GPU 信息
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, gpuIndex);
    std::cout << "当前使用的 GPU: " << deviceProp.name << std::endl;
}

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    setGPU(0);
    MainWindow w;
    w.show();
    return a.exec();
}
