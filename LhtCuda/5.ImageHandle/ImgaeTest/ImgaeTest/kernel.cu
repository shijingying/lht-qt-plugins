﻿#define CUDA_API_PER_THREAD_DEFAULT_STREAM 1 

#include <cuda_runtime.h>
#include "kernel.cuh"

#define BLOCK_SIZE 32  // 线程块大小
uchar* d_image1, *d_result1;
uchar* d_image2, *d_result2;

__global__ void imageAddKernel(uchar* img1, uchar* img2, uchar* imgres, int width1, int height1, int width2, int height2) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    uchar r1 = 0, g1 = 0, b1 = 0, a1 = 0;
    uchar r2 = 0, g2 = 0, b2 = 0, a2 = 0;

    // 计算当前像素位置
    int idx1 = (y * width1 + x) * 4;  // 对应 img1
    int idx2 = (y * width2 + x) * 4;  // 对应 img2

    if (x < width1 && y < height1) {
        // 获取当前图像1的RGBA值
        r1 = img1[idx1];
        g1 = img1[idx1 + 1];
        b1 = img1[idx1 + 2];
        a1 = img1[idx1 + 3];  // 获取透明度
    }

    if (x < width2 && y < height2) {
        // 获取当前图像2的RGBA值
        r2 = img2[idx2];
        g2 = img2[idx2 + 1];
        b2 = img2[idx2 + 2];
        a2 = img2[idx2 + 3];  // 获取透明度
    }
    int index = idx2 > idx1 ? idx2 : idx1;
    // 处理合并后的RGBA值
    if (x < width1 && y < height1 && x < width2 && y < height2) {
        
        // 透明度合并公式
        float alpha1 = a1 / 255.0f;
        float alpha2 = a2 / 255.0f;
        float out_alpha = alpha1 + alpha2 * (1 - alpha1);  // 每个图像的透明度独立合并

        r1 = (uchar)((r1 * alpha1 + r2 * alpha2 * (1 - alpha1)) / out_alpha);
        g1 = (uchar)((g1 * alpha1 + g2 * alpha2 * (1 - alpha1)) / out_alpha);
        b1 = (uchar)((b1 * alpha1 + b2 * alpha2 * (1 - alpha1)) / out_alpha);
        a1 = (uchar)(out_alpha * 255);  // 最终透明度

        imgres[index] = b1;
        imgres[index + 1] = g1;
        imgres[index + 2] = r1;
        imgres[index + 3] = a1;
    }
    else if (x < width1 && y < height1) {
        // 只保留图像1的像素
        imgres[index] = b1;
        imgres[index + 1] = g1;
        imgres[index + 2] = r1;
        imgres[index + 3] = a1;
    }
    else if (x < width2 && y < height2) {
        // 只保留图像2的像素
        imgres[index] = b2;
        imgres[index + 1] = g2;
        imgres[index + 2] = r2;
        imgres[index + 3] = a2;
    }
}

__global__ void adjustBrightnessAndOpacityKernel(uchar* d_image, uchar* d_result, int width, int height, int brightness, float opacity) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x < width && y < height) {
        // 计算当前像素位置
        int idx = (y * width + x) * 4;  // 假设图像是RGBA格式，每个像素4个字节

        // 获取当前像素的RGBA值
        uchar r = d_image[idx];
        uchar g = d_image[idx + 1];
        uchar b = d_image[idx + 2];
        uchar a = d_image[idx + 3];

        // 调整亮度（限制在0到255之间）
        r = min(max(r + brightness, 0), 255);
        g = min(max(g + brightness, 0), 255);
        b = min(max(b + brightness, 0), 255);
        a = static_cast<uchar>(a * opacity);  // 透明度值应该在0到255之间
        // 保存修改后的像素
        d_result[idx] = r;
        d_result[idx + 1] = g;
        d_result[idx + 2] = b;
        d_result[idx + 3] = a;  // 保持透明度不变
    }
}

void freeImageMemory(uchar*& d_image, uchar*& d_result)
{
    // 如果指针已经指向有效的内存块，释放它
    if (d_image != nullptr)
    {
        cudaFree(d_image);
        d_image = nullptr;
    }
    if (d_result != nullptr)
    {
        cudaFree(d_result);
        d_result = nullptr;
    }
}

void initImage1Memory(int w, int h, const cv::Mat& image)
{
    // 释放之前分配的内存（如果有）
    freeImageMemory(d_image1, d_result1);

    // 图像数据指针
    uchar* h_image = image.data;

    // 分配新内存
    cudaMalloc(&d_image1, w * h * 4 * sizeof(uchar));
    cudaMalloc(&d_result1, w * h * 4 * sizeof(uchar));

    // 将数据从主机内存复制到设备内存
    cudaMemcpy(d_image1, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
    cudaMemcpy(d_result1, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
}

void initImage2Memory(int w, int h, const cv::Mat& image)
{
    // 释放之前分配的内存（如果有）
    freeImageMemory(d_image2, d_result2);

    // 图像数据指针
    uchar* h_image = image.data;

    // 分配新内存
    cudaMalloc(&d_image2, w * h * 4 * sizeof(uchar));
    cudaMalloc(&d_result2, w * h * 4 * sizeof(uchar));

    // 将数据从主机内存复制到设备内存
    cudaMemcpy(d_image2, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
    cudaMemcpy(d_result2, h_image, w * h * 4 * sizeof(uchar), cudaMemcpyHostToDevice);
}

void initImageMemory(int index, int w, int h, const cv::Mat& image) 
{
    if (index == 0)
        initImage1Memory( w,  h, image);
    else if (index == 1)
        initImage2Memory(w, h, image);
}

void imageAdd(const cv::Mat& image1, const cv::Mat& image2, cv::Mat& result)
{
    LHT_TIME_CONSUMING;
    int width1 = image1.cols;
    int height1 = image1.rows;

    int width2 = image2.cols;
    int height2 = image2.rows;

    int w = std::max(width1, width2);
    int h = std::max(height1, height2);
    result = cv::Mat(h, w, CV_8UC4);
    static  uchar* d_result = nullptr;
    static  uchar* h_result = nullptr;
    static  int size = 0;
    if (size < (w * h * 4 * sizeof(uchar))) {
        cudaFree(d_result);
        cudaFreeHost(h_result);
        size = w * h * 4 * sizeof(uchar);
        cudaMalloc((void**)&d_result, size);
        cudaMallocHost((void**)&h_result, size);
        std::atexit([] {

            cudaFree(d_result);
            cudaFreeHost(h_result);
        });
    }
    
    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((w + BLOCK_SIZE - 1) / BLOCK_SIZE, (h + BLOCK_SIZE - 1) / BLOCK_SIZE);
    //uchar* h_result = result.data;

    imageAddKernel << <numBlocks, threadsPerBlock >> > (d_result1, d_result2, d_result, width1, height1, width2, height2);

    //cudaThreadSynchronize();

    cudaMemcpy(h_result, d_result, w * h * 4 * sizeof(uchar), cudaMemcpyDeviceToHost);

    //cudaThreadSynchronize();

    memcpy(result.data, h_result, w * h * 4 * sizeof(uchar));

}

void adjustBrightnessAndOpacity(const cv::Mat& image, int transparency, int brightness, cv::Mat& result, int index)
{
    LHT_TIME_CONSUMING;
    int width = image.cols;
    int height = image.rows;

    // 设置线程块和网格的大小
    dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 numBlocks((width + BLOCK_SIZE - 1) / BLOCK_SIZE, (height + BLOCK_SIZE - 1) / BLOCK_SIZE);
    float opacity = (100 - transparency * 1.f) / 100;

    uchar* d_image = (index == 1) ? d_image1 : d_image2;
    uchar* d_result = (index == 1) ? d_result1 : d_result2;

    adjustBrightnessAndOpacityKernel << <numBlocks, threadsPerBlock >> > (d_image, d_result, width, height, brightness, opacity);
    
}
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "iostream"

using namespace std;


float cuda_host_alloc_test(int size, bool up)
{
    // 耗时统计
    cudaEvent_t start, stop;
    float elapsedTime;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int* a, * dev_a;

    // 主机上分配页锁定内存
    // cudaError_t cudaStatus = cudaHostAlloc((void **)&a, size * sizeof(*a), cudaHostAllocDefault);
    cudaError_t cudaStatus = cudaMallocHost((void**)&a, size * sizeof(*a));
    if (cudaStatus != cudaSuccess)
    {
        printf("host alloc fail!\n");
        return -1;
    }

    // 在设备上分配内存空间
    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(*dev_a));
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "cudaMalloc failed\n");
        return -1;
    }

    cudaEventRecord(start, 0);

    // 计时开始
    for (int i = 0; i < 100; ++i)
    {
        // 主机拷贝到设备
        cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(*a), cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy host to device failed!!!\n");
            return -1;
        }

        // 从设备拷贝到主机
        cudaStatus = cudaMemcpy(a, dev_a, size * sizeof(*dev_a), cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy device to host failed!!!\n");
            return -1;
        }
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsedTime, start, stop);

    cudaFreeHost(a);
    cudaFree(dev_a);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return (float)elapsedTime / 1000;
}

float cuda_host_Malloc_test(int size, bool up)
{
    // 耗时统计
    cudaEvent_t start, stop;
    float elapsedTime;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int* a, * dev_a;

    // 主机上分配页锁定内存
    a = (int*)malloc(size * sizeof(*a));

    // 在设备上分配内存空间
    cudaError_t cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(*dev_a));
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "cudaMalloc failed\n");
        return -1;
    }

    cudaEventRecord(start, 0);

    // 计时开始
    for (int i = 0; i < 100; ++i)
    {
        // 主机拷贝到设备
        cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(*a), cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy host to device failed!!!\n");
            return -1;
        }

        // 从设备拷贝到主机
        cudaStatus = cudaMemcpy(a, dev_a, size * sizeof(*dev_a), cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy device to host failed!!!\n");
            return -1;
        }
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsedTime, start, stop);

    free(a);
    cudaFree(dev_a);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return (float)elapsedTime / 1000;
}

int test()
{
    float allocTime = cuda_host_alloc_test(100000, true);
    cout << "页锁定内存: " << allocTime << " s" << endl;

    float mallocTime = cuda_host_Malloc_test(100000, true);
    cout << "可分页内存: " << mallocTime << " s" << endl;
    return 1;
}
