#include "QtWidgetsClass.h"
#include <QFileDialog>
#include <QMessageBox>
#include "kernel.cuh"


/**
    1.多图片的   OK 
    2.多线程的
    3.多流
*/
QtWidgetsClass::QtWidgetsClass(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
    th = new ImageHandleThread;
    //th1 = new ImageHandleThread;
    connect(th,&ImageHandleThread::signUpdateImage,this,&QtWidgetsClass::newImage);
    th->init();

    test();
    //th1->init();
}

QtWidgetsClass::~QtWidgetsClass()
{}

void QtWidgetsClass::on_btn_image1_clicked()
{

    // 创建文件对话框，允许选择图片或视频文件
    QStringList filters;
    filters << "Image Files (*.jpg *.jpeg *.png *.gif *.bmp *.tiff *.webp)"
        << "Video Files (*.mp4 *.avi *.mkv *.mov *.flv *.webm)"
        << "All Files (*.*)";

    // 打开文件对话框
    auto filePath = QFileDialog::getOpenFileName(
        nullptr,
        "Open File",
        "",
        filters.join(";;") // 使用过滤器
    );
    if (!filePath.isEmpty()) {
        auto mat = cv::imread(filePath.toStdString(), cv::IMREAD_UNCHANGED);
        th->setImage(mat, 0);
        //th1->setImage(mat, 0);
        return;
    }
    // 如果选择了文件
    if (!filePath.isEmpty()) {
        ui.lineEdit_image1->setText(filePath);
        auto pix = QPixmap(filePath);
        auto w = pix.width();
        auto h = pix.height();

        image1 = cv::imread(filePath.toStdString(), cv::IMREAD_UNCHANGED);
        ui.image1_lab->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));
        image1Result = cv::Mat(h, w, CV_8UC4, image1.data);
        initImage1Memory(w, h, image1);
        updateShow();
    }
    else {
        QMessageBox::warning(nullptr, "No File Selected", "You didn't select any file.");
    }
}

void QtWidgetsClass::on_btn_image2_clicked()
{

    // 创建文件对话框，允许选择图片或视频文件
    QStringList filters;
    filters << "Image Files (*.jpg *.jpeg *.png *.gif *.bmp *.tiff *.webp)"
        << "Video Files (*.mp4 *.avi *.mkv *.mov *.flv *.webm)"
        << "All Files (*.*)";

    // 打开文件对话框
    auto filePath = QFileDialog::getOpenFileName(
        nullptr,
        "Open File",
        "",
        filters.join(";;") // 使用过滤器
    );
    if (!filePath.isEmpty()) {
        auto mat = cv::imread(filePath.toStdString(), cv::IMREAD_UNCHANGED);
        th->setImage(mat, 1);
        //th1->setImage(mat, 1);
        return;
    }
    // 如果选择了文件
    if (!filePath.isEmpty()) {
        ui.lineEdit_image2->setText(filePath);
        auto pix = QPixmap(filePath);
        auto w = pix.width();
        auto h = pix.height();
        image2 = cv::imread(filePath.toStdString(), cv::IMREAD_UNCHANGED);
        ui.image2_lab->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));

        image2Result = cv::Mat(h, w, CV_8UC4, image2.data);
        initImage2Memory(w, h, image2);
        updateShow();
    }
    else {
        QMessageBox::warning(nullptr, "No File Selected", "You didn't select any file.");
    }
}

void QtWidgetsClass::on_btn_merge_clicked()
{
    th->recv(DrawType::DrawChannel1TypeNone, 0, 0);
    //th1->recv(DrawType::DrawChannel1TypeNone, 0, 0);
    return;
    LHT_TIME_CONSUMING;
    cv::Mat ret;
    imageAdd(image1Result, image2Result, ret);
    cv::cvtColor(ret, ret, cv::COLOR_BGRA2RGBA);
    QImage image(
        (uchar*)(ret.data),
        ret.cols,
        ret.rows,
        ret.step,
        QImage::Format_RGBA8888
    );
    ui.label_8->setPixmap(QPixmap::fromImage(image));
}

void QtWidgetsClass::on_image1_slider_valueChanged(int value)
{

    auto brightness = ui.image1_slider_brightness->value();
    th->recv(DrawType::DrawChannel1TypeOpacityBrightness, value, brightness);
    //th1->recv(DrawType::DrawChannel1TypeOpacityBrightness, value, brightness);
    return;
    if (image1.empty())
        return;
    LHT_TIME_CONSUMING;
    ui.image1_transparency->setText(QString::number(value));
    adjustBrightnessAndOpacity(image1, value, brightness,image1Result);
    updateShow();
}

void QtWidgetsClass::on_image2_slider_valueChanged(int value)
{
    LHT_TIME_CONSUMING;

    auto brightness = ui.image2_slider_brightness->value();
    th->recv(DrawType::DrawChannel2TypeOpacityBrightness, value, brightness);
    //th1->recv(DrawType::DrawChannel2TypeOpacityBrightness, value, brightness);
    return;
    if (image2.empty())
        return;
    ui.image2_transparency->setText(QString::number(value));
    adjustBrightnessAndOpacity(image2, value, brightness, image2Result,2);
    updateShow();
}

void QtWidgetsClass::on_image1_slider_brightness_valueChanged(int value)
{

    auto opacity = ui.image1_slider->value();
    th->recv(DrawType::DrawChannel1TypeOpacityBrightness, opacity, value);
    //th1->recv(DrawType::DrawChannel1TypeOpacityBrightness, opacity, value);
    return;
    if (image1.empty())
        return;
    ui.image1_brightness->setText(QString::number(value));
    adjustBrightnessAndOpacity(image1, opacity, value, image1Result);
    updateShow();
}

void QtWidgetsClass::on_image2_slider_brightness_valueChanged(int value)
{

    auto opacity = ui.image2_slider->value();
    th->recv(DrawType::DrawChannel2TypeOpacityBrightness, opacity, value);
    //th1->recv(DrawType::DrawChannel2TypeOpacityBrightness, opacity, value);
    return;
    if (image2.empty())
        return;
    ui.image2_brightness->setText(QString::number(value));
    adjustBrightnessAndOpacity(image2, opacity, value, image2Result, 2);
    updateShow();
}

void QtWidgetsClass::updateShow()
{
    on_btn_merge_clicked();
}

void QtWidgetsClass::newImage(std::shared_ptr<QImage> image)
{
    if (!image || !image->isNull()) { // 检查空指针和数据有效性
        ui.label_8->setPixmap(QPixmap::fromImage(*image.get()));
    }
    else {
        qDebug() << "Invalid image pointer!";
    }
}
