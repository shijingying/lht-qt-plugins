#include "QtWidgetsClass.h"
#include <QApplication>


#include <cuda_runtime.h>

void setGPU(int gpuIndex) {
    int deviceCount = 0;

    cudaGetDeviceCount(&deviceCount);

    if (deviceCount == 0) {
        std::cerr << "GPU Count" << std::endl;
        return;
    }

    std::cout << "totle  " << deviceCount << "  GPU " << std::endl;

    // ���ָ���� GPU �����Ƿ���Ч
    if (gpuIndex >= deviceCount) {
        std::cerr << "out range  GPU index" << std::endl;
        return;
    }

    // ����ʹ�� GPU 1��ע�⣺GPU �����Ǵ� 0 ��ʼ�ģ����� GPU 1 ��Ӧ���� 1��
    cudaSetDevice(gpuIndex);

    // �����ǰѡ��� GPU ��Ϣ
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, gpuIndex);
    std::cout << "set GPU: " << deviceProp.name << std::endl;
}

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<std::shared_ptr<QImage>>("std::shared_ptr<QImage>");
    setGPU(0);
    QtWidgetsClass w;
    w.show();
    return a.exec();
}
