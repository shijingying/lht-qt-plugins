#pragma once

#include <QMainWindow>
#include <opencv2/opencv.hpp>
#include "ui_QtWidgetsClass.h"
#include "thread/image_handle_thread.h"

class QtWidgetsClass : public QMainWindow
{
	Q_OBJECT

public:
	QtWidgetsClass(QWidget *parent = nullptr);
	~QtWidgetsClass();
protected slots:
	void on_btn_image1_clicked();

	void on_btn_image2_clicked();

	void on_btn_merge_clicked();

	void on_image1_slider_valueChanged(int value);
	
	void on_image2_slider_valueChanged(int value);

	void on_image1_slider_brightness_valueChanged(int value);

	void on_image2_slider_brightness_valueChanged(int value);

	void updateShow();

	void newImage(std::shared_ptr<QImage> image);
private:
	Ui::QtWidgetsClassClass ui;

	cv::Mat image1;
	cv::Mat image2;
	cv::Mat image1Result;
	cv::Mat image2Result;

	cv::Mat result;
	ImageHandleThread* th;
	ImageHandleThread* th1;
};
