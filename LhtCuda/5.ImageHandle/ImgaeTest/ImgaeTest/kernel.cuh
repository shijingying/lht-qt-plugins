#include <iostream>
#include <opencv2/opencv.hpp>

#define LHT_TIME_CONSUMING LhtTimeConsuming __time_consuming(__func__, __LINE__)
//耗时计算
class LhtTimeConsuming {
public:
    LhtTimeConsuming(const char* func, int line)
        : func_name(func), line_number(line) {
        start_time = std::chrono::high_resolution_clock::now();
    }

    ~LhtTimeConsuming() {
        // 记录结束时间
        auto end_time = std::chrono::high_resolution_clock::now();

        // 计算耗时，并转换为毫秒
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        // 打印耗时（单位：毫秒）
        std::cout << "Time consuming in function: " << func_name << " at line " << line_number
            << " is " << duration.count() << " ms."<<std::endl;
    }

private:
    const char* func_name;
    int line_number;
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
};

int test();
//更新图像透明度
void updateOpacity(const cv::Mat& source1, int transparency, cv::Mat& result , int index = 1);

//合并图像  4通道合并  支持透明度合并
void imageAdd(const cv::Mat& image1, const cv::Mat& image2, cv::Mat& result);

//更新图像亮度
void adjustBrightness(const cv::Mat& image, int brightness, cv::Mat& result, int index = 1);

void adjustBrightnessAndOpacity(const cv::Mat& image, int transparency, int brightness, cv::Mat& result, int index = 1);

void initImageMemory(int index , int w, int h, const cv::Mat& image);

void initImage1Memory(int w, int h, const cv::Mat& image);

void initImage2Memory(int w, int h, const cv::Mat& image);