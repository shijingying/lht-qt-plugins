#include "image_handle_thread.h"
#include "../kernel.cuh"
#include <QImage>
#include <QPixmap>
ImageHandleThread::ImageHandleThread()
{

}

ImageHandleThread::~ImageHandleThread()
{

}

void ImageHandleThread::setImage(cv::Mat image, int index)
{
    if (index >= 0 && index < 4) {
        int w = image.cols;
        int h = image.rows;
        m_imageSource[index] = image;
        m_imageResult[index] = cv::Mat(h, w, CV_8UC4, image.data);
        initImage1Memory(w, h, image);
    }
    recv(DrawType::DrawChannel1TypeNone, 0, 0);
}


bool ImageHandleThread::handleData(const std::tuple<DrawType, int, int>& data)
{
    // 获取 std::tuple 中的各个元素
    DrawType drawType = std::get<0>(data);  // 获取 DrawType
    int opacity = std::get<1>(data);         // 获取第一个 int
    int brightness = std::get<2>(data);         // 获取第二个 int

    // 根据 drawType 执行不同的操作
    switch (drawType) {
    case DrawType::DrawChannel1TypeNone:
    case DrawType::DrawChannel2TypeNone:
        break;
    case DrawType::DrawChannel1TypeOpacityBrightness:
    case DrawType::DrawChannel2TypeOpacityBrightness:
    {
        int index;
        if (drawType == DrawType::DrawChannel1TypeOpacityBrightness)
            index = 0;
        else
            index = 1;
        adjustBrightnessAndOpacity(m_imageSource[index], opacity, brightness, m_imageResult[index]);
    }
    break;
    default:
        qDebug() << "Unhandled DrawType!";
        break;
    }


    signUpdateImage(std::move(mergeImage(DrawType::DrawChannel1TypeNone)));
    // 返回 true 表示数据处理成功
    return true;
}


std::shared_ptr<QImage> ImageHandleThread::mergeImage(DrawType type)
{
    LHT_TIME_CONSUMING;
    cv::Mat ret;
    imageAdd(m_imageResult[0], m_imageResult[1], ret);
    cv::cvtColor(ret, ret, cv::COLOR_BGRA2RGBA);
    std::shared_ptr<QImage> image(new QImage(
        (uchar*)(ret.data),
        ret.cols,
        ret.rows,
        ret.step,
        QImage::Format_RGBA8888
    ));
    return image;
}
