﻿#define CUDA_API_PER_THREAD_DEFAULT_STREAM 1 

#include <iostream>
#include <cuda_runtime.h>
#include <cuComplex.h>
#include <cufft.h>
#include "kernel.cuh"

#define BLOCK_SIZE 32  // 线程块大小
cuDoubleComplex* d_ww = nullptr;
cuDoubleComplex* d_y = nullptr;
cuDoubleComplex* d_source = nullptr;
cuDoubleComplex* d_result = nullptr;
cuDoubleComplex* d_fv = nullptr;
cuDoubleComplex* d_w = nullptr;
cuDoubleComplex* d_a = nullptr;
int* d_n = nullptr;
int* d_l = nullptr;
int* d_k = nullptr;
#include <cuda_runtime.h>
#include <cufft.h>
#include <complex>
#include <vector>
#include <stdexcept>
cufftHandle plan;
 __global__ void cuDiv_64fc(cuDoubleComplex* src2,int n)
 {
     int i = blockIdx.x * blockDim.x + threadIdx.x;
     if (i < n) {
         src2[i] = make_cuDoubleComplex(src2[i].x / n, src2[i].y / n);
     }
 }

 static void doFFTForward_C_64fc(cuDoubleComplex* data, int nfft)
{
    // 1) 创建 cuFFT 计划 (plan)
     static bool f = 1;
     if (f) {
         cufftResult ret = cufftPlan1d(&plan, nfft, CUFFT_Z2Z, 1);
         if (ret != CUFFT_SUCCESS) {
             throw std::runtime_error("cufftPlan1d for forward FFT failed.");
         }
     }


    // 4) 执行前向 FFT
    cufftExecZ2Z(plan, data, data, CUFFT_FORWARD);

}

static void doFFTInverse_C_64fc(cuDoubleComplex* data, int nfft)
{

    // 4) 执行逆向 FFT
    cufftExecZ2Z(plan, data, data, CUFFT_INVERSE);

    int blockSize = 256;
    int gridSize = (nfft + blockSize - 1) / blockSize;
    cuDiv_64fc << <gridSize, blockSize >> > (data, nfft);
    // ---- IPP 中若指定 IPP_FFT_DIV_INV_BY_N，逆变换会自动 / nfft
    // ---- cuFFT 不会自动除，以后需要自己除
}
#include <cuComplex.h>
#include <math.h>
__device__ double M_PI = 3.14159265358980;
__device__ cuDoubleComplex complexPow(const cuDoubleComplex& base, double exp)
{
    double real = cuCreal(base);
    double imag = cuCimag(base);
    double r = sqrt(real * real + imag * imag); // 模

    // 处理 r == 0 的情况
    if (r == 0.0)
    {
        if (exp > 0.0)
        {
            return make_cuDoubleComplex(0.0, 0.0);
        }
        else
        {
            // 未定义行为，可以返回一个特殊值或处理错误
            return make_cuDoubleComplex(INFINITY, INFINITY);
        }
    }

    double theta = atan2(imag, real); // 相位角

    // 幂运算
    double r_pow = pow(r, exp);      // r^exp
    double theta_exp = exp * theta;  // θ * exp

    // 将 theta_exp 归一化到 [0, 2π) 以提高精度
    theta_exp = fmod(theta_exp, 2.0 * M_PI);
    if (theta_exp < 0.0)
        theta_exp += 2.0 * M_PI;

    // 转回直角坐标
    double real_part = r_pow * cos(theta_exp);
    double imag_part = r_pow * sin(theta_exp);

    return make_cuDoubleComplex(real_part, imag_part);
}

__global__ void createDww(cuDoubleComplex*  ww, cuDoubleComplex* fv, cuDoubleComplex *w, int* n, int *L)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < L[0]) {
        int kk = (-n[0] + 1) + i;
        double kk2 = double(kk) * double(kk) / 2.0;
        // 计算 w^kk2
        ww[i] = complexPow(w[0], kk2);

    }
}

__global__ void createDfv(cuDoubleComplex* ww, cuDoubleComplex* fv, int L)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < L) {
        cuDoubleComplex one = make_cuDoubleComplex(1.0, 0.0);
        fv[i] = cuCdiv(one, ww[i]);
    }
}

__global__ void computeY(cuDoubleComplex* y, const cuDoubleComplex* x, const cuDoubleComplex* a,const cuDoubleComplex* ww,const int* n) 
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    // 确保不越界
    if (i < n[0]) {
        // 计算 a^(-i)
        double exp = -double(i);
        cuDoubleComplex a_neg_i = complexPow(a[0], exp);

        // 获取 ww 索引
        int ww_idx = (n[0] - 1) + i;

        // 更新 y[i] = x[i] * a^(-i) * ww[ww_idx]
        cuDoubleComplex temp = cuCmul(x[i], a_neg_i); // x[i] * a^(-i)
        y[i] = cuCmul(temp, ww[ww_idx]);              // * ww[ww_idx]
    }
}

__global__ void elementWiseComplexMulInplace(const cuDoubleComplex* src2,cuDoubleComplex* dst,int n)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) {
        dst[i] = cuCmul(src2[i], dst[i]);
    }
}

__global__ void elementWiseComplexMul(const cuDoubleComplex* src1, cuDoubleComplex* src2, cuDoubleComplex* dst, int offset,int n )
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) {
        dst[i] = cuCmul(src1[i + offset], src2[i + offset]);
    }
}

void freeImageMemory()
{
    if (d_ww != nullptr)
    {
        cudaFree(d_ww);
        d_ww = nullptr;
    }
    if (d_y != nullptr)
    {
        cudaFree(d_y);
        d_y = nullptr;
    }
    if (d_source != nullptr)
    {
        cudaFree(d_source);
        d_source = nullptr;
    }
    if (d_fv != nullptr)
    {
        cudaFree(d_fv);
        d_fv = nullptr;
    }
    if (d_result != nullptr)
    {
        cudaFree(d_result);
        d_result = nullptr;
    }
}

void initMemory(int n,int k ) 
{
    const int L = 2 * n - 1;
    // (1) 计算 FFT 大小 nfft = 2^nextpow2(L)
    int nfft = 1;
    while (nfft < L) {
        nfft <<= 1;
    }

    freeImageMemory();
    // 分配新内存
    cudaMalloc(&d_ww, L * sizeof(cuDoubleComplex));
    cudaMalloc(&d_y, nfft * sizeof(cuDoubleComplex));
    cudaMalloc(&d_source, n * sizeof(cuDoubleComplex));
    cudaMalloc(&d_fv, nfft * sizeof(cuDoubleComplex));
    cudaMalloc(&d_result, k * sizeof(cuDoubleComplex));
    

    cudaMemcpy(d_l, &L, 1 * sizeof(int), cudaMemcpyHostToDevice);

    int blockSize = 256;                           // 每个块 256 个线程
    int gridSize = (L + blockSize - 1) / blockSize; // 网格大小
    createDww << <gridSize, blockSize >> > (d_ww, d_fv,d_w,d_n, d_l);

}

void cuCzt(const std::vector<std::complex<double>>& x,  // 
    int k,                          // 
    std::complex<double> w,                      // czt 参数, e.g. exp(-j*2*pi*(f2-f1)/(k*fs))
    std::complex<double> a                       // czt 参数, e.g. exp( j*2*pi*f1/fs )
)
{
    //LHT_TIME_CONSUMING;
    using std::vector;
    using std::pow;

    const int n = (int)x.size(); // 输入长度
    const int L = 2 * n - 1;
    int nfft = 1;
    while (nfft < L) {
        nfft <<= 1;
    }
    if (d_w == nullptr)
    {
        cudaMalloc(&d_w, 1 * sizeof(cuDoubleComplex));
        cudaMalloc(&d_a, 1 * sizeof(cuDoubleComplex));
        cudaMalloc(&d_n, 1 * sizeof(int));
        cudaMalloc(&d_l, 1 * sizeof(int));
        cudaMalloc(&d_k, 1 * sizeof(int));
    }
    cudaMemcpy(d_w, &w, 1 * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice);
    cudaMemcpy(d_a, &a, 1 * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, 1 * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_k, &k, 1 * sizeof(int), cudaMemcpyHostToDevice);
    static int _len = 0;
    if (_len != n) {
        _len = n;
        initMemory(n,k);
        int blockSize = 256;
        int gridSize = (L + blockSize - 1) / blockSize; // 网格大小
        createDfv << <gridSize, blockSize >> > (d_ww, d_fv, L);
        doFFTForward_C_64fc(d_fv, nfft);
    }
    int blockSize = 256;
    int gridSize = (L + blockSize - 1) / blockSize; // 网格大小
    cudaMemcpy(d_source, x.data(), n * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice);
    //cudaMemset(d_fv , 0, nfft * sizeof(cuDoubleComplex));
    cudaMemset(d_y, 0, nfft * sizeof(cuDoubleComplex));
    gridSize = (n + blockSize - 1) / blockSize; // 网格大小
    computeY << <gridSize, blockSize >> > (d_y, d_source,d_a, d_ww, d_n);

    doFFTForward_C_64fc(d_y, nfft);

    gridSize = (nfft + blockSize - 1) / blockSize;
    elementWiseComplexMulInplace << <gridSize, blockSize >> > (d_fv, d_y, nfft);

    doFFTInverse_C_64fc(d_y, nfft);

    gridSize = (k + blockSize - 1) / blockSize;
    elementWiseComplexMul << <gridSize, blockSize >> > (d_y , d_ww , d_result,(n - 1), k);
    cudaThreadSynchronize();

    cuDoubleComplex ttt[1000];

    cudaMemcpy(ttt, d_result, 1000 * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);

}
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "iostream"

using namespace std;


float cuda_host_alloc_test(int size, bool up)
{
    // 耗时统计
    cudaEvent_t start, stop;
    float elapsedTime;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int* a, * dev_a;

    // 主机上分配页锁定内存
    // cudaError_t cudaStatus = cudaHostAlloc((void **)&a, size * sizeof(*a), cudaHostAllocDefault);
    cudaError_t cudaStatus = cudaMallocHost((void**)&a, size * sizeof(*a));
    if (cudaStatus != cudaSuccess)
    {
        printf("host alloc fail!\n");
        return -1;
    }

    // 在设备上分配内存空间
    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(*dev_a));
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "cudaMalloc failed\n");
        return -1;
    }

    cudaEventRecord(start, 0);

    // 计时开始
    for (int i = 0; i < 100; ++i)
    {
        // 主机拷贝到设备
        cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(*a), cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy host to device failed!!!\n");
            return -1;
        }

        // 从设备拷贝到主机
        cudaStatus = cudaMemcpy(a, dev_a, size * sizeof(*dev_a), cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy device to host failed!!!\n");
            return -1;
        }
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsedTime, start, stop);

    cudaFreeHost(a);
    cudaFree(dev_a);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return (float)elapsedTime / 1000;
}

float cuda_host_Malloc_test(int size, bool up)
{
    // 耗时统计
    cudaEvent_t start, stop;
    float elapsedTime;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int* a, * dev_a;

    // 主机上分配页锁定内存
    a = (int*)malloc(size * sizeof(*a));

    // 在设备上分配内存空间
    cudaError_t cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(*dev_a));
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "cudaMalloc failed\n");
        return -1;
    }

    cudaEventRecord(start, 0);

    // 计时开始
    for (int i = 0; i < 100; ++i)
    {
        // 主机拷贝到设备
        cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(*a), cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy host to device failed!!!\n");
            return -1;
        }

        // 从设备拷贝到主机
        cudaStatus = cudaMemcpy(a, dev_a, size * sizeof(*dev_a), cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess)
        {
            fprintf(stderr, "cudaMemcpy device to host failed!!!\n");
            return -1;
        }
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsedTime, start, stop);

    free(a);
    cudaFree(dev_a);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return (float)elapsedTime / 1000;
}

int test()
{
    float allocTime = cuda_host_alloc_test(100000, true);
    cout << "页锁定内存: " << allocTime << " s" << endl;

    float mallocTime = cuda_host_Malloc_test(100000, true);
    cout << "可分页内存: " << mallocTime << " s" << endl;
    return 1;
}
