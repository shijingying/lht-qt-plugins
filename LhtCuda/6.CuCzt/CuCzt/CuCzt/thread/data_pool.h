#ifndef DATA_POOL_H
#define DATA_POOL_H

#include <QThread>
#include <QMutex>
#include <QString>
#include <QWaitCondition>
#include <QDebug>
#include <queue>  // 使用 std::queue 替代 QQueue
#include <tuple>

enum class DrawType {
    DrawChannel1TypeNone,
    DrawChannel1TypeOpacityBrightness,
    DrawChannel2TypeNone,
    DrawChannel2TypeOpacityBrightness,
};

/**
 * @date 2024-02-29
 * @brief 一个简单的事件驱动线程基类，继承后实现handleData来处理数据即可，类型不同的话修改他的recv  和  handleData 即可
 * @author leehuitao
 */

class DataPool : public QThread
{
public:
    // 使用 std::queue 存储 std::tuple<DrawType, int, int> 数据
    std::queue<std::tuple<DrawType, int, int>> qqByteArr;  // 队列数组

    DataPool(QObject* parent = 0) : QThread(parent)
    {
        containerLen = 1000;
        stopped = true;
    }

    ~DataPool() {
        stopped = true;
        queueWait.wakeOne();
        quit();
        wait(100);
    }

    // 修改 recv 函数为接收 std::tuple<DrawType, int, int> 类型的数据
    bool recv(DrawType t, int Opacity, int Brightness) {

        bool ret = true;
        QMutexLocker locker(&mutex); // 锁定队列对象，禁止同时操作

        if (qqByteArr.size() >= containerLen) { // 当队列大于限定值时
            ret = false; // 队列已满，拒绝添加数据
        }
        else {
            std::tuple<DrawType, int, int> d{t,Opacity,Brightness };
            qqByteArr.push(d); // 插入数据到队列尾部
            queueWait.wakeOne(); // 唤醒睡着中的线程
        }

        return ret;
    } // 接收数据并缓存

    virtual void stop()
    {
        stopped = true;
        queueWait.wakeOne();

        quit();
        wait(100);
    } // 线程停止

    virtual void init() {
        if (stopped) {
            start();
        }
    }

    void setPoolLen(unsigned int nLen) { containerLen = nLen; }
    int currentSize() { return qqByteArr.size(); }

    // 清空队列中的所有数据
    void clearDataBuf() {
        QMutexLocker locker(&mutex);
        while (!qqByteArr.empty()) {
            qqByteArr.pop();
        }
    }

protected:
    void run() override
    {
        stopped = false;
        while (true) {

            QMutexLocker locker(&mutex);
            if (qqByteArr.empty()) {
                queueWait.wait(&mutex); // 队列为空时，等待数据
            }
            else {
                // 取出队列中的数据
                auto byteArrFirst = qqByteArr.front();
                qqByteArr.pop(); // 移除队列的第一个元素
                locker.unlock(); // 解锁队列

                // 处理数据
                handleData(byteArrFirst); // 将数据传递给子类实现的处理方法
            }

            if (stopped) {
                break;
            }
        }
    }

    // 处理队列中数据，派生类需要实现此方法
    virtual bool handleData(const std::tuple<DrawType, int, int>& data) = 0;

private:
    QMutex mutex;
    QWaitCondition queueWait;
    unsigned int containerLen;
    volatile bool stopped;
};

#endif // DATA_POOL_H
