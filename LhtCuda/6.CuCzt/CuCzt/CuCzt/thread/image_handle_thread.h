#pragma once
#include "data_pool.h"
#include <opencv2/opencv.hpp>
class ImageHandleThread : public DataPool ,public QObject{
	Q_OBJECT
public:
	ImageHandleThread();

	~ImageHandleThread();

	void setImage(cv::Mat image , int index);
signals:
	void signUpdateImage(std::shared_ptr<QImage> image);
protected:

	// ͨ�� DataPool �̳�
	virtual bool handleData(const std::tuple<DrawType, int, int>& data) override;

	std::shared_ptr<QImage> mergeImage(DrawType type);
private:
	cv::Mat m_imageSource[4];
	cv::Mat m_imageResult[4];
	cv::Mat m_result;
};