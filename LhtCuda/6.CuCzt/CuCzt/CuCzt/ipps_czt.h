﻿#pragma once

#include <iostream>
#include <vector>
#include <complex>
#include <cmath>
#include <stdexcept>
#include <ipp.h>

static const double PI = 3.14159265358979323846;
using Complex = std::complex<double>;

static void doFFTForward_C_64fc(
    const IppsFFTSpec_C_64fc* pSpec,
    std::vector<Complex>& data,
    Ipp8u* pBuffer
)
{
    IppStatus status = ippsFFTFwd_CToC_64fc(
        reinterpret_cast<Ipp64fc*>(data.data()),   // src
        reinterpret_cast<Ipp64fc*>(data.data()),   // dst
        pSpec,
        pBuffer
    );
    if (status != ippStsNoErr) {
        throw std::runtime_error("ippsFFTFwd_CToC_64fc failed.");
    }
}


static void doFFTInverse_C_64fc(
    const IppsFFTSpec_C_64fc* pSpec,
    std::vector<Complex>& data,
    Ipp8u* pBuffer
)
{
    IppStatus status = ippsFFTInv_CToC_64fc(
        reinterpret_cast<Ipp64fc*>(data.data()),   // src
        reinterpret_cast<Ipp64fc*>(data.data()),   // dst
        pSpec,
        pBuffer
    );
    if (status != ippStsNoErr) {
        throw std::runtime_error("ippsFFTInv_CToC_64fc failed.");
    }
}

/*====================================================================
   czt_by_matlab_ipp
     - 实现与 MATLAB czt.m 类似的逻辑(不带 a^(0:k-1) 那一步),
       即构造 ww, 然后 y[i] = x[i]*a^(-i)*ww[n-1+i],
       接着做快速卷积, 最后截取并乘回 ww[n-1 + i].
     - 若你的 MATLAB 结尾还要 .* (a.^(0:k-1)), 请在注释处添加.
======================================================================*/
std::vector<Complex> czt_by_matlab_ipp(
    const std::vector<Complex>& x,  // 
    int k,                          // 
    Complex w,                      // czt 参数, e.g. exp(-j*2*pi*(f2-f1)/(k*fs))
    Complex a                       // czt 参数, e.g. exp( j*2*pi*f1/fs )
)
{
    using std::vector;
    using std::pow;

    const int n = (int)x.size(); // 输入长度
    const int L = 2 * n - 1;     //
    // (1) 计算 FFT 大小 nfft = 2^nextpow2(L)
    int nfft = 1;
    while (nfft < L) {
        nfft <<= 1;
    }
    // (2)  构造 ww[]，长度 = L
    //      MATLAB: kk = -n+1 : ...
    //              ww = w^( (kk^2)/2 )
    vector<Complex> ww(L);
    for (int i = 0; i < L; i++) {
        int kk = (-n + 1) + i;
        double kk2 = double(kk) * double(kk) / 2.0;
        ww[i] = pow(w, kk2);
    }
    // (3)  构造输入 y[], 长度 n
   //      y[i] = x[i]* a^(-i)* ww[n-1 + i]
    vector<Complex> y(n);
    for (int i = 0; i < n; i++) {
        Complex a_neg_i = pow(a, -double(i));
        int ww_idx = (n - 1) + i;
        y[i] = x[i] * a_neg_i * ww[ww_idx];
    }
    // (4)  准备两个长度 nfft 的向量: fy, fv
   //      fy 用于存放 y 的零填充后 FFT
   //      fv = 1./ww(...)  也做 FFT, 用于卷积核
    vector<Complex> fy(nfft, Complex(0, 0));
    std::memcpy(fy.data(), y.data(), n * sizeof(Complex));
    //for (int i = 0; i < n; i++) {
    //    fy[i] = y[i];
    //}
    vector<Complex> fv(nfft, Complex(0, 0));
    //for (int i = 0; i < L; i++) {
    //    fv[i] = Complex(1.0, 0.0) / ww[i];
    //}
    std::vector<Complex> ones(L, Complex(1.0, 0.0));
    ippsDiv_64fc(
        reinterpret_cast<const Ipp64fc*>(ww.data()),
        reinterpret_cast<const Ipp64fc*>(ones.data()),
        reinterpret_cast<Ipp64fc*>(fv.data()),
        L
    );
    // 1) 计算 order
    int order = 0;
    {
        int tmp = nfft;
        while (tmp > 1) { tmp >>= 1; order++; }
    }
    // 2) 计算所需 spec/init/buf 大小
    int sizeSpec = 0, sizeInit = 0, sizeBuf = 0;
    IppStatus st = ippsFFTGetSize_C_64fc(
        order,
        IPP_FFT_DIV_INV_BY_N,    
        ippAlgHintFast,
        &sizeSpec,
        &sizeInit,
        &sizeBuf
    );
    if (st != ippStsNoErr) {
        throw std::runtime_error("ippsFFTGetSize_C_64fc failed.");
    }

    Ipp8u* pSpecMem = ippsMalloc_8u(sizeSpec);
    Ipp8u* pInitMem = ippsMalloc_8u(sizeInit);
    Ipp8u* pBufMem = ippsMalloc_8u(sizeBuf);

    if (!pSpecMem || !pInitMem || !pBufMem) {
        if (pSpecMem) ippsFree(pSpecMem);
        if (pInitMem) ippsFree(pInitMem);
        if (pBufMem)  ippsFree(pBufMem);
        throw std::runtime_error("ippsMalloc_8u failed to allocate memory.");
    }

    IppsFFTSpec_C_64fc* pSpec = nullptr;
    st = ippsFFTInit_C_64fc(
        &pSpec,
        order,
        IPP_FFT_DIV_INV_BY_N,
        ippAlgHintFast,
        pSpecMem,
        pInitMem
    );
    if (st != ippStsNoErr || !pSpec) {
        ippsFree(pSpecMem);
        ippsFree(pInitMem);
        ippsFree(pBufMem);
        throw std::runtime_error("ippsFFTInit_C_64fc failed.");
    }

    // (5)  Forward FFT on fy & fv
    doFFTForward_C_64fc(pSpec, fy, pBufMem);
    doFFTForward_C_64fc(pSpec, fv, pBufMem);

    IppStatus status = ippsMul_64fc_I(
        reinterpret_cast<const Ipp64fc*>(fv.data()),  // pSrc2
        reinterpret_cast<Ipp64fc*>(fy.data()),  // pSrcDst (in-place)
        nfft
    );
    // (7) IFFT => fy
    doFFTInverse_C_64fc(pSpec, fy, pBufMem);
    //     g[i] = fy[n-1 + i] * ww[n-1 + i], i=0..k-1
    std::vector<Complex> g(k);
    //for (int i = 0; i < k; i++) {
    //    int idx = (n - 1) + i;
    //    g[i] = fy[idx] * ww[idx];
    //}
    ippsMul_64fc(
        reinterpret_cast<const Ipp64fc*>(fy.data() + (n - 1)),
        reinterpret_cast<const Ipp64fc*>(ww.data() + (n - 1)),
        reinterpret_cast<Ipp64fc*>      (g.data()),
        k
    );

    ippsFree(pSpecMem);
    ippsFree(pInitMem);
    ippsFree(pBufMem);

    return g;
}