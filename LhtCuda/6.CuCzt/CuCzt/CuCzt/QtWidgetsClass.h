#pragma once

#include <QMainWindow>
#include <opencv2/opencv.hpp>
#include "ui_QtWidgetsClass.h"
//#include "thread/image_handle_thread.h"

class QtWidgetsClass : public QMainWindow
{
	Q_OBJECT

public:
	QtWidgetsClass(QWidget *parent = nullptr);
	~QtWidgetsClass();
protected slots:
	void on_czt_btn_clicked();

	void on_ipps_czt_btn_clicked();

	void on_cu_czt_btn_clicked();
private:
	Ui::QtWidgetsClassClass ui;

	//ImageHandleThread* th;
	//ImageHandleThread* th1;
};
