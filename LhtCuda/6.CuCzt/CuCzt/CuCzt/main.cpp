﻿
#include "ipps_czt.h"
#include "kernel.cuh"
int main()
{
    int n = 240000;          
    int k = 10240 * 2;     
    double f1 = 100.0;
    double f2 = 150.0;
    double fs = 1000.0;

    Complex W = std::exp(Complex(0.0, -2.0 * PI * (f2 - f1) / (k * fs)));
    Complex A = std::exp(Complex(0.0, 2.0 * PI * f1 / fs));

    std::vector<Complex> x(n, Complex(1.0, 0.0));

    try {
        LHT_TIME_CONSUMING;
        
        //for (int i = 0; i < 100; i ++) {
        //    auto result = czt_by_matlab_ipp(x, k, W, A);
        //}
        for (int i = 0; i < 100; i ++) {
            cuCzt(x, k, W, A);
        }
        
    }
    catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << "\n";
    }

    return 0;
}
