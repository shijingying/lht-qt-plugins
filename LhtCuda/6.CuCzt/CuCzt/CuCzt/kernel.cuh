#pragma once
#include <iostream>
#include <opencv2/opencv.hpp>
#include <complex>
#define LHT_TIME_CONSUMING LhtTimeConsuming __time_consuming(__func__, __LINE__)
//耗时计算
class LhtTimeConsuming {
public:
    LhtTimeConsuming(const char* func, int line)
        : func_name(func), line_number(line) {
        start_time = std::chrono::high_resolution_clock::now();
    }

    ~LhtTimeConsuming() {
        // 记录结束时间
        auto end_time = std::chrono::high_resolution_clock::now();

        // 计算耗时，并转换为毫秒
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        // 打印耗时（单位：毫秒）
        std::cout << "Time consuming in function: " << func_name << " at line " << line_number
            << " is " << duration.count() << " ms."<<std::endl;
    }

private:
    const char* func_name;
    int line_number;
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
};

int test();

void cuCzt(const std::vector<std::complex<double>>& x,  // 
    int k,                          // 
    std::complex<double> w,                      // czt 参数, e.g. exp(-j*2*pi*(f2-f1)/(k*fs))
    std::complex<double> a                       // czt 参数, e.g. exp( j*2*pi*f1/fs )
);