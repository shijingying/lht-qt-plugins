#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <iostream>
#include <time.h>
#include "windows.h"
#include <sys/timeb.h>
#include <chrono>

using namespace std;

#define CHECK_GPU_TIME_START1 {auto st = std::chrono::system_clock::now();\
    start1 =  std::chrono::time_point_cast<std::chrono::milliseconds>(st).time_since_epoch().count();}

#define CHECK_GPU_TIME_END1 {/*cudaDeviceSynchronize();*/ \
    auto et = std::chrono::system_clock::now();\
    stop1 =  std::chrono::time_point_cast<std::chrono::milliseconds>(et).time_since_epoch().count();\
    std::cout<<"use time : "<<stop1 - start1<<std::endl;}

//原始数据及长度
cudaError cudaInit(const float* a,  unsigned int size);

void Cal_Sum_Test(int& result, unsigned int size);

void Cal_Max_Test(int& result, unsigned int size);