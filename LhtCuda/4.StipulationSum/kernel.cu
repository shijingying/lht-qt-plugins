﻿#include "kernel.cuh"

//设置每个线程块有1024个线程
dim3 sumblock(1024);
//设置总共有多少个线程块
dim3 sumgrid;// (((size% sumblock.x) ? (size / sumblock.x + 1) : (size / sumblock.x)));
float* test_d_cuda;
float* blocksum_cuda;

//Para为输入数组，长度为N
//blocksum_cuda存储所有线程块的规约结果
__global__ void cal_sum_ker0(float* Para, float* blocksum_cuda, unsigned int size)
{
    //计算线程ID号
    //blockIdx.x为线程块的ID号
    //blockDim.x每个线程块中包含的线程总个数
    //threadIdx.x为每个线程块中的线程ID号
    int tid = blockIdx.x * blockDim.x + threadIdx.x;


    if (tid < size)
    {
        for (int index = 1; index < blockDim.x; index = (index * 2))
        {
            if (threadIdx.x % (index * 2) == 0)
            {
                Para[tid] += Para[tid + index];  //规约求和
            }
            __syncthreads();  //同步线程块中的所有线程
        }


        if (threadIdx.x == 0)   //整个数组相加完成后，将共享内存数组0号元素的值赋给全局内存数组0号元素，最后返回CPU端 
            blocksum_cuda[blockIdx.x] = Para[tid];
    }
}

__global__ void cal_max_ker0(float* Para, float* blocksum_cuda, unsigned int size)
{
    //计算线程ID号
    //blockIdx.x为线程块的ID号
    //blockDim.x每个线程块中包含的线程总个数
    //threadIdx.x为每个线程块中的线程ID号
    int tid = blockIdx.x * blockDim.x + threadIdx.x;


    if (tid < size)
    {
        for (int index = 1; index < blockDim.x; index = (index * 2))
        {
            if (threadIdx.x % (index * 2) == 0)
            {
                Para[tid] = Para[tid + index] > Para[tid] ? Para[tid + index] : Para[tid];  //规约求最大值
            }
            __syncthreads();  //同步线程块中的所有线程
        }


        if (threadIdx.x == 0)   //整个数组相加完成后，将共享内存数组0号元素的值赋给全局内存数组0号元素，最后返回CPU端 
            blocksum_cuda[blockIdx.x] = Para[tid];
    }
}

void Cal_Max_Test(int& result, unsigned int size)
{
    float* blocksum_host = (float*)malloc(sizeof(float) * sumgrid.x);
    //调用核函数进行规约求和
    cal_max_ker0 << < sumgrid, sumblock >> > (test_d_cuda, blocksum_cuda, size);
    //将每个线程块的规约求和结果拷贝到CPU端
    cudaMemcpy(blocksum_host, blocksum_cuda, sizeof(float) * sumgrid.x, cudaMemcpyDeviceToHost);
    ////在CPU端对所有线程块的规约求和结果做串行求和
    double sum = 0.0;
    for (int i = 0; i < sumgrid.x; i++)
    {
        sum = sum > blocksum_host[i] ? sum : blocksum_host[i];
    }
    result = sum;

    cout << " GPU result = " << sum << endl;   //显示GPU端结果

}

void Cal_Sum_Test(int& result, unsigned int size)
{
    float* blocksum_host = (float*)malloc(sizeof(float) * sumgrid.x);
    //调用核函数进行规约求和
    cal_sum_ker0 << < sumgrid, sumblock >> > (test_d_cuda, blocksum_cuda, size);
    //将每个线程块的规约求和结果拷贝到CPU端
    cudaMemcpy(blocksum_host, blocksum_cuda, sizeof(float) * sumgrid.x, cudaMemcpyDeviceToHost);
    ////在CPU端对所有线程块的规约求和结果做串行求和
    double sum = 0.0;
    for (int i = 0; i < sumgrid.x; i++)
    {
        sum += blocksum_host[i];
    }
    result = sum;

    //timer.stop_timer("GPU time:");


    //cout << " GPU result = " << sum << endl;   //显示GPU端结果


    //释放内存
    //cudaFree(test_d_cuda);
    //cudaFree(blocksum_cuda);
    //free(blocksum_host);
    //free(test_d);
}

cudaError cudaInit(const float* a, unsigned int size)
{
    cudaError_t cudaStatus;
    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
    }
    cudaStatus = cudaFree(0);

    //设置每个线程块有1024个线程
    sumblock = dim3(1024);
    //设置总共有多少个线程块
    sumgrid = dim3(((size % sumblock.x) ? (size / sumblock.x + 1) : (size / sumblock.x)));

    float* blocksum_host = (float*)malloc(sizeof(float) * sumgrid.x);
    //申请GPU端全局内存
    cudaMalloc((void**)&test_d_cuda, sizeof(float) * size);
    cudaMalloc((void**)&blocksum_cuda, sizeof(float) * sumgrid.x);

    //将数据从CPU端拷贝到GPU端
    cudaMemcpy(test_d_cuda, a, sizeof(float) * size, cudaMemcpyHostToDevice);

    return cudaStatus;
}
