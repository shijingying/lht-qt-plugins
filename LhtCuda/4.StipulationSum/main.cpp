#include "kernel.cuh"
#include <thread>
#include <vector>

class Timer_Us2
{
private:
    LARGE_INTEGER cpuFreq;
    LARGE_INTEGER startTime;
    LARGE_INTEGER endTime;
public:
    double rumTime;
    void get_frequence(void)
    {
        QueryPerformanceFrequency(&cpuFreq);   //获取时钟频率
    }


    void start_timer(void)
    {
        QueryPerformanceCounter(&startTime);    //开始计时
    }


    void stop_timer(char* str)
    {
        QueryPerformanceCounter(&endTime);    //结束计时
        rumTime = (((endTime.QuadPart - startTime.QuadPart) * 1000.0f) / cpuFreq.QuadPart);
        cout << str << rumTime << " ms" << endl;
    }


    Timer_Us2()    //构造函数
    {
        QueryPerformanceFrequency(&cpuFreq);
    }
};

int main() {
    
    int size = (1536 * 20480);
    //申请长度为N的float型动态内存
    float* test_d = (float*)malloc(size * sizeof(float));
    for (long long i = 0; i < size; i++)
    {
        test_d[i] = 0.5 ;  //将所有元素赋值为0.5
    }
    double ParaSum = 0.0;

    cudaInit(test_d, size);

    Timer_Us2 timer;
    timer.start_timer();
    //在CPU端按顺序计算数组元素和
    for (long long i = 0; i < size; i++)
    {
        ParaSum += test_d[i];  //CPU端数组累加 
    }
    timer.stop_timer("CPU time:");
    cout << " CPU result = " << ParaSum << endl;  //显示CPU端结果 

    int sum;
    timer.start_timer();
    //Cal_Sum_Test(sum, size);
    Cal_Max_Test(sum, size);
    timer.stop_timer("GPU time:");
    cout << " GPU result = " << sum << endl;   //显示GPU端结果
    system("pause");
    return 1;
}