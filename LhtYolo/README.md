#运行环境:
- VS 2022 17.6 1940 / CUDA 12.6 / cuDNN v8.9.7 (2023 年 12 月 5 日)/ opencv 4.8.1 / TensorRT-10.4.0.26
#提示
- 如果需要打开本地视频测试需要将opencv的opencv_videoio_ffmpeg481_64.dll 这个库放到运行路径下否则会无法解码

#老的显卡驱动下载
- https://down.gamersky.com/oth/soft/

#预训练模型下载
- https://github.com/ultralytics/ultralytics
- https://drive.google.com/drive/folders/1-8phZHkx_Z274UVqgw6Ma-6u5AKmqCOv

#导出onnx
- https://github.com/ultralytics/ultralytics
- {
- 	from ultralytics import YOLO
- 	model = YOLO("yolov8s.yaml")  # build a new model from scratch
- 	model = YOLO("yolov8s.pt")  # load a pretrained model (recommended for training)
- 	path = model.export(format="onnx")  # export the model to ONNX format
- }

#导出trt
- trtexec.exe --onnx=yolov8n.onnx --saveEngine=yolov8n.trt --fp16

#借鉴支持TensortRt10
- https://github.com/triple-Mu/YOLOv8-TensorRT

#目标追踪 byteTrack
- https://github.com/ifzhang/ByteTrack.git

#Eigen 3.4.0

#YoloV10 版本pt文件下载  导出trt在上面
- https://github.com/THU-MIG/yolov10/releases