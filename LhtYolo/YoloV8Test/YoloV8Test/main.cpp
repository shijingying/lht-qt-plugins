//
// Created by ubuntu on 1/20/23.
//
#include "opencv2/opencv.hpp"
#include "yolov8.hpp"
#include <chrono>
#include <fstream>

namespace fs = ghc::filesystem;

const std::vector<std::string> CLASS_NAMES = {
    "person",         "bicycle",    "car",           "motorcycle",    "airplane",     "bus",           "train",
    "truck",          "boat",       "traffic light", "fire hydrant",  "stop sign",    "parking meter", "bench",
    "bird",           "cat",        "dog",           "horse",         "sheep",        "cow",           "elephant",
    "bear",           "zebra",      "giraffe",       "backpack",      "umbrella",     "handbag",       "tie",
    "suitcase",       "frisbee",    "skis",          "snowboard",     "sports ball",  "kite",          "baseball bat",
    "baseball glove", "skateboard", "surfboard",     "tennis racket", "bottle",       "wine glass",    "cup",
    "fork",           "knife",      "spoon",         "bowl",          "banana",       "apple",         "sandwich",
    "orange",         "broccoli",   "carrot",        "hot dog",       "pizza",        "donut",         "cake",
    "chair",          "couch",      "potted plant",  "bed",           "dining table", "toilet",        "tv",
    "laptop",         "mouse",      "remote",        "keyboard",      "cell phone",   "microwave",     "oven",
    "toaster",        "sink",       "refrigerator",  "book",          "clock",        "vase",          "scissors",
    "teddy bear",     "hair drier", "toothbrush"};

const std::vector<std::vector<unsigned int>> COLORS = {
    {0, 114, 189},   {217, 83, 25},   {237, 177, 32},  {126, 47, 142},  {119, 172, 48},  {77, 190, 238},
    {162, 20, 47},   {76, 76, 76},    {153, 153, 153}, {255, 0, 0},     {255, 128, 0},   {191, 191, 0},
    {0, 255, 0},     {0, 0, 255},     {170, 0, 255},   {85, 85, 0},     {85, 170, 0},    {85, 255, 0},
    {170, 85, 0},    {170, 170, 0},   {170, 255, 0},   {255, 85, 0},    {255, 170, 0},   {255, 255, 0},
    {0, 85, 128},    {0, 170, 128},   {0, 255, 128},   {85, 0, 128},    {85, 85, 128},   {85, 170, 128},
    {85, 255, 128},  {170, 0, 128},   {170, 85, 128},  {170, 170, 128}, {170, 255, 128}, {255, 0, 128},
    {255, 85, 128},  {255, 170, 128}, {255, 255, 128}, {0, 85, 255},    {0, 170, 255},   {0, 255, 255},
    {85, 0, 255},    {85, 85, 255},   {85, 170, 255},  {85, 255, 255},  {170, 0, 255},   {170, 85, 255},
    {170, 170, 255}, {170, 255, 255}, {255, 0, 255},   {255, 85, 255},  {255, 170, 255}, {85, 0, 0},
    {128, 0, 0},     {170, 0, 0},     {212, 0, 0},     {255, 0, 0},     {0, 43, 0},      {0, 85, 0},
    {0, 128, 0},     {0, 170, 0},     {0, 212, 0},     {0, 255, 0},     {0, 0, 43},      {0, 0, 85},
    {0, 0, 128},     {0, 0, 170},     {0, 0, 212},     {0, 0, 255},     {0, 0, 0},       {36, 36, 36},
    {73, 73, 73},    {109, 109, 109}, {146, 146, 146}, {182, 182, 182}, {219, 219, 219}, {0, 114, 189},
    {80, 183, 189},  {128, 128, 0}};

int main(int argc, char** argv)
{
    //if (argc != 3) {
    //    fprintf(stderr, "Usage: %s [engine_path] [image_path/image_dir/video_path]\n", argv[0]);
    //    return -1;
    //}

    // cuda:0
    cudaSetDevice(0);
#ifdef YOLOV10
    const std::string engine_file_path = "E:/git/lht-qt-plugins/LhtYolo/YoloV8Test/x64/Debug/yolov10s.trt";//{argv[1]};
#else
    const std::string engine_file_path = "E:/git/lht-qt-plugins/LhtYolo/YoloV8Test/x64/Debug/yolov8n.trt";//{argv[1]};
#endif
    const fs::path    path = "E:/git/lht-qt-plugins/LhtYolo/YoloV8Test/x64/Debug/palace.mp4";// {argv[2]};

    std::vector<std::string> imagePathList;
    bool                     isVideo{false};
    //打开目标追踪
    bool                     useTrack = 1;
    //使用摄像头
    bool                     useLocalCamera = 1;
    auto yolov8 = new YOLOv8(engine_file_path);
    yolov8->make_pipe(true);

    if (fs::exists(path)) {
        std::string suffix = path.extension().string();
        if (suffix == ".jpg" || suffix == ".jpeg" || suffix == ".png") {
            imagePathList.push_back(path.string());
        }
        else if (suffix == ".mp4" || suffix == ".avi" || suffix == ".m4v" || suffix == ".mpeg" || suffix == ".mov"
                 || suffix == ".mkv") {
            isVideo = true;
        }
        else {
            printf("suffix %s is wrong !!!\n", suffix.c_str());
            std::abort();
        }
    }
    else if (fs::is_directory(path)) {
        cv::glob(path.string() + "/*.jpg", imagePathList);
    }

    cv::Mat             res, image;
    cv::Size            size = cv::Size{640, 640};
    int      num_labels  = 80;
    int      topk        = 100;
    float    score_thres = 0.55f;
    float    iou_thres   = 0.65f;

    std::vector<Object> objs;
    int total_ms = 0;
    int num_frames = 0;
    int fps = 30;
    BYTETracker tracker(fps, 30);
    cv::namedWindow("result", cv::WINDOW_AUTOSIZE);
    if (useLocalCamera) {
        cv::VideoCapture cap(0); // Open the default camera

        if (!cap.isOpened()) {
            printf("can not open camera\n");
            return -1;
        }

        while (cap.read(image)) {
            objs.clear();
            yolov8->copy_from_Mat(image, size);
            auto start = std::chrono::system_clock::now();
            yolov8->infer();
            yolov8->postprocess(objs, score_thres, iou_thres, topk, num_labels);
            if (useTrack) {
                //目标追踪
                vector<STrack> output_stracks = tracker.update(objs);
                auto end = std::chrono::system_clock::now();
                auto tc = (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.;
                printf("cost %2.4lf ms\n", tc);

                for (int i = 0; i < output_stracks.size(); i++)
                {
                    vector<float> tlwh = output_stracks[i].tlwh;
                    bool vertical = tlwh[2] / tlwh[3] > 1.6;
                    if (tlwh[2] * tlwh[3] > 20 && !vertical)
                    {
                        Scalar s = tracker.get_color(output_stracks[i].track_id);
                        putText(image, format("%d", output_stracks[i].track_id), Point(tlwh[0], tlwh[1] - 5),
                            0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
                        rectangle(image, Rect(tlwh[0], tlwh[1], tlwh[2], tlwh[3]), s, 2);
                    }
                }
                num_frames++;
                total_ms = total_ms + tc;
                putText(image, format("frame: %d fps: %d num: %d", num_frames, num_frames * 1000 / total_ms, output_stracks.size()),
                    Point(0, 30), 0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
                //yolov8->draw_objects(image, res, objs, CLASS_NAMES, COLORS);

                cv::imshow("result", image);
                if (cv::waitKey(10) == 'q') {
                    break;
                }
            }
            else {
                yolov8->draw_objects(image, res, objs, CLASS_NAMES, COLORS);
                auto end = std::chrono::system_clock::now();
                auto tc = (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.;
                printf("cost %2.4lf ms\n", tc);
                cv::imshow("result", res);
                if (cv::waitKey(10) == 'q') {
                    break;
                }
            }

        }
    }
    else if (isVideo) {
        cv::VideoCapture cap(path.string());
        if (!cap.isOpened()) {
            printf("can not open %s\n", "E:\\git\\lht-qt-plugins\\LhtYolo\\YoloV8Test\\x64\\Debug\\palace.mp4");
            return -1;
        }
        while (cap.read(image)) {
            objs.clear();
            yolov8->copy_from_Mat(image, size);
            auto start = std::chrono::system_clock::now();
            //检测
            yolov8->infer();
            //后处理
            yolov8->postprocess(objs, score_thres, iou_thres, topk, num_labels);
            if (useTrack) {
                //目标追踪
                vector<STrack> output_stracks = tracker.update(objs);
                auto end = std::chrono::system_clock::now();
                auto tc = (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.;
                printf("cost %2.4lf ms\n", tc);

                for (int i = 0; i < output_stracks.size(); i++)
                {
                    vector<float> tlwh = output_stracks[i].tlwh;
                    bool vertical = tlwh[2] / tlwh[3] > 1.6;
                    if (tlwh[2] * tlwh[3] > 20 && !vertical)
                    {
                        Scalar s = tracker.get_color(output_stracks[i].track_id);
                        putText(image, format("%d", output_stracks[i].track_id), Point(tlwh[0], tlwh[1] - 5),
                            0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
                        rectangle(image, Rect(tlwh[0], tlwh[1], tlwh[2], tlwh[3]), s, 2);
                    }
                }
                num_frames++;
                total_ms = total_ms + tc;
                putText(image, format("frame: %d fps: %d num: %d", num_frames, num_frames * 1000 / total_ms, output_stracks.size()),
                    Point(0, 30), 0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
                //yolov8->draw_objects(image, res, objs, CLASS_NAMES, COLORS);

                cv::imshow("result", image);
                if (cv::waitKey(10) == 'q') {
                    break;
                }
            }
            else {
                yolov8->draw_objects(image, res, objs, CLASS_NAMES, COLORS);
                auto end = std::chrono::system_clock::now();
                auto tc = (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.;
                printf("cost %2.4lf ms\n", tc);
                cv::imshow("result", res);
                if (cv::waitKey(10) == 'q') {
                    break;
                }
            }

        }
    }
    else {
        for (auto& p : imagePathList) {
            objs.clear();
            image = cv::imread(p);
            yolov8->copy_from_Mat(image, size);
            auto start = std::chrono::system_clock::now();
            yolov8->infer();
            auto end = std::chrono::system_clock::now();
            yolov8->postprocess(objs, score_thres, iou_thres, topk, num_labels);
            yolov8->draw_objects(image, res, objs, CLASS_NAMES, COLORS);
            auto tc = (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.;
            printf("cost %2.4lf ms\n", tc);
            cv::imshow("result", res);
            cv::waitKey(0);
        }
    }
    cv::destroyAllWindows();
    delete yolov8;
    return 0;
}
