VTK编译环境安装

1. [Download | VTK](http://https://vtk.org/download/)  下载Source中的文件

2. 复制到要安装的位置   同级路径新建build 和 install 路径

3. 配置源码路径，编译路径 然后Configure一下

4. 搜索配置项CMAKE_INSTALL_PREFIX设置到新建的install路径

5. Generate，然后在build路径下打开sln

6. 构建ALL_BUILD(这一步会有点久)

7. 构建INSTALL 完成

8. 到install下的lib路径，打开cmd 输入DIR *.lib*/B>LIST.TXT 生成库列表方便复制

9. 复制LIST文件内的所有的内容，填入附加依赖项，在C++附加包含目录添加install下的include路径

10. 代码运行需要一些dll的库  最简单的就是install下的bin下的所有dll拷贝过来

