package main

import (
	"GoSystemStatePlugin/Tools"
	"GoSystemStatePlugin/UdpServer"
	"fmt"
	"time"
)

func main() {
	// 创建一个 2 秒的 Ticker
	ticker := time.NewTicker(2 * time.Second)

	// 在程序退出前停止 Ticker
	defer ticker.Stop()

	go UdpServer.LhtListenUdp("127.0.0.1", 7771)

	// 使用 goroutine 来处理 Ticker 事件
	go func() {
		for {
			select {
			case <-ticker.C:
				{
					// 每次 Ticker 触发时执行的代码
					fmt.Println("Tick at", time.Now())
					ret := Tools.LhtGetCpuInfo()
					UdpServer.SendSystemState(ret)
					ret = Tools.LhtGetCpuLoad()
					UdpServer.SendSystemState(ret)
					ret = Tools.LhtGetMemInfo()
					UdpServer.SendSystemState(ret)
					ret = Tools.LhtGetHostInfo()
					UdpServer.SendSystemState(ret)
					ret = Tools.LhtGetDiskInfo()
					UdpServer.SendSystemState(ret)
					ret = Tools.LhtGetNetInfo()
					UdpServer.SendSystemState(ret)

				}

			}
		}
	}()
	// 阻止主 goroutine 退出
	select {}
}
