package UdpServer

import (
	"fmt"
	"log"
	"net"
)

var netList []*net.UDPAddr
var currentSize int
var listener *net.UDPConn
var err error

func LhtListenUdp(ip string, port int) {
	currentSize = 0
	netList = make([]*net.UDPAddr, 100) // 预先分配容量为100
	// 1.开启监听(建立套接字)
	listener, err = net.ListenUDP("udp", &net.UDPAddr{
		IP:   net.IPv4(0, 0, 0, 0),
		Port: 9091,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()
	// 2.循环读取数据
	for {
		var data [1024]byte
		n, client, err := listener.ReadFromUDP(data[:])
		if err != nil {
			log.Println(err)
			break
		}
		ret := true
		for i := 0; i < currentSize; i++ { //判断一下是否已经存在了
			v := netList[i]
			if v.IP.String() == client.IP.String() && v.Port == client.Port {
				ret = false
			}
		}
		fmt.Println(client, string(data[:n]))
		//保存连接用于后续的信息分发
		if ret {
			netList[currentSize] = client
			currentSize++
		}

		// 3.回复消息
		//listener.WriteToUDP([]byte("recevied success!"), client)
	}
}

func SendSystemState(data string) {
	for _, client := range netList { //判断一下是否已经存在了
		_, err := listener.WriteToUDP([]byte(data), client)
		if err != nil {
			fmt.Println(client, err.Error())
		}
	}
}
