package Tools

import (
	"fmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
	"time"
)

// LhtGetCpuInfo cpu info
func LhtGetCpuInfo() string {
	cpuInfos, err := cpu.Info()
	if err != nil {
		fmt.Printf("get cpu info failed, err:%v", err)
	}
	for _, ci := range cpuInfos {
		fmt.Println(ci)
	}
	// CPU使用率
	//for {
	percent, _ := cpu.Percent(time.Second, false)
	ret := fmt.Sprintf("cpu percent:%v\n", percent)
	return ret
	//}
}

func LhtGetCpuLoad() string {
	info, _ := load.Avg()
	ret := fmt.Sprintf("%v\n", info)
	return ret
}

func LhtGetMemInfo() string {
	memInfo, _ := mem.VirtualMemory()
	ret := fmt.Sprintf("mem info:%v\n", memInfo)
	return ret
}

func LhtGetHostInfo() string {
	hInfo, _ := host.Info()
	ret := fmt.Sprintf("host info:%v uptime:%v boottime:%v\n", hInfo, hInfo.Uptime, hInfo.BootTime)
	return ret
}

// LhtGetDiskInfo disk info
func LhtGetDiskInfo() string {
	parts, err := disk.Partitions(true)
	if err != nil {
		fmt.Printf("get Partitions failed, err:%v\n", err)
		return ""
	}
	str := ""
	for _, part := range parts {
		str += fmt.Sprintf("part:%v\n", part.String())
		diskInfo, _ := disk.Usage(part.Mountpoint)
		str += fmt.Sprintf("disk info:used:%v free:%v\n", diskInfo.UsedPercent, diskInfo.Free)
	}

	ioStat, _ := disk.IOCounters()
	for k, v := range ioStat {
		fmt.Printf("%v:%v\n", k, v)
	}
	return str
}

func LhtGetNetInfo() string {
	info, _ := net.IOCounters(true)
	str := ""
	for index, v := range info {
		str += fmt.Sprintf("%v:%v send:%v recv:%v\n", index, v, v.BytesSent, v.BytesRecv)
	}
	return str
}
