#初始化项目  go mod init /*项目名称*/

#IDEA设置go get 的代理

#go env -w GO111MODULE=on

#go env -w GOPROXY=https://goproxy.cn,direct


#插件列表

1.GoSqlPlugin 数据库插件（主要用于结构体存储）

2.GoSystemStatePlugin 获取系统状态的发布式服务

3.GoTestClient 用于测试服务端软件,可并发发送请求的客户端

4.GoWatchDog 用于检测本地服务启动状态，配合启动脚本实现服务自启动和错误后重启

#关于C++GRPC的HellowWord 编译

1.需要再安装的Vcpkg目录 运行vcpkg integrate install 生成vcpkg.cmake

2.在hellowworld目录新建一个BuildDir cd BuildDir

3.cmake .. -DCMAKE_TOOLCHAIN_FILE=*/vcpkg.cmake

4.生成sln完成

#Go 生成Proto文件方法

1.protoc --go-grpc_out=./build/gen .\SqlPlugin.proto 

2.protoc --go_out=./build/gen  .\SqlPlugin.proto

#C++ 生成proto文件方法

1.protoc -I . --grpc_out=. --plugin=protoc-gen-grpc="E:\install\vcpkg\packages\grpc_x64-windows\tools\grpc\grpc_cpp_plugin.exe" ./SqlPlugin.proto

2.protoc -I . --cpp_out=. ./SqlPlugin.proto