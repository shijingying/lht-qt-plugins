package settings

// GormModel 是一个用于标识所有 GORM 模型的空接口
type GormModel interface{}

const (
	UserTableType = iota
	DeptTableType
	GeneralDataTableType
)

// UserStructGorm 定义 GORM 的结构体
type UserStructGorm struct {
	//gorm.Model
	//ID   int32  `gorm:"column:ID; PRIMARY_KEY"`
	Name string `gorm:"column:Name"`
	Age  int32  `gorm:"column:Age"`
}
type UserStructGormSelect struct {
	//gorm.Model
	ID   int32  `gorm:"column:ID"`
	Name string `gorm:"column:Name"`
	Age  int32  `gorm:"column:Age"`
}

// DeptStructGorm 定义 GORM 的结构体
type DeptStructGorm struct {
	//gorm.Model
	DeptId   int32  `gorm:"column:DeptId; PRIMARY_KEY"`
	DeptName string `gorm:"column:DeptName"`
}

type GeneralDataGorm struct {
	//Id                  int32   `gorm:"column:ID"`
	ShipNumber         int32   `gorm:"column:ShipNumber"`
	TaskNumber         int32   `gorm:"column:TaskNumber"`
	DeviceNumber       int32   `gorm:"column:DeviceNumber"`
	Date               string  `gorm:"column:Date"`
	EqeType            string  `gorm:"column:EquType"`
	EquMode            string  `gorm:"column:EquModel"`
	StartTime          string  `gorm:"column:StartTime"`
	EndTime            string  `gorm:"column:EndTime"`
	DemossticNumber    int32   `gorm:"column:DomesticNumber"`
	ChsName            string  `gorm:"column:ChsName"`
	EnName             string  `gorm:"column:EnName"`
	TargetType         string  `gorm:"column:TargetType"`
	TargetLevel        string  `gorm:"column:TargetLevel"`
	District           string  `gorm:"column:District"`
	InternationNumber  string  `gorm:"column:InternationalNumber"`
	NoradNumber        int32   `gorm:"column:NoradNumber"`
	Platform           string  `gorm:"column:Platform"`
	PlatformType       string  `gorm:"column:PlatformType"`
	TransmissionTime   string  `gorm:"column:TransmissionTime"`
	LaunchSite         string  `gorm:"column:LaunchSite"`
	CarrierRocket      string  `gorm:"column:CarrierRocket"`
	Purpose            string  `gorm:"column:Purpose"`
	WorkStatus         string  `gorm:"column:WorkStatus"`
	TaskLongitude      string  `gorm:"column:TaskLongitude"`
	TaskLatitude       string  `gorm:"column:TaskLatitude"`
	SourceDataFileName string  `gorm:"column:SourceDataFileName"`
	SourceDataFileSize float64 `gorm:"column:SourceDataFileSize"`
	SourceDataCount    int32   `gorm:"column:SourceDataFileCount"`
	ResultFileName     string  `gorm:"column:ResultFileName"`
	DocumentFileName   string  `gorm:"column:DocumentFileName"`
	Operator           string  `gorm:"column:Operator"`
	Notes              string  `gorm:"column:Notes"`
	RxName             string  `gorm:"column:RxName"`
}
type GeneralDataGormSelect struct {
	Id                  int32   `gorm:"column:ID"`
	ShipNumber          int32   `gorm:"column:ShipNumber"`
	TaskNumber          int32   `gorm:"column:TaskNumber"`
	DeviceNumber        int32   `gorm:"column:DeviceNumber"`
	Date                string  `gorm:"column:Date"`
	EqeType             string  `gorm:"column:EquType"`
	EquMode             string  `gorm:"column:EquMode"`
	StartTime           string  `gorm:"column:StartTime"`
	EndTime             string  `gorm:"column:EndTime"`
	DomesticNumber      int32   `gorm:"column:DomesticNumber"`
	ChsName             string  `gorm:"column:ChsName"`
	EnName              string  `gorm:"column:EnName"`
	TargetType          string  `gorm:"column:TargetType"`
	TargetLevel         string  `gorm:"column:TargetLevel"`
	District            string  `gorm:"column:District"`
	InternationalNumber string  `gorm:"column:InternationalNumber"`
	NoradNumber         int32   `gorm:"column:NoradNumber"`
	Platform            string  `gorm:"column:Platform"`
	PlatformType        string  `gorm:"column:PlatformType"`
	TransmissionTime    string  `gorm:"column:TransmissionTime"`
	LaunchSite          string  `gorm:"column:LaunchSite"`
	CarrierRocket       string  `gorm:"column:CarrierRocket"`
	Purpose             string  `gorm:"column:Purpose"`
	WorkStatus          string  `gorm:"column:WorkStatus"`
	TaskLongitude       string  `gorm:"column:TaskLongitude"`
	TaskLatitude        string  `gorm:"column:TaskLatitude"`
	SourceDataFileName  string  `gorm:"column:SourceDataFileName"`
	SourceDataFileSize  float64 `gorm:"column:SourceDataFileSize"`
	SourceDataCount     int32   `gorm:"column:SourceDataFileCount"`
	ResultFileName      string  `gorm:"column:ResultFileName"`
	DocumentFileName    string  `gorm:"column:DocumentFileName"`
	Operator            string  `gorm:"column:Operator"`
	Notes               string  `gorm:"column:Notes"`
	RxName              string  `gorm:"column:RxName"`
}

// TableName 测试数据库
func (v UserStructGorm) TableName() string {
	return "TestTable"
}
func (v UserStructGormSelect) TableName() string {
	return "TestTable"
}
func (v DeptStructGorm) TableName() string {
	return "TestDeptTable"
}
func (v GeneralDataGorm) TableName() string {
	return "general_data"
}
func (v GeneralDataGormSelect) TableName() string {
	return "general_data"
}
