package GrpcTools

import (
	gen "GoSqlPlugin/GrpcTools/Proto/build/gen"
	"GoSqlPlugin/LogService"
	settings "GoSqlPlugin/Settings"
	"github.com/jinzhu/copier"
)

// ConvertToGormModel 结构体类型转换  将grpc传过来的结构体转换为可以直接写入数据库的类型
// 后续增加结构体这里函数要同步修改，其他已实现统一化
func ConvertToGormModel(input *gen.TableStruct) (settings.GormModel, int32) {
	switch v := input.TableData.(type) {
	case *gen.TableStruct_User:
		gormModel := settings.UserStructGorm{}
		copier.Copy(&gormModel, &v.User)
		return gormModel, settings.UserTableType
	case *gen.TableStruct_Dept:
		gormModel := settings.DeptStructGorm{}
		copier.Copy(&gormModel, &v.Dept)
		return gormModel, settings.DeptTableType
	case *gen.TableStruct_Gld:
		gormModel := settings.GeneralDataGorm{}
		copier.Copy(&gormModel, &v.Gld)
		return gormModel, settings.GeneralDataTableType
	default:
		return nil, -1
	}
}

func GetOrder(tableStruct *gen.TableStruct, structType int32) *gen.SelectOrder {
	var order *gen.SelectOrder
	if structType == settings.UserTableType {
		s := tableStruct.GetUser()
		order = s.GetOrder()
	} else if structType == settings.DeptTableType {
		s := tableStruct.GetDept()
		order = s.GetOrder()
	} else if structType == settings.GeneralDataTableType {
		s := tableStruct.GetGld()
		order = s.GetOrder()
	}
	return order
}

func ConvertToRpcData(ret interface{}, resultArray *gen.ResultStructArray) {
	if v, ok := ret.([]settings.UserStructGormSelect); ok {
		resultArray.ResultStructs = make([]*gen.TableStruct, len(v)) // 初始化 resultArray 的长度
		for k, value := range v {
			userStruct := &gen.UserStruct{}
			if err := copier.Copy(userStruct, &value); err != nil {
				LogService.Logger.Error("Failed to copy data: " + err.Error())
				continue
			}
			resultArray.ResultStructs[k] = &gen.TableStruct{
				TableData: &gen.TableStruct_User{
					User: userStruct,
				},
			}
		}

	} else if v, ok := ret.([]settings.GeneralDataGormSelect); ok {
		resultArray.ResultStructs = make([]*gen.TableStruct, len(v)) // 初始化 resultArray 的长度
		for k, value := range v {
			generalData := &gen.GeneralData{}
			if err := copier.Copy(generalData, &value); err != nil {
				LogService.Logger.Error("Failed to copy data: " + err.Error())
				continue
			}
			resultArray.ResultStructs[k] = &gen.TableStruct{
				TableData: &gen.TableStruct_Gld{
					Gld: generalData,
				},
			}
		}
	}
}
