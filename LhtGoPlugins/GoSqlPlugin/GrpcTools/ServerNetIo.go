package GrpcTools

import (
	gen "GoSqlPlugin/GrpcTools/Proto/build/gen"
	"GoSqlPlugin/LogService"
	"GoSqlPlugin/SqlTools/Mysql"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
)

// SqlPluginServer is the server implementation for the gRPC service
type SqlPluginServer struct {
	gen.UnimplementedSqlPluginServer
}

// LhtMysqlInsert Implement the  method
func (s *SqlPluginServer) LhtMysqlInsert(stream gen.SqlPlugin_LhtMysqlInsertServer) error {
	//ctx := stream.Context()
	for {
		userStruct, err := stream.Recv()
		if err == io.EOF {
			return stream.Send(&gen.Result{Success: true})
		}
		if err != nil {
			if status.Code(err) == codes.Canceled {
				log.Println("Client connection was closed")
				return nil
			}
			err = stream.Send(&gen.Result{Success: false})
			return err
		}

		// Implement your insertion logic here
		log.Printf("Inserting: %v", userStruct)
		//这里将他转换为GORM 可以支持的结构体
		goStruct, _ := ConvertToGormModel(userStruct)
		if goStruct != nil {
			err = MysqlManager.LhtInsert(goStruct)
			if err != nil {
				err = stream.Send(&gen.Result{Success: false, ErrorStr: err.Error()})
				//return err
			} else {
				// 实时回复客户端
				err = stream.Send(&gen.Result{Success: true})
			}
		} else {
			err = stream.Send(&gen.Result{Success: false, ErrorStr: "unsupported struct type"})
		}

		//return nil
		// You might want to process the received data here
	}
}

// LhtMysqlSelect Implement the  method
func (s *SqlPluginServer) LhtMysqlSelect(stream gen.SqlPlugin_LhtMysqlSelectServer) error {
	for {
		userStruct, err := stream.Recv()
		if err != nil {
			if status.Code(err) == codes.Canceled {
				log.Println("Client connection was closed")
				return nil
			}
			return err
		}
		log.Printf("Selecting: %v", userStruct)
		//转换为Gorm类型结构体
		goStruct, structType := ConvertToGormModel(userStruct)
		if goStruct != nil {
			//获取查询条件
			order := GetOrder(userStruct, structType)
			//执行sql
			_, ret := MysqlManager.LhtSelect(order, goStruct)
			var resultArray gen.ResultStructArray
			//抓换为Grpc结构类型
			ConvertToRpcData(ret, &resultArray)
			// Send back a response
			if err := stream.Send(&resultArray); err != nil {
				LogService.Logger.Error(err.Error())
			}
		} else {
			err = stream.Send(&gen.ResultStructArray{})
		}

		//return nil
	}
}

func (s *SqlPluginServer) LhtMysqlUpdate(stream gen.SqlPlugin_LhtMysqlUpdateServer) error {
	for {
		userStruct, err := stream.Recv()
		if err == io.EOF {
			return stream.Send(&gen.Result{Success: true})
		}
		if err != nil {
			if status.Code(err) == codes.Canceled {
				log.Println("Client connection was closed")
				return nil
			}
			err = stream.SendMsg(&gen.Result{Success: false})
			return err
		}

		//转换为Gorm类型结构体
		goStruct, structType := ConvertToGormModel(userStruct)
		if goStruct != nil {
			//获取查询条件
			order := GetOrder(userStruct, structType)
			//执行sql
			err = MysqlManager.LhtUpdate(order, goStruct)
			if err != nil {
				err = stream.Send(&gen.Result{Success: false, ErrorStr: err.Error()})
				//return err
			} else {
				// 实时回复客户端
				err = stream.Send(&gen.Result{Success: true})
			}

		} else {
			err = stream.Send(&gen.Result{Success: false, ErrorStr: "unsupported struct type"})
		}

	}
}

// LhtMysqlDelete Implement the  method
func (s *SqlPluginServer) LhtMysqlDelete(stream gen.SqlPlugin_LhtMysqlDeleteServer) error {
	for {
		userStruct, err := stream.Recv()
		if err == io.EOF {
			return stream.Send(&gen.Result{Success: true})
		}
		if err != nil {
			if status.Code(err) == codes.Canceled {
				log.Println("Client connection was closed")
				return nil
			}
			err = stream.SendMsg(&gen.Result{Success: false})
			return err
		}

		log.Printf("Deleting: %v", userStruct)
		//转换为Gorm类型结构体
		_, structType := ConvertToGormModel(userStruct)
		//获取查询条件
		order := GetOrder(userStruct, structType)

		MysqlManager.LhtDelete(order, userStruct)

		err = stream.SendMsg(&gen.Result{Success: true})
		//return nil

	}
}

// LhtMysqlUpdate Implement the  method

var GrpcServer *grpc.Server

func StartGRPCServer(netType string, port string) {
	lis, err := net.Listen(netType, port)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	GrpcServer = grpc.NewServer()
	gen.RegisterSqlPluginServer(GrpcServer, &SqlPluginServer{})

	// Register reflection service on gRPC server.
	reflection.Register(GrpcServer)

	log.Println("Starting gRPC server on port ", port, "net type ", netType)
	if err := GrpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
