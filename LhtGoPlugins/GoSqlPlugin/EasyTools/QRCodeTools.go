package EasyTools

import (
	"GoSqlPlugin/LogService"
	"github.com/skip2/go-qrcode"
	"image/color"
)

func CreateQRCode(content string) {
	qr, err := qrcode.New(content, qrcode.Medium)
	//qrcode.Encode("123456", qrcode.Medium, 256)
	if err != nil {
		LogService.Logger.Error(err.Error())
	} else {
		qr.BackgroundColor = color.RGBA{50, 205, 50, 255}
		qr.ForegroundColor = color.White
		err := qr.WriteFile(256, "./blog_qrcode.png")
		if err != nil {
			return
		}
	}
}
