package MysqlManager

import "gorm.io/gorm"

// 定义全局的db对象，我们执行数据库操作主要通过他实现。
var _db *gorm.DB

type UserData struct {
	id            int    `db:"id"`
	UserName      string `db:"UserName"`
	UserLoginName string `db:"UserLoginName"`
	PassWord      string `db:"PassWord"`
	Notice        int    `db:"Notice"`
	MacAddress    string `db:"MacAddress"`
	LoginTime     string `db:"LoginTime"`
	ParentDeptID  int    `db:"ParentDeptID"`
}

type DeptData struct {
	DeptName     string `db:"DeptName"`
	DeptID       int    `db:"DeptID"`
	ParentDeptID int    `db:"ParentDeptID"`
	Level        int    `db:"Level"`
}

const (
	SelectUsersQuery = "SELECT id,username,login_name,password,last_login,dept_id,permission_level," +
		"created_time,creator_id,phone_number,email,head_path from users"

	SelectUserQuery = "SELECT id,username,login_name,password,last_login,dept_id,permission_level," +
		"created_time,creator_id,phone_number,email,head_path from users where id = %d"
	SelectAllDeptInfo = "select * from dept_info"
	InsertLoginRecord = "INSERT INTO login_logs ( `user_id`, `user_name`, `login_time`, `ip_address`) " +
		"VALUES (%d, '%s', '%s', '%s');"
	InsertMessageRecord = "INSERT INTO personal_chat_messages (`msg_id`,`sender_id`, `sender_name`, `receiver_id`, `receiver_name`, `content`, `send_time`) " +
		"VALUES ( '%s',%d, '%s', %d, '%s', '%s', '%s');"
	UpdateUserHeadImagePathQuery = "UPDATE users SET head_path = '%s' WHERE id = %d"
)
