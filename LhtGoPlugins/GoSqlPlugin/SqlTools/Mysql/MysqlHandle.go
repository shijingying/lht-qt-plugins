package MysqlManager

import (
	gen "GoSqlPlugin/GrpcTools/Proto/build/gen"
	"GoSqlPlugin/LogService"
	settings "GoSqlPlugin/Settings"
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

// LhtInsert 字段注释说明了gorm库把struct字段转换为表字段名长什么样子。
//
//	type Food struct {
//		Id         int  //表字段名为：id
//		Name       string //表字段名为：name
//		Price      float64 //表字段名为：price
//		TypeId     int  //表字段名为：type_id
//		//字段定义后面使用两个反引号``包裹起来的字符串部分叫做标签定义，这个是golang的基础语法，不同的库会定义不同的标签，有不同的含义
//		CreateTime int64 `gorm:"column:createtime"`  //表字段名为：createtime
//	}
//
// column	指定列名	gorm:"column:createtime"
// primaryKey	指定主键	gorm:"column:id; PRIMARY_KEY"
// -	忽略字段	gorm:"-" 可以忽略struct字段，被忽略的字段不参与gorm的读写操作
// 设置表名，可以通过给Food struct类型定义 TableName函数，返回一个字符串作为表名
//
//	func (v Food) TableName() string {
//		return "food"
//	}
//
// GORM 定义一个 gorm.Model 结构体，其包括字段 ID、CreatedAt、UpdatedAt、DeletedAt。
//
//	type User struct {
//		gorm.Model // 嵌入gorm.Model的字段
//		Name string
//	}
//
// 支持正反向排序获取一个 -1 = 不启用 0 = 不排序 1 = 正向   2= 反向
// symbol 1 =  2 > 3 <
// 获取一列 为空表示不启用
// 获取所有
func LhtSelect(orders *gen.SelectOrder, u interface{}) (error, interface{}) {
	db := GetDB()
	var queryStr string
	asc := -1
	order := orders.GetOrder()
	var err error

	if len(order["asc"]) > 0 {
		asc, err = strconv.Atoi(order["asc"])
		if err != nil {
			LogService.Logger.Error(err.Error())
			return err, nil
		}
	}
	queryStr = order["query"]
	var users interface{}
	switch u.(type) {
	case settings.UserStructGorm:
		users = []settings.UserStructGormSelect{}
	case settings.GeneralDataGorm:
		users = []settings.GeneralDataGormSelect{}
	default:
		return nil, nil
	}
	if asc == 0 {
		db.Debug().Where(queryStr).Take(&users)
	} else if asc == 1 {
		db.Debug().Where(queryStr).First(&users)
	} else if asc == 2 {
		db.Debug().Where(queryStr).Last(&users)
	} else {
		db.Debug().Where(queryStr).Find(&users)
	}

	return nil, users
}
func LhtInsert(u interface{}) error {
	db := GetDB()
	if err := db.Debug().Create(u).Error; err != nil {
		fmt.Println("插入失败", err)
		return err // 返回错误，通知调用方发生了错误
	}
	return nil // 如果成功插入，返回 nil 表示没有错误
}
func LhtUpdate(orders *gen.SelectOrder, u interface{}) error {
	db := GetDB()
	order := orders.GetOrder()
	queryStr := order["query"]
	val := reflect.ValueOf(u)
	if val.Kind() == reflect.Ptr {
		val = val.Elem() // 获取指针指向的结构体
	}
	if val.Kind() != reflect.Struct {
		return errors.New("input is not a struct")
	}
	// 遍历结构体字段
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		fieldType := val.Type().Field(i)
		if !isEmptyValue(field) {
			LogService.Logger.Info("Updating field: " + fieldType.Name)
			// 执行数据库更新操作
			if err := db.Debug().Model(&u).Where(queryStr).Update(fieldType.Name, field.Interface()).Error; err != nil {
				return err
			}
		}
	}
	return nil
}

// 判断字段是否为空的辅助函数
func isEmptyValue(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.String, reflect.Array, reflect.Slice, reflect.Map:
		return v.Len() == 0
	case reflect.Bool:
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return v.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return v.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return v.IsNil()
	}
	return false
}

//func LhtUpdate(orders *gen.SelectOrder, u interface{}) error {
//	db := GetDB()
//	u1 := u
//	order := orders.GetOrder()
//	queryStr := order["query"]
//	for k, v := range order {
//		LogService.Logger.Info("update key " + k + " value " + v)
//		if err := db.Debug().Model(&u1).Where(queryStr).Update(k, v).Error; err != nil {
//			return err
//		}
//	}
//	//db.Debug().Where(queryStr).Take(&u1)
//	//db.Debug().Save(&u)
//	return nil
//}

func LhtDelete(orders *gen.SelectOrder, u interface{}) {
	db := GetDB()
	order := orders.GetOrder()
	queryStr := order["query"]
	db.Debug().Where(queryStr).Delete(u)
}
