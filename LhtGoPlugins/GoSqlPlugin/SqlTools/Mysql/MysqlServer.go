package MysqlManager

import (
	"GoSqlPlugin/LogService"
	settings "GoSqlPlugin/Settings"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)
import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func StartMysqlService(cfg *settings.MysqlConfig) {

	connectStr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.DbName,
	)

	// 声明err变量，下面不能使用:=赋值运算符，否则_db变量会当成局部变量，导致外部无法访问_db变量
	var err error
	//连接MYSQL, 获得DB类型实例，用于后面的数据库读写操作。
	_db, err = gorm.Open(mysql.Open(connectStr), &gorm.Config{})
	if err != nil {
		panic("连接数据库失败, error=" + err.Error())
	}

	sqlDB, _ := _db.DB()
	//设置数据库连接池参数
	sqlDB.SetMaxOpenConns(100) //设置数据库连接池最大连接数
	sqlDB.SetMaxIdleConns(20)  //连接池最大允许的空闲连接数，如果没有sql任务需要执行的连接数大于20，超过的连接会被连接池关闭。
	LogService.Logger.Info("Mysql 连接成功")
	//go databaseInit()
}
func databaseInit() {
	db, err := _db.DB()
	if err != nil {
		return
	}
	defer db.Close()
}

func GetDB() *gorm.DB {
	return _db
}
