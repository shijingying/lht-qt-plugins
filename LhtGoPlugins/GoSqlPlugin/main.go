package main

import (
	"GoSqlPlugin/GrpcTools"
	"GoSqlPlugin/LogService"
	settings "GoSqlPlugin/Settings"
	"GoSqlPlugin/SqlTools/Mysql"
	"GoSqlPlugin/SqlTools/Redis"
	"go.uber.org/zap"
)

func main() {
	// 初始化配置
	if err := settings.InitConfiguration(); err != nil {
		panic(err)
	}

	//日志系统初始化
	LogService.InitLogger(settings.Conf.ZapLogConfig)
	LogService.Logger.Info("日志服务启动")
	//Redis初始化
	RedisManager.InitRedis(settings.Conf.RedisConfig)
	//Mongo初始化
	//MongoManager.InitEngine(settings.Conf.MongoConfig)
	//Mysql连接池启动
	MysqlManager.StartMysqlService(settings.Conf.MysqlConfig)

	//北斗模块测试
	//BDTools.ConnectBeidou(settings.Conf.BDConfig)

	go GrpcTools.StartGRPCServer("tcp", ":50051")

	defer zap.L().Sync()

	defer LogService.Logger.Sync()
	select {}
}
