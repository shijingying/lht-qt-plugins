package BDTools

import (
	"GoSqlPlugin/LogService"
	settings "GoSqlPlugin/Settings"
	"github.com/adrianmo/go-nmea"
	"log"
)
import "github.com/tarm/serial"

// ConnectBeidou 连接北斗模块并返回串口连接对象
var bdPort *serial.Port

func ConnectBeidou(config *settings.BDConfig) {
	var err error
	portConfig := &serial.Config{Name: config.Name, Baud: config.Baud}
	bdPort, err = serial.OpenPort(portConfig)
	if err != nil {
		LogService.Logger.Error("ConnectBeidou Error " + err.Error())
	}
}

// ReadBeidouData 从北斗模块读取数据并返回BDData结构体
func ReadBeidouData() (nmea.Sentence, error) {
	buf := make([]byte, 128)

	n, err := bdPort.Read(buf)
	if err != nil {
		return nil, err
	}

	sentence := string(buf[:n])
	parsed, err := nmea.Parse(sentence)
	if err != nil {
		return nil, err
	}

	switch msg := parsed.(type) {
	case nmea.GGA:
		return msg, nil
	default:
		log.Println("Unsupported NMEA sentence type")
	}

	return nil, nil
}
