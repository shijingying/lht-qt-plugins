#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_lhtGrpcLibInterface = new LhtGrpcLibInterface("localhost:50051");
    // m_lhtGrpcLibInterface->init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

static int index = 1;
void MainWindow::on_insert_btn_clicked()
{
    ::sqlplugin::UserStruct userStruct;
    userStruct.set_age(index);
    userStruct.set_id(index++);
    userStruct.set_username("testttttttttttttt");
    Result result;
    ResultStructArray resultArray;
    m_lhtGrpcLibInterface->LhtSqlQuery1(INSERT_OPION,userStruct,result,resultArray);
}


void MainWindow::on_select_all_btn_clicked()
{
    ::sqlplugin::UserStruct userStruct;
    ::sqlplugin::SelectOrder *order = new ::sqlplugin::SelectOrder;
    // 获取 map 的引用并设置键值对
    std::string query;
    (*order->mutable_order())["query"] = QString("ID < %1").arg(index).toStdString();/*sprintf(query.,"ID < %d",index);*/
    userStruct.set_allocated_order(order);
    Result result1;
    ResultStructArray resultArray;
    m_lhtGrpcLibInterface->LhtSqlQuery1(SELECT_OPION,userStruct,result1,resultArray);
    printAll(resultArray);
}

void MainWindow::printAll(const ResultStructArray & resultArray)
{
    auto resultSize = resultArray.resultstructs_size();
    auto result = resultArray.resultstructs();
    // 遍历所有的 TableStruct 对象
    for (int i = 0; i < resultSize; ++i) {
        const sqlplugin::TableStruct& tableStruct = result.Get(i);

        // 检查 TableStruct 中存储的具体类型
        switch (tableStruct.tableData_case()) {
        case sqlplugin::TableStruct::kUser: {
            const sqlplugin::UserStruct& userStruct = tableStruct.user();
            int id = userStruct.id();
            int age = userStruct.age();
            std::string name = userStruct.username();
            qDebug()<<"id:"<<id<<" age:"<<age;
            // 你可以在这里使用这些字段进行其他操作
            break;
        }
        case sqlplugin::TableStruct::kDept: {
            const sqlplugin::DeptStruct& deptStruct = tableStruct.dept();
            int deptId = deptStruct.deptid();
            std::string deptName = deptStruct.deptname();
            // 你可以在这里使用这些字段进行其他操作
            break;
        }
        // 如果有其他类型，可以继续添加 case
        default:
            // 处理未知的或不支持的结构体类型
            qDebug() << "Unsupported structure type";
            break;
        }
    }
}

