#ifndef LHT_GRPC_LIB_INTERFACE_H
#define LHT_GRPC_LIB_INTERFACE_H

#include <QMutex>
#include "thread/event_driven_thread.h"
#include "protos/SqlPlugin.grpc.pb.h"
#include <grpcpp/grpcpp.h>
#include <any>
#define INSERT_OPION 1
#define SELECT_OPION 2
#define DELETE_OPION 3
#define UPDATE_OPION 4
/*
 * @author leehuitao
 * @date 2024 8 21
 *
* 这是一个调用grpc的接口
* 后续调用是只需要实例化他并且初始化连接即可
* 这可以是一个线程，或者你可以将他直接使用不启动他的线程
* init 即表示启动线程
* 这是一个线程安全的接口，可以放心使用
* EventDrivenPool<int,T>
* int 表示输入数据的操作类型  1 插入 2 查询 3 删除  4 修改
* T 表示他的结构体类型  每种数据库的存入需要示例化不同的类型
**/
using namespace ::sqlplugin;
typedef std::shared_ptr<grpc::Channel> LhtChannel;
typedef std::unique_ptr<sqlplugin::SqlPlugin::Stub> LhtStub;
typedef grpc::ClientContext LhtClientContext;
typedef std::unique_ptr< ::grpc::ClientReaderWriter< TableStruct, Result>> LhtInsertClientReaderWriter;
typedef std::unique_ptr< ::grpc::ClientReaderWriter< TableStruct, ResultStructArray>> LhtSelectClientReaderWriter;
typedef std::map<int, std::function<bool(const TableStruct&, Result&, ResultStructArray&)>> LhtHandleMap;

// template<typename ResponseType,typename ResponseType1>
class LhtGrpcLibInterface : public EventDrivenPool<int,std::any>
{

public:
    explicit LhtGrpcLibInterface(std::string serverUrl);
    /**
     * @brief LhtSqlQuery 统一接口
     * @param option 操作类型
     * @param T in out  输入输出结构体
     * @result bool 返回操作结果
    */
    bool LhtSqlQuery1(int option,const std::any& sqlStruct,Result &result,ResultStructArray &resultArray);

    bool LhtSqlQuery(int option,const TableStruct& sqlStruct,Result &result,ResultStructArray &resultArray);

private:
    bool lhtInsert(const TableStruct& sqlStruct,Result &result);

    bool lhtSelect(const TableStruct& sqlStruct,ResultStructArray &resultArray);

    bool lhtDelete(const TableStruct& sqlStruct,Result&result);

    bool lhtUpdate(const TableStruct& sqlStruct,Result &result);

    // EventDrivenPool interface
protected:
    void handleData(std::tuple<int,std::any> &args);
private:
    bool checkGrpcStream(int option);
    //后续增加结构体这两个函数要同步修改，其他已实现统一化
    void createTableStruct(const std::any & input,TableStruct & ret );

    void getAllData(ResultStructArray &resultArray);
private:
    std::string             m_serverUrl;
    bool                    m_initState[5];
    QMutex                  m_mutex[5];
    LhtChannel              m_channel[5];
    LhtStub                 m_stub[5];
    LhtClientContext        *m_context[5];
    LhtInsertClientReaderWriter  m_stream[5];
    LhtSelectClientReaderWriter  m_selectStream;
    LhtHandleMap                 m_handleMap;

};

#endif // LHT_GRPC_LIB_INTERFACE_H
