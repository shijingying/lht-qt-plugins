// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: SqlPlugin.proto

#include "SqlPlugin.pb.h"
#include "SqlPlugin.grpc.pb.h"

#include <functional>
#include <grpcpp/support/async_stream.h>
#include <grpcpp/support/async_unary_call.h>
#include <grpcpp/impl/channel_interface.h>
#include <grpcpp/impl/client_unary_call.h>
#include <grpcpp/support/client_callback.h>
#include <grpcpp/support/message_allocator.h>
#include <grpcpp/support/method_handler.h>
#include <grpcpp/impl/rpc_service_method.h>
#include <grpcpp/support/server_callback.h>
#include <grpcpp/impl/server_callback_handlers.h>
#include <grpcpp/server_context.h>
#include <grpcpp/impl/service_type.h>
#include <grpcpp/support/sync_stream.h>
namespace sqlplugin {

static const char* SqlPlugin_method_names[] = {
  "/sqlplugin.SqlPlugin/LhtMysqlInsert",
  "/sqlplugin.SqlPlugin/LhtMysqlSelect",
  "/sqlplugin.SqlPlugin/LhtMysqlDelete",
  "/sqlplugin.SqlPlugin/LhtMysqlUpdate",
};

std::unique_ptr< SqlPlugin::Stub> SqlPlugin::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< SqlPlugin::Stub> stub(new SqlPlugin::Stub(channel, options));
  return stub;
}

SqlPlugin::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options)
  : channel_(channel), rpcmethod_LhtMysqlInsert_(SqlPlugin_method_names[0], options.suffix_for_stats(),::grpc::internal::RpcMethod::BIDI_STREAMING, channel)
  , rpcmethod_LhtMysqlSelect_(SqlPlugin_method_names[1], options.suffix_for_stats(),::grpc::internal::RpcMethod::BIDI_STREAMING, channel)
  , rpcmethod_LhtMysqlDelete_(SqlPlugin_method_names[2], options.suffix_for_stats(),::grpc::internal::RpcMethod::BIDI_STREAMING, channel)
  , rpcmethod_LhtMysqlUpdate_(SqlPlugin_method_names[3], options.suffix_for_stats(),::grpc::internal::RpcMethod::BIDI_STREAMING, channel)
  {}

::grpc::ClientReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::LhtMysqlInsertRaw(::grpc::ClientContext* context) {
  return ::grpc::internal::ClientReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), rpcmethod_LhtMysqlInsert_, context);
}

void SqlPlugin::Stub::async::LhtMysqlInsert(::grpc::ClientContext* context, ::grpc::ClientBidiReactor< ::sqlplugin::TableStruct,::sqlplugin::Result>* reactor) {
  ::grpc::internal::ClientCallbackReaderWriterFactory< ::sqlplugin::TableStruct,::sqlplugin::Result>::Create(stub_->channel_.get(), stub_->rpcmethod_LhtMysqlInsert_, context, reactor);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::AsyncLhtMysqlInsertRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), cq, rpcmethod_LhtMysqlInsert_, context, true, tag);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::PrepareAsyncLhtMysqlInsertRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), cq, rpcmethod_LhtMysqlInsert_, context, false, nullptr);
}

::grpc::ClientReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>* SqlPlugin::Stub::LhtMysqlSelectRaw(::grpc::ClientContext* context) {
  return ::grpc::internal::ClientReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>::Create(channel_.get(), rpcmethod_LhtMysqlSelect_, context);
}

void SqlPlugin::Stub::async::LhtMysqlSelect(::grpc::ClientContext* context, ::grpc::ClientBidiReactor< ::sqlplugin::TableStruct,::sqlplugin::ResultStructArray>* reactor) {
  ::grpc::internal::ClientCallbackReaderWriterFactory< ::sqlplugin::TableStruct,::sqlplugin::ResultStructArray>::Create(stub_->channel_.get(), stub_->rpcmethod_LhtMysqlSelect_, context, reactor);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>* SqlPlugin::Stub::AsyncLhtMysqlSelectRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>::Create(channel_.get(), cq, rpcmethod_LhtMysqlSelect_, context, true, tag);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>* SqlPlugin::Stub::PrepareAsyncLhtMysqlSelectRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>::Create(channel_.get(), cq, rpcmethod_LhtMysqlSelect_, context, false, nullptr);
}

::grpc::ClientReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::LhtMysqlDeleteRaw(::grpc::ClientContext* context) {
  return ::grpc::internal::ClientReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), rpcmethod_LhtMysqlDelete_, context);
}

void SqlPlugin::Stub::async::LhtMysqlDelete(::grpc::ClientContext* context, ::grpc::ClientBidiReactor< ::sqlplugin::TableStruct,::sqlplugin::Result>* reactor) {
  ::grpc::internal::ClientCallbackReaderWriterFactory< ::sqlplugin::TableStruct,::sqlplugin::Result>::Create(stub_->channel_.get(), stub_->rpcmethod_LhtMysqlDelete_, context, reactor);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::AsyncLhtMysqlDeleteRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), cq, rpcmethod_LhtMysqlDelete_, context, true, tag);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::PrepareAsyncLhtMysqlDeleteRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), cq, rpcmethod_LhtMysqlDelete_, context, false, nullptr);
}

::grpc::ClientReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::LhtMysqlUpdateRaw(::grpc::ClientContext* context) {
  return ::grpc::internal::ClientReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), rpcmethod_LhtMysqlUpdate_, context);
}

void SqlPlugin::Stub::async::LhtMysqlUpdate(::grpc::ClientContext* context, ::grpc::ClientBidiReactor< ::sqlplugin::TableStruct,::sqlplugin::Result>* reactor) {
  ::grpc::internal::ClientCallbackReaderWriterFactory< ::sqlplugin::TableStruct,::sqlplugin::Result>::Create(stub_->channel_.get(), stub_->rpcmethod_LhtMysqlUpdate_, context, reactor);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::AsyncLhtMysqlUpdateRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), cq, rpcmethod_LhtMysqlUpdate_, context, true, tag);
}

::grpc::ClientAsyncReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>* SqlPlugin::Stub::PrepareAsyncLhtMysqlUpdateRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) {
  return ::grpc::internal::ClientAsyncReaderWriterFactory< ::sqlplugin::TableStruct, ::sqlplugin::Result>::Create(channel_.get(), cq, rpcmethod_LhtMysqlUpdate_, context, false, nullptr);
}

SqlPlugin::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      SqlPlugin_method_names[0],
      ::grpc::internal::RpcMethod::BIDI_STREAMING,
      new ::grpc::internal::BidiStreamingHandler< SqlPlugin::Service, ::sqlplugin::TableStruct, ::sqlplugin::Result>(
          [](SqlPlugin::Service* service,
             ::grpc::ServerContext* ctx,
             ::grpc::ServerReaderWriter<::sqlplugin::Result,
             ::sqlplugin::TableStruct>* stream) {
               return service->LhtMysqlInsert(ctx, stream);
             }, this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      SqlPlugin_method_names[1],
      ::grpc::internal::RpcMethod::BIDI_STREAMING,
      new ::grpc::internal::BidiStreamingHandler< SqlPlugin::Service, ::sqlplugin::TableStruct, ::sqlplugin::ResultStructArray>(
          [](SqlPlugin::Service* service,
             ::grpc::ServerContext* ctx,
             ::grpc::ServerReaderWriter<::sqlplugin::ResultStructArray,
             ::sqlplugin::TableStruct>* stream) {
               return service->LhtMysqlSelect(ctx, stream);
             }, this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      SqlPlugin_method_names[2],
      ::grpc::internal::RpcMethod::BIDI_STREAMING,
      new ::grpc::internal::BidiStreamingHandler< SqlPlugin::Service, ::sqlplugin::TableStruct, ::sqlplugin::Result>(
          [](SqlPlugin::Service* service,
             ::grpc::ServerContext* ctx,
             ::grpc::ServerReaderWriter<::sqlplugin::Result,
             ::sqlplugin::TableStruct>* stream) {
               return service->LhtMysqlDelete(ctx, stream);
             }, this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      SqlPlugin_method_names[3],
      ::grpc::internal::RpcMethod::BIDI_STREAMING,
      new ::grpc::internal::BidiStreamingHandler< SqlPlugin::Service, ::sqlplugin::TableStruct, ::sqlplugin::Result>(
          [](SqlPlugin::Service* service,
             ::grpc::ServerContext* ctx,
             ::grpc::ServerReaderWriter<::sqlplugin::Result,
             ::sqlplugin::TableStruct>* stream) {
               return service->LhtMysqlUpdate(ctx, stream);
             }, this)));
}

SqlPlugin::Service::~Service() {
}

::grpc::Status SqlPlugin::Service::LhtMysqlInsert(::grpc::ServerContext* context, ::grpc::ServerReaderWriter< ::sqlplugin::Result, ::sqlplugin::TableStruct>* stream) {
  (void) context;
  (void) stream;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status SqlPlugin::Service::LhtMysqlSelect(::grpc::ServerContext* context, ::grpc::ServerReaderWriter< ::sqlplugin::ResultStructArray, ::sqlplugin::TableStruct>* stream) {
  (void) context;
  (void) stream;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status SqlPlugin::Service::LhtMysqlDelete(::grpc::ServerContext* context, ::grpc::ServerReaderWriter< ::sqlplugin::Result, ::sqlplugin::TableStruct>* stream) {
  (void) context;
  (void) stream;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status SqlPlugin::Service::LhtMysqlUpdate(::grpc::ServerContext* context, ::grpc::ServerReaderWriter< ::sqlplugin::Result, ::sqlplugin::TableStruct>* stream) {
  (void) context;
  (void) stream;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


}  // namespace sqlplugin

