#include "lht_grpc_lib_interface.h"

LhtGrpcLibInterface::LhtGrpcLibInterface(std::string serverUrl):
    m_serverUrl(serverUrl),
    m_initState{false,false,false,false,false}
{
     m_handleMap = {
        {INSERT_OPION, [this](const TableStruct& retStruct, Result& result, ResultStructArray&) -> bool {
             return lhtInsert(retStruct, result);
         }},
        {SELECT_OPION, [this](const TableStruct& retStruct, Result&, ResultStructArray& resultArray) -> bool {
             return lhtSelect(retStruct, resultArray);
         }},
        {DELETE_OPION, [this](const TableStruct& retStruct, Result& result, ResultStructArray&) -> bool {
             return lhtDelete(retStruct, result);
         }},
        {UPDATE_OPION, [this](const TableStruct& retStruct, Result& result, ResultStructArray&) -> bool {
             return lhtUpdate(retStruct, result);
         }},
    };
}

bool LhtGrpcLibInterface::LhtSqlQuery(int option,const TableStruct &sqlStruct, Result &result, ResultStructArray &resultArray)
{
    QMutexLocker lock(&m_mutex[option]);
    if(!checkGrpcStream(option))
        return false;
    bool ret = false;
    ret = m_handleMap[option](sqlStruct,result,resultArray);
    qDebug() << "Received state: " << ret ;
    return true;
}

bool LhtGrpcLibInterface::LhtSqlQuery1(int option,const std::any &sqlStruct, Result &result, ResultStructArray &resultArray)
{
    if(!checkGrpcStream(option))
        return false;
    bool ret = false;
    TableStruct retStruct;
    createTableStruct(sqlStruct,retStruct);
    ret = m_handleMap[option](retStruct,result,resultArray);
    if(!ret){
        m_initState[option] = false;
        checkGrpcStream(option);
    }
    qDebug() << "Received state: " << ret ;
}

bool LhtGrpcLibInterface::lhtInsert(const TableStruct &sqlStruct, Result &result)
{
    auto ret = m_stream[INSERT_OPION]->Write(sqlStruct);
    if(!ret){
        m_stream[INSERT_OPION]->Finish();
        return false;
        qDebug()<<__FUNCTION__<<"Write Error";
    }
    ret = m_stream[INSERT_OPION]->Read(&result);
    if(!ret){
        qDebug()<<__FUNCTION__<<"error";
    }
    auto resultRet = result.success();
    auto retStr = QString::fromStdString(result.errorstr());
    if(!resultRet)
        qDebug()<<__FUNCTION__<<"error:"<<retStr;
    // sqlStruct.Clear();
    return ret;
}

bool LhtGrpcLibInterface::lhtSelect(const TableStruct &sqlStruct, ResultStructArray &resultArray)
{
    auto ret = m_selectStream->Write(sqlStruct);
    if(!ret){
        m_selectStream->Finish();
        return false;
        qDebug()<<__FUNCTION__<<"Write Error";
    }
    ret = m_selectStream->Read(&resultArray);
    getAllData(resultArray);
    if(!ret){
        qDebug()<<__FUNCTION__<<"error";
    }
    return ret;
}

bool LhtGrpcLibInterface::lhtDelete(const TableStruct &sqlStruct, Result &result)
{
    return false;
}

bool LhtGrpcLibInterface::lhtUpdate(const TableStruct &sqlStruct, Result &result)
{
    return false;
}

void LhtGrpcLibInterface::handleData(std::tuple<int, std::any> &args)
{
    int option = std::get<0>(args);
    std::any sqlStruct = std::get<1>(args);
    if(!checkGrpcStream(option))
        return;
    bool ret = false;
    TableStruct retStruct;
    createTableStruct(sqlStruct,retStruct);
    Result result;
    ResultStructArray resultArray;
    ret = m_handleMap[option](retStruct,result,resultArray);
    qDebug() << "Received state: " << ret ;
}

bool LhtGrpcLibInterface::checkGrpcStream(int option)
{
    if(option > 0 && option < 5 && !m_initState[option]){
        m_initState[option] = true;
        m_channel[option] = grpc::CreateChannel(m_serverUrl, grpc::InsecureChannelCredentials());
        m_stub[option] = sqlplugin::SqlPlugin::NewStub(m_channel[option]);
        m_context[option] = new LhtClientContext();
        if(option == INSERT_OPION){
            m_stream[option] = LhtInsertClientReaderWriter(m_stub[option]->LhtMysqlInsert(m_context[option]));
        }else if(option == SELECT_OPION){
            m_selectStream = LhtSelectClientReaderWriter(m_stub[option]->LhtMysqlSelect(m_context[option]));
        }else if(option == DELETE_OPION){
            m_stream[option] = LhtInsertClientReaderWriter(m_stub[option]->LhtMysqlDelete(m_context[option]));
        }else if(option == UPDATE_OPION){
            m_stream[option] = LhtInsertClientReaderWriter(m_stub[option]->LhtMysqlUpdate(m_context[option]));
        }

    }else{
        if(option <= 0 || option >= 5)
            return false;
    }
    return true;
}

void LhtGrpcLibInterface::createTableStruct(const std::any &input, TableStruct &ret){
    if (input.type() == typeid(::sqlplugin::UserStruct)) {
        UserStruct *userStruct = new UserStruct;
        userStruct->CopyFrom(std::any_cast<::sqlplugin::UserStruct>(input));
        ret.set_allocated_user(userStruct);
    } else if (input.type() == typeid(::sqlplugin::DeptStruct)) {
        DeptStruct *deptStruct = new DeptStruct;
        deptStruct->CopyFrom(std::any_cast<::sqlplugin::DeptStruct>(input));
        ret.set_allocated_dept(deptStruct);
    }
}

void LhtGrpcLibInterface::getAllData(ResultStructArray &resultArray){
    auto resultSize = resultArray.resultstructs_size();
    auto result = resultArray.resultstructs();

    // 遍历所有的 TableStruct 对象
    for (int i = 0; i < resultSize; ++i) {
        const sqlplugin::TableStruct& tableStruct = result.Get(i);

        // 检查 TableStruct 中存储的具体类型
        switch (tableStruct.tableData_case()) {
        case sqlplugin::TableStruct::kUser: {
            const sqlplugin::UserStruct& userStruct = tableStruct.user();
            int id = userStruct.id();
            int age = userStruct.age();
            std::string name = userStruct.username();
            qDebug()<<"id:"<<id<<" age:"<<age;
            // 你可以在这里使用这些字段进行其他操作
            break;
        }
        case sqlplugin::TableStruct::kDept: {
            const sqlplugin::DeptStruct& deptStruct = tableStruct.dept();
            int deptId = deptStruct.deptid();
            std::string deptName = deptStruct.deptname();
            // 你可以在这里使用这些字段进行其他操作
            break;
        }
        // 如果有其他类型，可以继续添加 case
        default:
            // 处理未知的或不支持的结构体类型
            qDebug() << "Unsupported structure type";
            break;
        }
    }
}
