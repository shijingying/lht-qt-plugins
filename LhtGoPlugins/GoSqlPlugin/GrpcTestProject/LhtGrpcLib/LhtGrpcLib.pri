

HEADERS += $$PWD/protos/SqlPlugin.pb.h \
    $$PWD/lht_grpc_lib_interface.h \
    $$PWD/thread/event_driven_thread.h
SOURCES += $$PWD/protos/SqlPlugin.pb.cc \
    $$PWD/lht_grpc_lib_interface.cpp
HEADERS += $$PWD/protos/SqlPlugin.grpc.pb.h
SOURCES += $$PWD/protos/SqlPlugin.grpc.pb.cc
DISTFILES += $$PWD/protos/SqlPlugin.proto

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include


LIBS += $$PWD/lib/absl_flags_parse.lib
LIBS += $$PWD/lib/grpc++_reflection.lib
LIBS += $$PWD/lib/grpc++.lib
LIBS += $$PWD/lib/libprotobufd.lib
LIBS += $$PWD/lib/grpc.lib
LIBS += $$PWD/lib/upb.lib
LIBS += $$PWD/lib/utf8_range.lib
LIBS += $$PWD/lib/re2.lib
LIBS += $$PWD/lib/zlibd.lib
LIBS += $$PWD/lib/cares.lib
LIBS += $$PWD/lib/gpr.lib
LIBS += $$PWD/lib/libssl.lib
LIBS += $$PWD/lib/libcrypto.lib
LIBS += $$PWD/lib/address_sorting.lib
LIBS += $$PWD/lib/absl_flags_usage.lib
LIBS += $$PWD/lib/absl_flags_usage_internal.lib
LIBS += $$PWD/lib/absl_flags_internal.lib
LIBS += $$PWD/lib/absl_flags_marshalling.lib
LIBS += $$PWD/lib/absl_flags_reflection.lib
LIBS += $$PWD/lib/absl_flags_config.lib
LIBS += $$PWD/lib/absl_flags_private_handle_accessor.lib
LIBS += $$PWD/lib/absl_flags_commandlineflag.lib
LIBS += $$PWD/lib/absl_flags_commandlineflag_internal.lib
LIBS += $$PWD/lib/absl_flags_program_name.lib
LIBS += $$PWD/lib/abseil_dll.lib

LIBS += -ladvapi32 -liphlpapi -lwsock32 -lws2_32 -lcrypt32 -lgdi32
LIBS += -lkernel32 -luser32 -lwinspool -lshell32 -lole32 -loleaut32
LIBS += -luuid -lcomdlg32
