#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "LhtGrpcLib/lht_grpc_lib_interface.h"
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_insert_btn_clicked();

    void on_select_all_btn_clicked();

    void printAll(const ResultStructArray &);
private:
    Ui::MainWindow *ui;
    //每种类型的结构体需要示例化一次  这里是测试结构体的初始化
    LhtGrpcLibInterface *m_lhtGrpcLibInterface;
};
#endif // MAINWINDOW_H
