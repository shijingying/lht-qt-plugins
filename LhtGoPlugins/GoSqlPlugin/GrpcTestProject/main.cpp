#include "mainwindow.h"

#include <QApplication>
void RunClient() {
    std::shared_ptr<grpc::Channel> channel = grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials());
    std::unique_ptr<sqlplugin::SqlPlugin::Stub> stub = sqlplugin::SqlPlugin::NewStub(channel);
    // 接收响应
    ::sqlplugin::Result response;
    grpc::ClientContext context;
    std::unique_ptr< ::grpc::ClientReaderWriter< ::sqlplugin::TableStruct, ::sqlplugin::Result>> stream(
        stub->LhtMysqlInsert(&context));
    int index = 0;
    // 发送请求
    while(1){
        // response.Clear();
        ::sqlplugin::TableStruct request;
        ::sqlplugin::UserStruct user;
        user.set_age(123 + index++);
        user.set_id(321+ index++);
        user.set_username("testttttttttttttt");
        request.set_allocated_user(&user);
        auto ret = stream->Write(request);
        ret = stream->Read(&response);
        // Sleep(1000);
        qDebug()<<"ret ="<<ret;
    }
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // RunClient();
    MainWindow w;
    w.show();
    return a.exec();
}
