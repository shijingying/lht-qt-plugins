package CommandTools

import (
	"GoWatchDog/RegisterServer"
	"fmt"
	"golang.org/x/sys/windows/svc/mgr"
	"log"
	"os/exec"
	"time"
)

// checkServiceStatus checks if the service is running
func checkServiceStatus(serviceName string) (bool, error) {
	m, err := mgr.Connect()
	if err != nil {
		return false, err
	}
	defer m.Disconnect()

	service, err := m.OpenService(serviceName)
	if err != nil {
		return false, err
	}
	defer service.Close()

	status, err := service.Query()
	if err != nil {
		return false, err
	}

	return status.State == mgr.NoAction, nil
}

// restartService restarts the service by executing a .bat script
func restartService(scriptPath string) error {
	cmd := exec.Command("cmd", scriptPath)
	err := cmd.Run()
	return err
}

var ServerList map[string]string //服务名称 -》 启动服务的批处理脚本路径

func StartMonitorServer() {
	ServerList = make(map[string]string)
	RegisterServer.Register(&ServerList)

	for {
		for serviceName, restartScriptPath := range ServerList {
			isRunning, err := checkServiceStatus(serviceName)
			if err != nil {
				log.Fatalf("Failed to check service status: %v", err)
			}

			if !isRunning {
				fmt.Printf("Service %s is not running. Restarting...\n", serviceName)
				err := restartService(restartScriptPath)
				if err != nil {
					log.Printf("Failed to restart service: %v", err)
				} else {
					fmt.Printf("Service %s restarted successfully.\n", serviceName)
				}
			} else {
				fmt.Printf("Service %s is running.\n", serviceName)
			}

			time.Sleep(30 * time.Second) // 每30秒检查一次
		}

	}
}
