package main

import "GoWatchDog/CommandTools"

// 看门狗服务，监控服务启动状态
// 包含注册服务,命令工具等
func main() {
	go CommandTools.StartMonitorServer()

	select {}
}
