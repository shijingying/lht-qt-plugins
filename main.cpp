#include "mainwindow.h"

#include <QApplication>
#include <QCefView.h>
#include <QCefConfig.h>
#include <QCefContext.h>>

void loadStyle(const QString &qssFile)
{
    //开启计时
    QElapsedTimer time;
    time.start();

    //加载样式表
    QString qss;
    QFile file(qssFile);
    if (file.open(QFile::ReadOnly)) {
        //用QTextStream读取样式文件不用区分文件编码 带bom也行
        QStringList list;
        QTextStream in(&file);
        //in.setCodec("utf-8");
        while (!in.atEnd()) {
            QString line;
            in >> line;
            list << line;
        }

        file.close();
        qss = list.join("\n");
        QString paletteColor = qss.mid(20, 7);
        qApp->setPalette(QPalette(paletteColor));
        //用时主要在下面这句
        qApp->setStyleSheet(qss);
    }

    qDebug() << "用时:" << time.elapsed();
}

void initCef(QApplication & a ,int argc, char *argv[]){

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    loadStyle(":/qss/lightblue.css");

    // build QCefConfig
    QCefConfig config;
    // set user agent
    config.setUserAgent("QCefViewTest");
    // set log level
    config.setLogLevel(QCefConfig::LOGSEVERITY_DEFAULT);
    // set JSBridge object name (default value is QCefViewClient)
    config.setBridgeObjectName("CallBridge");
    // port for remote debugging (default is 0 and means to disable remote debugging)
    config.setRemoteDebuggingPort(9000);
    // set background color for all browsers
    // (QCefSetting.setBackgroundColor will overwrite this value for specified browser instance)
    config.setBackgroundColor(Qt::lightGray);

    // WindowlessRenderingEnabled is set to true by default,
    // set to false to disable the OSR mode
    config.setWindowlessRenderingEnabled(true);

    // add command line args, you can any cef supported switches or parameters
    config.addCommandLineSwitch("use-mock-keychain");
    // config.addCommandLineSwitch("disable-gpu");
    // config.addCommandLineSwitch("enable-media-stream");
    // config.addCommandLineSwitch("allow-file-access-from-files");
    // config.addCommandLineSwitch("disable-spell-checking");
    // config.addCommandLineSwitch("disable-site-isolation-trials");
    // config.addCommandLineSwitch("enable-aggressive-domstorage-flushing");
    config.addCommandLineSwitchWithValue("renderer-process-limit", "1");
    // allow remote debugging
    config.addCommandLineSwitchWithValue("remote-allow-origins", "*");
    // config.addCommandLineSwitchWithValue("disable-features", "BlinkGenPropertyTrees,TranslateUI,site-per-process");

#if defined(Q_OS_MACOS) && defined(QT_DEBUG)
    // cef bugs on macOS debug build
    config.setCachePath(QDir::tempPath());
#endif

    // create QCefContext instance with config,
    // the lifecycle of cefContext must be the same as QApplication instance
    QCefContext cefContext(&a, argc, argv, &config);

    MainWindow w;
    w.show();
    return a.exec();
}
