QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
DEFINES += WINDOWS HAVE_STRUCT_TIMESPEC
# DEFINES += __cplusplus
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    ns_turn/apputils.c \
    ns_turn/getopt.c \
    ns_turn/ns_turn_ioaddr.c \
    ns_turn/ns_turn_msg.c \
    ns_turn/ns_turn_msg_addr.c \
    ns_turn/ns_turn_utils.c \
    ns_turn/stun_buffer.c \
    test_client.cpp

HEADERS += \
    client_define.h \
    mainwindow.h \
    ns_turn/TurnMsgLib.h \
    ns_turn/apputils.h \
    ns_turn/getopt.h \
    ns_turn/ns_turn_defs.h \
    ns_turn/ns_turn_ioaddr.h \
    ns_turn/ns_turn_ioalib.h \
    ns_turn/ns_turn_msg.h \
    ns_turn/ns_turn_msg_addr.h \
    ns_turn/ns_turn_msg_defs.h \
    ns_turn/ns_turn_msg_defs_experimental.h \
    ns_turn/ns_turn_openssl.h \
    ns_turn/ns_turn_utils.h \
    ns_turn/stun_buffer.h \
    test_client.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -lws2_32 -lnetapi32 -ladvapi32 -luser32

LIBS += -L$$PWD/../../../../../install/OpenSSL-Win64/lib/ -lopenssl
LIBS += -L$$PWD/../../../../../install/OpenSSL-Win64/lib/ -llibcrypto_static
LIBS += -L$$PWD/../../../../../install/OpenSSL-Win64/lib/ -llibssl

LIBS += -L$$PWD/../../../../../install/libevent-2.1.12-stable/ -llibevent_core
LIBS += -L$$PWD/../../../../../install/libevent-2.1.12-stable/ -llibevent
LIBS += -L$$PWD/../../../../../install/libevent-2.1.12-stable/ -llibevent_extras
LIBS += -L$$PWD/../../../../../install/libevent-2.1.12-stable/ -llibevent_openssl

INCLUDEPATH += $$PWD/../../../../../install/OpenSSL-Win64/include
DEPENDPATH += $$PWD/../../../../../install/OpenSSL-Win64/include

INCLUDEPATH += E:\install\libevent-2.1.12-stable\include
DEPENDPATH += E:\install\libevent-2.1.12-stable\include



# LIBS += -L$$PWD/../../../../../install/pthread/Pre-built.2/lib/ -lpthreadVC2
# LIBS += -L$$PWD/../../../../../install/pthread/Pre-built.2/lib/ -lpthreadVCE2
# LIBS += -L$$PWD/../../../../../install/pthread/Pre-built.2/lib/ -lpthreadVSE2

INCLUDEPATH += $$PWD/../../../../../install/pthread/Pre-built.2/include
DEPENDPATH += $$PWD/../../../../../install/pthread/Pre-built.2/include

