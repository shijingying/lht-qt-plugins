#ifndef TEST_CLIENT_H
#define TEST_CLIENT_H

#include <QObject>
#include "client_define.h"

class TestClient : public QObject
{
    Q_OBJECT
public:
    explicit TestClient(QObject *parent = nullptr);

    void sendReq();
signals:
};

#endif // TEST_CLIENT_H
