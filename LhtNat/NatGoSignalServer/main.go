package main

import (
	"NatGoSignalServer/LogService"
	"NatGoSignalServer/NetHelper/TcpSignalServer"
	settings "NatGoSignalServer/Settings"
)

//Tcp作为信令服务器
//Udp测试打洞
//Grpc用来传输数据
//NetHelper udp  tcp  grpc网络接口(包含文件，消息，测试包)

func main() {
	// 初始化配置
	if err := settings.InitConfiguration(); err != nil {
		panic(err)
	}
	//日志系统初始化
	LogService.InitLogger(settings.Conf.ZapLogConfig)
	LogService.Logger.Info("日志服务启动")
	go TcpSignalServer.StartListen(settings.Conf.NetConfig)

	select {}
}
