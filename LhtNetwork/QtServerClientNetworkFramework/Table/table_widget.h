#ifndef TABLE_WIDGET_H
#define TABLE_WIDGET_H

#include <QWidget>
#include <QTableView>
#include <QSqlQueryModel>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include "../Sql/sqlite_module.h"

class TableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TableWidget(QWidget *parent = nullptr);
    ~TableWidget();

    /**
     * @brief 初始化数据库并加载数据到视图
     * @param dbName 数据库文件名称
     * @param tableName 表名
     */
    void initialize(const QString &dbName);

    void updateEmployeeId(int employeeId);
private:

    void _updateEmployeeId(int employeeId);
private:
    QTableView *m_tableView;
    QSqlQueryModel *m_model;
    SQLiteModule *m_dbModule;

    QString m_tableName = "customers";
    QString m_dbName;

    // 分页相关
    int m_currentPage;
    int m_totalPages;
    int m_recordsPerPage;
    int m_totalRecords;

    // UI 元素
    QPushButton *m_firstPageButton;
    QPushButton *m_prevPageButton;
    QPushButton *m_nextPageButton;
    QPushButton *m_lastPageButton;
    QLabel *m_pageInfoLabel;
    QString    m_query;
    // 批量插入控件
    QPushButton *m_bulkInsertButton;
    QLineEdit *m_nameEdit;
    QLineEdit *m_ageEdit;
    QPushButton *m_addButton;
    QPushButton *m_deleteButton;
    QPushButton *m_refreshButton;
    // 查询相关
    QLineEdit *m_searchEdit;
    QPushButton *m_searchButton;
    QString m_searchKeyword;
    int m_employeeId;
    /**
     * @brief 计算总页数
     */
    void calculateTotalPages();

    /**
     * @brief 加载当前页的数据
     */
    void loadPage();
    /**
     * @brief 执行搜索并更新视图
     */
    void searchRecords();
private slots:
    void onDataChanged();
    void firstPage();
    void prevPage();
    void nextPage();
    void lastPage();
    void addRecord();
    void deleteRecord();
    void refreshData();
    void bulkInsertRecords();
    void reload();
};

#endif // TABLE_WIDGET_H
