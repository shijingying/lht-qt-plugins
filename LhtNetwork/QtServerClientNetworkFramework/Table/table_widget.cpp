﻿#include "table_widget.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QHeaderView>
#include <QItemSelectionModel>
#include <QProgressDialog>
#include <QSqlError>
#include <QDebug>
TableWidget::TableWidget(QWidget *parent)
    : QWidget(parent),
    m_tableView(new QTableView(this)),
    m_model(new QSqlQueryModel(this)),
    m_dbModule(new SQLiteModule(this)),
    m_currentPage(1),
    m_totalPages(1),
    m_recordsPerPage(100),
    m_totalRecords(0),
    m_firstPageButton(new QPushButton(QStringLiteral("第一页"), this)),
    m_prevPageButton(new QPushButton(QStringLiteral("上一页"), this)),
    m_nextPageButton(new QPushButton(QStringLiteral("下一页"), this)),
    m_lastPageButton(new QPushButton(QStringLiteral("最后一页"), this)),
    m_pageInfoLabel(new QLabel(this)),
    m_bulkInsertButton(new QPushButton(QStringLiteral("批量插入一万条"), this)),
    m_nameEdit(new QLineEdit(this)),
    m_ageEdit(new QLineEdit(this)),
    m_addButton(new QPushButton(QStringLiteral("添加"), this)),
    m_deleteButton(new QPushButton(QStringLiteral("删除"), this)), // 每页显示100条记录
    m_refreshButton(new QPushButton(QStringLiteral("刷新"), this)),
    m_searchEdit(new QLineEdit(this)),
    m_searchButton(new QPushButton(QStringLiteral("搜索"), this)),
    m_searchKeyword(""),
    m_employeeId(-1)
{
    // 设置布局
    QVBoxLayout *mainLayout = new QVBoxLayout(this);

    // 设置表格视图
    mainLayout->addWidget(m_tableView);

    // 设置分页控制布局
    QHBoxLayout *paginationLayout = new QHBoxLayout();
    paginationLayout->addWidget(m_firstPageButton);
    paginationLayout->addWidget(m_prevPageButton);
    paginationLayout->addWidget(m_pageInfoLabel);
    paginationLayout->addWidget(m_nextPageButton);
    paginationLayout->addWidget(m_lastPageButton);
    mainLayout->addLayout(paginationLayout);

    // 设置按钮和输入框的布局
    QHBoxLayout *controlsLayout = new QHBoxLayout();
    // 添加查询控件
    m_searchEdit->setPlaceholderText(QStringLiteral("请输入姓名进行搜索"));
    m_searchButton->setText(QStringLiteral("搜索"));
    controlsLayout->addWidget(m_searchEdit);
    controlsLayout->addWidget(m_searchButton);

    m_nameEdit->setPlaceholderText(QStringLiteral("姓名"));
    m_ageEdit->setPlaceholderText(QStringLiteral("年龄"));

    controlsLayout->addWidget(m_nameEdit);
    controlsLayout->addWidget(m_ageEdit);
    controlsLayout->addWidget(m_addButton);
    controlsLayout->addWidget(m_deleteButton);
    controlsLayout->addWidget(m_refreshButton);
    controlsLayout->addWidget(m_bulkInsertButton);

    mainLayout->addLayout(controlsLayout);
    setLayout(mainLayout);

    // 连接数据变化信号
    connect(m_dbModule, &SQLiteModule::dataChanged, this, &TableWidget::onDataChanged);

    // 连接分页按钮信号
    connect(m_firstPageButton, &QPushButton::clicked, this, &TableWidget::firstPage);
    connect(m_prevPageButton, &QPushButton::clicked, this, &TableWidget::prevPage);
    connect(m_nextPageButton, &QPushButton::clicked, this, &TableWidget::nextPage);
    connect(m_lastPageButton, &QPushButton::clicked, this, &TableWidget::lastPage);

    // 连接其他按钮信号
    connect(m_addButton, &QPushButton::clicked, this, &TableWidget::addRecord);
    connect(m_deleteButton, &QPushButton::clicked, this, &TableWidget::deleteRecord);
    connect(m_refreshButton, &QPushButton::clicked, this, &TableWidget::refreshData);
    connect(m_bulkInsertButton, &QPushButton::clicked, this, &TableWidget::bulkInsertRecords);
    // 连接搜索按钮信号
    connect(m_searchButton, &QPushButton::clicked, this, &TableWidget::searchRecords);
    connect(m_searchEdit,&QLineEdit::returnPressed,this, &TableWidget::searchRecords);
}

TableWidget::~TableWidget()
{
    // SQLiteModule 的析构函数会自动关闭数据库连接
}

void TableWidget::initialize(const QString &dbName)
{
    m_dbName = dbName;
    // 初始化数据库连接
    if (!m_dbModule->initDatabase(dbName)) {
        QMessageBox::critical(this, QStringLiteral("错误"), QStringLiteral("无法初始化数据库连接！"));
        return;
    }

    // 创建表（如果尚未创建）
    if (!m_dbModule->createTable()) {
        QMessageBox::critical(this, QStringLiteral("错误"), QStringLiteral("无法创建或访问表！"));
        return;
    }

    // 获取总记录数
    m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    if (m_totalRecords < 0) {
        QMessageBox::critical(this, QStringLiteral("错误"), QStringLiteral("无法获取记录总数！"));
        return;
    }

    // 计算总页数
    calculateTotalPages();

    // 如果表为空，可以插入一些测试数据或批量插入
    if (m_totalRecords == 0) {
        // 插入一万条数据
        QList<QVariantList> bulkData;
        bulkData.reserve(10000);

        for (int i = 1; i <= 10000; ++i) {
            QVariantList record;
            record << QStringLiteral("测试姓名%1").arg(i) << (20 + (i % 50)); // 姓名和年龄
            bulkData.append(record);
        }

        // 显示进度对话框
        QProgressDialog progress(QStringLiteral("正在插入一万条数据..."), QStringLiteral("取消"), 0, bulkData.size(), this);
        progress.setWindowModality(Qt::WindowModal);
        progress.setMinimumDuration(0);
        progress.show();

        // 开始批量插入
        if (m_dbModule->bulkInsertData(m_tableName, bulkData)) {
            QMessageBox::information(this, QStringLiteral("成功"), QStringLiteral("一万条数据已成功插入！"));
        } else {
            QMessageBox::warning(this, QStringLiteral("失败"), QStringLiteral("批量插入数据失败！"));
        }

        progress.setValue(bulkData.size());

        // 更新总记录数和总页数
        m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
        calculateTotalPages();
    }

    // 加载当前页的数据
    loadPage();

    // 更新分页信息标签
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::updateEmployeeId(int employeeId)
{
    m_employeeId = employeeId;
    _updateEmployeeId(employeeId);
}

void TableWidget::_updateEmployeeId(int employeeId)
{
    // 重置到第一页
    m_currentPage = 1;

    // 获取总记录数（根据搜索关键词）
    QString searchFilter = QString("WHERE employee_id = '%1'").arg(employeeId);
    if (employeeId < 0) {
        m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    } else {
        m_totalRecords = m_dbModule->getFilteredRecords(m_tableName, searchFilter);
    }

    if (m_totalRecords < 0) {
        QMessageBox::critical(this, "错误", "无法获取记录总数！");
        return;
    }

    // 计算总页数
    calculateTotalPages();
    m_query = searchFilter;
    // 加载当前页的数据
    loadPage();

    // 更新分页信息标签
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::calculateTotalPages()
{
    if (m_recordsPerPage <= 0) {
        m_totalPages = 1;
    } else {
        m_totalPages = m_totalRecords / m_recordsPerPage;
        if (m_totalRecords % m_recordsPerPage != 0) {
            m_totalPages += 1;
        }
    }

    if (m_totalPages == 0) {
        m_totalPages = 1;
    }

    // 确保当前页不超过总页数
    if (m_currentPage > m_totalPages) {
        m_currentPage = m_totalPages;
    }
}

void TableWidget::loadPage()
{
    if(!m_dbModule->database().isOpen()){
        reload();
    }
    int offset = (m_currentPage - 1) * m_recordsPerPage;
    QString sql = QString("SELECT id, name, age FROM %1 ")
                      .arg(m_tableName);

    if (!m_query.isEmpty()) {
        sql += m_query + " ";
    }

    sql += QString("LIMIT %1 OFFSET %2")
               .arg(m_recordsPerPage)
               .arg(offset);

    m_model->setQuery(sql, m_dbModule->database());

    if (m_model->lastError().isValid()) {
        qDebug() << "Failed to execute query:" << m_model->lastError().text();
        QMessageBox::warning(this, QStringLiteral("错误"), QStringLiteral("无法加载数据！"));
        return;
    }

    // 动态设置表头（根据实际字段名称）
    int columnCount = m_model->columnCount();
    for (int col = 0; col < columnCount; ++col) {
        QString fieldName = m_model->headerData(col, Qt::Horizontal).toString();
        // 根据字段名称设置中文表头
        if (fieldName == "id") {
            m_model->setHeaderData(col, Qt::Horizontal, QStringLiteral("ID"));
        } else if (fieldName == "name") {
            m_model->setHeaderData(col, Qt::Horizontal, QStringLiteral("姓名"));
        } else if (fieldName == "age") {
            m_model->setHeaderData(col, Qt::Horizontal, QStringLiteral("年龄"));
        }
        // 根据需要添加更多字段的映射
    }
    m_tableView->setModel(m_model);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    m_tableView->resizeColumnsToContents();
    m_tableView->horizontalHeader()->setStretchLastSection(true);
}

void TableWidget::onDataChanged()
{
    // 更新总记录数和总页数
    m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    calculateTotalPages();

    // 如果当前页超过总页数，调整到最后一页
    if (m_currentPage > m_totalPages) {
        m_currentPage = m_totalPages;
    }

    // 重新加载当前页的数据
    loadPage();

    // 更新分页信息标签
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::firstPage()
{
    if (m_currentPage == 1)
        return;

    m_currentPage = 1;
    loadPage();
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(false);
    m_prevPageButton->setEnabled(false);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::prevPage()
{
    if (m_currentPage <= 1)
        return;

    m_currentPage--;
    loadPage();
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::nextPage()
{
    if (m_currentPage >= m_totalPages)
        return;

    m_currentPage++;
    loadPage();
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::lastPage()
{
    if (m_currentPage == m_totalPages)
        return;

    m_currentPage = m_totalPages;
    loadPage();
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(false);
    m_lastPageButton->setEnabled(false);
}

void TableWidget::addRecord()
{
    QString name = m_nameEdit->text().trimmed();
    QString ageStr = m_ageEdit->text().trimmed();

    if (name.isEmpty() || ageStr.isEmpty()) {
        QMessageBox::warning(this, QStringLiteral("警告"), QStringLiteral("请输入姓名和年龄！"));
        return;
    }

    bool ok;
    int age = ageStr.toInt(&ok);
    if (!ok) {
        QMessageBox::warning(this, QStringLiteral("警告"), QStringLiteral("年龄必须是数字！"));
        return;
    }

    QVariantList data;
    data << name << age;
    if (!m_dbModule->insertData(m_tableName, data)) {
        QMessageBox::warning(this, QStringLiteral("错误"), QStringLiteral("插入数据失败！"));
        return;
    }

    // 清空输入框
    m_nameEdit->clear();
    m_ageEdit->clear();
}

void TableWidget::deleteRecord()
{
    QItemSelectionModel *selectModel = m_tableView->selectionModel();
    if (!selectModel->hasSelection()) {
        QMessageBox::warning(this, QStringLiteral("警告"), QStringLiteral("请选择要删除的记录！"));
        return;
    }

    QModelIndexList selectedRows = selectModel->selectedRows();
    if (selectedRows.isEmpty()) {
        QMessageBox::warning(this, QStringLiteral("警告"), QStringLiteral("请选择要删除的记录！"));
        return;
    }

    // 假设只删除第一条选中的记录
    QModelIndex index = selectedRows.first();
    int row = index.row();
    QModelIndex idIndex = m_model->index(row, 0); // ID 列
    QVariant id = m_model->data(idIndex);

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "确认", "确定要删除选中的记录吗？",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        if (!m_dbModule->deleteData(m_tableName, id)) {
            QMessageBox::warning(this, "错误", "删除数据失败！");
            return;
        }
    }
}

void TableWidget::refreshData()
{
    m_model->clear();
    m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    calculateTotalPages();
    loadPage();
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::bulkInsertRecords()
{
    // 确认是否插入一万条数据
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, QStringLiteral("确认"),
                                  QStringLiteral("确定要插入一万条测试数据吗？"),
                                  QMessageBox::Yes | QMessageBox::No);
    if (reply != QMessageBox::Yes) {
        return;
    }

    // 生成一万条数据
    QList<QVariantList> bulkData;
    bulkData.reserve(10000);

    for (int i = 1; i <= 10000; ++i) {
        QVariantList record;
        record << QString(QStringLiteral("测试姓名%1")).arg(i) << (20 + (i % 50)); // 姓名和年龄
        bulkData.append(record);
    }

    // 显示进度对话框
    QProgressDialog progress(QStringLiteral("正在插入一万条数据..."), QStringLiteral("取消"), 0, bulkData.size(), this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(0);
    progress.show();

    // 开始批量插入
    if (m_dbModule->bulkInsertData(m_tableName, bulkData)) {
        QMessageBox::information(this, QStringLiteral("成功"), QStringLiteral("一万条数据已成功插入！"));
    } else {
        QMessageBox::warning(this, QStringLiteral("失败"),QStringLiteral( "批量插入数据失败！"));
    }

    progress.setValue(bulkData.size());

    // 更新总记录数和总页数
    m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    calculateTotalPages();

    // 刷新当前页数据
    loadPage();
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}

void TableWidget::reload()
{
    // 初始化数据库连接
    if (!m_dbModule->initDatabase(m_dbName)) {
        QMessageBox::critical(this, "错误", "无法初始化数据库连接！");
        return;
    }
    // 获取总记录数
    m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    if (m_totalRecords < 0) {
        QMessageBox::critical(this, "错误", "无法获取记录总数！");
        return;
    }

    // 计算总页数
    calculateTotalPages();
    // 更新分页信息标签
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}
void TableWidget::searchRecords()
{
    QString keyword = m_searchEdit->text().trimmed();
    m_searchKeyword = keyword;

    // 重置到第一页
    m_currentPage = 1;

    // 获取总记录数（根据搜索关键词）
    QString searchFilter = QString("WHERE name LIKE '%%1%'").arg(m_searchKeyword);
    if (m_searchKeyword.isEmpty()) {
        m_totalRecords = m_dbModule->getTotalRecords(m_tableName);
    } else {
        m_totalRecords = m_dbModule->getFilteredRecords(m_tableName, searchFilter);
    }

    if (m_totalRecords < 0) {
        QMessageBox::critical(this, "错误", "无法获取记录总数！");
        return;
    }

    // 计算总页数
    calculateTotalPages();
    m_query = searchFilter;
    // 加载当前页的数据
    loadPage();

    // 更新分页信息标签
    m_pageInfoLabel->setText(QStringLiteral("第 %1 页 / 共 %2 页").arg(m_currentPage).arg(m_totalPages));

    // 更新分页按钮状态
    m_firstPageButton->setEnabled(m_currentPage > 1);
    m_prevPageButton->setEnabled(m_currentPage > 1);
    m_nextPageButton->setEnabled(m_currentPage < m_totalPages);
    m_lastPageButton->setEnabled(m_currentPage < m_totalPages);
}
