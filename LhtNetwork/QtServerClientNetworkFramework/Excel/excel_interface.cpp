﻿#include "excel_interface.h"
#include "../Sql/sqlite_module.h" // 确保路径正确
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QSqlError>
#include "xlsxdocument.h"
#include "xlsxworksheet.h"
#include "xlsxcell.h"
#include "xlsxcellrange.h"
#include "../LhtLog/QsLog/QsLog.h"

using namespace QXlsx;

ExcelInterface::ExcelInterface(QObject *parent)
    : QObject{parent}
{}

ExcelInterface::~ExcelInterface()
{
}

QString ExcelInterface::importExcelToDatabase(const QString &excelFilePath, const QString &tableName) {
    // 初始化数据库
    SQLiteModule dbModule;
    if (!dbModule.initDatabase(QStringLiteral("test.db"))) {
        QLOG_ERROR() << QStringLiteral("数据库初始化失败！");
        return QStringLiteral("数据库初始化失败！");
    }

    // 打开Excel文件
    Document xlsx(excelFilePath);
    if (!xlsx.load()) {
        QLOG_ERROR() << QStringLiteral("无法加载Excel文件:") << excelFilePath;
        return QStringLiteral("无法加载Excel文件:") + excelFilePath;
    }

    // 获取第一个工作表
    Worksheet *sheet = xlsx.currentWorksheet();
    if (!sheet) {
        QLOG_ERROR() << QStringLiteral("无法获取工作表。");
        return QStringLiteral("无法获取工作表。");
    }

    // 获取行和列的范围
    int rowCount = sheet->dimension().lastRow();
    int colCount = sheet->dimension().lastColumn();

    if (rowCount < 1 || colCount < 1) {
        QLOG_ERROR() << QStringLiteral("Excel文件为空或格式不正确。");
        return QStringLiteral("Excel文件为空或格式不正确。");
    }

    // 读取标题行
    QStringList headers;
    for (int col = 1; col <= colCount; ++col) {
        QVariant cell = sheet->cellAt(1, col)->value();
        headers << cell.toString();
    }

    // 开始数据库事务
    QSqlDatabase db = dbModule.database();
    if (!db.isOpen()) {
        QLOG_ERROR() << QStringLiteral("数据库未打开。");
        return QStringLiteral("数据库未打开。");
    }

    QSqlQuery query(db);
    if (!db.transaction()) {
        QLOG_ERROR() << QStringLiteral("无法启动事务:") << db.lastError().text();
        return QStringLiteral("无法启动事务:") + db.lastError().text();
    }

    // 准备插入语句
    QStringList placeholders;
    for (int i = 0; i < headers.size(); ++i) {
        placeholders << "?";
    }

    QString sql = QStringLiteral("INSERT INTO %1 (%2) VALUES (%3)")
                      .arg(tableName)
                      .arg(headers.join(", "))
                      .arg(placeholders.join(", "));

    if (!query.prepare(sql)) {
        QLOG_ERROR() << QStringLiteral("无法准备查询:") << query.lastError().text();
        db.rollback();
        return QStringLiteral("无法准备查询:") + query.lastError().text();
    }

    // 遍历每一行数据（从第二行开始）
    for (int row = 2; row <= rowCount; ++row) {
        QList<QVariant> rowData;
        for (int col = 1; col <= colCount; ++col) {
            QVariant cell = sheet->cellAt(row, col)->value();
            rowData << cell;
        }

        // 绑定值
        for (int i = 0; i < rowData.size(); ++i) {
            query.bindValue(i, rowData[i]);
        }

        if (!query.exec()) {
            QLOG_ERROR() << QStringLiteral("插入数据失败，行:") << row << QStringLiteral("错误:") << query.lastError().text();
            db.rollback();
            return QStringLiteral("插入数据失败，行:") + row + QStringLiteral("错误:") + query.lastError().text();
        }
    }

    // 提交事务
    if (!db.commit()) {
        QLOG_ERROR() << QStringLiteral("无法提交事务:") << db.lastError().text();
        db.rollback();
        return QStringLiteral("无法提交事务:") + db.lastError().text();
    }

    QLOG_INFO() << QStringLiteral("Excel数据成功导入到数据库表:") << tableName;
    emit excelImported(true);
    return QStringLiteral("Excel数据成功导入到数据库表:") + tableName + QStringLiteral(" 共:") + QString::number(rowCount) + QStringLiteral("用户");
}
