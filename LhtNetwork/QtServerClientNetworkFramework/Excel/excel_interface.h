#ifndef EXCEL_INTERFACE_H
#define EXCEL_INTERFACE_H

#include <QObject>
#include <QString>

namespace QXlsx {
class Document;  // 前置声明
}

class ExcelInterface : public QObject
{
    Q_OBJECT
public:
    explicit ExcelInterface(QObject *parent = nullptr);
    ~ExcelInterface();

    /**
     * @brief 将指定路径的 Excel 文件读取并存储到数据库
     * @param excelFilePath Excel 文件的绝对路径
     * @param tableName 数据库中的表名
     * @return 操作是否成功
     */
    QString importExcelToDatabase(const QString &excelFilePath, const QString &tableName);

private:


signals:
    /**
     * @brief 当读取Excel数据并写入数据库完成后发出信号
     * @param success 是否成功
     */
    void excelImported(bool success);
};

#endif // EXCEL_INTERFACE_H
