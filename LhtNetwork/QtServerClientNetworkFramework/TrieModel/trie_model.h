﻿#ifndef TRIE_MODEL_H
#define TRIE_MODEL_H

#include <QObject>
#include <QLineEdit>
#include <QCompleter>
#include <QStringList>
#include <QAbstractListModel>
#include <QMap>
#include <QChar>

// TrieNode 结构体
struct TrieNode {
    QMap<QChar, TrieNode*> children;
    bool isEndOfNumber;

    TrieNode() : isEndOfNumber(false) {}
    ~TrieNode() {
        qDeleteAll(children);
    }
};

// Trie 类
class Trie {
public:
    Trie() {
        root = new TrieNode();
    }

    ~Trie() {
        delete root;
    }

    // 插入电话号码到Trie
    void insert(const QString &number) {
        TrieNode *node = root;
        for (const QChar &ch : number) {
            if (!node->children.contains(ch)) {
                node->children[ch] = new TrieNode();
            }
            node = node->children[ch];
        }
        node->isEndOfNumber = true;
    }

    // 根据前缀搜索电话号码，最多返回maxSuggestions个
    QStringList searchByPrefix(const QString &prefix, int maxSuggestions = 100) const {
        QStringList results;
        TrieNode *node = root;
        for (const QChar &ch : prefix) {
            if (!node->children.contains(ch)) {
                return results; // 前缀不存在
            }
            node = node->children[ch];
        }
        collectAllNumbers(node, prefix, results, maxSuggestions);
        return results;
    }

private:
    TrieNode *root;

    // 递归收集所有以当前节点为起点的电话号码
    void collectAllNumbers(TrieNode *node, QString current, QStringList &results, int maxSuggestions) const {
        if (results.size() >= maxSuggestions) {
            return;
        }
        if (node->isEndOfNumber) {
            results.append(current);
        }
        for (auto it = node->children.constBegin(); it != node->children.constEnd(); ++it) {
            collectAllNumbers(it.value(), current + it.key(), results, maxSuggestions);
            if (results.size() >= maxSuggestions) {
                return;
            }
        }
    }
};

// 自定义的TrieModel，继承自QAbstractListModel
class TrieModel : public QAbstractListModel {
    Q_OBJECT
public:
    TrieModel(Trie *trie, int maxSuggestions = 100, QObject *parent = nullptr)
        : QAbstractListModel(parent), trie(trie), maxSuggestions(maxSuggestions) {}

    // 返回行数，即当前过滤后的电话号码数量
    int rowCount(const QModelIndex &parent = QModelIndex()) const override {
        Q_UNUSED(parent);
        return suggestions.size();
    }

    // 返回每行的数据，即电话号码
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override {
        if (role == Qt::DisplayRole && index.isValid() && index.row() < suggestions.size()) {
            // qDebug() << "data() called for row:" << index.row() << "value:" << suggestions.at(index.row());
            return suggestions.at(index.row());
        }
        return QVariant();
    }

    // 定义每个项的标志
    Qt::ItemFlags flags(const QModelIndex &index) const override {
        if (!index.isValid())
            return Qt::NoItemFlags;
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

public slots:
    // 根据前缀设置过滤后的电话号码列表
    void setFilter(const QString &prefix) {
        // qDebug() << "setFilter called with prefix:" << prefix;

        beginResetModel();
        suggestions = trie->searchByPrefix(prefix, maxSuggestions);
        // qDebug() << "Found" << suggestions.size() << "matches for prefix" << prefix;
        endResetModel();
    }

private:
    Trie *trie;
    QStringList suggestions;
    int maxSuggestions;
};


#endif // TRIE_MODEL_H
