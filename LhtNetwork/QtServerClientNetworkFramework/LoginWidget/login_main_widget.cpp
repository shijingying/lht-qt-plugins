#include "login_main_widget.h"
#include "ui_login_main_widget.h"
#include "Global/global_center.h"
#include <QMessageBox>

LoginMainWidget::LoginMainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::LoginMainWidget)
{
    ui->setupUi(this);
}

LoginMainWidget::~LoginMainWidget()
{
    delete ui;
}

void LoginMainWidget::on_login_btn_clicked()
{
    QString user = ui->user_line->text().trimmed();
    QString pwd = ui->pwd_line->text().trimmed();

    if(user.isEmpty()){
        QMessageBox::warning(nullptr,"提示","请输入账号");
        return;
    }
    if(pwd.isEmpty()){
        QMessageBox::warning(nullptr,"提示","请输入密码");
        return;
    }
    //登录一下
    GlobalCenter::getInstance()->login(user,pwd);
}

