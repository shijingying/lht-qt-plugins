#ifndef LOGIN_MAIN_WIDGET_H
#define LOGIN_MAIN_WIDGET_H

#include <QWidget>

namespace Ui {
class LoginMainWidget;
}

class LoginMainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LoginMainWidget(QWidget *parent = nullptr);
    ~LoginMainWidget();

private slots:
    void on_login_btn_clicked();

private:
    Ui::LoginMainWidget *ui;
};

#endif // LOGIN_MAIN_WIDGET_H
