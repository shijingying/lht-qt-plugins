#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../LhtLog/QsLog/QsLog.h"
#include "../Table/table_widget.h"
#include "../Network/tcp_server.h"
#include "User/edit_user_dialog.h"
using namespace QsLogging;
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void initTrieModel();

    void testSql();

    void testTable();

    void testTcpServer();
protected slots:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void slotReadyRead(QString ,NewTcpProtocolPtr);
private slots:
    void on_add_new_employee_btn_clicked();

    void on_import_btn_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::MainWindow *ui;
    TableWidget *m_tableWidget;
    SQLiteModule m_dbModule;
    TcpServer * m_tcpServer;
    EditUserDialog m_editUserDialog;
    void initQsLog(QsLogging::Level level, const QString &logPath, long long maxSizeBytes, int maxOldLogCount);
    void loadStyle(const QString &qssFile);
};
#endif // MAINWINDOW_H
