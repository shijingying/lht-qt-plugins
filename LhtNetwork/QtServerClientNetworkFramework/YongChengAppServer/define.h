﻿#ifndef DEFINE_H
#define DEFINE_H

#include <chrono>
#include <QDebug>
#include <QRandomGenerator>

#define LHT_TIME_CONSUMING_CAT2(x, y) x##y
#define LHT_TIME_CONSUMING_CAT(x, y) LHT_TIME_CONSUMING_CAT2(x, y)
#define LHT_TIME_CONSUMING static const auto LHT_TIME_CONSUMING_CAT(lhtTimeConsuming_, __COUNTER__) = LhtTimeConsuming(__func__, __LINE__)
//耗时计算
class LhtTimeConsuming {
public:
    LhtTimeConsuming(const char* func, int line)
        : func_name(func), line_number(line) {
        start_time = std::chrono::high_resolution_clock::now();
    }

    ~LhtTimeConsuming() {
        // 记录结束时间
        auto end_time = std::chrono::high_resolution_clock::now();

        // 计算耗时，并转换为毫秒
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        // 打印耗时（单位：毫秒）
        // std::cout << "Time consuming in function: " << func_name << " at line " << line_number
        //           << " is " << duration.count() << " ms."<<std::endl;
        qDebug() << "Time consuming in function: " << func_name << " at line " << line_number
                 << " is " << duration.count() << " ms.";
    }

private:
    const char* func_name;
    int line_number;
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
};

// 函数：生成随机电话号码
inline QStringList generateRandomPhoneNumbers(int count) {
    QStringList prefixes = {"1866", "1234", "1987", "1555", "1777", "1888", "1666", "1899", "1800"};
    QStringList phoneNumbers;
    phoneNumbers.reserve(count); // 预留空间，提升性能

    for(int i = 0; i < count; ++i) {
        // 随机选择一个前缀
        int prefixIndex = QRandomGenerator::global()->bounded(prefixes.size());
        QString prefix = prefixes.at(prefixIndex);

        // 生成后缀，假设后缀为7位数字
        QString suffix;
        for(int j = 0; j < 7; ++j) {
            int digit = QRandomGenerator::global()->bounded(10);
            suffix.append(QString::number(digit));
        }

        phoneNumbers.append(prefix + suffix);
    }

    return phoneNumbers;
}

#endif // DEFINE_H
