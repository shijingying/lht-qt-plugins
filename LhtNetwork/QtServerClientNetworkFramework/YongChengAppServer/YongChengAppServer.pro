QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

include($$PWD/../LhtLog/LhtLog.pri);
include($$PWD/../Network/Network.pri);
include($$PWD/../Sql/Sql.pri);
include($$PWD/../Table/Table.pri);
include($$PWD/../Excel/Excel.pri);

QMAKE_CXXFLAGS += /utf-8
QMAKE_LFLAGS += /utf-8

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    User/edit_user_dialog.cpp \
    User/list_widget.cpp \
    User/user_info_widget.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../TrieModel/trie_model.h \
    User/edit_user_dialog.h \
    User/list_widget.h \
    User/user_info_widget.h \
    define.h \
    mainwindow.h

FORMS += \
    User/editu_ser_dialog.ui \
    User/user_info_widget.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ../qrc/font.qrc \
    ../qrc/main.qrc \
    ../qrc/qm.qrc \
    ../qrc/qss.qrc
