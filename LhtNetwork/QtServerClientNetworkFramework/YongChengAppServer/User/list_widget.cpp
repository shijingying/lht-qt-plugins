#include "list_widget.h"

#include <QContextMenuEvent>
#include <QMenu>

ListWidget::ListWidget(QWidget *parent)
{
    m_sql.initDatabase(DBName);
    connect(this, &QListWidget::itemClicked,
            this, &ListWidget::onItemClicked);

    connect(this, &QListWidget::itemDoubleClicked,
            this, &ListWidget::onItemDoubleClicked);
    init();
}

void ListWidget::init()
{
    this->clear();
    QList<QVariantList> allUsers = m_sql.getUserDetails();
    if (allUsers.isEmpty()) {
        qDebug() << "没有查询到任何用户信息或查询失败";
        return;
    }
    // 遍历每个用户
    for (const QVariantList &user : allUsers) {
        int    id           = user.at(0).toInt();
        QString username    = user.at(1).toString();
        QString loginName   = user.at(2).toString();
        QString password    = user.at(3).toString();
        QString email       = user.at(4).toString();
        QString phone       = user.at(5).toString();
        int    isActive     = user.at(6).toInt();
        QListWidgetItem * item = new QListWidgetItem(username);
        item->setToolTip(QString::number(id));
        item->setTextAlignment(Qt::AlignCenter);
        this->addItem(item);
    }

}
// 单击槽函数
void ListWidget::onItemClicked(QListWidgetItem *item)
{
    if (!item) return;
    signItemClicked(item->text(),item->toolTip().toInt());
}

// 双击槽函数
void ListWidget::onItemDoubleClicked(QListWidgetItem *item)
{
    // if (!item) return;
    // signItemDoubleClicked(item->toolTip().toInt());
}

void ListWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QListWidgetItem *clickedItem = itemAt(event->pos());
    if (!clickedItem) {
        return;
    }
    // 2. 构造一个菜单
    QMenu menu(this);
    QAction *editAction = menu.addAction(tr("编辑"));

    connect(editAction, &QAction::triggered, this, [this, clickedItem]() {
        emit signItemDoubleClicked(clickedItem->toolTip().toInt());
    });

    menu.exec(event->globalPos());
}
