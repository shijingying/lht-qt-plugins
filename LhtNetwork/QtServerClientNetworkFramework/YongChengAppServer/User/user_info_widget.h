#ifndef USER_INFO_WIDGET_H
#define USER_INFO_WIDGET_H

#include <QWidget>
#include "../../Table/table_widget.h"
namespace Ui {
class UserInfoWidget;
}

class UserInfoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UserInfoWidget(QWidget *parent = nullptr);
    ~UserInfoWidget();

    void initTable(QString name,int id);
private:
    Ui::UserInfoWidget *ui;
    int m_userId;
    TableWidget * m_tableWidget;
};

#endif // USER_INFO_WIDGET_H
