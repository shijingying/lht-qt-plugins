#ifndef LIST_WIDGET_H
#define LIST_WIDGET_H

#include <QListWidget>
#include "../../Sql/sqlite_module.h"

class ListWidget : public QListWidget
{
    Q_OBJECT

public:
    explicit ListWidget(QWidget *parent = nullptr);
    void init();

signals:
    void signItemClicked(QString name,int id);
    void signItemDoubleClicked(int id);
protected slots:
protected:
    // 重写右键菜单事件
    void contextMenuEvent(QContextMenuEvent *event) override;
private slots:
    void onItemClicked(QListWidgetItem *item);
    void onItemDoubleClicked(QListWidgetItem *item);

private:
    SQLiteModule m_sql;
};

#endif // LIST_WIDGET_H
