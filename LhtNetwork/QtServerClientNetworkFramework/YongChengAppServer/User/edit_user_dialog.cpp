#include "edit_user_dialog.h"
#include "ui_editu_ser_dialog.h"
#include <QDebug>
#include <QSqlQuery>
#include <QCryptographicHash>
EditUserDialog::EditUserDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::EditUserDialog)
{
    ui->setupUi(this);
    m_sql.initDatabase(DBName);
}

EditUserDialog::~EditUserDialog()
{
    delete ui;
}

void EditUserDialog::initUi()
{
    if (m_updateUser && userId >= 0) {
        setWindowTitle(tr("更新用户"));
        ui->login_name_line->setReadOnly(true);
        ui->name_line->setReadOnly(true);
        QVariantList userDetails = m_sql.getUserDetailsById(userId);
        if (userDetails.isEmpty()) {
            QLOG_ERROR() << "未查询到对应的用户记录或查询失败, userId=" << userId;
            return;
        }
        QString username     = userDetails.at(0).toString();
        QString loginName    = userDetails.at(1).toString();
        QString passwordHash = userDetails.at(2).toString();
        QString email        = userDetails.at(3).toString();
        QString phone        = userDetails.at(4).toString();
        int isActive         = userDetails.at(5).toInt();

        // 然后将这些数据填充到 UI 控件
        ui->name_line->setText(username);
        ui->login_name_line->setText(loginName);
        ui->pwd_line->setText(passwordHash);
        ui->email_line->setText(email);
        ui->phone1_line->setText(phone);
        ui->active_btn->setChecked(isActive == 1);
    }
    else {
        setWindowTitle(tr("新增用户"));
        ui->login_name_line->setReadOnly(false);
        ui->name_line->setReadOnly(false);
        // 如果是新增用户，可以在这里初始化为空或默认值
    }
}

void EditUserDialog::accept()
{
    // 1. 从界面控件获取用户输入
    QString username = ui->name_line->text().trimmed();       // 用户名
    QString loginName = ui->login_name_line->text().trimmed(); // 登录名
    QString email = ui->email_line->text().trimmed();         // 邮箱
    QString phone = ui->phone1_line->text().trimmed();        // 电话
    int isActive = ui->active_btn->isChecked() ? 1 : 0;       // 是否启用
    QString passwordPlain = ui->pwd_line->text().trimmed(); // 用户明文密码
    // 计算 MD5 哈希，并转为十六进制字符串
    QString passwordHash = QCryptographicHash::hash(passwordPlain.toUtf8(),
                                                    QCryptographicHash::Md5).toHex();

    if (m_updateUser && userId >= 0) {
        m_sql.updatePassword(loginName, passwordHash);
        m_sql.setUserActive(loginName, isActive);
        m_sql.updatePhone(loginName, phone);
        QLOG_INFO() << "用户(id=" << userId << ")更新成功!";
        QDialog::accept();
    } else {
        bool success = m_sql.createUser(username, loginName, passwordHash, email, phone, isActive);
        if (!success) {
            QLOG_ERROR() << "新建用户失败, username=" << username;
            return;
        }
        QLOG_INFO() << "新建用户成功, username=" << username;
        // QDialog::accept();
        hide();
    }
}

void EditUserDialog::reject()
{
    hide();
}
