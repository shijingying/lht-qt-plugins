#include "user_info_widget.h"
#include "ui_user_info_widget.h"

UserInfoWidget::UserInfoWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::UserInfoWidget)
    ,m_tableWidget(new TableWidget)
{
    ui->setupUi(this);
    // 设置 TableWidget 为中央组件
    ui->verticalLayout->insertWidget(2,m_tableWidget);
    // 初始化数据库并加载数据
    m_tableWidget->initialize("test.db");
}

UserInfoWidget::~UserInfoWidget()
{
    delete ui;
}

void UserInfoWidget::initTable(QString name,int id)
{
    ui->user_name->setText(name);
    m_tableWidget->updateEmployeeId(id);
}
