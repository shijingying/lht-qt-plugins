#ifndef EDIT_USER_DIALOG_H
#define EDIT_USER_DIALOG_H

#include <QDialog>
#include "../../Sql/sqlite_module.h"
namespace Ui {
class EditUserDialog;
}

class EditUserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditUserDialog(QWidget *parent = nullptr);
    ~EditUserDialog();

    void setUpdateUser(int id = -1){
        userId = id;
        m_updateUser = (id == - 1?  0 : 1);
        initUi();
    }
private:
    void initUi();
private:
    Ui::EditUserDialog *ui;
    int userId = -1;
    bool m_updateUser = false;
    SQLiteModule m_sql;

    // QDialog interface
public slots:
    void accept();
    void reject();
};

#endif // EDIT_USER_DIALOG_H
