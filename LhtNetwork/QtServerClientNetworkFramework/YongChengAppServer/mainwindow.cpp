﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDragEnterEvent>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QMimeData>
#include <QFileInfo>
#include <QTranslator>
#include "define.h"
#include "../TrieModel/trie_model.h"
#include "../Sql/sqlite_module.h"
#include "../Excel/excel_interface.h"
#include "../Network/connection_manager.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), m_tableWidget(new TableWidget(this)),m_tcpServer(nullptr)
{
    ui->setupUi(this);
    QTranslator *translator1 = new QTranslator(qApp);
    if (translator1->load(":/qm/qt_zh_CN.qm")) {
        qApp->installTranslator(translator1);
    }
    loadStyle(":/qss/flatgray.css");
    initQsLog(DebugLevel,QCoreApplication::applicationDirPath() + "/YongChengAppLog.txt",1024 * 10 ,10);
    setAcceptDrops(true);
    // testSql();
    connect(ui->listWidget,&ListWidget::signItemClicked,[=](QString name,int id){
        ui->user_info_widget->initTable(name,id);
    });
    connect(ui->listWidget,&ListWidget::signItemDoubleClicked,[=](int id){
        m_editUserDialog.setUpdateUser(id);
        m_editUserDialog.exec();
        ui->listWidget->init();
    });
    testTable();
    testTcpServer();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initTrieModel()
{
    // 创建主窗口
    QWidget *window = new QWidget(this);
    QVBoxLayout *layout = new QVBoxLayout(window);

    // 创建LineEdit
    QLineEdit *lineEdit = new QLineEdit();
    lineEdit->setPlaceholderText("请输入电话号码前缀...");
    layout->addWidget(lineEdit);

    // 生成100,000个随机电话号码
    QStringList phoneNumbers = generateRandomPhoneNumbers(100000);

    // 构建Trie
    Trie *trie = new Trie;
    for(const QString &number : phoneNumbers) {
        trie->insert(number);
    }

    // 创建自定义模型
    TrieModel *model = new TrieModel(trie, 100, window);

    // 设置QCompleter
    QCompleter *completer = new QCompleter(model, window);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setFilterMode(Qt::MatchStartsWith);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    completer->setCompletionRole(Qt::DisplayRole);
    lineEdit->setCompleter(completer);

    // 连接输入信号以更新过滤
    QObject::connect(lineEdit, &QLineEdit::textChanged, model, &TrieModel::setFilter);

    // 设置布局和窗口属性
    window->setLayout(layout);
    window->setWindowTitle("电话号码提示搜索");
    window->resize(400, 100);
    window->show();
}

void MainWindow::testSql()
{
    SQLiteModule dbModule;
    if (!dbModule.initDatabase("test.db")) {
        qDebug() << "数据库初始化失败！";
        return ;
    }

    // 创建表
    if (!dbModule.createTable()) {
        qDebug() << "创建表失败！";
        return ;
    }
}

void MainWindow::testTable()
{
    // 设置 TableWidget 为中央组件
    ui->verticalLayout_3->addWidget(m_tableWidget);
    // 初始化数据库并加载数据
    m_tableWidget->initialize("test.db");
}

void MainWindow::testTcpServer()
{
    if(m_tcpServer == nullptr){
        m_tcpServer = new TcpServer;
    }
    m_tcpServer->startListen("127.0.0.1","6666");
    connect(ConnectionManager::getInstance(),&ConnectionManager::signReadyRead,this,&MainWindow::slotReadyRead);
}

//------------------------------------------------QsLog测试--------------------------------------------------------//
void logFunction(const QString& message, QsLogging::Level level)
{

}

void MainWindow::initQsLog(QsLogging::Level level, const QString &logPath, long long maxSizeBytes, int maxOldLogCount)
{
    using namespace QsLogging;
    // 1. init the logging mechanism
    Logger& logger = Logger::instance();
    logger.setLoggingLevel(level);
    const QString sLogPath(logPath);

    // 2. add two destinations
    DestinationPtr fileDestination(DestinationFactory::MakeFileDestination(
        sLogPath, EnableLogRotation, MaxSizeBytes(maxSizeBytes), MaxOldLogCount(maxOldLogCount)));
    DestinationPtr debugDestination(DestinationFactory::MakeDebugOutputDestination());
    DestinationPtr functorDestination(DestinationFactory::MakeFunctorDestination(&logFunction));//日志执行后函数             两种添加方法 第一种外部函数
    //    DestinationPtr functorDestination(DestinationFactory::MakeFunctorDestination(this,SLOT(qsLogFunc(const QString& , QsLogging::Level ))));//日志执行后函数       两种添加方法 第二种类函数,不能是线程类
    logger.addDestination(debugDestination);
    logger.addDestination(fileDestination);
    logger.addDestination(functorDestination);
}

/* dragEnterEvent 过滤无用Event */
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasUrls())
        event->acceptProposedAction();
}

/* dropEvent 过滤无用urls，并将有效部分交给文件读取方式加载 */
void MainWindow::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls = event->mimeData()->urls();
    QStringList files;
    QString suffixs = "xlsx";

    for( QUrl url : urls ){
        QFileInfo file(url.toLocalFile());	//toLocalFile可以获取文件路径，而非QUrl的file://开头的路径

        if( file.isFile() && suffixs.contains(file.suffix())) //过滤掉目录和不支持的后缀，如果要支持目录，则要自己遍历每一个目录。
            files.append(file.filePath());
    }

    if(!files.isEmpty())
        ui->excel_path_line->setText(files.at(0)); //将文件交给文件打开逻辑进行后续的处理
}

void MainWindow::slotReadyRead(QString name , NewTcpProtocolPtr info)
{
    QString str = QString::fromUtf8(info->content);
    ui->statusbar->showMessage(name + ":"+str, 5000);
}

void MainWindow::on_add_new_employee_btn_clicked()
{
    m_editUserDialog.setUpdateUser();
    m_editUserDialog.exec();
    ui->listWidget->init();
}

void MainWindow::on_import_btn_clicked()
{
    ExcelInterface excelInterface;
    auto path = ui->excel_path_line->text();
    if(!path.isEmpty()){
        auto str = excelInterface.importExcelToDatabase(path,"customers"); //将文件交给文件打开逻辑进行后续的处理
        ui->import_status->setText(str);
    }

}


void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::loadStyle(const QString &qssFile)
{
    //加载样式表
    QString qss;
    QFile file(qssFile);
    if (file.open(QFile::ReadOnly)) {
        //用QTextStream读取样式文件不用区分文件编码 带bom也行
        QStringList list;
        QTextStream in(&file);
        //in.setCodec("utf-8");
        while (!in.atEnd()) {
            QString line;
            in >> line;
            list << line;
        }

        file.close();
        qss = list.join("\n");
        QString paletteColor = qss.mid(20, 7);
        qApp->setPalette(QPalette(paletteColor));
        //用时主要在下面这句
        qApp->setStyleSheet(qss);
    }

}

