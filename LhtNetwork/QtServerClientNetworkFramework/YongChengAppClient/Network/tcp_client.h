#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <QHostAddress>
#include <QTcpSocket>
#include <QObject>
#include <QMutex>
#include "Handle/client_parse_handles.h"
class TcpClient : public QObject
{
    Q_OBJECT
public:
    TcpClient(QHostAddress,int port = 6666);
    ~TcpClient();
    void reconnectToServer();
    void sendTest(QString str = "");
    void write2Tcp(QByteArray arr);
signals:
    void connected();
    void disconnected();
    void dataReceived(const QByteArray &data);
    void errorOccurred(const QString &errorString);
private slots:
    void onConnected();
    void onDisconnected();
    void onReadyRead();
    void onError(QAbstractSocket::SocketError socketError);
    void parseData();
private:
    QTcpSocket * m_socket;

    QMutex      m_mutex;

    QHostAddress    m_ip;

    int             m_port;

    QByteArray  m_buffer;

    ClientParseHandles m_clientParseHandles;
};

#endif // TCP_CLIENT_H
