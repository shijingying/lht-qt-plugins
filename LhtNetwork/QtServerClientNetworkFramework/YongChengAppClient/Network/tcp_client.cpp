#include "tcp_client.h"

#include <QTimer>
#include "../../Network/protocol_define.h"
#include "../LhtLog/QsLog/QsLog.h"
const int ReconnectTime = 5000;

TcpClient::TcpClient(QHostAddress ip, int port):
    m_socket(new QTcpSocket),m_ip(ip),m_port(port)
{
    // 连接 QTcpSocket 的信号到 TcpClient 的槽
    connect(m_socket, &QTcpSocket::connected, this, &TcpClient::onConnected);
    connect(m_socket, &QTcpSocket::disconnected, this, &TcpClient::onDisconnected);
    connect(m_socket, &QTcpSocket::readyRead, this, &TcpClient::onReadyRead);
    connect(m_socket, QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error),
            this, &TcpClient::onError);

    reconnectToServer();
}

TcpClient::~TcpClient()
{
    if(m_socket->isOpen())
        m_socket->close();
    delete m_socket;
}

void TcpClient::reconnectToServer()
{
    if (m_socket->state() != QAbstractSocket::ConnectedState &&
        m_socket->state() != QAbstractSocket::ConnectingState) {
        qDebug() << "Attempting to connect to server at" << m_ip.toString() << ":" << m_port;
        m_socket->connectToHost(m_ip, m_port);
    }
}

void TcpClient::sendTest(QString content)
{
    if(m_socket->state() == QAbstractSocket::ConnectedState){
        NewTcpProtocolPtr data(new NewTcpProtocol);
        data->header.cmdId = CmdLogin;
        data->header.clientType = 0;
        data->header.orderId = 3;
        QString str = QStringLiteral("leehuitao");
        data->name = str.toUtf8();
        QString pwd = QStringLiteral("123");
        data->pwd = pwd.toUtf8();
        data->content = content.toUtf8();
        bool ret;
        write2Tcp(data->convertToByteArray(ret));

    }
}

void TcpClient::write2Tcp(QByteArray arr)
{
    QMutexLocker lock(&m_mutex);
    m_socket->write(arr);
    m_socket->waitForBytesWritten();
}

void TcpClient::onConnected()
{
    qDebug() << "Connected to server.";
    emit connected();
}

void TcpClient::onDisconnected()
{
    qDebug() << "Disconnected from server.";
    emit disconnected();

    // 可选：尝试重连
    // QTimer::singleShot(3000, this, &TcpClient::reconnectToServer);
}

void TcpClient::onReadyRead()
{
    parseData();
    // emit dataReceived(data);
}

void TcpClient::onError(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError);
    QString errorMsg = m_socket->errorString();
    qDebug() << "Socket error:" << errorMsg;
    emit errorOccurred(errorMsg);
    QTimer::singleShot(ReconnectTime,[&]{
        reconnectToServer();
    });
}

void TcpClient::parseData()
{
    QByteArray newData = m_socket->readAll();
    if (newData.isEmpty()) {
        return;
    }
    m_buffer.append(newData);

    while (true) {
        const int headerSize = 18; // 14 bytes

        // 检查是否有足够的数据读取头部
        if (m_buffer.size() < headerSize) {
            break; // 等待更多数据
        }

        // 使用 QDataStream 读取头部
        QDataStream stream(m_buffer);
        stream.setByteOrder(QDataStream::LittleEndian); // 根据协议设置字节序

        NewHeader header;
        stream.readRawData(reinterpret_cast<char*>(&header.checkCode1), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.checkCode2), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.cmdId), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.clientType), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.orderId), 2);
        quint32 packSize;
        stream >> packSize;

        // 验证 packSize 合理性
        if (packSize < headerSize || header.checkCode1 != 77 || header.checkCode2 != 88) {
            QLOG_ERROR() << "Invalid packSize:" << packSize;
            m_buffer.clear();
            m_socket->disconnect();
            return;
        }

        // 检查是否有足够的数据读取整个消息
        if (m_buffer.size() < packSize) {
            break; // 等待更多数据
        }

        // 提取完整的消息
        QByteArray message = m_buffer.left(packSize);
        m_buffer.remove(0, packSize);

        // 解析消息
        NewTcpProtocolPtr data = std::make_shared<NewTcpProtocol>(message);

        m_clientParseHandles.parseData(data);


    }
}


