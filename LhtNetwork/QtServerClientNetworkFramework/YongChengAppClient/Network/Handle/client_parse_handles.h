#ifndef CLIENT_PARSE_HANDLES_H
#define CLIENT_PARSE_HANDLES_H

#include <QObject>
#include "../../../Network/protocol_define.h"

class ClientParseHandles : public QObject
{
    Q_OBJECT
public:
    explicit ClientParseHandles(QObject *parent = nullptr);

    bool parseData(NewTcpProtocolPtr data);

private:
    bool _parseLogin(NewTcpProtocolPtr data);
signals:
};

#endif // CLIENT_PARSE_HANDLES_H
