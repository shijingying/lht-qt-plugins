#include "client_parse_handles.h"
#include "../../LhtLog/QsLog/QsLog.h"
#include "../../Global/global_center.h"
#include <QJsonObject>
#include <QJsonParseError>

ClientParseHandles::ClientParseHandles(QObject *parent)
    : QObject{parent}
{}

bool ClientParseHandles::parseData(NewTcpProtocolPtr data)
{
    auto cmd = data->header.cmdId;
    bool ret = true;
    switch (cmd) {
    case CmdLogin:
        _parseLogin(data);
        break;
    default:
        ret = false;
        QLOG_ERROR() << "Unknown cmdId:" << cmd;
        break;
    }

    return ret;
}

bool ClientParseHandles::_parseLogin(NewTcpProtocolPtr data)
{

    // 1. 取出 JSON 数据
    QByteArray jsonData = data->content;
    if (jsonData.isEmpty()) {
        QLOG_ERROR() << "Login content is empty!";
        GlobalCenter::getInstance()->LoginStatus = (data->header.clientType == 1 ? 1 : 0);
        GlobalCenter::getInstance()->signUpdateUser();
        return false;
    }

    // 2. 解析 JSON 文档
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(jsonData, &parseError);
    if (parseError.error != QJsonParseError::NoError) {
        QLOG_ERROR() << "Failed to parse login JSON:" << parseError.errorString();
        return false;
    }

    // 确保这是一个 JSON 对象
    if (!doc.isObject()) {
        QLOG_ERROR() << "Login JSON is not an object!";
        return false;
    }

    // 3. 获取 JSON 对象，并依次取出字段
    QJsonObject obj = doc.object();

    int id                 = obj.value("id").toInt(-1);
    QString username       = obj.value("username").toString();
    QString loginName      = obj.value("loginName").toString();
    QString passwordHash   = obj.value("passwordHash").toString();
    QString email          = obj.value("email").toString();
    QString phone          = obj.value("phone").toString();
    int isActive           = obj.value("isActive").toInt(-1);
    if(data->header.clientType == 1 && !data->name.isEmpty()){
        GlobalCenter::getInstance()->LoginStatus = true;

    }
    GlobalCenter::getInstance()->m_id          = id              ;
    GlobalCenter::getInstance()->m_username    = username        ;
    GlobalCenter::getInstance()->m_loginName   = loginName       ;
    GlobalCenter::getInstance()->m_passwordHash= passwordHash    ;
    GlobalCenter::getInstance()->m_email       = email           ;
    GlobalCenter::getInstance()->m_phone       = phone           ;
    GlobalCenter::getInstance()->m_isActive    = isActive        ;
    GlobalCenter::getInstance()->signUpdateUser();
    return true;
}
