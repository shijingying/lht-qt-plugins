#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Global/global_center.h"

#include <QFile>
#include <QTranslator>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTranslator *translator1 = new QTranslator(qApp);
    if (translator1->load(":/qm/qt_zh_CN.qm")) {
        qApp->installTranslator(translator1);
    }
    loadStyle(":/qss/flatgray.css");
    connect(GlobalCenter::getInstance(),&GlobalCenter::signUpdateUser,this,&MainWindow::slotUpdateUser);
    this->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_test_send_btn_clicked()
{
    GlobalCenter::getInstance()->testLogin();
}

void MainWindow::slotUpdateUser()
{
    QLOG_INFO()<<"success login slotUpdateUser"<<GlobalCenter::getInstance()->m_id
                <<GlobalCenter::getInstance()->m_username
                <<GlobalCenter::getInstance()->m_loginName
                <<GlobalCenter::getInstance()->m_passwordHash
                <<GlobalCenter::getInstance()->m_email
                <<GlobalCenter::getInstance()->m_phone
                <<GlobalCenter::getInstance()->m_isActive    ;
    ui->user_name_lab->setText(GlobalCenter::getInstance()->m_username);
}


void MainWindow::loadStyle(const QString &qssFile)
{
    //加载样式表
    QString qss;
    QFile file(qssFile);
    if (file.open(QFile::ReadOnly)) {
        //用QTextStream读取样式文件不用区分文件编码 带bom也行
        QStringList list;
        QTextStream in(&file);
        //in.setCodec("utf-8");
        while (!in.atEnd()) {
            QString line;
            in >> line;
            list << line;
        }

        file.close();
        qss = list.join("\n");
        QString paletteColor = qss.mid(20, 7);
        qApp->setPalette(QPalette(paletteColor));
        //用时主要在下面这句
        qApp->setStyleSheet(qss);
    }

}

