QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

QMAKE_CXXFLAGS += /utf-8
QMAKE_LFLAGS += /utf-8

include($$PWD/../LhtLog/LhtLog.pri);
include($$PWD/../Network/Network.pri);
include($$PWD/../Sql/Sql.pri);

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../LoginWidget/login_main_widget.cpp \
    Global/global_center.cpp \
    Network/Handle/client_parse_handles.cpp \
    Network/tcp_client.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../LoginWidget/login_main_widget.h \
    Global/global_center.h \
    Network/Handle/client_parse_handles.h \
    Network/tcp_client.h \
    mainwindow.h

FORMS += \
    ../LoginWidget/login_main_widget.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ../qrc/font.qrc \
    ../qrc/main.qrc \
    ../qrc/qm.qrc \
    ../qrc/qss.qrc
