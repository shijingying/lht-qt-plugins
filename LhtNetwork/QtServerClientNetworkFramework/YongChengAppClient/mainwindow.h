#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "../LhtLog/QsLog/QsLog.h"
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_test_send_btn_clicked();

    void slotUpdateUser();

    void loadStyle(const QString &qssFile);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
