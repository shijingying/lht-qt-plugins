#include "mainwindow.h"

#include <QApplication>
#include <qmessagebox.h>
#include "Global/global_center.h"
#include "../LoginWidget/login_main_widget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    GlobalCenter::getInstance()->init();

    LoginMainWidget login;
    login.show();
    MainWindow w;
    QObject::connect(GlobalCenter::getInstance(),&GlobalCenter::signUpdateUser,[&]{
        if(GlobalCenter::getInstance()->LoginStatus == 1){
            login.close();
            w.show();
        }else{
            QMessageBox::critical(nullptr, QStringLiteral("错误"), QStringLiteral("账号或密码错误"));
        }
    });

    return a.exec();
}
