#include "global_center.h"

#include <QCoreApplication>
#include <QTimer>

GlobalCenter * GlobalCenter::_instance = new GlobalCenter;

void GlobalCenter::init()
{
    initQsLog(QsLogging::DebugLevel,QCoreApplication::applicationDirPath() + "/YongChengAppLog.txt",1024 * 10 ,10);

    m_client = new TcpClient(QHostAddress("127.0.0.1") ,6666);
}

void GlobalCenter::login(QString name, QString pwd)
{
    NewTcpProtocolPtr data(new NewTcpProtocol);
    data->header.cmdId = CmdLogin;
    data->header.clientType = 0;
    data->header.orderId = 3;
    data->name = name.toUtf8();
    data->pwd = pwd.toUtf8();
    bool ret;
    m_client->write2Tcp(data->convertToByteArray(ret));
}

void GlobalCenter::testLogin()
{
    m_client->sendTest();
}

GlobalCenter::GlobalCenter(QObject *parent)
    : QObject{parent}
{
}

//------------------------------------------------QsLog测试--------------------------------------------------------//
void logFunction(const QString& message, QsLogging::Level level)
{

}

void GlobalCenter::initQsLog(QsLogging::Level level, const QString &logPath, long long maxSizeBytes, int maxOldLogCount)
{
    using namespace QsLogging;
    // 1. init the logging mechanism
    Logger& logger = Logger::instance();
    logger.setLoggingLevel(level);
    const QString sLogPath(logPath);

    // 2. add two destinations
    DestinationPtr fileDestination(DestinationFactory::MakeFileDestination(
        sLogPath, EnableLogRotation, MaxSizeBytes(maxSizeBytes), MaxOldLogCount(maxOldLogCount)));
    DestinationPtr debugDestination(DestinationFactory::MakeDebugOutputDestination());
    DestinationPtr functorDestination(DestinationFactory::MakeFunctorDestination(&logFunction));//日志执行后函数             两种添加方法 第一种外部函数
    //    DestinationPtr functorDestination(DestinationFactory::MakeFunctorDestination(this,SLOT(qsLogFunc(const QString& , QsLogging::Level ))));//日志执行后函数       两种添加方法 第二种类函数,不能是线程类
    logger.addDestination(debugDestination);
    logger.addDestination(fileDestination);
    logger.addDestination(functorDestination);
}
