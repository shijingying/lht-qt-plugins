#ifndef GLOBAL_CENTER_H
#define GLOBAL_CENTER_H

#include <QMutex>
#include <QObject>
#include "../../LhtLog/QsLog/QsLog.h"
#include "Network/tcp_client.h"

class GlobalCenter : public QObject
{
    Q_OBJECT
public:
    static GlobalCenter * getInstance(){
        return _instance;
    }
    void init();

    void login(QString name,QString pwd);
    std::atomic_bool LoginStatus = false;
    int     m_id              ;
    QString m_username    ;
    QString m_loginName   ;
    QString m_passwordHash;
    QString m_email       ;
    QString m_phone       ;
    int     m_isActive        ;

    void testLogin();
private:
    static GlobalCenter * _instance;
    QMutex m_mutex;
    TcpClient * m_client;
    explicit GlobalCenter(QObject *parent = nullptr);

    void initQsLog(QsLogging::Level level, const QString &logPath, long long maxSizeBytes, int maxOldLogCount);
signals:
    void signUpdateUser();
};

#endif // GLOBAL_CENTER_H
