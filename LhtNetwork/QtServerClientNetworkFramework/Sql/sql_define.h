#ifndef SQL_DEFINE_H
#define SQL_DEFINE_H

// 定义创建 users 表的 SQL 语句
#define CREATE_USERS_TABLE_SQL \
"CREATE TABLE users ( " \
    "user_id INTEGER PRIMARY KEY AUTOINCREMENT, " \
    "username TEXT NOT NULL UNIQUE, " \
    "password_hash TEXT NOT NULL, " \
    "email TEXT NOT NULL UNIQUE, " \
    "created_at DATETIME DEFAULT CURRENT_TIMESTAMP, " \
    "updated_at DATETIME DEFAULT CURRENT_TIMESTAMP, " \
    "last_login DATETIME, " \
    "status TEXT DEFAULT 'active', " \
    "CHECK(status IN ('active', 'inactive', 'banned')) " \
    ");"

// 定义创建更新触发器的 SQL 语句
#define CREATE_UPDATE_TRIGGER_SQL \
    "CREATE TRIGGER update_users_updated_at " \
    "AFTER UPDATE ON users " \
    "FOR EACH ROW " \
    "BEGIN " \
    "    UPDATE users " \
    "    SET updated_at = CURRENT_TIMESTAMP " \
    "    WHERE user_id = OLD.user_id; " \
    "END;"


#define INSERT_USER_SQL \
"INSERT INTO users (username, password_hash, email) " \
    "VALUES ('%1', '%2', '%3');"


#define UPDATE_USER_SQL \
    "UPDATE users " \
    "SET email = '%1', last_login = CURRENT_TIMESTAMP " \
    "WHERE user_id = %2;"


//----------------------------------------------员工表定义----------------------------------------
/*
 * 1) 创建用户:
 *    需要插入字段:
 *      - username
 *      - login_name
 *      - password_hash
 *      - email
 *      - phone
 *      - is_active
 *    其中 created_at, updated_at 默认使用当前时间
 *
 *    用法：
 *      QString sql = QString(CREATE_USER_SQL)
 *          .arg(username)
 *          .arg(loginName)
 *          .arg(passwordHash)
 *          .arg(email)
 *          .arg(phone)
 *          .arg(isActive);
 *
 *      query.exec(sql);
 */
#define CREATE_USER_SQL \
"INSERT INTO employee (username, login_name, password_hash, email, phone, is_active, created_at, updated_at) "\
    "VALUES ('%1', '%2', '%3', '%4', '%5', %6, datetime('now','localtime'), datetime('now','localtime'));"


/*
 * 2) 修改用户密码:
 *    根据 login_name 定位用户，并更新 password_hash。
 *
 *    用法：
 *      QString sql = QString(UPDATE_PASSWORD_SQL)
 *          .arg(newPasswordHash)
 *          .arg(loginName);
 *
 *      query.exec(sql);
 */
#define UPDATE_PASSWORD_SQL \
    "UPDATE employee "\
    "SET password_hash = '%1', "\
    "    updated_at = datetime('now','localtime') "\
    "WHERE login_name = '%2';"

//用户自己修改密码需要原密码
#define UPDATE_PASSWORD_SQL_USER \
"UPDATE employee "\
    "SET password_hash = '%1', "\
    "    updated_at = datetime('now','localtime') "\
    "WHERE login_name = '%2' and password_hash = %3;"



/*
 * 3) 设置用户是否启用 (is_active):
 *    0 表示禁用, 1 表示启用，
 *    根据 login_name 定位用户。
 *
 *    用法：
 *      QString sql = QString(SET_USER_ACTIVE_SQL)
 *          .arg(isActive)
 *          .arg(loginName);
 *
 *      query.exec(sql);
 */
#define SET_USER_ACTIVE_SQL \
    "UPDATE employee "\
    "SET is_active = %1, "\
    "    updated_at = datetime('now','localtime') "\
    "WHERE login_name = '%2';"


/*
 * 4) 修改用户手机:
 *    根据 login_name 定位用户，并更新 phone。
 *
 *    用法：
 *      QString sql = QString(UPDATE_PHONE_SQL)
 *          .arg(phone)
 *          .arg(loginName);
 *
 *      query.exec(sql);
 */
#define UPDATE_PHONE_SQL \
    "UPDATE employee "\
    "SET phone = '%1', "\
    "    updated_at = datetime('now','localtime') "\
    "WHERE login_name = '%2';"


#endif // SQL_DEFINE_H
