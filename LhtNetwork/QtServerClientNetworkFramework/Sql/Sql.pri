QT += sql

HEADERS += \
    $$PWD/sql_define.h \
    $$PWD/sql_manager.h \
    $$PWD/sqlite_module.h

SOURCES += \
    $$PWD/sql_manager.cpp \
    $$PWD/sqlite_module.cpp
