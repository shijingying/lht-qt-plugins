#ifndef SQL_MANAGER_H
#define SQL_MANAGER_H
#define OpenInstance 0
#if OpenInstance
#include <QObject>
#include <QMutex>
#include "sqlite_module.h"

class SqlManager : public QObject
{
    Q_OBJECT
public:
    static SqlManager * getInstance(){
        return _instance;
    }
    bool initDatabase(const QString &dbName);
    bool createTable(const QString &tableName);
    bool insertData(const QString &tableName, const QVariantList &data);
    bool updateData(const QString &tableName, const QVariant &id, const QVariantList &data);
    bool deleteData(const QString &tableName, const QVariant &id);
private:
    explicit SqlManager(QObject *parent = nullptr);
    static SqlManager * _instance;
    QMutex m_mutex;
    SQLiteModule m_sql;
signals:
};
#endif
#endif // SQL_MANAGER_H
