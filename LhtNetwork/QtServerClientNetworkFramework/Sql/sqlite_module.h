#ifndef SQLITEMODULE_H
#define SQLITEMODULE_H

#include <QObject>
#include <QSqlDatabase>
#include <QVariantList>
#include "../LhtLog/QsLog/QsLog.h"
const QString DBName = "test.db";
class SQLiteModule : public QObject
{
    Q_OBJECT
public:
    explicit SQLiteModule(QObject *parent = nullptr);
    ~SQLiteModule();

    bool initDatabase(const QString &dbName);
    bool createTable();
    bool insertData(const QString &tableName, const QVariantList &data);
    bool updateData(const QString &tableName, const QVariant &id, const QVariantList &data);
    bool deleteData(const QString &tableName, const QVariant &id);
    QList<QVariantList> selectData(const QString &tableName);
    bool bulkInsertData(const QString &tableName, const QList<QVariantList> &dataList);

    /**
     * @brief 获取总记录数
     * @param tableName 表名
     * @return 总记录数，失败返回 -1
     */
    int getTotalRecords(const QString &tableName);
    /**
     * @brief 获取过滤后的记录数
     * @param tableName 表名
     * @param filter SQL WHERE 子句（不包含 WHERE 关键字）
     * @return 过滤后的记录数，失败返回 -1
     */
    int getFilteredRecords(const QString &tableName, const QString &filter);
    QSqlDatabase database() const { return m_db; }
    //用户表操作
    bool createUser(const QString &username, const QString &loginName, const QString &passwordHash, const QString &email, const QString &phone, int isActive);
    bool updatePassword(const QString &loginName, const QString &newPasswordHash);
    bool setUserActive(const QString &loginName, int isActive);
    bool updatePhone(const QString &loginName, const QString &newPhone);
    /**
     * @brief 根据用户id获取用户详情
     * @param userId 用户表主键 id
     * @return 若查询成功，返回存有 [username, login_name, password_hash, email, phone, is_active] 的 QVariantList
     *         如果失败或查不到，返回空的 QVariantList
     */
    QVariantList getUserDetailsById(int userId);
    QVariantList getUserDetailsByLoginName(QString loginName);
    QList<QVariantList> getUserDetails();
signals:
    void dataChanged();

private:
    QSqlDatabase m_db;
};

#endif // SQLITEMODULE_H
