#include "sql_manager.h"

#if OpenInstance
SqlManager * SqlManager::_instance = new SqlManager;

bool SqlManager::initDatabase(const QString &dbName)
{
    return m_sql.initDatabase(dbName);
}

bool SqlManager::createTable(const QString &tableName)
{
    return m_sql.createTable(tableName);
}

bool SqlManager::insertData(const QString &tableName, const QVariantList &data)
{
    return m_sql.insertData(tableName,data);
}

bool SqlManager::updateData(const QString &tableName, const QVariant &id, const QVariantList &data)
{
    return m_sql.updateData(tableName,id,data);
}

bool SqlManager::deleteData(const QString &tableName, const QVariant &id)
{
    return m_sql.deleteData(tableName,id);
}

SqlManager::SqlManager(QObject *parent)
    : QObject{parent}
{}

#endif
