#include "sqlite_module.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QUuid>
#include "sql_define.h"

#define LHT_STRINGIFY_IMPL(x) #x
#define LHT_STRINGIFY(x) LHT_STRINGIFY_IMPL(x)
#define LHT_DBCONNECTION_CONSUMING \
"lhtDbConnection_" LHT_STRINGIFY(__COUNTER__)

SQLiteModule::SQLiteModule(QObject *parent)
    : QObject(parent)
{
}

SQLiteModule::~SQLiteModule()
{
    if (m_db.isOpen()) {
        m_db.close();
    }
}

bool SQLiteModule::initDatabase(const QString &dbName)
{
    const QString connectionName = "lhtDbConnection_" + QUuid::createUuid().toString(QUuid::WithoutBraces);;
    qDebug()<<"connectionName = "<<connectionName;
    if (QSqlDatabase::contains(connectionName)) {
        m_db = QSqlDatabase::database(connectionName);
    } else {
        m_db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        m_db.setDatabaseName(dbName);
    }

    if (!m_db.open()) {
        QLOG_ERROR() << "Failed to open database:" << m_db.lastError().text();
        return false;
    }
    return true;
}

bool SQLiteModule::createTable()
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return false;
    }
    QString sql;

    QSqlQuery query(m_db);
    sql = QString(
        "CREATE TABLE IF NOT EXISTS employee ("
        "    id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "    username TEXT NOT NULL,"
        "    login_name TEXT NOT NULL UNIQUE,"
        "    password_hash TEXT NOT NULL,"
        "    email TEXT,"
        "    phone TEXT,"
        "    is_active INTEGER NOT NULL DEFAULT 1,"
        "    created_at TIMESTAMP NOT NULL DEFAULT (datetime('now','localtime')),"
        "    updated_at TIMESTAMP NOT NULL DEFAULT (datetime('now','localtime'))"
        ");"
        );
    if (!query.exec(sql)) {
        QLOG_ERROR() << "Failed to create table:" << query.lastError().text();
        return false;
    }
    sql = QString("CREATE TABLE IF NOT EXISTS customers "
                  "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                  " name TEXT, "
                  " age INTEGER)");
    if (!query.exec(sql)) {
        QLOG_ERROR() << "Failed to create table:" << query.lastError().text();
        return false;
    }

    return true;
}

bool SQLiteModule::insertData(const QString &tableName, const QVariantList &data)
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return false;
    }

    if (data.size() < 2) {
        QLOG_ERROR() << "Insufficient data for insert!";
        return false;
    }

    QString sql = QString("INSERT INTO %1 (name, age) VALUES (?, ?)").arg(tableName);

    QSqlQuery query(m_db);
    query.prepare(sql);
    query.addBindValue(data.at(0));  // name
    query.addBindValue(data.at(1));  // age

    if (!query.exec()) {
        QLOG_ERROR() << "Failed to insert data:" << query.lastError().text();
        return false;
    }

    emit dataChanged();
    return true;
}

bool SQLiteModule::updateData(const QString &tableName, const QVariant &id, const QVariantList &data)
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return false;
    }

    if (data.size() < 2) {
        QLOG_ERROR() << "Insufficient data for update!";
        return false;
    }

    QString sql = QString("UPDATE %1 SET name = ?, age = ? WHERE id = ?").arg(tableName);

    QSqlQuery query(m_db);
    query.prepare(sql);
    query.addBindValue(data.at(0));  // name
    query.addBindValue(data.at(1));  // age
    query.addBindValue(id);

    if (!query.exec()) {
        QLOG_ERROR() << "Failed to update data:" << query.lastError().text();
        return false;
    }

    emit dataChanged();
    return true;
}

bool SQLiteModule::deleteData(const QString &tableName, const QVariant &id)
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return false;
    }

    QString sql = QString("DELETE FROM %1 WHERE id = ?").arg(tableName);

    QSqlQuery query(m_db);
    query.prepare(sql);
    query.addBindValue(id);

    if (!query.exec()) {
        QLOG_ERROR() << "Failed to delete data:" << query.lastError().text();
        return false;
    }

    emit dataChanged();
    return true;
}

QList<QVariantList> SQLiteModule::selectData(const QString &tableName)
{
    QList<QVariantList> result;

    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return result;
    }

    QString sql = QString("SELECT id, name, age FROM %1").arg(tableName);

    QSqlQuery query(m_db);
    if (!query.exec(sql)) {
        QLOG_ERROR() << "Failed to select data:" << query.lastError().text();
        return result;
    }

    while (query.next()) {
        QVariantList row;
        row << query.value("id") << query.value("name") << query.value("age");
        result.append(row);
    }

    return result;
}

bool SQLiteModule::bulkInsertData(const QString &tableName, const QList<QVariantList> &dataList)
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return false;
    }

    if (dataList.isEmpty()) {
        QLOG_ERROR() << "No data provided for bulk insert!";
        return false;
    }

    QString sql = QString("INSERT INTO %1 (name, age) VALUES (?, ?)").arg(tableName);

    QSqlQuery query(m_db);
    query.prepare(sql);

    if (!m_db.transaction()) {
        QLOG_ERROR() << "Failed to start transaction:" << m_db.lastError().text();
        return false;
    }

    for (const QVariantList &data : dataList) {
        if (data.size() < 2) {
            QLOG_ERROR() << "Insufficient data for insert!";
            m_db.rollback();
            return false;
        }

        query.bindValue(0, data.at(0));  // name
        query.bindValue(1, data.at(1));  // age

        if (!query.exec()) {
            QLOG_ERROR() << "Failed to insert data:" << query.lastError().text();
            m_db.rollback();
            return false;
        }
    }

    if (!m_db.commit()) {
        QLOG_ERROR() << "Failed to commit transaction:" << m_db.lastError().text();
        m_db.rollback();
        return false;
    }

    emit dataChanged();
    return true;
}

int SQLiteModule::getTotalRecords(const QString &tableName)
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return -1;
    }

    QString sql = QString("SELECT COUNT(*) FROM %1").arg(tableName);
    QSqlQuery query(m_db);
    if (!query.exec(sql)) {
        QLOG_ERROR() << "Failed to count records:" << query.lastError().text();
        return -1;
    }

    if (query.next()) {
        return query.value(0).toInt();
    }

    return -1;
}
int SQLiteModule::getFilteredRecords(const QString &tableName, const QString &filter)
{
    if (!m_db.isOpen()) {
        QLOG_ERROR() << "Database is not open!";
        return -1;
    }

    QString sql = QString("SELECT COUNT(*) FROM %1 %2").arg(tableName).arg(filter);
    QSqlQuery query(m_db);
    if (!query.exec(sql)) {
        QLOG_ERROR() << "Failed to count filtered records:" << query.lastError().text();
        return -1;
    }

    if (query.next()) {
        return query.value(0).toInt();
    }

    return -1;
}

bool SQLiteModule::createUser(const QString &username,
                              const QString &loginName,
                              const QString &passwordHash,
                              const QString &email,
                              const QString &phone,
                              int isActive)
{
    QString sql = QString(CREATE_USER_SQL)
    .arg(username)
        .arg(loginName)
        .arg(passwordHash)
        .arg(email)
        .arg(phone)
        .arg(isActive);

    QSqlQuery query(m_db);
    bool success = query.exec(sql);
    if (!success) {
        QLOG_ERROR() << "创建用户失败:" << query.lastError().text();
    }
    return success;
}

bool SQLiteModule::updatePassword(const QString &loginName, const QString &newPasswordHash)
{
    QString sql = QString(UPDATE_PASSWORD_SQL)
    .arg(newPasswordHash)
        .arg(loginName);

    QSqlQuery query(m_db);
    bool success = query.exec(sql);
    if (!success) {
        QLOG_ERROR() << "修改用户密码失败:" << query.lastError().text();
    }
    return success;
}

bool SQLiteModule::setUserActive(const QString &loginName, int isActive)
{
    QString sql = QString(SET_USER_ACTIVE_SQL)
    .arg(isActive)
        .arg(loginName);

    QSqlQuery query(m_db);
    bool success = query.exec(sql);
    if (!success) {
        QLOG_ERROR() << "设置用户状态失败:" << query.lastError().text();
    }
    return success;
}

bool SQLiteModule::updatePhone(const QString &loginName, const QString &newPhone)
{
    QString sql = QString(UPDATE_PHONE_SQL)
    .arg(newPhone)
        .arg(loginName);

    QSqlQuery query(m_db);
    bool success = query.exec(sql);
    if (!success) {
        QLOG_ERROR() << "更新手机失败:" << query.lastError().text();
    }
    return success;
}

QVariantList SQLiteModule::getUserDetailsById(int userId)
{
    QVariantList userDetails;

    QString sql = QString("SELECT username, login_name, password_hash, email, phone, is_active "
                          "FROM employee WHERE id = :id");

    QSqlQuery query(m_db);
    query.prepare(sql);
    query.bindValue(":id", userId);

    if (!query.exec()) {
        qDebug() << "查询用户详情失败:" << query.lastError().text();
        return userDetails;  // 空列表
    }

    if (query.next()) {
        // 将所需字段依次加入到 userDetails 中
        userDetails << query.value("username")
                    << query.value("login_name")
                    << query.value("password_hash")
                    << query.value("email")
                    << query.value("phone")
                    << query.value("is_active");
    }
    // 如果没有 next() 表示没查到结果，会直接返回空列表
    return userDetails;
}

QVariantList SQLiteModule::getUserDetailsByLoginName(QString loginName)
{
    if(!m_db.isOpen()){

    }
    QVariantList userDetails;

    QString sql = QString("SELECT id,username, login_name, password_hash, email, phone, is_active "
                          "FROM employee WHERE login_name = :login_name");

    QSqlQuery query(m_db);
    query.prepare(sql);
    query.bindValue(":login_name", loginName);

    if (!query.exec()) {
        qDebug() << "查询用户详情失败:" << query.lastError().text();
        return userDetails;
    }

    if (query.next()) {
        userDetails << query.value("id")
                    << query.value("username")
                    << query.value("login_name")
                    << query.value("password_hash")
                    << query.value("email")
                    << query.value("phone")
                    << query.value("is_active");
    }
    return userDetails;
}

QList<QVariantList> SQLiteModule::getUserDetails()
{
    QList<QVariantList> userDetailsList;

    QString sql = QString("SELECT id, username, login_name, password_hash, email, phone, is_active "
                          "FROM employee where is_active == 1");

    QSqlQuery query(m_db);
    query.prepare(sql);

    if (!query.exec()) {
        qDebug() << "查询用户详情失败:" << query.lastError().text();
        return userDetailsList;  // 返回空列表
    }

    // 使用 while(query.next()) 来获取所有记录
    while (query.next()) {
        QVariantList singleUser;
        singleUser << query.value("id")
                   << query.value("username")
                   << query.value("login_name")
                   << query.value("password_hash")
                   << query.value("email")
                   << query.value("phone")
                   << query.value("is_active");

        userDetailsList.append(singleUser);
    }

    return userDetailsList;
}
