#include "tcp_server.h"
#include <QFuture>
#include <QtConcurrent>
#include "QsLog.h"
#include "connection_manager.h"
TcpServer::TcpServer(QObject *parent) : QObject(parent)
{
    ConnectionManager::getInstance();
    init();
}

void TcpServer::init()
{
    m_server = new QTcpServer;
    m_tcpServer = new QTcpServer;
    m_sendpayloadSize=64*1024;
    connect(m_server,SIGNAL(newConnection()),this,SLOT(newClientConnect()));
    connect(m_tcpServer,SIGNAL(newConnection()),this,SLOT(acceptConnection()));
    //connect(m_server,SIGNAL(QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error)(QAbstractSocket::SocketError)),this,SLOT(displayServerError(QAbstractSocket::SocketError)));
}

void TcpServer::acceptConnection(){
    m_filesocket=m_tcpServer->nextPendingConnection();
    connect(m_filesocket,SIGNAL(bytesWritten(qint64)),this,SLOT(updateClientProgress(qint64)));
    connect(m_filesocket,SIGNAL(readyRead()),this,SLOT(updateServerProgress()));
    connect(m_filesocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
}

void TcpServer::displayError(QAbstractSocket::SocketError socketError){
    Q_UNUSED(socketError)
    QLOG_DEBUG()<<m_filesocket->errorString();
    m_filesocket->close();
}

void TcpServer::displayServerError(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError)
    QLOG_DEBUG()<<m_filesocket->errorString();
    signDisplayServerError(1);
}



void TcpServer::startListen(QString ip, QString port)
{
    if(m_server->listen(QHostAddress(ip),port.toInt())){
        signDisplayServerError(0);
        QLOG_INFO() << "listen ok";
    }
    else
        QLOG_ERROR() << "listen err "<<m_server->errorString();
    m_totalBytes=0;
    m_bytesReceived=0;
    m_fileNameSize=0;
}

void TcpServer::newClientConnect()
{
    auto socket = m_server->nextPendingConnection();
    auto name = socket->peerAddress().toString() + ":" +QString::number( socket->peerPort());
    Connections * connection = new Connections(socket);
    connect(connection,&Connections::signReadyRead,ConnectionManager::getInstance(),&ConnectionManager::signReadyRead);
}
