QT += network concurrent

HEADERS += \
    $$PWD/Handle/RegisterHandler.h \
    $$PWD/Handle/handle/login_handle.h \
    $$PWD/Handle/handler_common.h \
    $$PWD/Handle/handler_manager.h \
    $$PWD/Handle/msghandler_repository.h \
    $$PWD/Handle/new_msghandler_repository.h \
    $$PWD/connection_manager.h \
    $$PWD/connections.h \
    $$PWD/order_define.h \
    $$PWD/protocol_define.h \
    $$PWD/tcp_server.h

SOURCES += \
    $$PWD/Handle/RegisterHandler.cpp \
    $$PWD/Handle/handler_manager.cpp \
    $$PWD/connection_manager.cpp \
    $$PWD/connections.cpp \
    $$PWD/tcp_server.cpp
