﻿#ifndef PROTOCOL_DEFINE_H
#define PROTOCOL_DEFINE_H

#include <QByteArray>
#include <iostream>
#include <qdatastream.h>
#include <QTcpSocket>
#include <QDebug>
#include <shared_mutex>
#include <functional>
#include "order_define.h"

#define  OtherSize              22
#define  SendFileBufferSize     1024*4

#define  FreqRegionStart240     240
#define  FreqRegionEnd270       270

#define  FreqRegionStart290     290
#define  FreqRegionEnd320       320

enum ClientType{
    SenderClient = 1,
    RecvClient
};

const QStringList ClientTypeStr{"unknow",QString::fromLocal8Bit("发送端"),QString::fromLocal8Bit("接收端")};

enum ErrorNumber{
    NOERROR,
    ERROR1,
    ERROR2,
    ERROR3,
    ERROR4,
    ERROR5
};



struct NewHeader{
    char        checkCode1 = 77;//校验码1  （121）
    char        checkCode2 = 88;//校验码2  （212）
    char        cmdId = 1;//指令号
    char        clientType = -1;//客户端类型  1为发送端   2为客户端
    short       orderId = -1;//指令次数
    int         packSize = -1;//数据包包大小
};
class Connections;
struct NewTcpProtocol {
    NewHeader header;
    int nameSize = 0;
    QByteArray name;
    int pwdSize = 0;
    QByteArray pwd;
    int contentSize = 0;
    QByteArray content;
    Connections* m_socket = nullptr;

    QString getInfoStr() const {
        QString ret;
        ret += " cmdId = " + QString::number(this->header.cmdId);
        ret += " orderId = " + QString::number(this->header.orderId);
        ret += " packSize = " + QString::number(this->header.packSize);
        ret += " loginId = ";
        // 根据实际需求添加 loginId 的信息
        return ret;
    }

    QByteArray convertToByteArray(bool& ret) const {
        QByteArray byteArray;
        QDataStream stream(&byteArray, QIODevice::WriteOnly);
        stream.setByteOrder(QDataStream::LittleEndian); // 根据协议设置字节序

        // 写入 NewHeader 部分
        stream.writeRawData(reinterpret_cast<const char*>(&header.checkCode1), 1);
        stream.writeRawData(reinterpret_cast<const char*>(&header.checkCode2), 1);
        stream.writeRawData(reinterpret_cast<const char*>(&header.cmdId), 1);
        stream.writeRawData(reinterpret_cast<const char*>(&header.clientType), 1);
        stream.writeRawData(reinterpret_cast<const char*>(&header.orderId), 2);
        quint32 packsize = OtherSize +
                           name.size()+
                           content.size()+
                            pwd.size();
        stream << packsize;

        // 写入其他字段
        stream << static_cast<quint32>(name.size());
        if (name.size() > 0) {
            stream.writeRawData(name.constData(), name.size());
        }
        stream << static_cast<quint32>(content.size());
        if (content.size() > 0) {
            stream.writeRawData(content.constData(), content.size());
        }
        stream << static_cast<quint32>(pwd.size());
        if (pwd.size() > 0) {
            stream.writeRawData(pwd.constData(), pwd.size());
        }
        // 修改 packSize 字段（假设 packSize 位于 byteArray 的特定位置）
        // 确保 byteArray 的大小足够
        if (byteArray.size() >= OtherSize) { // 根据 packSize 的位置调整
            // packSize 位于 offset 6 到 9
            QByteArray packSizeBytes = QByteArray::fromRawData(reinterpret_cast<const char*>(&packsize), 4);
            byteArray.replace(6, 4, packSizeBytes);
        } else {
            ret = false;
            return QByteArray();
        }

        ret = true;
        return byteArray;
    }

    // 复制构造函数
    NewTcpProtocol(const NewTcpProtocol& other)
        : header(other.header),
        nameSize(other.nameSize), name(other.name),
        contentSize(other.contentSize), content(other.content),
        pwdSize(other.pwdSize), pwd(other.pwd),
        m_socket(other.m_socket) {}

    // 赋值运算符
    NewTcpProtocol& operator=(const NewTcpProtocol& other) {
        if (this == &other) {
            return *this;
        }

        header = other.header;
        nameSize = other.nameSize;
        name = other.name;
        contentSize = other.contentSize;
        content = other.content;
        pwdSize = other.pwdSize;
        pwd = other.pwd;
        m_socket = other.m_socket;

        return *this;
    }

    // 默认构造函数
    NewTcpProtocol() {}

    // 从 QByteArray 解析构造函数
    NewTcpProtocol(const QByteArray& arr) {
        QDataStream stream(arr);
        stream.setByteOrder(QDataStream::LittleEndian); // 根据协议设置字节序

        // 读取 NewHeader 部分
        stream.readRawData(reinterpret_cast<char*>(&header.checkCode1), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.checkCode2), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.cmdId), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.clientType), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.orderId), 2);
        stream >> header.packSize;
        stream >> nameSize;

        if (nameSize > 0) {
            name.resize(nameSize);
            stream.readRawData(name.data(), nameSize);
            qDebug()<<QString::fromUtf8(name);
        }

        stream >> contentSize;
        if (contentSize > 0) {
            content.resize(contentSize);
            stream.readRawData(content.data(), contentSize);
            qDebug()<<QString::fromUtf8(content);
        }
        stream >> pwdSize;
        if (pwdSize > 0) {
            pwd.resize(pwdSize);
            stream.readRawData(pwd.data(), pwdSize);
            qDebug()<<QString::fromUtf8(pwd);
        }
    }
};

typedef std::shared_ptr<NewTcpProtocol>     NewTcpProtocolPtr;
typedef std::function<void(NewTcpProtocolPtr)> RequestHandler;


#endif // PROTOCOL_DEFINE_H
