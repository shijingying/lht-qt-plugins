#ifndef ORDER_DEFINE_H
#define ORDER_DEFINE_H

enum CmdList{
    CmdTest = 1,
    CmdUnkown = 10,
    CmdLogin,
    CmdLogin1,
    CmdLast
};

//--------------------------普通数据包--------------------------
//登录确认设备属性
#define CMD_LOGIN                                   0x10
//心跳  10S超时
#define CMD_HEART                                   0x11
//任务
#define CMD_SIGNAL_TASK                             0x12

#define CMD_SIGNAL_RADIATION                        0x13
//工作状态控制
#define CMD_WORK_STATE_CONTROL                      0x14
//设备信息上传    客户端到服务器        由服务器拉取
#define CMD_DEVICE_INFO_UPLOAD                      0x15
//自检上报  客户端到服务器             主动发送异常
#define CMD_ERROR_UPLOAD                            0x16


//--------------------------文件上传--------------------------
#define CMD_SIGNAL_FILE_UPLOAD_START                    0x51
//信号文件上传    接收端->导调
#define CMD_SIGNAL_FILE_UPLOAD_DATA                     0x52

//--------------------------文件下载--------------------------
#define CMD_SIGNAL_FILE_DOWNLOAD_START                    0x61
//信号文件上传    接收端->导调
#define CMD_SIGNAL_FILE_DOWNLOAD_DATA                     0x62







#endif // ORDER_DEFINE_H
