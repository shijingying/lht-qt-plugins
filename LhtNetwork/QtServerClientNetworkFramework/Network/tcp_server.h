#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "protocol_define.h"
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QFile>
#include "connections.h"

class TcpServer : public QObject
{
    Q_OBJECT
public:
    explicit TcpServer(QObject *parent = nullptr);
public slots:
    void init();

    void startListen(QString ip , QString port);

signals:
    void signReadyRead(QString ,NewTcpProtocolPtr);

    void signConnected(QString);

    void signDisconnected(QString);

    void signDisplayError(QAbstractSocket::SocketError);

    void signDisplayServerError(bool);
public slots:
    void newClientConnect();

    void acceptConnection();

    void displayError(QAbstractSocket::SocketError socketError);

    void displayServerError(QAbstractSocket::SocketError socketError);
private:
    QTcpServer* m_server;
    QMap<ClientType, QTcpSocket*> m_socket;
    QFile       m_file;
    QByteArray  m_buffer;
    QString     m_filePath;
    QTcpServer* m_tcpServer;
    QTcpSocket* m_filesocket;
    qint64      m_totalBytes;
    qint64      m_bytesReceived;
    qint64      m_fileNameSize;
    QString     m_fileName;
    QFile *     m_localFile;
    QByteArray  m_inBlock;
    QFile *     m_sendlocalFile;
    qint64      m_sendtotalBytes;
    qint64      m_sendbytesWritten;
    qint64      m_sendbytesToWrite;
    qint64      m_sendpayloadSize;
    QString     m_sendfileName;
    QByteArray  m_sendoutBlock;
    //QString     m_currentRecvClient;
    ClientType  m_currentRecvClient;
};

#endif // TCPSERVER_H
