#include "connections.h"
#include <QHostAddress>
#include <QtConcurrent>
#include "../LhtLog/QsLog/QsLog.h"
#include "connection_manager.h"
#include "Handle/msghandler_repository.h"
#include "Handle/handle/login_handle.h"
Connections::Connections(QTcpSocket *socket):m_socket(socket)
{
    init();
}

void Connections::init()
{
    connect(m_socket,&QTcpSocket::readyRead,this,&Connections::readMessage);
    connect(m_socket,&QTcpSocket::disconnected,this,&Connections::disConnect);
}

void Connections::write2Socket(QByteArray arr)
{
    QMutexLocker lock(&m_mutex);
    m_socket->write(arr);
    m_socket->waitForBytesWritten();
}

void Connections::readMessage()
{
    // 使用 qobject_cast 进行安全的类型转换
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    if (!socket) {
        qWarning() << "readMessage called by non-QTcpSocket sender";
        return;
    }

    // 读取所有可用数据并追加到缓冲区
    QByteArray newData = socket->readAll();
    if (newData.isEmpty()) {
        return;
    }
    m_buffer.append(newData);

    while (true) {
        const int headerSize = 18; // 14 bytes

        // 检查是否有足够的数据读取头部
        if (m_buffer.size() < headerSize) {
            break; // 等待更多数据
        }

        // 使用 QDataStream 读取头部
        QDataStream stream(m_buffer);
        stream.setByteOrder(QDataStream::LittleEndian); // 根据协议设置字节序

        NewHeader header;
        stream.readRawData(reinterpret_cast<char*>(&header.checkCode1), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.checkCode2), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.cmdId), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.clientType), 1);
        stream.readRawData(reinterpret_cast<char*>(&header.orderId), 2);
        quint32 packSize;
        stream >> packSize;

        // 验证 packSize 合理性
        if (packSize < headerSize || header.checkCode1 != 77 || header.checkCode2 != 88) {
            qWarning() << "Invalid packSize:" << packSize;
            socket->disconnectFromHost();
            return;
        }

        // 检查是否有足够的数据读取整个消息
        if (m_buffer.size() < packSize) {
            break; // 等待更多数据
        }

        // 提取完整的消息
        QByteArray message = m_buffer.left(packSize);
        m_buffer.remove(0, packSize);

        // 解析消息
        std::shared_ptr<NewTcpProtocol> data = std::make_shared<NewTcpProtocol>(message);
        data->m_socket = this;
        auto cmd = data->header.cmdId;

        // 数据包分发
        if (cmd > CmdLogin && cmd < CmdLast) {
            // 并行处理
            QtConcurrent::run([cmd, data]() {
                auto handle = MsgHandlerRepository<RequestHandler>::instance().handler(cmd);
                if (handle != nullptr)
                    handle(data);
                else{
                    QLOG_ERROR() << "error cmd == " << cmd;
                }
            });
        }
        else if (cmd == CmdLogin) {
            LoginHandle handle;
            handle.HandleMessage(data);
            if(handle.loginStatus)
                ConnectionManager::getInstance()->setNewConnection(data->name, this);
        }else if (cmd == CmdTest) {
            signReadyRead(data->name, data);
        }
        else {
            qWarning() << "Unknown cmdId:" << cmd;
        }
    }
}

void Connections::disConnect()
{
    ConnectionManager::getInstance()->removeConnection(this);
}
