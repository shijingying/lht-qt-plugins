#ifndef CONNECTION_MANAGER_H
#define CONNECTION_MANAGER_H

#include <QMutex>
#include <QObject>
#include "connections.h"
class ConnectionManager : public QObject
{
    Q_OBJECT
public:
    static ConnectionManager * getInstance(){
        return _instance;
    }
    void setNewConnection(QString name,Connections * connection);

    void removeConnection(QString name);

    void removeConnection(Connections * connection);

    void updateConnection(QString nameSource,QString nameNow);

private:
    explicit ConnectionManager(QObject *parent = nullptr);
    static ConnectionManager * _instance;

    QMap<QString , Connections *> m_connections;
    QMutex m_mutex;
signals:
    void signNewConnection(QString name);
    void signUpdateConnectionName(QString nameSource,QString nameNow);
    void signDeleteConnectionName(QString name);
    void signReadyRead(QString ,NewTcpProtocolPtr);
};

#endif // CONNECTION_MANAGER_H
