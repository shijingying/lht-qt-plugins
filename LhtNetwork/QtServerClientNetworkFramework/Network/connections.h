#ifndef CONNECTIONS_H
#define CONNECTIONS_H

#include <QMutex>
#include <QTcpSocket>
#include <memory>
#include "protocol_define.h"
class Connections : public QObject ,public std::enable_shared_from_this<Connections>
{
    Q_OBJECT
public:
    Connections(QTcpSocket * socket);

    void init();

    void write2Socket(QByteArray arr);
private slots:

    void readMessage();

    void disConnect();
signals:
    void signReadyRead(QString ,NewTcpProtocolPtr);
private:
    QTcpSocket * m_socket;
    QByteArray  m_buffer;
    QMutex      m_mutex;
};

#endif // CONNECTIONS_H
