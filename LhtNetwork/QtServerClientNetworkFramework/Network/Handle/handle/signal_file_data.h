#ifndef SIGNAL_FILE_DATA_H
#define SIGNAL_FILE_DATA_H
#include <QByteArray>
#include <QObject>
#include "../../protocol_define.h"
#include "../handler_common.h"
#include <QFile>
#include "file_manager.h"
class SignalFileData
{
public:
    virtual void HandleMessage(NewTcpProtocolPtr msg);

};

void SignalFileData::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg)
    {
        return;
    }
    NewTcpProtocol newTcpProtocol(*msg.get());
    newTcpProtocol.fileSeek = msg->fileSeek + msg->fileCurrentLen;

    //存文件
    static QString basic = "./";
    auto file = FileManager::getInstance()->getWriteFile(basic + msg->fileName);
    if(file){
        file->seek(msg->fileSeek);
        file->write(msg->fileData);
        if(msg->fileIsLastPacket){
            FileManager::getInstance()->closeFile(basic + msg->fileName);
        }
    }else{
        qDebug()<<"file open error path = "<<basic+msg->fileName<<"||||||"<<file->fileName();
    }
    //response
    if(msg->m_socket){
        bool ret;
        msg->m_socket->write(newTcpProtocol.convertToByteArray(ret));
        msg->m_socket->waitForBytesWritten();
    }
   // qDebug() << __FUNCTION__ << "receive data "<<msg->getInfoStr();
}
#endif // SIGNAL_FILE_DATA_H
