#ifndef SNED_SIGNAL_INFO_FILE_DATA_H
#define SNED_SIGNAL_INFO_FILE_DATA_H

#include <QByteArray>
#include <QObject>
#include "../../protocol_define.h"
#include "../handler_common.h"
#include "file_manager.h"
#define BufferSize 8192
class SendSignalInfoFileData
{
public:
    virtual void HandleMessage(NewTcpProtocolPtr msg);

};

void SendSignalInfoFileData::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg )
    {
        return;
    }
    NewTcpProtocol newTcpProtocol(*msg.get());
    auto fileName = FileManager::getInstance()->signalCurrentFilePath;
    auto filename = fileName.split("/").last();
    auto file = FileManager::getInstance()->getReadFile(fileName);
    char * temp = new char[BufferSize];
    newTcpProtocol.header.cmdId = CMD_SIGNAL_FILE_DOWNLOAD_DATA;
    file->seek(msg->fileSeek);
    auto len = file->read(temp,BufferSize);
    memcpy(newTcpProtocol.fileName,filename.toLocal8Bit().data() , filename.toLocal8Bit().length());
    newTcpProtocol.fileSeek = msg->fileSeek;
    newTcpProtocol.fileTotalSize = file->size();
    if(msg->fileSeek >= newTcpProtocol.fileTotalSize){
        QLOG_INFO()<<"send file finsh";
        return;
    }
    newTcpProtocol.fileCurrentLen = len;

    bool isLast = (newTcpProtocol.fileSeek + len >= newTcpProtocol.fileTotalSize);
    if(isLast){
        FileManager::getInstance()->closeFile(fileName);
    }
    newTcpProtocol.fileIsLastPacket = isLast;
    newTcpProtocol.fileData = QByteArray(temp,len);
    if(msg->m_socket){
        bool ret;
        msg->m_socket->write(newTcpProtocol.convertToByteArray(ret));
//        msg->m_socket->waitForBytesWritten();
    }
    delete []temp;
    //qDebug() << __FUNCTION__ << "receive data "<<msg->getInfoStr();
}


#endif // SNED_SIGNAL_INFO_FILE_DATA_H
