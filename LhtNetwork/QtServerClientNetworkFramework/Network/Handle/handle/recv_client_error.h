#ifndef RECV_CLIENT_ERROR_H
#define RECV_CLIENT_ERROR_H
#include <QByteArray>
#include <QObject>
#include "../../protocol_define.h"
#include "../handler_common.h"
class RecvClientErrorHandle
{
public:
    virtual void HandleMessage(NewTcpProtocolPtr msg);

};

void RecvClientErrorHandle::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg)
    {
        return;
    }
    NewTcpProtocol newTcpProtocol(*msg.get());

    if(msg->m_socket){
        bool ret;
        msg->m_socket->write(newTcpProtocol.convertToByteArray(ret));
        msg->m_socket->waitForBytesWritten();
    }
    qDebug() << __FUNCTION__ << "receive data "<<msg->getInfoStr();
}
#endif // RECV_CLIENT_ERROR_H
