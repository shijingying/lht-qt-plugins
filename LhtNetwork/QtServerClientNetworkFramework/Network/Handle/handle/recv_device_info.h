#ifndef DEVICE_INFO_RECV_H
#define DEVICE_INFO_RECV_H
#include <QByteArray>
#include <QObject>
#include "../../protocol_define.h"
#include "../handler_common.h"
#include "Cache/client_cache.h"
class RecvDeviceInfoHandle
{
public:
    virtual void HandleMessage(NewTcpProtocolPtr msg);

};

void RecvDeviceInfoHandle::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg)
    {
        return;
    }
    ClientCache::getInstance()->parseData(msg);

    qDebug() << __FUNCTION__ << "receive data "<<msg->getInfoStr() ;
    qDebug() << __FUNCTION__ <<"device info " <<QString::fromUtf8(msg->deviceInfo,msg->deviceInfoSize);
}
#endif // DEVICE_INFO_RECV_H
