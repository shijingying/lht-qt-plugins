#ifndef SIGNAL_FILE_START_H
#define SIGNAL_FILE_START_H
#include <QByteArray>
#include <QObject>
#include "../../protocol_define.h"
#include "../handler_common.h"
#include "file_manager.h"

class SignalFileStart
{
public:
    virtual void HandleMessage(NewTcpProtocolPtr msg);

};

void SignalFileStart::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg)
    {
        return;
    }
    NewTcpProtocol newTcpProtocol(*msg.get());


    static QString basic = "./";
    FileManager::getInstance()->getWriteFile(basic + msg->fileName);

    if(msg->m_socket){
        bool ret;
        msg->m_socket->write(newTcpProtocol.convertToByteArray(ret));
        msg->m_socket->waitForBytesWritten();
    }
    qDebug() << __FUNCTION__ << "receive data "<<msg->getInfoStr();
}
#endif // SIGNAL_FILE_START_H
