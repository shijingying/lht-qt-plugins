#ifndef SNED_SIGNAL_INFO_FILE_START_H
#define SNED_SIGNAL_INFO_FILE_START_H

#include <QByteArray>
#include <QObject>
#include "../../protocol_define.h"
#include "../handler_common.h"
#include "file_manager.h"
#include "sned_signal_info_file_data.h"
class SendSignalInfoFileStart
{
public:
    virtual void HandleMessage(NewTcpProtocolPtr msg);

};

void SendSignalInfoFileStart::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg)
    {
        return;
    }
    NewTcpProtocol newTcpProtocol(*msg.get());

    FileManager::getInstance()->getReadFile(FileManager::getInstance()->signalCurrentFilePath);
    msg->fileSeek = 0;
    SendSignalInfoFileData sendSignalInfoFileData;
    sendSignalInfoFileData.HandleMessage(msg);

//    if(msg->m_socket){
//        bool ret;
//        msg->m_socket->write(newTcpProtocol.convertToByteArray(ret));
//        msg->m_socket->waitForBytesWritten();
//    }
    //qDebug() << __FUNCTION__ << "receive data "<<msg->getInfoStr();
}

#endif // SNED_SIGNAL_INFO_FILE_START_H
