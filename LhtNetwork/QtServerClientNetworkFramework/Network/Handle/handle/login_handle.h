#ifndef LOGIN_HANDLE_H
#define LOGIN_HANDLE_H
#include <QByteArray>
#include <QObject>
#include <QCryptographicHash>
#include <QJsonObject>
#include <QJsonDocument>
#include "../../protocol_define.h"
#include "../Sql/sqlite_module.h"
#include "../../connections.h"
class LoginHandle
{
public:
    inline virtual void HandleMessage(NewTcpProtocolPtr msg);

    bool loginStatus = false;

    bool checkUser(const QString & name,const QString & pwd,NewTcpProtocolPtr msg);
};

void LoginHandle::HandleMessage(NewTcpProtocolPtr msg)
{
    if (!msg)
    {
        return;
    }
    QString name = QString::fromUtf8(msg->name);
    QString pwd = QString::fromUtf8(msg->pwd);
    checkUser(name,pwd,msg);
}

inline bool LoginHandle::checkUser(const QString &name, const QString &pwd,NewTcpProtocolPtr msg)
{

    SQLiteModule sql;
    sql.initDatabase(DBName);
    QVariantList userDetails = sql.getUserDetailsByLoginName(name);
    QString passwordHash1 = QCryptographicHash::hash(pwd.toUtf8(),
                                                     QCryptographicHash::Md5).toHex();
    if (userDetails.isEmpty()) {
        QLOG_ERROR() << "未查询到对应的用户记录或查询失败, username=" << name;
        return false;
    }
    int id = userDetails.at(0).toInt();
    QString username     = userDetails.at(1).toString();
    QString loginName    = userDetails.at(2).toString();
    QString passwordHash = userDetails.at(3).toString();
    QString email        = userDetails.at(4).toString();
    QString phone        = userDetails.at(5).toString();
    int isActive         = userDetails.at(6).toInt();
    if(passwordHash == passwordHash1){
        QJsonObject userJson;
        userJson["id"]        = id;
        userJson["username"]  = username;
        userJson["loginName"] = loginName;
        userJson["passwordHash"] = passwordHash;  // 注意：真实项目中是否要给客户端返回密码Hash？
        userJson["email"]     = email;
        userJson["phone"]     = phone;
        userJson["isActive"]  = isActive;

        // 2) 将 JSON 对象转换为 QByteArray
        QJsonDocument doc(userJson);
        QByteArray jsonData = doc.toJson(QJsonDocument::Compact); // 使用紧凑格式

        // 3) 写入到 msg->content 中，并设置 contentSize
        msg->header.clientType = 1;
        msg->content = jsonData;
        msg->contentSize = jsonData.size();
    }else{
        msg->header.clientType = -1;
    }

    if(msg->m_socket){
        bool ret;
        msg->m_socket->write2Socket(msg->convertToByteArray(ret));\
    }
    return true;
}

#endif // LOGIN_HANDLE_H
