#pragma once
#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <QFile>
#include <QObject>
#include <QMap>
#include <QDebug>
#include "QsLog.h"

class FileManager{
public:
    static FileManager * getInstance(){
        return m_instance;
    }

    void closeFile(QString name){
        if(fileMap.find(name) != fileMap.end()){
            fileMap[name]->close();
        }
    }
    QFile * getWriteFile(QString name){
        if(fileMap.find(name) == fileMap.end()){
            QFile * file = new QFile(name);
            if(!file->open(QIODevice::WriteOnly)){
                qDebug()<<__FUNCTION__<<__LINE__<< "file open error "<<name;
            }
            fileMap[name] = file;
        }else{
            if(!fileMap[name]->isOpen()){
                if(!fileMap[name]->open(QIODevice::WriteOnly)){
                    qDebug()<<__FUNCTION__<<__LINE__<< "file open error "<<name;
                }
            }

        }
        return fileMap[name];
    }

    QFile * getReadFile(QString name){
        if(fileMap.find(name) == fileMap.end()){
            QFile * file = new QFile(name);
            if(!file->open(QIODevice::ReadOnly)){
                qDebug()<<__FUNCTION__<<__LINE__<< "file open error "<<name;
            }
            fileMap[name] = file;
        }else{
            if(!fileMap[name]->isOpen()){
                if(!fileMap[name]->open(QIODevice::ReadOnly)){
                    qDebug()<<__FUNCTION__<<__LINE__<< "file open error "<<name;
                }
            }

        }
        return fileMap[name];
    }


    QString signalCurrentFilePath = "";
private:
    QMap<QString ,QFile *>  fileMap;
    static FileManager * m_instance;
};


#endif // FILE_MANAGER_H
