#ifndef HANDLERMANAGER_H
#define HANDLERMANAGER_H

#include <QObject>
#include "RegisterHandler.h"
class HandlerManager : public QObject
{
    Q_OBJECT
public:
    static HandlerManager* Instance();

     void init();

    template<class Handler>
    bool InitHandler(const unsigned short cmd);
private:

    template<class Handler>
    void NotifyHandler(NewTcpProtocolPtr msg);


private:
    bool registered_;
    static  HandlerManager*g_instance ;
    explicit HandlerManager(QObject *parent = nullptr);


signals:


private:
    void initRecvMethod();
};

#endif // HANDLERMANAGER_H
