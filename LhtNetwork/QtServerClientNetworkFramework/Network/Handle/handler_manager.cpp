#include "handler_manager.h"
#include "RegisterHandler.h"
#include "../order_define.h"
#include "handler_common.h"
#include "handle/login_handle.h"
#include "new_msghandler_repository.h"
HandlerManager * HandlerManager::g_instance = new HandlerManager;

HandlerManager::HandlerManager(QObject *parent) : QObject(parent)
{

}

template<class Handler>
bool HandlerManager::InitHandler(const unsigned short cmd)
{
    RegisterHandler::Instance()->InitHandler(cmd, std::bind(&HandlerManager::NotifyHandler<Handler>, this, std::placeholders::_1));
    return 1;
}

template<class Handler>
void HandlerManager::NotifyHandler(NewTcpProtocolPtr msg)
{
    Handler handler;
    handler.HandleMessage(msg);
}


HandlerManager *HandlerManager::Instance()
{
    return g_instance;
}

void HandlerManager::init()
{
    initRecvMethod();
}

void HandlerManager::initRecvMethod()
{
    InitHandler<LoginHandle>(CmdLogin);
}


