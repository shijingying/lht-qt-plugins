#ifndef NEW_MSGHANDLER_REPOSITORY_H
#define NEW_MSGHANDLER_REPOSITORY_H

#include <QObject>
#include "../protocol_define.h"
#include <unordered_map>

typedef void(*TcpHandler)(NewTcpProtocolPtr);

class NewMsgRepository {
public:
    static NewMsgRepository& instance( );

    bool registerHandler(const unsigned short code, TcpHandler);

    void removeHandler(const unsigned short code);

    TcpHandler handler(const unsigned short  code) const;

private:
    std::unordered_map<unsigned short,TcpHandler> handlers;/**< 存放处理的容器 */
};


NewMsgRepository& NewMsgRepository::instance()
{
    static NewMsgRepository _gInstance;
    return _gInstance;
}

bool NewMsgRepository::registerHandler(const unsigned short code, TcpHandler handler)
{
    this->handlers[code] = handler;
    return true;
}

void NewMsgRepository::removeHandler(const unsigned short code)
{
    this->handlers.erase(code);
}

TcpHandler NewMsgRepository::handler(const unsigned short code) const
{
    if( this->handlers.find(code) !=  this->handlers.end()){
        return  this->handlers.at(code);
    }
    return nullptr;
}


#endif // NEW_MSGHANDLER_REPOSITORY_H
