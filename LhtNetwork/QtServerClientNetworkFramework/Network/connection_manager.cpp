#include "connection_manager.h"

ConnectionManager * ConnectionManager::_instance = new ConnectionManager;

void ConnectionManager::setNewConnection(QString name, Connections *connection)
{
    QMutexLocker lock(&m_mutex);
    m_connections[name] = connection;
    emit signNewConnection(name);
}

void ConnectionManager::removeConnection(Connections *connection)
{
    QMutexLocker lock(&m_mutex);
    auto keys = m_connections.keys();
    for(auto it : keys){
        if(m_connections[it] == connection){
            m_connections.remove(it);
            emit signDeleteConnectionName(it);
            break;
        }
    }
}

void ConnectionManager::removeConnection(QString name)
{
    QMutexLocker lock(&m_mutex);
    m_connections.remove(name);
    emit signDeleteConnectionName(name);
}

void ConnectionManager::updateConnection(QString nameSource, QString nameNow)
{
    QMutexLocker lock(&m_mutex);
    auto connection = m_connections[nameSource];
    m_connections.remove(nameSource);
    m_connections[nameNow] = connection;
    emit signUpdateConnectionName(nameSource,nameNow);
}

ConnectionManager::ConnectionManager(QObject *parent)
    : QObject{parent}
{}
