﻿/*
    1.通讯采用GRPC  流式长连接
    2.服务注册与服务发现服务健康监测 使用redis（后续如果需要升级则采用consul进行升级）
    3.由于算法问题，所有服务采用C++开发（go的话非常简单）
    4.分布式日志采用log4cplus 在中心服务器开启一个日志服务器，所有设备发往中心服务器
    5.mysql数据库所有数据都发送到中心服务器进行存储，redis开放外接权限，所有服务都可以联通
    6.对外的API接口 可以开启 mysql+redis 让他可以获取所有当前的数据
**/
#include <iostream>

int main()
{
    std::cout << "Hello World!\n";
}
