#pragma once
#include "connection_pool_redis.hpp"

class RedisManager : public std::enable_shared_from_this<RedisManager>
{
public:
    /**
     * @brief 获取 RedisManager 的单例实例。
     *
     * 此方法提供对 RedisManager 单例实例的访问。
     *
     * @return RedisManager 实例的引用。
     */
    static RedisManager& instance();

    /**
     * @brief 初始化 Redis 连接池。
     *
     * 使用给定参数设置连接池。
     *
     * @param redis_addr Redis 服务器地址。
     * @param port Redis 服务器端口号。
     * @param pwd 身份验证密码（如果需要）。
     * @param conn_timeout 连接超时（秒）。
     * @param pool_size 连接池的初始大小。
     * @param pool_max_size 连接池的最大大小。
     * @return 如果初始化成功返回 true，否则返回 false。
     */
    bool InitPool(const std::string& redis_addr, const std::size_t& port, const std::string& pwd, int conn_timeout, std::size_t pool_size, std::size_t pool_max_size);

    /**
     * @brief 释放连接回连接池。
     *
     * 此函数释放不再使用的连接，使其可被其他请求重用。
     */
    void ReleasePool();

    /**
     * @brief 检查 Redis 连接池的状态。
     *
     * 此方法验证连接池中连接的健康状况和可用性，并可以记录或报告状态。
     */
    void CheckStatus();

    /**
     * @brief 为给定键设置过期时间。
     *
     * @param key 要设置过期的键。
     * @param time 过期时间（秒）。
     * @return 如果设置成功返回 true，否则返回 false。
     */
    bool SetKeyExpire(std::string& key, int time);

    /**
     * @brief 从 Redis 中删除一个键。
     *
     * @param key 要删除的键。
     * @return 如果删除成功返回 true，否则返回 false。
     */
    bool DelKey(const std::string& key);

    /**
     * @brief 检查键是否存在于 Redis 中。
     *
     * @param key 要检查的键。
     * @return 如果键存在返回 true，否则返回 false。
     */
    bool KeyExists(const std::string& key);

    // 字符串操作

    /**
     * @brief 在 Redis 中设置字符串值。
     *
     * @param key 存储值的键。
     * @param value 要存储的值。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool SetString(const std::string& key, const std::string& value);

    /**
     * @brief 从 Redis 中检索字符串值。
     *
     * @param key 要检索值的键。
     * @return 与键关联的值。
     */
    std::string GetString(const std::string& key);

    /**
     * @brief 在指定键的哈希中设置一个字段。
     *
     * @param key 哈希的键。
     * @param field 要设置的字段。
     * @param value 字段的值。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool Set(const std::string& key, const std::string& field, const std::string& value);

    /**
     * @brief 从指定键的哈希中检索一个字段。
     *
     * @param key 哈希的键。
     * @param field 要检索的字段。
     * @param response 输出参数，用于存储字段值。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool Get(const std::string& key, const std::string& field, std::string& response);

    // 哈希操作

    /**
     * @brief 在指定键的哈希中设置字段（使用命令）。
     *
     * @param key 哈希的键。
     * @param commond 命令字符串。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool HSet(const std::string& key, const std::string& commond);

    /**
     * @brief 在指定键的哈希中设置字段。
     *
     * @param key 哈希的键。
     * @param field 要设置的字段。
     * @param value 字段的值。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool HSet(const std::string& key, const std::string& field, const std::string& value);

    /**
     * @brief 从指定键的哈希中检索字段。
     *
     * @param key 哈希的键。
     * @param field 要检索的字段。
     * @param response 输出参数，用于存储字段值。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool HGet(const std::string& key, const std::string& field, std::string& response);

    /**
     * @brief 从指定键的哈希中检索所有字段和值。
     *
     * @param key 哈希的键。
     * @param response 用于存储所有字段值对的映射。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool HGetAll(const std::string& key, std::map<std::string, std::string>& response);

    /**
     * @brief 从指定键的哈希中删除字段。
     *
     * @param key 哈希的键。
     * @param field 要删除的字段。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool HDelField(const std::string& key, const std::string& field);

    /**
     * @brief 计算指定键的哈希中字段的数量。
     *
     * @param key 哈希的键。
     * @param count 输出参数，用于存储数量。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool HFieldCount(const std::string& key, std::string& count);

    /**
     * @brief 刷新所有缓冲数据。
     *
     * 清空整个 Redis 服务器的数据（删除所有数据库的所有键）。
     *
     * @return 如果刷新成功返回 true，否则返回 false。
     */
    bool flushal();

    /**
     * @brief 根据给定模式检索所有匹配的键。
     *
     * @param key 要匹配的模式。
     * @param response 用于存储匹配键的向量。
     * @return 如果操作成功返回 true，否则返回 false。
     */
    bool getAllKeys(const std::string& key, std::vector<std::string>& response);

private:
    std::string     redis_ip_;     ///< Redis 服务器的 IP 地址。
    std::uint32_t   redis_port_;   ///< Redis 服务器的端口号。
    std::uint32_t   time_out_;     ///< 连接超时时间。
    std::string     redis_pwd_;    ///< 身份验证密码。

    RedisPoolPtr pool_; ///< 连接池的指针。

private:
    RedisManager();      ///< 私有构造函数，用于单例模式。
    virtual ~RedisManager(); ///< 虚析构函数。
};
