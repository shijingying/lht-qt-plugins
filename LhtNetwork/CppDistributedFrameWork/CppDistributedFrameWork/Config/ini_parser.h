#pragma once

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <iostream>
#include <string>

/**
 * @brief IniParser 类用于解析和管理 INI 配置文件。
 *
 * @author 李惠涛
 * @date 2024-9-27
 *
 * 这个类提供了获取和设置 INI 文件中键值对的功能。
 */

class IniParser {
public:
    IniParser(const std::string& confPath) : m_confPath(confPath) {
        load();
    }

    std::string getKeyValue(const std::string& section, const std::string& key) {
        return pt.get<std::string>(section + "." + key, "");
    }

    void setKeyValue(const std::string& section, const std::string& key, const std::string& value) {
        pt.put(section + "." + key, value);
        save();
    }

private:
    void load() {
        try {
            boost::property_tree::ini_parser::read_ini(m_confPath, pt);
        }
        catch (const boost::property_tree::ini_parser_error& e) {
            std::cerr << "Error reading INI file: " << e.what() << std::endl;
        }
    }

    void save() {
        try {
            boost::property_tree::ini_parser::write_ini(m_confPath, pt);
        }
        catch (const boost::property_tree::ini_parser_error& e) {
            std::cerr << "Error writing INI file: " << e.what() << std::endl;
        }
    }

    std::string m_confPath;
    boost::property_tree::ptree pt;
};
